force clk 0 0, 1 1 -repeat 2
force rst 1 0, 0 2

force com_options 00 5
force nouvelle_couleur 0000 10
force nouvelle_couleur 0001 15
force nouvelle_couleur 0010 20
force nouvelle_couleur 0100 25
force nouvelle_couleur 1000 30
force nouvelle_couleur 1111 35

force com_options 01 40
force nouvelle_couleur 0000 45
force nouvelle_couleur 0001 50
force nouvelle_couleur 0010 55
force nouvelle_couleur 0100 60
force nouvelle_couleur 1000 65

force com_options 10 70
force nouvelle_couleur 0000 75
force nouvelle_couleur 0001 80
force nouvelle_couleur 0010 85
force nouvelle_couleur 0100 90
force nouvelle_couleur 1000 95

force com_options 11 100
force nouvelle_couleur 0000 105
force nouvelle_couleur 0001 110
force nouvelle_couleur 0010 115
force nouvelle_couleur 0100 120
force nouvelle_couleur 1000 125

run 400
