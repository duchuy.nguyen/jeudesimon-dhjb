#Etat initial
force score_j1(7) 1 0
force score_j1(6) 0 0
force score_j1(5) 0 0
force score_j1(4) 1 0
force score_j1(3) 1 0
force score_j1(2) 0 0
force score_j1(1) 0 0
force score_j1(0) 1 0

force score_j2(7) 1 0
force score_j2(6) 0 0
force score_j2(5) 0 0
force score_j2(4) 1 0
force score_j2(3) 0 0
force score_j2(2) 0 0
force score_j2(1) 0 0
force score_j2(0) 0 0

force score_j3(7) 0 0
force score_j3(6) 0 0
force score_j3(5) 0 0
force score_j3(4) 0 0
force score_j3(3) 1 0
force score_j3(2) 0 0
force score_j3(1) 0 0
force score_j3(0) 1 0

force score_j4(7) 1 0
force score_j4(6) 0 0
force score_j4(5) 0 0
force score_j4(4) 0 0
force score_j4(3) 1 0
force score_j4(2) 0 0
force score_j4(1) 0 0
force score_j4(0) 0 0

force mode_jeu(1) 0 0
force mode_jeu(0) 0 0

force mode_nbr_joueur(2) 0 0
force mode_nbr_joueur(1) 0 0
force mode_nbr_joueur(0) 0 0

force etat_systeme(2) 0 0
force etat_systeme(1) 0 0
force etat_systeme(0) 0 0

#test lors de la phase du choix du mode de jeu (etat_systeme <= 000)
force mode_jeu(0) 1 2

force score_j1(0) 0 4

force score_j4(0) 1 6

force mode_nbr_joueur(1) 1 8

#test lors de la phase du choix du nombre de joueurs (etat_systeme <= 011)
force etat_systeme(1) 1 10
force etat_systeme(0) 1 10

force mode_jeu(0) 0 12

force score_j1(0) 1 14

force score_j4(0) 0 16

force mode_nbr_joueur(0) 1 18

#test lors d'un �tat autre dans la machine
force etat_systeme(2) 1 20

force mode_jeu(0) 1 22

force score_j1(2) 1 24

force score_j4(2) 1 26

force mode_nbr_joueur(2) 1 28

run 30