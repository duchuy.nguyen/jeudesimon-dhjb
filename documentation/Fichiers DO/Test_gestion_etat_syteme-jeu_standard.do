# Etat initial
force clk 0 0, 1 1 -repeat 2
force rst 1 1, 0 4
force btn_mode 0 0
force btn_start 0 0
force fin_sequence 0 0
force evaluer(1) 0 0
force evaluer(0) 0 0
force joueur_vivant(2) 0 0
force joueur_vivant(1) 0 0
force joueur_vivant(0) 0 0

# Test partie en jeu standard
# ---------------------------
force btn_mode 1 4, 0 8, 1 12, 0 16
force btn_start 1 20, 0 24

# fin de la sequence de demarrage
force fin_sequence 1 40, 0 44
# fin de la sequnce de demonstration
force fin_sequence 1 48, 0 52
# joueur donne la bonne reponse
force evaluer(1) 0 54, 0 58
force evaluer(0) 1 54, 0 58
#fin de la sequence de demonstration
force fin_sequence 1 60, 0 64
# joueur donne la mauvaise réponse
force evaluer(1) 1 68, 0 72
force evaluer(0) 0 68, 0 72

run 100
