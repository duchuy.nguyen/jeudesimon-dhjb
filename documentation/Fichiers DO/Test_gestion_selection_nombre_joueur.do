# Input Clock
force clk 0 0, 1 1 -repeat 2
force rst 1 0, 0 1

# Change state between DJS => DJNS => TJS => TJNS => QJS => QJNS => DJS => ...
force btn_mode 1 4
force btn_mode 1 8
force btn_mode 1 12
force btn_mode 1 16
force btn_mode 1 20
force btn_mode 1 24
force btn_mode 1 28
force btn_mode 1 32
force btn_mode 1 36
force btn_mode 1 40
force btn_mode 1 44
force btn_mode 1 48
force btn_mode 1 52
force btn_mode 1 56
force btn_mode 1 60
force btn_mode 1 64
force btn_mode 1 68
force btn_mode 1 72
force btn_mode 1 76
force btn_mode 1 80

force btn_mode 0 5
force btn_mode 0 10
force btn_mode 0 13
force btn_mode 0 19
force btn_mode 0 22
force btn_mode 0 25
force btn_mode 0 29
force btn_mode 0 35
force btn_mode 0 37
force btn_mode 0 41
force btn_mode 0 45
force btn_mode 0 50
force btn_mode 0 55
force btn_mode 0 57
force btn_mode 0 62
force btn_mode 0 67
force btn_mode 0 73
force btn_mode 0 78
force btn_mode 0 83

run 100