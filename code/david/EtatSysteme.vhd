-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            David Rossy
-- 
-- Create Date:         18:34:00 06/03/2021 
-- Design Name: 
-- Module Name:         EtatSysteme - EtatSysteme_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions:
-- Version:             1.4
-- Description:         Ce composant permet de gerer les etats du syteme en fonctions des 
--                      differents evenements qui demandent un changement
-- --------------------------------------------------------------------------------
library IEEE;
library work;
use IEEE.STD_LOGIC_1164.all;
-- --------------------------------------------------------------------------------
entity EtatSysteme is
    Port (
            clk              : in  STD_LOGIC;
            rst              : in  STD_LOGIC;
            btn_start        : in  STD_LOGIC;
            mode_selectionne : in  STD_LOGIC;
            fin_sequence     : in  STD_LOGIC;
            evaluer          : in  STD_LOGIC_VECTOR (1 downto 0);
            joueur_vivant    : in  STD_LOGIC_VECTOR (2 downto 0);
            etat_systeme     : out STD_LOGIC_VECTOR (2 downto 0);
            reset            : out STD_LOGIC
         );
end EtatSysteme;
-- --------------------------------------------------------------------------------
architecture EtatSysteme_arch of EtatSysteme is
    -- Etats possibles de la machine d'etat
    type etat is (
                    selection_mode_jeu,
                    jeu_base,
                    multijoueur,
                    selection_nb_joueur,
                    sequence_demarrage,
                    demonstration_sequence,
                    ecoute
                 );
    
    -- Etat du systeme
    signal etat_present, etat_futur         : etat;
    signal etat_present_vecteur             : STD_LOGIC_VECTOR (2 downto 0);

    -- Signaux permettant la transition d'etat a partir du choix du mode et du choix
    -- du nombre de joueurs
    signal validation_mode_jeu              : STD_LOGIC;
    signal validation_nbr_joueur            : STD_LOGIC;

    -- Signal permettant de reinitialiser le systeme lorsque l'on termine une partie
    signal reinit                           : STD_LOGIC;

    -- Constantes permettant de declencher ou non les modules de selection de mode de jeu
    -- et de selection du nombre de joueurs, il permettent la différenciation des deux modules
    -- qui sont geres par le meme bouton (btn_mode)
    signal etat_actif_selection_mode        : STD_LOGIC_VECTOR (2 downto 0);
    signal etat_actif_selection_nbr_joueur  : STD_LOGIC_VECTOR (2 downto 0);

begin
    -- Module permettant de valider le nombre de joueurs
    -- en appuyant sur le bouton start (btn_start)
    -- --------------------------------------------------
    valider_mode_jeu :  entity work.ValidationBoutonEtat(ValidationBoutonEtat_arch)
                            port map (
                                        clk => clk,
                                        rst => reinit,
                                        btn => btn_start,
                                        etat_actif => etat_actif_selection_mode,
                                        etat_systeme => etat_present_vecteur,
                                        validation => validation_mode_jeu
                                     );
    -- --------------------------------------------------

    -- Module permettant de valider le mode de jeu
    -- en appuyant sur le bouton start (btn_start)
    -- --------------------------------------------------
    valider_nombre_joueur : entity work.ValidationBoutonEtat(ValidationBoutonEtat_arch)
                            port map (
                                        clk => clk,
                                        rst => reinit,
                                        btn => btn_start,
                                        etat_actif => etat_actif_selection_nbr_joueur,
                                        etat_systeme => etat_present_vecteur,
                                        validation => validation_nbr_joueur
                                     );
    -- --------------------------------------------------


    -- Process de mémorisation de l'etat du systeme
    -- --------------------------------------------------
    registre : process (clk, rst)
    begin
        if rst = '1' then
            etat_present <= selection_mode_jeu;
        elsif rising_edge(clk) then
            etat_present <= etat_futur;
            etat_actif_selection_mode <= "000";         --constante
            etat_actif_selection_nbr_joueur <= "011";   --constante
        end if;
    end process registre;
    -- --------------------------------------------------

    -- Process declanchant la reinitialisation du systeme
    -- lorsque l'on termine la partie
    -- --------------------------------------------------
    reinitialisation : process (clk, rst)
    begin
        if rst = '1' then
            reinit <= '1';
        elsif rising_edge(clk) then
            if etat_present = ecoute and etat_futur = selection_mode_jeu then
                reinit <= '1';
            else
                reinit <= '0';
            end if;
        end if;
    end process reinitialisation;
    -- --------------------------------------------------


    -- Process de gestion de l'etat du systeme en fonction
    -- des evenements
    -- --------------------------------------------------
    gestion : process (clk, rst, btn_start, fin_sequence)
    begin
        if rst = '1' then
            etat_futur <= selection_mode_jeu;
        elsif rising_edge(clk) then
            case etat_present is
            -- Changement d'etat depuis Selection_mode_jeu
            when selection_mode_jeu =>
                if validation_mode_jeu = '1' then
                    -- Le mode choisi est le jeu standard, on commence directement
                    if mode_selectionne = '0' then
                        etat_futur <= sequence_demarrage;
                    -- Le mode choisi est le modemultijoueur, on choisit le nombre de joueurs
                    else
                        etat_futur <= selection_nb_joueur;
                    end if;
                end if;

            -- Changement d'etat depuis Selection_nb_joueur
            when selection_nb_joueur =>
                if validation_nbr_joueur = '1' then
                    etat_futur <= sequence_demarrage;
                end if;
                
            -- Changement d'etat depuis Sequence_demarrage
            when sequence_demarrage =>
                if fin_sequence = '1' then
                    etat_futur <= demonstration_sequence;
                end if;

            -- Changement d'etat depuis Sequence_demarrage
            when demonstration_sequence =>
                if fin_sequence = '1' then
                    etat_futur <= ecoute;
                end if;

            -- Changement d'etat depuis ecoute
            when ecoute =>
                -- Gestion des evenements en mode jeu de base
                if mode_selectionne = '0' then
                    case evaluer is
                        -- Le joueur ne fait rien
                        when "00"   => etat_futur <= ecoute;
                        -- Le joueur reussi sa sequence
                        when "01"   => etat_futur <= demonstration_sequence;
                        -- Le joueur comment une erreur dans la sequence
                        when "10"   => etat_futur <= selection_mode_jeu;
                        when others => etat_futur <= selection_mode_jeu; -- Par defaut on retourne a la
                    end case;                                            -- selection du mode de jeu

                -- Gestion des evenements en mode jeu multijoueur
                else
                    case evaluer is
                        -- Le joueur ne fait rien
                        when "00" =>
                            etat_futur <= ecoute;
                        -- Le joueur reussi sa sequence
                        when "01" =>
                            etat_futur <= demonstration_sequence;
                        -- Le joueur comment une erreur dans la sequence
                        when "10" =>
                            -- Il ne reste plus qu'un joueur, la partie est terminee
                            if joueur_vivant = "001" then
                                etat_futur <= selection_mode_jeu;
                            --Il reste des joueurs, la partie continue
                            else
                                etat_futur <= demonstration_sequence;
                            end if;
                        when others => etat_futur <= selection_mode_jeu;
                    end case;
                end if;
            when others =>
                etat_futur <= selection_mode_jeu;

            end case;
        end if;
    end process gestion;
    -- --------------------------------------------------


    -- Process permettant de diriger la sortie de l'etat du systeme
    -- --------------------------------------------------
    sortie : process (etat_present)
    begin
        case etat_present is
            when selection_mode_jeu     => etat_present_vecteur <= "000";
            when jeu_base               => etat_present_vecteur <= "001";
            when multijoueur            => etat_present_vecteur <= "010";
            when selection_nb_joueur    => etat_present_vecteur <= "011";
            when sequence_demarrage     => etat_present_vecteur <= "100";
            when demonstration_sequence => etat_present_vecteur <= "101";
            when ecoute                 => etat_present_vecteur <= "110";
            when others                 => etat_present_vecteur <= "000";
        end case;
    end process sortie;
    -- --------------------------------------------------


    etat_systeme <= etat_present_vecteur;
    reset <= reinit;
    
    
end architecture EtatSysteme_arch;
-- --------------------------------------------------------------------------------