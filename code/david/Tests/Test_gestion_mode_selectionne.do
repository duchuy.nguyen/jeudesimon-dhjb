# Input Clock
force clk 0 0, 1 1 -repeat 2
force rst 1 0, 0 1

# Change state between ZST => UNST => UST => ZNST => ZST => ...
force btn 1 4
force btn 1 8
force btn 1 12
force btn 1 16
force btn 1 20
force btn 1 24
force btn 1 28
force btn 1 32
force btn 1 36

force btn 0 5
force btn 0 10
force btn 0 13
force btn 0 19
force btn 0 22
force btn 0 25
force btn 0 29
force btn 0 35
force btn 0 37

run 50