# Etat initial
# ---------------------------
force clk 0 0, 1 1 -repeat 2
force rst 1 1, 0 4
force btn_mode 0 0
force btn_start 0 0
force fin_sequence 0 0
force evaluer(1) 0 0
force evaluer(0) 0 0
force joueur_vivant(2) 0 0
force joueur_vivant(1) 0 0
force joueur_vivant(0) 0 0

# Test partie en multijoueur
# ---------------------------

# Choix du mode de jeu multijoueur
force btn_mode 1 4, 0 8
force btn_start 1 10, 0 14

# Choix du nombre de joueur
force btn_mode 1 16, 0 20
force btn_start 1 24, 0 28

# Passer la sequence de demarrage
force fin_sequence 1 32, 0 36

# Passer la sequence de demonstration
force fin_sequence 1 40, 0 44

# Le joueur repond correctement
force evaluer(1) 0 48, 0 52
force evaluer(0) 1 48, 0 52

# Passer la sequence de demonstration
force fin_sequence 1 54, 0 58

# Le joueur ne repond pas correctement (reste 2 joueurs)
force joueur_vivant(2) 0 60, 0 64
force joueur_vivant(1) 0 60, 0 64
force joueur_vivant(0) 1 60, 0 64

force evaluer(1) 1 60, 0 64
force evaluer(0) 0 60, 0 64

run 100