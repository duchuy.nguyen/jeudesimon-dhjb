-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            David Rossy
-- 
-- Create Date:         13:16:20 05/14/2021 
-- Design Name: 
-- Module Name:         GameHandler - GameHandler_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions:
-- Version:             1.4
-- Description:         Ce composant a le role de chef d'orchestre. Il est charge d'informer les autres
--                      composants de l'etat du systeme actuel.
--
--                      Il est egalement charge de transmettre aux composants qui en ont besoin :
--                      le mode de jeu selectionne et le nombre de joueur selectionne.
-- --------------------------------------------------------------------------------
library IEEE;
library work;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
-- --------------------------------------------------------------------------------
entity GameHandler is
    Port (
            clk              : in  STD_LOGIC;
            rst              : in  STD_LOGIC;
            btn_mode         : in  STD_LOGIC;
            btn_start        : in  STD_LOGIC;
            fin_sequence     : in  STD_LOGIC;
            evaluer          : in  STD_LOGIC_VECTOR (1 downto 0);
            joueur_vivant    : in  STD_LOGIC_VECTOR (2 downto 0);
            mode_jeu         : out STD_LOGIC_VECTOR (1 downto 0);
            mode_nbr_joueur  : out STD_LOGIC_VECTOR (2 downto 0);
            etat_systeme     : out STD_LOGIC_VECTOR (2 downto 0);
            reset            : out STD_LOGIC
         );
end GameHandler;
-- --------------------------------------------------------------------------------
architecture GameHandler_arch of GameHandler is
    -- Choix du mode de jeu (=/= etat de la machine d'etat meme si on profitera ici du type etat)
    signal mode_selectionne                : STD_LOGIC;

    -- Etat du systeme actuel (voir machine d'etats)
    signal etat_actuel                     : STD_LOGIC_VECTOR (2 downto 0);

    -- Selection du nombre de joueurs en cours
    signal nombre_joueur                   : STD_LOGIC_VECTOR (1 downto 0);

    -- Réinitialistion du système lorsque l'on termine une partie
    signal reinit                          : STD_LOGIC;

    -- Constantes permettant de declencher ou non les modules de selection de mode de jeu
    -- et de selection du nombre de joueurs, il permettent la différenciation des deux modules
    -- qui sont geres par le meme bouton (btn_mode)
    signal etat_actif_selection_mode,
           etat_actif_selection_nbr_joueur : STD_LOGIC_VECTOR (2 downto 0);
    
begin
    -- Module permettant de gerer les chagements d'etats du
    -- systeme
    -- --------------------------------------------------
    gestion_etat_syteme :   entity work.EtatSysteme(EtatSysteme_arch)
                            port map (
                                        clk => clk,
                                        rst => rst,
                                        btn_start => btn_start,
                                        mode_selectionne => mode_selectionne,
                                        fin_sequence => fin_sequence,
                                        evaluer => evaluer,
                                        joueur_vivant => joueur_vivant,
                                        etat_systeme => etat_actuel,
                                        reset => reinit
                                    );
    -- --------------------------------------------------


    -- Module permettant de gerer la modification du choix
    -- du mode de jeu par l'utilisateur (modifie en appuyant sur le bouton mode)
    -- --------------------------------------------------
    gestion_mode_selectionne :  entity work.ValidationBoutonEtat(ValidationBoutonEtat_arch)
                                port map (
                                            clk => clk,
                                            rst => reinit,
                                            btn => btn_mode,
                                            etat_actif => etat_actif_selection_mode,
                                            etat_systeme => etat_actuel,
                                            validation => mode_selectionne
                                        );
    -- --------------------------------------------------


    -- Module permettant de gerer le nombre de joueurs que l'on
    -- souhaite selectionner pour le mode multijoueur
    -- --------------------------------------------------
    gestion_selection_nombre_joueur :   entity work.SelectionNombreJoueur(SelectionNombreJoueur_arch)
                                        port map (
                                                    clk => clk,
                                                    rst => reinit,
                                                    btn_mode => btn_mode,
                                                    etat_actif => etat_actif_selection_nbr_joueur,
                                                    etat_systeme => etat_actuel,
                                                    mode_nbr_joueur => nombre_joueur
                                                 );
    -- --------------------------------------------------


    -- Process permettant d'initialiser les constantes
    -- --------------------------------------------------
    constantes : process (clk)
    begin
        if rising_edge(clk) then
            etat_actif_selection_mode <= "000";
            etat_actif_selection_nbr_joueur <= "011";
        end if;
    end process constantes;
    -- --------------------------------------------------


    -- Process vehiculant l'information du mode de jeu selectionne
    -- (A afficher sur le composant 7 segment de l'affichage)
    -- --------------------------------------------------
    selection_mode : process (mode_selectionne)
        begin
            case mode_selectionne is
                when '0'    => mode_jeu <= "00"; --Jeu de base selectionne
                when '1'    => mode_jeu <= "01"; --Multijoueur selectionne
                when others => mode_jeu <= "00"; --Jeu de base selectionne par defaut
            end case;
        end process selection_mode;
    -- --------------------------------------------------


    -- Process vehiculant l'information du nombre de joueur selectionne
    -- (A afficher sur le composant 7 segment de l'affichage)
    -- --------------------------------------------------
    selection_nombre_joueur : process (nombre_joueur)
        begin
            case nombre_joueur is
                when "00"   => mode_nbr_joueur <= "010"; --2 joueurs selectionnes
                when "01"   => mode_nbr_joueur <= "011"; --3 joueurs selectionnes
                when "10"   => mode_nbr_joueur <= "100"; --4 joueurs selectionnes
                when others => mode_nbr_joueur <= "010"; --nombre de joueur par defaut (2)
            end case;
        end process selection_nombre_joueur;
    -- --------------------------------------------------


    etat_systeme <= etat_actuel;
    reset <= reinit;
    

end architecture GameHandler_arch;
-- --------------------------------------------------------------------------------