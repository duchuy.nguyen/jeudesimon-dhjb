
force clk 0 0, 1 1 -repeat 2
# une séquence de 2
force rst 1 0, 0 5
# valeur 1  au temps zér, valeur 0 au temps 5

force btn_bleu 0 0
force btn_jaune 0 0
force btn_rouge 0 0
force btn_vert 0 0
force clk_2hz 0 0, 1 20 -repeat 40
force compteur_couleur_vector 00001000 0
force etat_system 000 10
force id_joueur 01 0
# force sequence_complete UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU0000000000011000 1
force sequence_complete   UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU001000 1
# passer en waitUser à 20
force etat_system 110 20

# touché bleu
force btn_bleu 1 30, 0 50

# deuxième cas
force btn_jaune 1 60, 0 70

# troisième cas
force btn_bleu 1 80, 0 90

run 600
# jusqu'au temps de 200 secondes
