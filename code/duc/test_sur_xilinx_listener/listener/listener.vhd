-- commentaire
LIBRARY ieee;                -- bibliotheque a charger
USE ieee.std_logic_1164.all; -- utilisation des definitions std_logic
USE ieee.numeric_std.all;

ENTITY listener IS  -- calculer le score et le convertir en BCD
    PORT(
        -- declaration des entrées
        etat_system         : in std_logic_vector(2 downto 0);
        sequence_complete   : in std_logic_vector(59 downto 0);

        btn_rouge           : in std_logic;
        btn_jaune           : in std_logic;
        btn_vert            : in std_logic;
        btn_bleu            : in std_logic;
        clk                 : in std_logic;
        rst                 : in std_logic;

        clk_2hz             : in std_logic;
        id_joueur           : in std_logic_vector(1 downto 0);
        compteur_couleur_vector   : in std_logic_vector(7 downto 0);

        -- declaration des sorties
        evaluer         : out std_logic_vector(1 downto 0);
        couleur_jouer   : out std_logic_vector(3 downto 0)
    );

END ENTITY listener;

ARCHITECTURE listener_arch OF listener IS
 -- section declarative de signaux

Type etats is (waitUser, TrueAnswer, FalseAnswer, IDLE);
signal etat_present, etat_futur : etats;
signal bus_color_present, bus_color_futur : std_logic_vector(2 downto 0);

signal compteur_couleur_int : integer range 0 to 150 := 0;
signal compteur_local_int : integer range 0 to 150 := 0;
signal timer_checker:  integer range 0 to 10 := 0;
signal bus_color_out : std_logic_vector(3 downto 0);

signal color_a_tester : std_logic_vector(1 downto 0);
signal color_comparante : std_logic_vector(1 downto 0);
signal counter_reset : std_logic;
BEGIN
    compteur_couleur_int <= to_integer(unsigned(compteur_couleur_vector));
    -- corps de l’architecture
    registre : process(clk,rst)
    begin
      if (rst = '1') then
        etat_present <= IDLE;
        bus_color_present <= "111";
      elsif rising_edge(clk) then
        etat_present <= etat_futur;
        bus_color_present <= bus_color_futur;
      end if;
    end process;

    input_timer : process(clk_2hz, counter_reset ) -- timer de 5 secondes
    begin
        if (counter_reset = '1') then
            timer_checker <= 0 ;
        elsif (clk_2hz = '1') and (etat_present = waitUser) then -- check only in waitUser
            if (timer_checker >= 10) then
                timer_checker <= 0 ;
            else
                timer_checker <= timer_checker + 1;
            end if;
       end if;
    end process;

    input_color : process(btn_bleu,btn_vert,btn_jaune,btn_rouge)
    begin
        if(btn_bleu = '1') then     --blue
            bus_color_futur <=  "000";
            bus_color_out <= "0001";
        elsif(btn_vert = '1') then -- vert
            bus_color_futur <= "001";
            bus_color_out <= "0010";
        elsif (btn_jaune = '1')then -- jaune
            bus_color_futur <=  "010";
            bus_color_out <= "0100";
        elsif (btn_rouge = '1') then  -- rouge
            bus_color_futur <= "011";
            bus_color_out <= "1000";
        else
            bus_color_futur <= "111";  -- couleur neutre (état_neutre) rien apppuyé
            bus_color_out <= "0000";
        end if;

    end process;

    etatCombi: process(etat_system, bus_color_futur, bus_color_present, timer_checker)
    begin


        case( etat_present ) is
          when IDLE => if (etat_system = "110") then   -- en phase d'écoute
                        etat_futur <= waitUser;
                        compteur_local_int <= 0;
                      else
                        etat_futur <= IDLE;
                        compteur_local_int <= 0;
                      end if;
          when waitUser =>
                      counter_reset <= '0';
                      if (compteur_local_int < compteur_couleur_int)  then

                          color_comparante <= sequence_complete((compteur_local_int + 1) downto compteur_local_int );  -- prépare ?
                          if (bus_color_present /= "111") then                                                         -- une couleur touchée

                             color_a_tester <= bus_color_present(1 downto 0) ;                                         -- prépare couleur à tester
                             if (bus_color_present /= bus_color_futur) then                                 -- on relâche la touche mais synchronisé
                                  if (color_a_tester = color_comparante) then
                                      if (counter_reset /= '1') then
                                        compteur_local_int <= compteur_local_int + 2;
                                      end if;
                                      counter_reset <= '1';
                                      etat_futur <= waitUser;
                                  else
                                      etat_futur <= FalseAnswer;
                                  end if;
                              end if;

                          else    -- checker le temps
                              if (timer_checker >= 10) then
                                  etat_futur <= FalseAnswer;
                              else
                                  etat_futur <= waitUser;
                              end if;
                          end if;
                      else
                              etat_futur <= TrueAnswer;
                      end if;

          when TrueAnswer =>
                      if etat_system /= "110" then
                        etat_futur <= IDLE;
                      else
                        etat_futur <= TrueAnswer;
                      end if;

          when FalseAnswer =>

                    if etat_system /= "110" then
                      etat_futur <= IDLE;
                    else
                      etat_futur <= FalseAnswer;
                    end if;

          when others => etat_futur <= IDLE;

        end case;

    end process;

    output_evaluer : process(etat_present)
    begin
      case( etat_present ) is

        when waitUser => evaluer <= "00";
        when TrueAnswer=> evaluer <= "01";
        when FalseAnswer => evaluer <= "10";
        when others => evaluer <= "00";

      end case;

    end process;

couleur_jouer <= bus_color_out;

END ARCHITECTURE listener_arch;
