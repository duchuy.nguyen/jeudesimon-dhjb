--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: M.81d
--  \   \         Application: netgen
--  /   /         Filename: listener_synthesis.vhd
-- /___/   /\     Timestamp: Sun Jun 13 11:52:37 2021
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm listener -w -dir netgen/synthesis -ofmt vhdl -sim listener.ngc listener_synthesis.vhd 
-- Device	: xc3s50a-5-tq144
-- Input file	: listener.ngc
-- Output file	: C:\Users\eia\Documents\duchuy-vhdl\listener\netgen\synthesis\listener_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: listener
-- Xilinx	: C:\Xilinx\12.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity listener is
  port (
    clk_2hz : in STD_LOGIC := 'X'; 
    clk : in STD_LOGIC := 'X'; 
    btn_rouge : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    btn_bleu : in STD_LOGIC := 'X'; 
    btn_jaune : in STD_LOGIC := 'X'; 
    btn_vert : in STD_LOGIC := 'X'; 
    evaluer : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    couleur_jouer : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    compteur_couleur_vector : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
    etat_system : in STD_LOGIC_VECTOR ( 2 downto 0 ); 
    sequence_complete : in STD_LOGIC_VECTOR ( 59 downto 0 ); 
    id_joueur : in STD_LOGIC_VECTOR ( 1 downto 0 ) 
  );
end listener;

architecture Structure of listener is
  signal Mmux_color_comparante_mux0001_10 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_101 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_3_f7_19 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_3_f71 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_4_f6_21 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_4_f61 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_4_f7_23 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_4_f71 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f5_25 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f51 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f5_0_rt_27 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f5_0_rt1_28 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f5_rt_29 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f5_rt1_30 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f6_31 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f61 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f62 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_5_f63 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f5_35 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f51 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f52 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f53 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f54 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f55 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f6_41 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_6_f61 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_71 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_72 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_73 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_74 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_75 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_76 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_77 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f5_51 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f51 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f52 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f53 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f54 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_7_f55 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_8 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_81 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_810 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_811 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_82 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_83 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_84 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_85 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_86 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_87 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_88 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_89 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_8_f5_69 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_8_f51 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_9 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_91 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_92 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_93 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_94 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_95 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_96 : STD_LOGIC; 
  signal Mmux_color_comparante_mux0001_97 : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal N15 : STD_LOGIC; 
  signal N17 : STD_LOGIC; 
  signal N23 : STD_LOGIC; 
  signal N25 : STD_LOGIC; 
  signal N27 : STD_LOGIC; 
  signal N33 : STD_LOGIC; 
  signal N43 : STD_LOGIC; 
  signal N45 : STD_LOGIC; 
  signal N49 : STD_LOGIC; 
  signal N51 : STD_LOGIC; 
  signal N52 : STD_LOGIC; 
  signal N9 : STD_LOGIC; 
  signal btn_jaune_IBUF_95 : STD_LOGIC; 
  signal btn_rouge_IBUF_97 : STD_LOGIC; 
  signal btn_vert_IBUF_99 : STD_LOGIC; 
  signal clk_2hz_BUFGP_108 : STD_LOGIC; 
  signal clk_BUFGP_109 : STD_LOGIC; 
  signal color_comparante_cmp_lt0000 : STD_LOGIC; 
  signal compteur_couleur_vector_0_IBUF_127 : STD_LOGIC; 
  signal compteur_couleur_vector_1_IBUF_128 : STD_LOGIC; 
  signal compteur_couleur_vector_2_IBUF_129 : STD_LOGIC; 
  signal compteur_couleur_vector_3_IBUF_130 : STD_LOGIC; 
  signal compteur_couleur_vector_4_IBUF_131 : STD_LOGIC; 
  signal compteur_couleur_vector_5_IBUF_132 : STD_LOGIC; 
  signal compteur_couleur_vector_6_IBUF_133 : STD_LOGIC; 
  signal compteur_couleur_vector_7_IBUF_134 : STD_LOGIC; 
  signal compteur_local_int_2_1_137 : STD_LOGIC; 
  signal compteur_local_int_cmp_eq0001 : STD_LOGIC; 
  signal compteur_local_int_not0001 : STD_LOGIC; 
  signal couleur_jouer_0_OBUF_156 : STD_LOGIC; 
  signal couleur_jouer_1_OBUF_157 : STD_LOGIC; 
  signal couleur_jouer_2_OBUF_158 : STD_LOGIC; 
  signal couleur_jouer_3_OBUF_159 : STD_LOGIC; 
  signal counter_reset_160 : STD_LOGIC; 
  signal counter_reset_mux0003 : STD_LOGIC; 
  signal etat_futur_cmp_eq0000 : STD_LOGIC; 
  signal etat_futur_cmp_eq0001 : STD_LOGIC; 
  signal etat_futur_cmp_ge0000 : STD_LOGIC; 
  signal etat_futur_cmp_ne0001_169 : STD_LOGIC; 
  signal etat_futur_mux0005_2_0_173 : STD_LOGIC; 
  signal etat_futur_not0001 : STD_LOGIC; 
  signal etat_present_1_1_178 : STD_LOGIC; 
  signal etat_present_2_1_180 : STD_LOGIC; 
  signal etat_system_0_IBUF_185 : STD_LOGIC; 
  signal etat_system_1_IBUF_186 : STD_LOGIC; 
  signal etat_system_2_IBUF_187 : STD_LOGIC; 
  signal rst_IBUF_191 : STD_LOGIC; 
  signal sequence_complete_0_IBUF_252 : STD_LOGIC; 
  signal sequence_complete_10_IBUF_253 : STD_LOGIC; 
  signal sequence_complete_11_IBUF_254 : STD_LOGIC; 
  signal sequence_complete_12_IBUF_255 : STD_LOGIC; 
  signal sequence_complete_13_IBUF_256 : STD_LOGIC; 
  signal sequence_complete_14_IBUF_257 : STD_LOGIC; 
  signal sequence_complete_15_IBUF_258 : STD_LOGIC; 
  signal sequence_complete_16_IBUF_259 : STD_LOGIC; 
  signal sequence_complete_17_IBUF_260 : STD_LOGIC; 
  signal sequence_complete_18_IBUF_261 : STD_LOGIC; 
  signal sequence_complete_19_IBUF_262 : STD_LOGIC; 
  signal sequence_complete_1_IBUF_263 : STD_LOGIC; 
  signal sequence_complete_20_IBUF_264 : STD_LOGIC; 
  signal sequence_complete_21_IBUF_265 : STD_LOGIC; 
  signal sequence_complete_22_IBUF_266 : STD_LOGIC; 
  signal sequence_complete_23_IBUF_267 : STD_LOGIC; 
  signal sequence_complete_24_IBUF_268 : STD_LOGIC; 
  signal sequence_complete_25_IBUF_269 : STD_LOGIC; 
  signal sequence_complete_26_IBUF_270 : STD_LOGIC; 
  signal sequence_complete_27_IBUF_271 : STD_LOGIC; 
  signal sequence_complete_28_IBUF_272 : STD_LOGIC; 
  signal sequence_complete_29_IBUF_273 : STD_LOGIC; 
  signal sequence_complete_2_IBUF_274 : STD_LOGIC; 
  signal sequence_complete_30_IBUF_275 : STD_LOGIC; 
  signal sequence_complete_31_IBUF_276 : STD_LOGIC; 
  signal sequence_complete_32_IBUF_277 : STD_LOGIC; 
  signal sequence_complete_33_IBUF_278 : STD_LOGIC; 
  signal sequence_complete_34_IBUF_279 : STD_LOGIC; 
  signal sequence_complete_35_IBUF_280 : STD_LOGIC; 
  signal sequence_complete_36_IBUF_281 : STD_LOGIC; 
  signal sequence_complete_37_IBUF_282 : STD_LOGIC; 
  signal sequence_complete_38_IBUF_283 : STD_LOGIC; 
  signal sequence_complete_39_IBUF_284 : STD_LOGIC; 
  signal sequence_complete_3_IBUF_285 : STD_LOGIC; 
  signal sequence_complete_40_IBUF_286 : STD_LOGIC; 
  signal sequence_complete_41_IBUF_287 : STD_LOGIC; 
  signal sequence_complete_42_IBUF_288 : STD_LOGIC; 
  signal sequence_complete_43_IBUF_289 : STD_LOGIC; 
  signal sequence_complete_44_IBUF_290 : STD_LOGIC; 
  signal sequence_complete_45_IBUF_291 : STD_LOGIC; 
  signal sequence_complete_46_IBUF_292 : STD_LOGIC; 
  signal sequence_complete_47_IBUF_293 : STD_LOGIC; 
  signal sequence_complete_48_IBUF_294 : STD_LOGIC; 
  signal sequence_complete_49_IBUF_295 : STD_LOGIC; 
  signal sequence_complete_4_IBUF_296 : STD_LOGIC; 
  signal sequence_complete_50_IBUF_297 : STD_LOGIC; 
  signal sequence_complete_51_IBUF_298 : STD_LOGIC; 
  signal sequence_complete_52_IBUF_299 : STD_LOGIC; 
  signal sequence_complete_53_IBUF_300 : STD_LOGIC; 
  signal sequence_complete_54_IBUF_301 : STD_LOGIC; 
  signal sequence_complete_55_IBUF_302 : STD_LOGIC; 
  signal sequence_complete_58_IBUF_303 : STD_LOGIC; 
  signal sequence_complete_59_IBUF_304 : STD_LOGIC; 
  signal sequence_complete_5_IBUF_305 : STD_LOGIC; 
  signal sequence_complete_6_IBUF_306 : STD_LOGIC; 
  signal sequence_complete_7_IBUF_307 : STD_LOGIC; 
  signal sequence_complete_8_IBUF_308 : STD_LOGIC; 
  signal sequence_complete_9_IBUF_309 : STD_LOGIC; 
  signal Madd_compteur_local_int_addsub0000_cy : STD_LOGIC_VECTOR ( 4 downto 4 ); 
  signal Mcompar_color_comparante_cmp_lt0000_cy : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal Mcompar_color_comparante_cmp_lt0000_lut : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal bus_color_futur : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal bus_color_present : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal color_a_tester : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal color_a_tester_mux0001 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal color_comparante : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal color_comparante_mux0001 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal compteur_local_int : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal compteur_local_int_mux0004 : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal etat_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal etat_futur_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal etat_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_checker : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal timer_checker_mux0002 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  XST_VCC : VCC
    port map (
      P => N1
    );
  bus_color_present_0 : FDP
    port map (
      C => clk_BUFGP_109,
      D => bus_color_futur(0),
      PRE => rst_IBUF_191,
      Q => bus_color_present(0)
    );
  bus_color_present_1 : FDP
    port map (
      C => clk_BUFGP_109,
      D => bus_color_futur(1),
      PRE => rst_IBUF_191,
      Q => bus_color_present(1)
    );
  bus_color_present_2 : FDP
    port map (
      C => clk_BUFGP_109,
      D => bus_color_futur(2),
      PRE => rst_IBUF_191,
      Q => bus_color_present(2)
    );
  counter_reset : LD
    port map (
      D => counter_reset_mux0003,
      G => etat_present(0),
      Q => counter_reset_160
    );
  etat_futur_0 : LD
    generic map(
      INIT => '1'
    )
    port map (
      D => etat_futur_mux0005(0),
      G => etat_futur_not0001,
      Q => etat_futur(0)
    );
  etat_futur_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => etat_futur_mux0005(1),
      G => etat_futur_not0001,
      Q => etat_futur(1)
    );
  etat_futur_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => etat_futur_mux0005(2),
      G => etat_futur_not0001,
      Q => etat_futur(2)
    );
  etat_futur_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => etat_futur_mux0005(3),
      G => etat_futur_not0001,
      Q => etat_futur(3)
    );
  color_a_tester_0 : LDE
    port map (
      D => color_a_tester_mux0001(0),
      G => etat_present(0),
      GE => color_comparante_cmp_lt0000,
      Q => color_a_tester(0)
    );
  color_a_tester_1 : LDE
    port map (
      D => color_a_tester_mux0001(1),
      G => etat_present(0),
      GE => color_comparante_cmp_lt0000,
      Q => color_a_tester(1)
    );
  color_comparante_0 : LDE
    port map (
      D => color_comparante_mux0001(0),
      G => etat_present(0),
      GE => color_comparante_cmp_lt0000,
      Q => color_comparante(0)
    );
  color_comparante_1 : LDE
    port map (
      D => color_comparante_mux0001(1),
      G => etat_present(0),
      GE => color_comparante_cmp_lt0000,
      Q => color_comparante(1)
    );
  etat_present_3 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_109,
      D => etat_futur(3),
      PRE => rst_IBUF_191,
      Q => etat_present(3)
    );
  etat_present_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_109,
      CLR => rst_IBUF_191,
      D => etat_futur(2),
      Q => etat_present(2)
    );
  etat_present_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_109,
      CLR => rst_IBUF_191,
      D => etat_futur(1),
      Q => etat_present(1)
    );
  etat_present_0 : FDC
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_109,
      CLR => rst_IBUF_191,
      D => etat_futur(0),
      Q => etat_present(0)
    );
  timer_checker_0 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => counter_reset_160,
      D => timer_checker_mux0002(3),
      G => clk_2hz_BUFGP_108,
      GE => etat_present(0),
      Q => timer_checker(0)
    );
  timer_checker_1 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => counter_reset_160,
      D => timer_checker_mux0002(2),
      G => clk_2hz_BUFGP_108,
      GE => etat_present(0),
      Q => timer_checker(1)
    );
  timer_checker_2 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => counter_reset_160,
      D => timer_checker_mux0002(1),
      G => clk_2hz_BUFGP_108,
      GE => etat_present(0),
      Q => timer_checker(2)
    );
  timer_checker_3 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => counter_reset_160,
      D => timer_checker_mux0002(0),
      G => clk_2hz_BUFGP_108,
      GE => etat_present(0),
      Q => timer_checker(3)
    );
  Mmux_color_comparante_mux0001_2_f8_0 : MUXF8
    port map (
      I0 => Mmux_color_comparante_mux0001_4_f71,
      I1 => Mmux_color_comparante_mux0001_3_f71,
      S => compteur_local_int(5),
      O => color_comparante_mux0001(1)
    );
  Mmux_color_comparante_mux0001_4_f7_0 : MUXF7
    port map (
      I0 => Mmux_color_comparante_mux0001_6_f61,
      I1 => Mmux_color_comparante_mux0001_5_f63,
      S => compteur_local_int(4),
      O => Mmux_color_comparante_mux0001_4_f71
    );
  Mmux_color_comparante_mux0001_6_f6_0 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_8_f51,
      I1 => Mmux_color_comparante_mux0001_7_f55,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_6_f61
    );
  Mmux_color_comparante_mux0001_8_f5_0 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_101,
      I1 => Mmux_color_comparante_mux0001_97,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_8_f51
    );
  Mmux_color_comparante_mux0001_7_f5_4 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_96,
      I1 => Mmux_color_comparante_mux0001_811,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f55
    );
  Mmux_color_comparante_mux0001_5_f6_2 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_7_f54,
      I1 => Mmux_color_comparante_mux0001_6_f55,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_5_f63
    );
  Mmux_color_comparante_mux0001_7_f5_3 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_95,
      I1 => Mmux_color_comparante_mux0001_810,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f54
    );
  Mmux_color_comparante_mux0001_6_f5_4 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_89,
      I1 => Mmux_color_comparante_mux0001_77,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f55
    );
  Mmux_color_comparante_mux0001_3_f7_0 : MUXF7
    port map (
      I0 => Mmux_color_comparante_mux0001_5_f62,
      I1 => Mmux_color_comparante_mux0001_4_f61,
      S => compteur_local_int(4),
      O => Mmux_color_comparante_mux0001_3_f71
    );
  Mmux_color_comparante_mux0001_5_f6_1 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_7_f53,
      I1 => Mmux_color_comparante_mux0001_6_f54,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_5_f62
    );
  Mmux_color_comparante_mux0001_7_f5_2 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_94,
      I1 => Mmux_color_comparante_mux0001_88,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f53
    );
  Mmux_color_comparante_mux0001_6_f5_3 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_87,
      I1 => Mmux_color_comparante_mux0001_76,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f54
    );
  Mmux_color_comparante_mux0001_4_f6_0 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_6_f53,
      I1 => Mmux_color_comparante_mux0001_5_f51,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_4_f61
    );
  Mmux_color_comparante_mux0001_6_f5_2 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_86,
      I1 => Mmux_color_comparante_mux0001_75,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f53
    );
  Mmux_color_comparante_mux0001_5_f5_0 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_5_f5_0_rt_27,
      I1 => Mmux_color_comparante_mux0001_5_f5_0_rt1_28,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_5_f51
    );
  Mmux_color_comparante_mux0001_2_f8 : MUXF8
    port map (
      I0 => Mmux_color_comparante_mux0001_4_f7_23,
      I1 => Mmux_color_comparante_mux0001_3_f7_19,
      S => compteur_local_int(5),
      O => color_comparante_mux0001(0)
    );
  Mmux_color_comparante_mux0001_4_f7 : MUXF7
    port map (
      I0 => Mmux_color_comparante_mux0001_6_f6_41,
      I1 => Mmux_color_comparante_mux0001_5_f61,
      S => compteur_local_int(4),
      O => Mmux_color_comparante_mux0001_4_f7_23
    );
  Mmux_color_comparante_mux0001_6_f6 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_8_f5_69,
      I1 => Mmux_color_comparante_mux0001_7_f52,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_6_f6_41
    );
  Mmux_color_comparante_mux0001_8_f5 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_10,
      I1 => Mmux_color_comparante_mux0001_93,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_8_f5_69
    );
  Mmux_color_comparante_mux0001_7_f5_1 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_92,
      I1 => Mmux_color_comparante_mux0001_85,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f52
    );
  Mmux_color_comparante_mux0001_5_f6_0 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_7_f51,
      I1 => Mmux_color_comparante_mux0001_6_f52,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_5_f61
    );
  Mmux_color_comparante_mux0001_7_f5_0 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_91,
      I1 => Mmux_color_comparante_mux0001_84,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f51
    );
  Mmux_color_comparante_mux0001_6_f5_1 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_83,
      I1 => Mmux_color_comparante_mux0001_73,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f52
    );
  Mmux_color_comparante_mux0001_3_f7 : MUXF7
    port map (
      I0 => Mmux_color_comparante_mux0001_5_f6_31,
      I1 => Mmux_color_comparante_mux0001_4_f6_21,
      S => compteur_local_int(4),
      O => Mmux_color_comparante_mux0001_3_f7_19
    );
  Mmux_color_comparante_mux0001_5_f6 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_7_f5_51,
      I1 => Mmux_color_comparante_mux0001_6_f51,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_5_f6_31
    );
  Mmux_color_comparante_mux0001_7_f5 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_9,
      I1 => Mmux_color_comparante_mux0001_82,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_7_f5_51
    );
  Mmux_color_comparante_mux0001_6_f5_0 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_81,
      I1 => Mmux_color_comparante_mux0001_72,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f51
    );
  Mmux_color_comparante_mux0001_4_f6 : MUXF6
    port map (
      I0 => Mmux_color_comparante_mux0001_6_f5_35,
      I1 => Mmux_color_comparante_mux0001_5_f5_25,
      S => compteur_local_int(3),
      O => Mmux_color_comparante_mux0001_4_f6_21
    );
  Mmux_color_comparante_mux0001_6_f5 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_8,
      I1 => Mmux_color_comparante_mux0001_71,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_6_f5_35
    );
  Mmux_color_comparante_mux0001_5_f5 : MUXF5
    port map (
      I0 => Mmux_color_comparante_mux0001_5_f5_rt_29,
      I1 => Mmux_color_comparante_mux0001_5_f5_rt1_30,
      S => compteur_local_int(1),
      O => Mmux_color_comparante_mux0001_5_f5_25
    );
  Mcompar_color_comparante_cmp_lt0000_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => N0,
      S => Mcompar_color_comparante_cmp_lt0000_lut(0),
      O => Mcompar_color_comparante_cmp_lt0000_cy(0)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_1_IBUF_128,
      I1 => compteur_local_int(1),
      O => Mcompar_color_comparante_cmp_lt0000_lut(1)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_1_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => compteur_local_int(1),
      S => Mcompar_color_comparante_cmp_lt0000_lut(1),
      O => Mcompar_color_comparante_cmp_lt0000_cy(1)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_2_IBUF_129,
      I1 => compteur_local_int_2_1_137,
      O => Mcompar_color_comparante_cmp_lt0000_lut(2)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_2_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(1),
      DI => compteur_local_int_2_1_137,
      S => Mcompar_color_comparante_cmp_lt0000_lut(2),
      O => Mcompar_color_comparante_cmp_lt0000_cy(2)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_3_IBUF_130,
      I1 => compteur_local_int(3),
      O => Mcompar_color_comparante_cmp_lt0000_lut(3)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_3_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(2),
      DI => compteur_local_int(3),
      S => Mcompar_color_comparante_cmp_lt0000_lut(3),
      O => Mcompar_color_comparante_cmp_lt0000_cy(3)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_4_IBUF_131,
      I1 => compteur_local_int(4),
      O => Mcompar_color_comparante_cmp_lt0000_lut(4)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_4_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(3),
      DI => compteur_local_int(4),
      S => Mcompar_color_comparante_cmp_lt0000_lut(4),
      O => Mcompar_color_comparante_cmp_lt0000_cy(4)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_5_IBUF_132,
      I1 => compteur_local_int(5),
      O => Mcompar_color_comparante_cmp_lt0000_lut(5)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_5_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(4),
      DI => compteur_local_int(5),
      S => Mcompar_color_comparante_cmp_lt0000_lut(5),
      O => Mcompar_color_comparante_cmp_lt0000_cy(5)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_couleur_vector_6_IBUF_133,
      I1 => compteur_local_int(6),
      O => Mcompar_color_comparante_cmp_lt0000_lut(6)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_6_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(5),
      DI => compteur_local_int(6),
      S => Mcompar_color_comparante_cmp_lt0000_lut(6),
      O => Mcompar_color_comparante_cmp_lt0000_cy(6)
    );
  Mcompar_color_comparante_cmp_lt0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => compteur_local_int(7),
      I1 => compteur_couleur_vector_7_IBUF_134,
      O => Mcompar_color_comparante_cmp_lt0000_lut(7)
    );
  Mcompar_color_comparante_cmp_lt0000_cy_7_Q : MUXCY
    port map (
      CI => Mcompar_color_comparante_cmp_lt0000_cy(6),
      DI => compteur_local_int(7),
      S => Mcompar_color_comparante_cmp_lt0000_lut(7),
      O => Mcompar_color_comparante_cmp_lt0000_cy(7)
    );
  compteur_local_int_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(1),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(1)
    );
  compteur_local_int_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(2),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(2)
    );
  compteur_local_int_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(3),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(3)
    );
  compteur_local_int_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(4),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(4)
    );
  compteur_local_int_5 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(5),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(5)
    );
  compteur_local_int_6 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(6),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(6)
    );
  compteur_local_int_7 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(7),
      G => compteur_local_int_not0001,
      Q => compteur_local_int(7)
    );
  compteur_local_int_cmp_eq00011 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(2),
      I1 => bus_color_present(1),
      I2 => bus_color_present(0),
      O => compteur_local_int_cmp_eq0001
    );
  bus_color_out_1_1 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => couleur_jouer_0_OBUF_156,
      I1 => btn_vert_IBUF_99,
      O => couleur_jouer_1_OBUF_157
    );
  timer_checker_mux0002_0_1 : LUT4
    generic map(
      INIT => X"4202"
    )
    port map (
      I0 => timer_checker(3),
      I1 => timer_checker(1),
      I2 => timer_checker(2),
      I3 => timer_checker(0),
      O => timer_checker_mux0002(0)
    );
  etat_futur_cmp_eq00001 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => etat_system_2_IBUF_187,
      I1 => etat_system_1_IBUF_186,
      I2 => etat_system_0_IBUF_185,
      O => etat_futur_cmp_eq0000
    );
  etat_futur_mux0005_3_1 : LUT4
    generic map(
      INIT => X"5554"
    )
    port map (
      I0 => etat_futur_cmp_eq0000,
      I1 => etat_present(2),
      I2 => etat_present(1),
      I3 => etat_present(3),
      O => etat_futur_mux0005(3)
    );
  etat_futur_cmp_ge00001 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => timer_checker(3),
      I1 => timer_checker(1),
      I2 => timer_checker(2),
      O => etat_futur_cmp_ge0000
    );
  compteur_local_int_2_61 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_0_IBUF_252,
      I2 => sequence_complete_4_IBUF_296,
      O => Mmux_color_comparante_mux0001_10
    );
  compteur_local_int_2_51 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_8_IBUF_308,
      I2 => sequence_complete_12_IBUF_255,
      O => Mmux_color_comparante_mux0001_92
    );
  compteur_local_int_2_41 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_16_IBUF_259,
      I2 => sequence_complete_20_IBUF_264,
      O => Mmux_color_comparante_mux0001_91
    );
  compteur_local_int_2_31 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_24_IBUF_268,
      I2 => sequence_complete_28_IBUF_272,
      O => Mmux_color_comparante_mux0001_83
    );
  compteur_local_int_2_21 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_32_IBUF_277,
      I2 => sequence_complete_36_IBUF_281,
      O => Mmux_color_comparante_mux0001_9
    );
  compteur_local_int_2_1491 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_41_IBUF_287,
      I2 => sequence_complete_45_IBUF_291,
      O => Mmux_color_comparante_mux0001_87
    );
  compteur_local_int_2_1481 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_2_IBUF_274,
      I2 => sequence_complete_6_IBUF_306,
      O => Mmux_color_comparante_mux0001_93
    );
  compteur_local_int_2_1471 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_35_IBUF_280,
      I2 => sequence_complete_39_IBUF_284,
      O => Mmux_color_comparante_mux0001_88
    );
  compteur_local_int_2_1461 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_34_IBUF_279,
      I2 => sequence_complete_38_IBUF_283,
      O => Mmux_color_comparante_mux0001_82
    );
  compteur_local_int_2_1451 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_33_IBUF_278,
      I2 => sequence_complete_37_IBUF_282,
      O => Mmux_color_comparante_mux0001_94
    );
  compteur_local_int_2_1441 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_27_IBUF_271,
      I2 => sequence_complete_31_IBUF_276,
      O => Mmux_color_comparante_mux0001_77
    );
  compteur_local_int_2_1431 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_26_IBUF_270,
      I2 => sequence_complete_30_IBUF_275,
      O => Mmux_color_comparante_mux0001_73
    );
  compteur_local_int_2_14201 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_1_IBUF_263,
      I2 => sequence_complete_5_IBUF_305,
      O => Mmux_color_comparante_mux0001_101
    );
  compteur_local_int_2_1421 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_25_IBUF_269,
      I2 => sequence_complete_29_IBUF_273,
      O => Mmux_color_comparante_mux0001_89
    );
  compteur_local_int_2_14191 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_19_IBUF_262,
      I2 => sequence_complete_23_IBUF_267,
      O => Mmux_color_comparante_mux0001_810
    );
  compteur_local_int_2_14181 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_18_IBUF_261,
      I2 => sequence_complete_22_IBUF_266,
      O => Mmux_color_comparante_mux0001_84
    );
  compteur_local_int_2_14171 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_17_IBUF_260,
      I2 => sequence_complete_21_IBUF_265,
      O => Mmux_color_comparante_mux0001_95
    );
  compteur_local_int_2_14161 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_11_IBUF_254,
      I2 => sequence_complete_15_IBUF_258,
      O => Mmux_color_comparante_mux0001_811
    );
  compteur_local_int_2_14151 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_10_IBUF_253,
      I2 => sequence_complete_14_IBUF_257,
      O => Mmux_color_comparante_mux0001_85
    );
  compteur_local_int_2_14141 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_9_IBUF_309,
      I2 => sequence_complete_13_IBUF_256,
      O => Mmux_color_comparante_mux0001_96
    );
  compteur_local_int_2_14131 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_49_IBUF_295,
      I2 => sequence_complete_53_IBUF_300,
      O => Mmux_color_comparante_mux0001_86
    );
  compteur_local_int_2_14121 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_3_IBUF_285,
      I2 => sequence_complete_7_IBUF_307,
      O => Mmux_color_comparante_mux0001_97
    );
  compteur_local_int_2_14111 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_43_IBUF_289,
      I2 => sequence_complete_47_IBUF_293,
      O => Mmux_color_comparante_mux0001_76
    );
  compteur_local_int_2_14101 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_42_IBUF_288,
      I2 => sequence_complete_46_IBUF_292,
      O => Mmux_color_comparante_mux0001_72
    );
  compteur_local_int_2_1411 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_51_IBUF_298,
      I2 => sequence_complete_55_IBUF_302,
      O => Mmux_color_comparante_mux0001_75
    );
  compteur_local_int_2_141 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_50_IBUF_297,
      I2 => sequence_complete_54_IBUF_301,
      O => Mmux_color_comparante_mux0001_71
    );
  compteur_local_int_2_11 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_40_IBUF_286,
      I2 => sequence_complete_44_IBUF_290,
      O => Mmux_color_comparante_mux0001_81
    );
  compteur_local_int_2_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => compteur_local_int(2),
      I1 => sequence_complete_48_IBUF_294,
      I2 => sequence_complete_52_IBUF_299,
      O => Mmux_color_comparante_mux0001_8
    );
  bus_color_futur_0_1 : LUT3
    generic map(
      INIT => X"51"
    )
    port map (
      I0 => couleur_jouer_0_OBUF_156,
      I1 => btn_jaune_IBUF_95,
      I2 => btn_vert_IBUF_99,
      O => bus_color_futur(0)
    );
  bus_color_futur_2_111 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => couleur_jouer_0_OBUF_156,
      I1 => btn_vert_IBUF_99,
      O => bus_color_futur(1)
    );
  etat_futur_cmp_ne0001_SW0 : LUT4
    generic map(
      INIT => X"6FF6"
    )
    port map (
      I0 => bus_color_futur(0),
      I1 => bus_color_present(0),
      I2 => bus_color_futur(1),
      I3 => bus_color_present(1),
      O => N9
    );
  etat_futur_cmp_ne0001 : LUT3
    generic map(
      INIT => X"BE"
    )
    port map (
      I0 => N9,
      I1 => bus_color_present(2),
      I2 => bus_color_futur(2),
      O => etat_futur_cmp_ne0001_169
    );
  btn_rouge_IBUF : IBUF
    port map (
      I => btn_rouge,
      O => btn_rouge_IBUF_97
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_191
    );
  btn_bleu_IBUF : IBUF
    port map (
      I => btn_bleu,
      O => couleur_jouer_0_OBUF_156
    );
  btn_jaune_IBUF : IBUF
    port map (
      I => btn_jaune,
      O => btn_jaune_IBUF_95
    );
  btn_vert_IBUF : IBUF
    port map (
      I => btn_vert,
      O => btn_vert_IBUF_99
    );
  compteur_couleur_vector_7_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(7),
      O => compteur_couleur_vector_7_IBUF_134
    );
  compteur_couleur_vector_6_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(6),
      O => compteur_couleur_vector_6_IBUF_133
    );
  compteur_couleur_vector_5_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(5),
      O => compteur_couleur_vector_5_IBUF_132
    );
  compteur_couleur_vector_4_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(4),
      O => compteur_couleur_vector_4_IBUF_131
    );
  compteur_couleur_vector_3_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(3),
      O => compteur_couleur_vector_3_IBUF_130
    );
  compteur_couleur_vector_2_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(2),
      O => compteur_couleur_vector_2_IBUF_129
    );
  compteur_couleur_vector_1_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(1),
      O => compteur_couleur_vector_1_IBUF_128
    );
  compteur_couleur_vector_0_IBUF : IBUF
    port map (
      I => compteur_couleur_vector(0),
      O => compteur_couleur_vector_0_IBUF_127
    );
  etat_system_2_IBUF : IBUF
    port map (
      I => etat_system(2),
      O => etat_system_2_IBUF_187
    );
  etat_system_1_IBUF : IBUF
    port map (
      I => etat_system(1),
      O => etat_system_1_IBUF_186
    );
  etat_system_0_IBUF : IBUF
    port map (
      I => etat_system(0),
      O => etat_system_0_IBUF_185
    );
  sequence_complete_59_IBUF : IBUF
    port map (
      I => sequence_complete(59),
      O => sequence_complete_59_IBUF_304
    );
  sequence_complete_58_IBUF : IBUF
    port map (
      I => sequence_complete(58),
      O => sequence_complete_58_IBUF_303
    );
  sequence_complete_57_IBUF : IBUF
    port map (
      I => sequence_complete(57),
      O => Mmux_color_comparante_mux0001_74
    );
  sequence_complete_56_IBUF : IBUF
    port map (
      I => sequence_complete(56),
      O => Mmux_color_comparante_mux0001_7
    );
  sequence_complete_55_IBUF : IBUF
    port map (
      I => sequence_complete(55),
      O => sequence_complete_55_IBUF_302
    );
  sequence_complete_54_IBUF : IBUF
    port map (
      I => sequence_complete(54),
      O => sequence_complete_54_IBUF_301
    );
  sequence_complete_53_IBUF : IBUF
    port map (
      I => sequence_complete(53),
      O => sequence_complete_53_IBUF_300
    );
  sequence_complete_52_IBUF : IBUF
    port map (
      I => sequence_complete(52),
      O => sequence_complete_52_IBUF_299
    );
  sequence_complete_51_IBUF : IBUF
    port map (
      I => sequence_complete(51),
      O => sequence_complete_51_IBUF_298
    );
  sequence_complete_50_IBUF : IBUF
    port map (
      I => sequence_complete(50),
      O => sequence_complete_50_IBUF_297
    );
  sequence_complete_49_IBUF : IBUF
    port map (
      I => sequence_complete(49),
      O => sequence_complete_49_IBUF_295
    );
  sequence_complete_48_IBUF : IBUF
    port map (
      I => sequence_complete(48),
      O => sequence_complete_48_IBUF_294
    );
  sequence_complete_47_IBUF : IBUF
    port map (
      I => sequence_complete(47),
      O => sequence_complete_47_IBUF_293
    );
  sequence_complete_46_IBUF : IBUF
    port map (
      I => sequence_complete(46),
      O => sequence_complete_46_IBUF_292
    );
  sequence_complete_45_IBUF : IBUF
    port map (
      I => sequence_complete(45),
      O => sequence_complete_45_IBUF_291
    );
  sequence_complete_44_IBUF : IBUF
    port map (
      I => sequence_complete(44),
      O => sequence_complete_44_IBUF_290
    );
  sequence_complete_43_IBUF : IBUF
    port map (
      I => sequence_complete(43),
      O => sequence_complete_43_IBUF_289
    );
  sequence_complete_42_IBUF : IBUF
    port map (
      I => sequence_complete(42),
      O => sequence_complete_42_IBUF_288
    );
  sequence_complete_41_IBUF : IBUF
    port map (
      I => sequence_complete(41),
      O => sequence_complete_41_IBUF_287
    );
  sequence_complete_40_IBUF : IBUF
    port map (
      I => sequence_complete(40),
      O => sequence_complete_40_IBUF_286
    );
  sequence_complete_39_IBUF : IBUF
    port map (
      I => sequence_complete(39),
      O => sequence_complete_39_IBUF_284
    );
  sequence_complete_38_IBUF : IBUF
    port map (
      I => sequence_complete(38),
      O => sequence_complete_38_IBUF_283
    );
  sequence_complete_37_IBUF : IBUF
    port map (
      I => sequence_complete(37),
      O => sequence_complete_37_IBUF_282
    );
  sequence_complete_36_IBUF : IBUF
    port map (
      I => sequence_complete(36),
      O => sequence_complete_36_IBUF_281
    );
  sequence_complete_35_IBUF : IBUF
    port map (
      I => sequence_complete(35),
      O => sequence_complete_35_IBUF_280
    );
  sequence_complete_34_IBUF : IBUF
    port map (
      I => sequence_complete(34),
      O => sequence_complete_34_IBUF_279
    );
  sequence_complete_33_IBUF : IBUF
    port map (
      I => sequence_complete(33),
      O => sequence_complete_33_IBUF_278
    );
  sequence_complete_32_IBUF : IBUF
    port map (
      I => sequence_complete(32),
      O => sequence_complete_32_IBUF_277
    );
  sequence_complete_31_IBUF : IBUF
    port map (
      I => sequence_complete(31),
      O => sequence_complete_31_IBUF_276
    );
  sequence_complete_30_IBUF : IBUF
    port map (
      I => sequence_complete(30),
      O => sequence_complete_30_IBUF_275
    );
  sequence_complete_29_IBUF : IBUF
    port map (
      I => sequence_complete(29),
      O => sequence_complete_29_IBUF_273
    );
  sequence_complete_28_IBUF : IBUF
    port map (
      I => sequence_complete(28),
      O => sequence_complete_28_IBUF_272
    );
  sequence_complete_27_IBUF : IBUF
    port map (
      I => sequence_complete(27),
      O => sequence_complete_27_IBUF_271
    );
  sequence_complete_26_IBUF : IBUF
    port map (
      I => sequence_complete(26),
      O => sequence_complete_26_IBUF_270
    );
  sequence_complete_25_IBUF : IBUF
    port map (
      I => sequence_complete(25),
      O => sequence_complete_25_IBUF_269
    );
  sequence_complete_24_IBUF : IBUF
    port map (
      I => sequence_complete(24),
      O => sequence_complete_24_IBUF_268
    );
  sequence_complete_23_IBUF : IBUF
    port map (
      I => sequence_complete(23),
      O => sequence_complete_23_IBUF_267
    );
  sequence_complete_22_IBUF : IBUF
    port map (
      I => sequence_complete(22),
      O => sequence_complete_22_IBUF_266
    );
  sequence_complete_21_IBUF : IBUF
    port map (
      I => sequence_complete(21),
      O => sequence_complete_21_IBUF_265
    );
  sequence_complete_20_IBUF : IBUF
    port map (
      I => sequence_complete(20),
      O => sequence_complete_20_IBUF_264
    );
  sequence_complete_19_IBUF : IBUF
    port map (
      I => sequence_complete(19),
      O => sequence_complete_19_IBUF_262
    );
  sequence_complete_18_IBUF : IBUF
    port map (
      I => sequence_complete(18),
      O => sequence_complete_18_IBUF_261
    );
  sequence_complete_17_IBUF : IBUF
    port map (
      I => sequence_complete(17),
      O => sequence_complete_17_IBUF_260
    );
  sequence_complete_16_IBUF : IBUF
    port map (
      I => sequence_complete(16),
      O => sequence_complete_16_IBUF_259
    );
  sequence_complete_15_IBUF : IBUF
    port map (
      I => sequence_complete(15),
      O => sequence_complete_15_IBUF_258
    );
  sequence_complete_14_IBUF : IBUF
    port map (
      I => sequence_complete(14),
      O => sequence_complete_14_IBUF_257
    );
  sequence_complete_13_IBUF : IBUF
    port map (
      I => sequence_complete(13),
      O => sequence_complete_13_IBUF_256
    );
  sequence_complete_12_IBUF : IBUF
    port map (
      I => sequence_complete(12),
      O => sequence_complete_12_IBUF_255
    );
  sequence_complete_11_IBUF : IBUF
    port map (
      I => sequence_complete(11),
      O => sequence_complete_11_IBUF_254
    );
  sequence_complete_10_IBUF : IBUF
    port map (
      I => sequence_complete(10),
      O => sequence_complete_10_IBUF_253
    );
  sequence_complete_9_IBUF : IBUF
    port map (
      I => sequence_complete(9),
      O => sequence_complete_9_IBUF_309
    );
  sequence_complete_8_IBUF : IBUF
    port map (
      I => sequence_complete(8),
      O => sequence_complete_8_IBUF_308
    );
  sequence_complete_7_IBUF : IBUF
    port map (
      I => sequence_complete(7),
      O => sequence_complete_7_IBUF_307
    );
  sequence_complete_6_IBUF : IBUF
    port map (
      I => sequence_complete(6),
      O => sequence_complete_6_IBUF_306
    );
  sequence_complete_5_IBUF : IBUF
    port map (
      I => sequence_complete(5),
      O => sequence_complete_5_IBUF_305
    );
  sequence_complete_4_IBUF : IBUF
    port map (
      I => sequence_complete(4),
      O => sequence_complete_4_IBUF_296
    );
  sequence_complete_3_IBUF : IBUF
    port map (
      I => sequence_complete(3),
      O => sequence_complete_3_IBUF_285
    );
  sequence_complete_2_IBUF : IBUF
    port map (
      I => sequence_complete(2),
      O => sequence_complete_2_IBUF_274
    );
  sequence_complete_1_IBUF : IBUF
    port map (
      I => sequence_complete(1),
      O => sequence_complete_1_IBUF_263
    );
  sequence_complete_0_IBUF : IBUF
    port map (
      I => sequence_complete(0),
      O => sequence_complete_0_IBUF_252
    );
  evaluer_1_OBUF : OBUF
    port map (
      I => etat_present_2_1_180,
      O => evaluer(1)
    );
  evaluer_0_OBUF : OBUF
    port map (
      I => etat_present_1_1_178,
      O => evaluer(0)
    );
  couleur_jouer_3_OBUF : OBUF
    port map (
      I => couleur_jouer_3_OBUF_159,
      O => couleur_jouer(3)
    );
  couleur_jouer_2_OBUF : OBUF
    port map (
      I => couleur_jouer_2_OBUF_158,
      O => couleur_jouer(2)
    );
  couleur_jouer_1_OBUF : OBUF
    port map (
      I => couleur_jouer_1_OBUF_157,
      O => couleur_jouer(1)
    );
  couleur_jouer_0_OBUF : OBUF
    port map (
      I => couleur_jouer_0_OBUF_156,
      O => couleur_jouer(0)
    );
  Mmux_color_comparante_mux0001_5_f5_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Mmux_color_comparante_mux0001_74,
      O => Mmux_color_comparante_mux0001_5_f5_0_rt_27
    );
  Mmux_color_comparante_mux0001_5_f5_0_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => sequence_complete_59_IBUF_304,
      O => Mmux_color_comparante_mux0001_5_f5_0_rt1_28
    );
  Mmux_color_comparante_mux0001_5_f5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Mmux_color_comparante_mux0001_7,
      O => Mmux_color_comparante_mux0001_5_f5_rt_29
    );
  Mmux_color_comparante_mux0001_5_f5_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => sequence_complete_58_IBUF_303,
      O => Mmux_color_comparante_mux0001_5_f5_rt1_30
    );
  etat_futur_mux0005_0_14 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      I1 => N51,
      I2 => etat_futur_cmp_ne0001_169,
      I3 => compteur_local_int_cmp_eq0001,
      O => counter_reset_mux0003
    );
  compteur_local_int_not000111_SW4 : LUT3
    generic map(
      INIT => X"95"
    )
    port map (
      I0 => compteur_local_int(6),
      I1 => compteur_local_int(5),
      I2 => N52,
      O => N23
    );
  etat_futur_mux0005_0_14_SW0 : LUT4
    generic map(
      INIT => X"FFBF"
    )
    port map (
      I0 => counter_reset_160,
      I1 => etat_present(0),
      I2 => etat_futur_cmp_ne0001_169,
      I3 => compteur_local_int_cmp_eq0001,
      O => N27
    );
  compteur_local_int_mux0004_1_1 : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => N27,
      I1 => etat_futur_cmp_eq0001,
      I2 => compteur_local_int(1),
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(1)
    );
  compteur_local_int_mux0004_7_Q : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => N27,
      I1 => etat_futur_cmp_eq0001,
      I2 => N15,
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(7)
    );
  compteur_local_int_mux0004_4_Q : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => N27,
      I1 => etat_futur_cmp_eq0001,
      I2 => N17,
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(4)
    );
  compteur_local_int_mux0004_5_1 : LUT4
    generic map(
      INIT => X"0014"
    )
    port map (
      I0 => N33,
      I1 => compteur_local_int(5),
      I2 => Madd_compteur_local_int_addsub0000_cy(4),
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(5)
    );
  compteur_local_int_mux0004_2_1 : LUT4
    generic map(
      INIT => X"0014"
    )
    port map (
      I0 => N33,
      I1 => compteur_local_int(1),
      I2 => compteur_local_int(2),
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(2)
    );
  compteur_local_int_mux0004_6_1 : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => N27,
      I1 => etat_futur_cmp_eq0001,
      I2 => N23,
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(6)
    );
  compteur_local_int_mux0004_3_1 : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => N27,
      I1 => etat_futur_cmp_eq0001,
      I2 => N25,
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => compteur_local_int_mux0004(3)
    );
  compteur_local_int_not00012 : LUT4
    generic map(
      INIT => X"44E4"
    )
    port map (
      I0 => etat_present(0),
      I1 => etat_present(3),
      I2 => counter_reset_mux0003,
      I3 => counter_reset_160,
      O => compteur_local_int_not0001
    );
  etat_futur_mux0005_0_18 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => etat_present(3),
      I1 => etat_futur_cmp_eq0000,
      I2 => N43,
      I3 => etat_present(0),
      O => etat_futur_mux0005(0)
    );
  timer_checker_mux0002_1_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => timer_checker(3),
      I1 => timer_checker(2),
      I2 => timer_checker(1),
      I3 => timer_checker(0),
      O => timer_checker_mux0002(1)
    );
  timer_checker_mux0002_2_1 : LUT4
    generic map(
      INIT => X"1434"
    )
    port map (
      I0 => timer_checker(3),
      I1 => timer_checker(1),
      I2 => timer_checker(0),
      I3 => timer_checker(2),
      O => timer_checker_mux0002(2)
    );
  timer_checker_mux0002_3_1 : LUT4
    generic map(
      INIT => X"010F"
    )
    port map (
      I0 => timer_checker(2),
      I1 => timer_checker(1),
      I2 => timer_checker(0),
      I3 => timer_checker(3),
      O => timer_checker_mux0002(3)
    );
  color_a_tester_mux0001_1_1 : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => bus_color_present(2),
      I2 => color_a_tester(1),
      I3 => bus_color_present(0),
      O => color_a_tester_mux0001(1)
    );
  color_a_tester_mux0001_0_1 : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => bus_color_present(2),
      I2 => color_a_tester(0),
      I3 => bus_color_present(1),
      O => color_a_tester_mux0001(0)
    );
  etat_futur_not00011 : LUT4
    generic map(
      INIT => X"FFFB"
    )
    port map (
      I0 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      I1 => etat_present(0),
      I2 => etat_futur_cmp_ne0001_169,
      I3 => compteur_local_int_cmp_eq0001,
      O => etat_futur_not0001
    );
  bus_color_out_2_1 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => btn_jaune_IBUF_95,
      I1 => couleur_jouer_0_OBUF_156,
      I2 => btn_vert_IBUF_99,
      O => couleur_jouer_2_OBUF_158
    );
  bus_color_out_3_1 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_jouer_0_OBUF_156,
      I1 => btn_vert_IBUF_99,
      I2 => btn_rouge_IBUF_97,
      I3 => btn_jaune_IBUF_95,
      O => couleur_jouer_3_OBUF_159
    );
  etat_futur_mux0005_1_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => etat_futur_cmp_eq0000,
      I1 => etat_present(1),
      I2 => etat_present(0),
      I3 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => etat_futur_mux0005(1)
    );
  etat_futur_mux0005_2_0 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => etat_system_1_IBUF_186,
      I1 => etat_system_0_IBUF_185,
      I2 => etat_system_2_IBUF_187,
      I3 => etat_present(2),
      O => etat_futur_mux0005_2_0_173
    );
  bus_color_futur_2_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => couleur_jouer_0_OBUF_156,
      I1 => btn_vert_IBUF_99,
      I2 => btn_rouge_IBUF_97,
      I3 => btn_jaune_IBUF_95,
      O => bus_color_futur(2)
    );
  etat_futur_mux0005_0_14_SW3_SW0 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => etat_futur_cmp_eq0001,
      I1 => etat_futur_cmp_ne0001_169,
      I2 => etat_present(0),
      I3 => counter_reset_160,
      O => N45
    );
  etat_futur_mux0005_0_14_SW3 : LUT4
    generic map(
      INIT => X"FF80"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => bus_color_present(1),
      I2 => bus_color_present(2),
      I3 => N45,
      O => N33
    );
  etat_futur_mux0005_2_23_SW1 : LUT4
    generic map(
      INIT => X"BA10"
    )
    port map (
      I0 => compteur_local_int_cmp_eq0001,
      I1 => etat_futur_cmp_eq0001,
      I2 => etat_futur_cmp_ne0001_169,
      I3 => etat_futur_cmp_ge0000,
      O => N49
    );
  etat_futur_mux0005_2_23 : LUT4
    generic map(
      INIT => X"FF08"
    )
    port map (
      I0 => N49,
      I1 => etat_present(0),
      I2 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      I3 => etat_futur_mux0005_2_0_173,
      O => etat_futur_mux0005(2)
    );
  etat_futur_mux0005_0_18_SW0 : LUT4
    generic map(
      INIT => X"AABA"
    )
    port map (
      I0 => counter_reset_mux0003,
      I1 => Mcompar_color_comparante_cmp_lt0000_cy(7),
      I2 => compteur_local_int_cmp_eq0001,
      I3 => etat_futur_cmp_ge0000,
      O => N43
    );
  compteur_local_int_2_1_227 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_local_int_mux0004(2),
      G => compteur_local_int_not0001,
      Q => compteur_local_int_2_1_137
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_109
    );
  clk_2hz_BUFGP : BUFGP
    port map (
      I => clk_2hz,
      O => clk_2hz_BUFGP_108
    );
  Mcompar_color_comparante_cmp_lt0000_cy_7_inv_INV_0 : INV
    port map (
      I => Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => color_comparante_cmp_lt0000
    );
  Mcompar_color_comparante_cmp_lt0000_lut_0_1_INV_0 : INV
    port map (
      I => compteur_couleur_vector_0_IBUF_127,
      O => Mcompar_color_comparante_cmp_lt0000_lut(0)
    );
  etat_futur_cmp_eq000121 : LUT4_D
    generic map(
      INIT => X"9009"
    )
    port map (
      I0 => color_a_tester(0),
      I1 => color_comparante(0),
      I2 => color_a_tester(1),
      I3 => color_comparante(1),
      LO => N51,
      O => etat_futur_cmp_eq0001
    );
  Madd_compteur_local_int_addsub0000_cy_4_11 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur_local_int(4),
      I1 => compteur_local_int(3),
      I2 => compteur_local_int(1),
      I3 => compteur_local_int(2),
      LO => N52,
      O => Madd_compteur_local_int_addsub0000_cy(4)
    );
  compteur_local_int_not000111_SW5 : LUT3_L
    generic map(
      INIT => X"87"
    )
    port map (
      I0 => compteur_local_int(1),
      I1 => compteur_local_int(2),
      I2 => compteur_local_int(3),
      LO => N25
    );
  compteur_local_int_not000111_SW0 : LUT4_L
    generic map(
      INIT => X"9555"
    )
    port map (
      I0 => compteur_local_int(7),
      I1 => compteur_local_int(6),
      I2 => compteur_local_int(5),
      I3 => Madd_compteur_local_int_addsub0000_cy(4),
      LO => N15
    );
  compteur_local_int_not000111_SW1 : LUT4_L
    generic map(
      INIT => X"9333"
    )
    port map (
      I0 => compteur_local_int(3),
      I1 => compteur_local_int(4),
      I2 => compteur_local_int(2),
      I3 => compteur_local_int(1),
      LO => N17
    );
  etat_present_2_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_109,
      CLR => rst_IBUF_191,
      D => etat_futur(2),
      Q => etat_present_2_1_180
    );
  etat_present_1_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_109,
      CLR => rst_IBUF_191,
      D => etat_futur(1),
      Q => etat_present_1_1_178
    );

end Structure;

