--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: M.81d
--  \   \         Application: netgen
--  /   /         Filename: workerScore_synthesis.vhd
-- /___/   /\     Timestamp: Sun Jun 13 13:21:08 2021
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm workerScore -w -dir netgen/synthesis -ofmt vhdl -sim workerScore.ngc workerScore_synthesis.vhd 
-- Device	: xc3s50a-5-tq144
-- Input file	: workerScore.ngc
-- Output file	: C:\Users\eia\Documents\duchuy-vhdl\workerScore\netgen\synthesis\workerScore_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: workerScore
-- Xilinx	: C:\Xilinx\12.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity workerScore is
  port (
    clk : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    score_j1 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    score_j2 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    score_j3 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    score_j4 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    etat_system : in STD_LOGIC_VECTOR ( 2 downto 0 ); 
    evaluer : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
    id_joueur : in STD_LOGIC_VECTOR ( 1 downto 0 ) 
  );
end workerScore;

architecture Structure of workerScore is
  signal N01 : STD_LOGIC; 
  signal N101 : STD_LOGIC; 
  signal N107 : STD_LOGIC; 
  signal N11 : STD_LOGIC; 
  signal N110 : STD_LOGIC; 
  signal N111 : STD_LOGIC; 
  signal N112 : STD_LOGIC; 
  signal N113 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal N115 : STD_LOGIC; 
  signal N116 : STD_LOGIC; 
  signal N117 : STD_LOGIC; 
  signal N118 : STD_LOGIC; 
  signal N119 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N23 : STD_LOGIC; 
  signal N33 : STD_LOGIC; 
  signal N34 : STD_LOGIC; 
  signal N35 : STD_LOGIC; 
  signal N36 : STD_LOGIC; 
  signal N37 : STD_LOGIC; 
  signal N38 : STD_LOGIC; 
  signal N39 : STD_LOGIC; 
  signal N40 : STD_LOGIC; 
  signal N41 : STD_LOGIC; 
  signal N42 : STD_LOGIC; 
  signal N57 : STD_LOGIC; 
  signal N58 : STD_LOGIC; 
  signal N59 : STD_LOGIC; 
  signal N60 : STD_LOGIC; 
  signal N63 : STD_LOGIC; 
  signal N67 : STD_LOGIC; 
  signal N69 : STD_LOGIC; 
  signal N71 : STD_LOGIC; 
  signal N85 : STD_LOGIC; 
  signal N87 : STD_LOGIC; 
  signal N89 : STD_LOGIC; 
  signal N91 : STD_LOGIC; 
  signal N93 : STD_LOGIC; 
  signal N95 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal N99 : STD_LOGIC; 
  signal clk_BUFGP_44 : STD_LOGIC; 
  signal etat_present_FSM_FFd1_45 : STD_LOGIC; 
  signal etat_present_FSM_FFd1_In : STD_LOGIC; 
  signal etat_present_FSM_FFd2_47 : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In_48 : STD_LOGIC; 
  signal etat_system_0_IBUF_52 : STD_LOGIC; 
  signal etat_system_1_IBUF_53 : STD_LOGIC; 
  signal etat_system_2_IBUF_54 : STD_LOGIC; 
  signal evaluer_0_IBUF_57 : STD_LOGIC; 
  signal evaluer_1_IBUF_58 : STD_LOGIC; 
  signal id_joueur_0_IBUF_61 : STD_LOGIC; 
  signal id_joueur_1_IBUF_62 : STD_LOGIC; 
  signal joueur1_L_mux0004_0_2 : STD_LOGIC; 
  signal joueur1_L_mux0004_3_1_72 : STD_LOGIC; 
  signal joueur1_L_not0001 : STD_LOGIC; 
  signal joueur1_R_not0001_82 : STD_LOGIC; 
  signal joueur2_L_mux0005_3_1_91 : STD_LOGIC; 
  signal joueur2_L_not0001 : STD_LOGIC; 
  signal joueur2_R_mux0006_3_1_101 : STD_LOGIC; 
  signal joueur2_R_not0001 : STD_LOGIC; 
  signal joueur3_L_mux0006_3_1_111 : STD_LOGIC; 
  signal joueur3_L_not0001 : STD_LOGIC; 
  signal joueur3_R_mux0007_3_1_121 : STD_LOGIC; 
  signal joueur3_R_not0001 : STD_LOGIC; 
  signal joueur4_L_mux0006_3_1_131 : STD_LOGIC; 
  signal joueur4_L_not0001 : STD_LOGIC; 
  signal joueur4_R_mux0007_3_1_141 : STD_LOGIC; 
  signal joueur4_R_not0001 : STD_LOGIC; 
  signal rst_IBUF_144 : STD_LOGIC; 
  signal validation_177 : STD_LOGIC; 
  signal validation_mux0000 : STD_LOGIC; 
  signal validation_or0000 : STD_LOGIC; 
  signal joueur1_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur1_L_mux0004 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur1_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur1_R_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur2_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur2_L_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur2_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur2_R_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur3_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur3_L_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur3_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur3_R_mux0007 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur4_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur4_L_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur4_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal joueur4_R_mux0007 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
begin
  validation : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => validation_mux0000,
      G => validation_or0000,
      Q => validation_177
    );
  etat_present_FSM_FFd1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_44,
      CLR => rst_IBUF_144,
      D => etat_present_FSM_FFd1_In,
      Q => etat_present_FSM_FFd1_45
    );
  etat_present_FSM_FFd2 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_44,
      D => etat_present_FSM_FFd2_In_48,
      PRE => rst_IBUF_144,
      Q => etat_present_FSM_FFd2_47
    );
  joueur4_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_R_mux0007(0),
      G => joueur4_R_not0001,
      Q => joueur4_R(0)
    );
  joueur4_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_R_mux0007(1),
      G => joueur4_R_not0001,
      Q => joueur4_R(1)
    );
  joueur4_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_R_mux0007(2),
      G => joueur4_R_not0001,
      Q => joueur4_R(2)
    );
  joueur4_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_R_mux0007(3),
      G => joueur4_R_not0001,
      Q => joueur4_R(3)
    );
  joueur1_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_L_mux0004(0),
      G => joueur1_L_not0001,
      Q => joueur1_L(0)
    );
  joueur1_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_L_mux0004(1),
      G => joueur1_L_not0001,
      Q => joueur1_L(1)
    );
  joueur1_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_L_mux0004(2),
      G => joueur1_L_not0001,
      Q => joueur1_L(2)
    );
  joueur1_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_L_mux0004(3),
      G => joueur1_L_not0001,
      Q => joueur1_L(3)
    );
  joueur1_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_R_mux0005(0),
      G => joueur1_R_not0001_82,
      Q => joueur1_R(0)
    );
  joueur1_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_R_mux0005(1),
      G => joueur1_R_not0001_82,
      Q => joueur1_R(1)
    );
  joueur1_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_R_mux0005(2),
      G => joueur1_R_not0001_82,
      Q => joueur1_R(2)
    );
  joueur1_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur1_R_mux0005(3),
      G => joueur1_R_not0001_82,
      Q => joueur1_R(3)
    );
  joueur2_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_L_mux0005(0),
      G => joueur2_L_not0001,
      Q => joueur2_L(0)
    );
  joueur2_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_L_mux0005(1),
      G => joueur2_L_not0001,
      Q => joueur2_L(1)
    );
  joueur2_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_L_mux0005(2),
      G => joueur2_L_not0001,
      Q => joueur2_L(2)
    );
  joueur2_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_L_mux0005(3),
      G => joueur2_L_not0001,
      Q => joueur2_L(3)
    );
  joueur2_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_R_mux0006(0),
      G => joueur2_R_not0001,
      Q => joueur2_R(0)
    );
  joueur2_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_R_mux0006(1),
      G => joueur2_R_not0001,
      Q => joueur2_R(1)
    );
  joueur2_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_R_mux0006(2),
      G => joueur2_R_not0001,
      Q => joueur2_R(2)
    );
  joueur2_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur2_R_mux0006(3),
      G => joueur2_R_not0001,
      Q => joueur2_R(3)
    );
  joueur3_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_L_mux0006(0),
      G => joueur3_L_not0001,
      Q => joueur3_L(0)
    );
  joueur3_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_L_mux0006(1),
      G => joueur3_L_not0001,
      Q => joueur3_L(1)
    );
  joueur3_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_L_mux0006(2),
      G => joueur3_L_not0001,
      Q => joueur3_L(2)
    );
  joueur3_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_L_mux0006(3),
      G => joueur3_L_not0001,
      Q => joueur3_L(3)
    );
  joueur3_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_R_mux0007(0),
      G => joueur3_R_not0001,
      Q => joueur3_R(0)
    );
  joueur3_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_R_mux0007(1),
      G => joueur3_R_not0001,
      Q => joueur3_R(1)
    );
  joueur3_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_R_mux0007(2),
      G => joueur3_R_not0001,
      Q => joueur3_R(2)
    );
  joueur3_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur3_R_mux0007(3),
      G => joueur3_R_not0001,
      Q => joueur3_R(3)
    );
  joueur4_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_L_mux0006(0),
      G => joueur4_L_not0001,
      Q => joueur4_L(0)
    );
  joueur4_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_L_mux0006(1),
      G => joueur4_L_not0001,
      Q => joueur4_L(1)
    );
  joueur4_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_L_mux0006(2),
      G => joueur4_L_not0001,
      Q => joueur4_L(2)
    );
  joueur4_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => joueur4_L_mux0006(3),
      G => joueur4_L_not0001,
      Q => joueur4_L(3)
    );
  validation_or00001 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => etat_present_FSM_FFd1_45,
      I1 => etat_present_FSM_FFd2_47,
      O => validation_or0000
    );
  joueur1_L_not000111 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      O => N33
    );
  joueur4_R_not00011 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => joueur4_R(3),
      I1 => N40,
      I2 => N59,
      I3 => N33,
      O => joueur4_R_not0001
    );
  joueur1_L_not00011 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => joueur1_L(3),
      I1 => N39,
      I2 => N60,
      I3 => N33,
      O => joueur1_L_not0001
    );
  joueur1_R_not0001_SW0 : LUT4
    generic map(
      INIT => X"CC4C"
    )
    port map (
      I0 => joueur1_R(0),
      I1 => N58,
      I2 => joueur1_L(3),
      I3 => N60,
      O => N63
    );
  joueur1_R_not0001 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => joueur1_R(3),
      I1 => N57,
      I2 => N63,
      I3 => N33,
      O => joueur1_R_not0001_82
    );
  validation_mux00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => etat_present_FSM_FFd1_45,
      I1 => N35,
      O => validation_mux0000
    );
  joueur4_L_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N36,
      I1 => joueur4_L(0),
      I2 => joueur4_L(1),
      I3 => joueur4_L(3),
      O => joueur4_L_mux0006(1)
    );
  joueur3_L_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N37,
      I1 => joueur3_L(0),
      I2 => joueur3_L(1),
      I3 => joueur3_L(3),
      O => joueur3_L_mux0006(1)
    );
  joueur2_L_mux0005_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N38,
      I1 => joueur2_L(0),
      I2 => joueur2_L(1),
      I3 => joueur2_L(3),
      O => joueur2_L_mux0005(1)
    );
  joueur3_L_mux0006_0_1_SW0 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => id_joueur_0_IBUF_61,
      I1 => joueur3_R(3),
      I2 => joueur3_R(2),
      I3 => id_joueur_1_IBUF_62,
      O => N67
    );
  joueur3_L_mux0006_0_1 : LUT4
    generic map(
      INIT => X"0200"
    )
    port map (
      I0 => joueur3_R(0),
      I1 => joueur3_R(1),
      I2 => N67,
      I3 => N34,
      O => N37
    );
  joueur2_L_mux0005_0_1_SW0 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => joueur2_R(2),
      I1 => joueur2_R(3),
      I2 => id_joueur_1_IBUF_62,
      I3 => id_joueur_0_IBUF_61,
      O => N69
    );
  joueur2_L_mux0005_0_1 : LUT4
    generic map(
      INIT => X"0200"
    )
    port map (
      I0 => joueur2_R(0),
      I1 => joueur2_R(1),
      I2 => N69,
      I3 => N34,
      O => N38
    );
  etat_present_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => etat_present_FSM_FFd1_45,
      I1 => etat_present_FSM_FFd2_47,
      I2 => N111,
      O => etat_present_FSM_FFd1_In
    );
  etat_present_FSM_FFd2_In_SW0 : LUT3
    generic map(
      INIT => X"FB"
    )
    port map (
      I0 => evaluer_1_IBUF_58,
      I1 => evaluer_0_IBUF_57,
      I2 => validation_177,
      O => N71
    );
  etat_present_FSM_FFd2_In : LUT4
    generic map(
      INIT => X"8F80"
    )
    port map (
      I0 => N71,
      I1 => etat_present_FSM_FFd2_47,
      I2 => etat_present_FSM_FFd1_45,
      I3 => N35,
      O => etat_present_FSM_FFd2_In_48
    );
  joueur4_L_mux0006_0_2 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N36,
      I1 => joueur4_L(0),
      I2 => N115,
      O => joueur4_L_mux0006(0)
    );
  joueur3_L_mux0006_0_2 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N37,
      I1 => joueur3_L(0),
      I2 => N116,
      O => joueur3_L_mux0006(0)
    );
  joueur2_L_mux0005_0_2 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N38,
      I1 => joueur2_L(0),
      I2 => N118,
      O => joueur2_L_mux0005(0)
    );
  joueur4_R_mux0007_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N40,
      I1 => joueur4_R(2),
      I2 => joueur4_R(3),
      I3 => N87,
      O => joueur4_R_mux0007(2)
    );
  joueur4_L_mux0006_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N36,
      I1 => joueur4_L(2),
      I2 => joueur4_L(3),
      I3 => N89,
      O => joueur4_L_mux0006(2)
    );
  joueur3_R_mux0007_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N41,
      I1 => joueur3_R(2),
      I2 => joueur3_R(3),
      I3 => N91,
      O => joueur3_R_mux0007(2)
    );
  joueur3_L_mux0006_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N37,
      I1 => joueur3_L(2),
      I2 => joueur3_L(3),
      I3 => N93,
      O => joueur3_L_mux0006(2)
    );
  joueur2_R_mux0006_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N42,
      I1 => joueur2_R(2),
      I2 => joueur2_R(3),
      I3 => N95,
      O => joueur2_R_mux0006(2)
    );
  joueur2_L_mux0005_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N38,
      I1 => joueur2_L(2),
      I2 => joueur2_L(3),
      I3 => N97,
      O => joueur2_L_mux0005(2)
    );
  joueur1_R_mux0005_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N57,
      I1 => joueur1_R(2),
      I2 => joueur1_R(3),
      I3 => N99,
      O => joueur1_R_mux0005(2)
    );
  joueur3_R_mux0007_0_1 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N41,
      I1 => joueur3_R(0),
      I2 => N117,
      O => joueur3_R_mux0007(0)
    );
  joueur2_R_mux0006_0_1 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N42,
      I1 => joueur2_R(0),
      I2 => N119,
      O => joueur2_R_mux0006(0)
    );
  joueur4_R_mux0007_0_21 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N01,
      I1 => id_joueur_0_IBUF_61,
      I2 => id_joueur_1_IBUF_62,
      I3 => N34,
      O => N40
    );
  joueur4_R_mux0007_0_1 : LUT4
    generic map(
      INIT => X"2202"
    )
    port map (
      I0 => N40,
      I1 => joueur4_R(0),
      I2 => joueur4_R(3),
      I3 => N112,
      O => joueur4_R_mux0007(0)
    );
  joueur3_R_mux0007_0_21 : LUT4
    generic map(
      INIT => X"4000"
    )
    port map (
      I0 => id_joueur_0_IBUF_61,
      I1 => N11,
      I2 => id_joueur_1_IBUF_62,
      I3 => N34,
      O => N41
    );
  joueur2_R_mux0006_0_21 : LUT4
    generic map(
      INIT => X"4000"
    )
    port map (
      I0 => id_joueur_1_IBUF_62,
      I1 => id_joueur_0_IBUF_61,
      I2 => N2,
      I3 => N34,
      O => N42
    );
  joueur1_R_mux0005_0_1 : LUT4
    generic map(
      INIT => X"0A02"
    )
    port map (
      I0 => N57,
      I1 => joueur1_R(3),
      I2 => joueur1_R(0),
      I3 => N113,
      O => joueur1_R_mux0005(0)
    );
  joueur4_R_mux0007_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N40,
      I1 => joueur4_R(1),
      I2 => joueur4_R(0),
      I3 => joueur4_R(3),
      O => joueur4_R_mux0007(1)
    );
  joueur3_R_mux0007_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N41,
      I1 => joueur3_R(1),
      I2 => joueur3_R(0),
      I3 => joueur3_R(3),
      O => joueur3_R_mux0007(1)
    );
  joueur2_R_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N42,
      I1 => joueur2_R(1),
      I2 => joueur2_R(0),
      I3 => joueur2_R(3),
      O => joueur2_R_mux0006(1)
    );
  joueur1_R_mux0005_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N57,
      I1 => joueur1_R(1),
      I2 => joueur1_R(0),
      I3 => joueur1_R(3),
      O => joueur1_R_mux0005(1)
    );
  joueur1_L_mux0004_2_Q : LUT4
    generic map(
      INIT => X"0802"
    )
    port map (
      I0 => N39,
      I1 => joueur1_L(2),
      I2 => joueur1_L(3),
      I3 => N101,
      O => joueur1_L_mux0004(2)
    );
  joueur1_L_mux0004_0_311 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => N34,
      I1 => id_joueur_0_IBUF_61,
      I2 => id_joueur_1_IBUF_62,
      O => N57
    );
  joueur1_L_mux0004_0_31 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => joueur1_R(0),
      I1 => joueur1_R(3),
      I2 => N57,
      I3 => N58,
      O => N39
    );
  joueur1_L_mux0004_0_1 : LUT4
    generic map(
      INIT => X"2202"
    )
    port map (
      I0 => N39,
      I1 => joueur1_L(0),
      I2 => joueur1_L(3),
      I3 => N114,
      O => joueur1_L_mux0004(0)
    );
  joueur1_L_mux0004_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => N39,
      I1 => joueur1_L(1),
      I2 => joueur1_L(0),
      I3 => joueur1_L(3),
      O => joueur1_L_mux0004(1)
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_144
    );
  etat_system_2_IBUF : IBUF
    port map (
      I => etat_system(2),
      O => etat_system_2_IBUF_54
    );
  etat_system_1_IBUF : IBUF
    port map (
      I => etat_system(1),
      O => etat_system_1_IBUF_53
    );
  etat_system_0_IBUF : IBUF
    port map (
      I => etat_system(0),
      O => etat_system_0_IBUF_52
    );
  evaluer_1_IBUF : IBUF
    port map (
      I => evaluer(1),
      O => evaluer_1_IBUF_58
    );
  evaluer_0_IBUF : IBUF
    port map (
      I => evaluer(0),
      O => evaluer_0_IBUF_57
    );
  id_joueur_1_IBUF : IBUF
    port map (
      I => id_joueur(1),
      O => id_joueur_1_IBUF_62
    );
  id_joueur_0_IBUF : IBUF
    port map (
      I => id_joueur(0),
      O => id_joueur_0_IBUF_61
    );
  score_j1_7_OBUF : OBUF
    port map (
      I => joueur1_L(3),
      O => score_j1(7)
    );
  score_j1_6_OBUF : OBUF
    port map (
      I => joueur1_L(2),
      O => score_j1(6)
    );
  score_j1_5_OBUF : OBUF
    port map (
      I => joueur1_L(1),
      O => score_j1(5)
    );
  score_j1_4_OBUF : OBUF
    port map (
      I => joueur1_L(0),
      O => score_j1(4)
    );
  score_j1_3_OBUF : OBUF
    port map (
      I => joueur1_R(3),
      O => score_j1(3)
    );
  score_j1_2_OBUF : OBUF
    port map (
      I => joueur1_R(2),
      O => score_j1(2)
    );
  score_j1_1_OBUF : OBUF
    port map (
      I => joueur1_R(1),
      O => score_j1(1)
    );
  score_j1_0_OBUF : OBUF
    port map (
      I => joueur1_R(0),
      O => score_j1(0)
    );
  score_j2_7_OBUF : OBUF
    port map (
      I => joueur2_L(3),
      O => score_j2(7)
    );
  score_j2_6_OBUF : OBUF
    port map (
      I => joueur2_L(2),
      O => score_j2(6)
    );
  score_j2_5_OBUF : OBUF
    port map (
      I => joueur2_L(1),
      O => score_j2(5)
    );
  score_j2_4_OBUF : OBUF
    port map (
      I => joueur2_L(0),
      O => score_j2(4)
    );
  score_j2_3_OBUF : OBUF
    port map (
      I => joueur2_R(3),
      O => score_j2(3)
    );
  score_j2_2_OBUF : OBUF
    port map (
      I => joueur2_R(2),
      O => score_j2(2)
    );
  score_j2_1_OBUF : OBUF
    port map (
      I => joueur2_R(1),
      O => score_j2(1)
    );
  score_j2_0_OBUF : OBUF
    port map (
      I => joueur2_R(0),
      O => score_j2(0)
    );
  score_j3_7_OBUF : OBUF
    port map (
      I => joueur3_L(3),
      O => score_j3(7)
    );
  score_j3_6_OBUF : OBUF
    port map (
      I => joueur3_L(2),
      O => score_j3(6)
    );
  score_j3_5_OBUF : OBUF
    port map (
      I => joueur3_L(1),
      O => score_j3(5)
    );
  score_j3_4_OBUF : OBUF
    port map (
      I => joueur3_L(0),
      O => score_j3(4)
    );
  score_j3_3_OBUF : OBUF
    port map (
      I => joueur3_R(3),
      O => score_j3(3)
    );
  score_j3_2_OBUF : OBUF
    port map (
      I => joueur3_R(2),
      O => score_j3(2)
    );
  score_j3_1_OBUF : OBUF
    port map (
      I => joueur3_R(1),
      O => score_j3(1)
    );
  score_j3_0_OBUF : OBUF
    port map (
      I => joueur3_R(0),
      O => score_j3(0)
    );
  score_j4_7_OBUF : OBUF
    port map (
      I => joueur4_L(3),
      O => score_j4(7)
    );
  score_j4_6_OBUF : OBUF
    port map (
      I => joueur4_L(2),
      O => score_j4(6)
    );
  score_j4_5_OBUF : OBUF
    port map (
      I => joueur4_L(1),
      O => score_j4(5)
    );
  score_j4_4_OBUF : OBUF
    port map (
      I => joueur4_L(0),
      O => score_j4(4)
    );
  score_j4_3_OBUF : OBUF
    port map (
      I => joueur4_R(3),
      O => score_j4(3)
    );
  score_j4_2_OBUF : OBUF
    port map (
      I => joueur4_R(2),
      O => score_j4(2)
    );
  score_j4_1_OBUF : OBUF
    port map (
      I => joueur4_R(1),
      O => score_j4(1)
    );
  score_j4_0_OBUF : OBUF
    port map (
      I => joueur4_R(0),
      O => score_j4(0)
    );
  joueur1_R_mux0005_3_Q : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => id_joueur_0_IBUF_61,
      I1 => N34,
      I2 => id_joueur_1_IBUF_62,
      I3 => N85,
      O => joueur1_R_mux0005(3)
    );
  joueur4_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      I2 => N36,
      I3 => N01,
      O => joueur4_L_not0001
    );
  joueur3_R_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      I2 => N41,
      I3 => N23,
      O => joueur3_R_not0001
    );
  joueur3_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      I2 => N37,
      I3 => N11,
      O => joueur3_L_not0001
    );
  joueur2_R_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      I2 => N42,
      I3 => N21,
      O => joueur2_R_not0001
    );
  joueur2_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_47,
      I1 => etat_present_FSM_FFd1_45,
      I2 => N38,
      I3 => N2,
      O => joueur2_L_not0001
    );
  joueur4_L_mux0006_0_1_SW1 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => joueur4_R(1),
      I1 => joueur4_R(0),
      I2 => joueur4_R(2),
      I3 => joueur4_R(3),
      O => N107
    );
  joueur4_L_mux0006_0_1 : LUT4
    generic map(
      INIT => X"4000"
    )
    port map (
      I0 => N107,
      I1 => id_joueur_0_IBUF_61,
      I2 => id_joueur_1_IBUF_62,
      I3 => N34,
      O => N36
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_44
    );
  XST_GND : GND
    port map (
      G => N110
    );
  joueur4_L_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur4_L(3),
      I1 => joueur4_L(2),
      I2 => joueur4_L(1),
      I3 => joueur4_L(0),
      O => joueur4_L_mux0006_3_1_131
    );
  joueur4_L_mux0006_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur4_L_mux0006_3_1_131,
      S => N36,
      O => joueur4_L_mux0006(3)
    );
  joueur3_L_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur3_L(3),
      I1 => joueur3_L(2),
      I2 => joueur3_L(1),
      I3 => joueur3_L(0),
      O => joueur3_L_mux0006_3_1_111
    );
  joueur3_L_mux0006_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur3_L_mux0006_3_1_111,
      S => N37,
      O => joueur3_L_mux0006(3)
    );
  joueur2_L_mux0005_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur2_L(3),
      I1 => joueur2_L(2),
      I2 => joueur2_L(1),
      I3 => joueur2_L(0),
      O => joueur2_L_mux0005_3_1_91
    );
  joueur2_L_mux0005_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur2_L_mux0005_3_1_91,
      S => N38,
      O => joueur2_L_mux0005(3)
    );
  joueur4_R_mux0007_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur4_R(3),
      I1 => joueur4_R(0),
      I2 => joueur4_R(1),
      I3 => joueur4_R(2),
      O => joueur4_R_mux0007_3_1_141
    );
  joueur4_R_mux0007_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur4_R_mux0007_3_1_141,
      S => N40,
      O => joueur4_R_mux0007(3)
    );
  joueur3_R_mux0007_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur3_R(3),
      I1 => joueur3_R(0),
      I2 => joueur3_R(1),
      I3 => joueur3_R(2),
      O => joueur3_R_mux0007_3_1_121
    );
  joueur3_R_mux0007_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur3_R_mux0007_3_1_121,
      S => N41,
      O => joueur3_R_mux0007(3)
    );
  joueur2_R_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur2_R(3),
      I1 => joueur2_R(0),
      I2 => joueur2_R(1),
      I3 => joueur2_R(2),
      O => joueur2_R_mux0006_3_1_101
    );
  joueur2_R_mux0006_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur2_R_mux0006_3_1_101,
      S => N42,
      O => joueur2_R_mux0006(3)
    );
  joueur1_L_mux0004_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => joueur1_L(3),
      I1 => joueur1_L(0),
      I2 => joueur1_L(1),
      I3 => joueur1_L(2),
      O => joueur1_L_mux0004_3_1_72
    );
  joueur1_L_mux0004_3_f5 : MUXF5
    port map (
      I0 => N110,
      I1 => joueur1_L_mux0004_3_1_72,
      S => N39,
      O => joueur1_L_mux0004(3)
    );
  joueur1_L_mux0004_0_21 : LUT4
    generic map(
      INIT => X"0200"
    )
    port map (
      I0 => etat_present_FSM_FFd1_45,
      I1 => etat_present_FSM_FFd2_47,
      I2 => evaluer_1_IBUF_58,
      I3 => evaluer_0_IBUF_57,
      O => joueur1_L_mux0004_0_2
    );
  joueur1_L_mux0004_0_2_f5 : MUXF5
    port map (
      I0 => joueur1_L_mux0004_0_2,
      I1 => N110,
      S => validation_177,
      O => N34
    );
  etat_present_FSM_FFd2_In11 : LUT4_D
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => etat_system_1_IBUF_53,
      I1 => etat_system_0_IBUF_52,
      I2 => etat_system_2_IBUF_54,
      I3 => etat_present_FSM_FFd2_47,
      LO => N111,
      O => N35
    );
  joueur4_R_mux0007_3_111 : LUT2_D
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => joueur4_R(2),
      I1 => joueur4_R(1),
      LO => N112,
      O => N59
    );
  joueur1_R_mux0005_3_111 : LUT2_D
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => joueur1_R(2),
      I1 => joueur1_R(1),
      LO => N113,
      O => N58
    );
  joueur1_L_mux0004_3_111 : LUT2_D
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => joueur1_L(2),
      I1 => joueur1_L(1),
      LO => N114,
      O => N60
    );
  joueur4_R_mux0007_2_11 : LUT3_D
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => joueur4_L(2),
      I1 => joueur4_L(1),
      I2 => joueur4_L(3),
      LO => N115,
      O => N01
    );
  joueur3_R_mux0007_2_11 : LUT3_D
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => joueur3_L(2),
      I1 => joueur3_L(1),
      I2 => joueur3_L(3),
      LO => N116,
      O => N11
    );
  joueur3_R_mux0007_0_11 : LUT3_D
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => joueur3_R(2),
      I1 => joueur3_R(1),
      I2 => joueur3_R(3),
      LO => N117,
      O => N23
    );
  joueur2_R_mux0006_2_11 : LUT3_D
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => joueur2_L(2),
      I1 => joueur2_L(1),
      I2 => joueur2_L(3),
      LO => N118,
      O => N2
    );
  joueur2_R_mux0006_0_11 : LUT3_D
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => joueur2_R(2),
      I1 => joueur2_R(1),
      I2 => joueur2_R(3),
      LO => N119,
      O => N21
    );
  joueur1_R_mux0005_3_SW0 : LUT4_L
    generic map(
      INIT => X"FE7F"
    )
    port map (
      I0 => joueur1_R(2),
      I1 => joueur1_R(1),
      I2 => joueur1_R(0),
      I3 => joueur1_R(3),
      LO => N85
    );
  joueur4_R_mux0007_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur4_R(1),
      I1 => joueur4_R(0),
      LO => N87
    );
  joueur4_L_mux0006_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur4_L(1),
      I1 => joueur4_L(0),
      LO => N89
    );
  joueur3_R_mux0007_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur3_R(1),
      I1 => joueur3_R(0),
      LO => N91
    );
  joueur3_L_mux0006_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur3_L(1),
      I1 => joueur3_L(0),
      LO => N93
    );
  joueur2_R_mux0006_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur2_R(1),
      I1 => joueur2_R(0),
      LO => N95
    );
  joueur2_L_mux0005_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur2_L(1),
      I1 => joueur2_L(0),
      LO => N97
    );
  joueur1_R_mux0005_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur1_R(1),
      I1 => joueur1_R(0),
      LO => N99
    );
  joueur1_L_mux0004_2_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => joueur1_L(1),
      I1 => joueur1_L(0),
      LO => N101
    );

end Structure;

