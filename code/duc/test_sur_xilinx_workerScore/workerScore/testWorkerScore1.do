force clk 0 0, 1 1 -repeat 2
# une séquence de 2
force rst 1 0, 0 5
# valeur 1  au temps zér, valeur 0 au temps 5

# premier point pour joueur1_R
force etat_system 110 0
force id_joueur 00 5
force evaluer  01 10

#changement de joueur2_R
force etat_system 000 20
force evaluer  00 25
force id_joueur 00 30
force id_joueur 01 35

force etat_system 110 40
force etat_system 000 50
force evaluer  01 40

force etat_system 110 60
force evaluer  01 70

force etat_system 000 80

# fin de test
# en expectant score joueur1_R = 1, joueur2_R = 2
run 300
