-- commentaire
LIBRARY ieee;                -- bibliotheque a charger
USE ieee.std_logic_1164.all; -- utilisation des definitions std_logic
USE ieee.numeric_std.all;

ENTITY workerScore IS  -- calculer le score et le convertir en BCD
   PORT(
     -- declaration des entrées
     clk          : in std_logic;
     rst          : in std_logic;
     etat_system  : in std_logic_vector(2 downto 0);
     evaluer      : in std_logic_vector(1 downto 0);
     id_joueur    : in std_logic_vector(1 downto 0);

     -- declaration des sorties
     score_j1   : out std_logic_vector(7 downto 0);
     score_j2    : out std_logic_vector(7 downto 0);
     score_j3   : out std_logic_vector(7 downto 0);
     score_j4   : out std_logic_vector(7 downto 0)
   );
END ENTITY workerScore;

ARCHITECTURE score_arch OF workerScore IS
 -- section declarative de signaux

    -- une mini machine d'état,
    -- les état par l'instruction state
    type etats IS (IDLE, get_Value, add_Value, RESET);
    signal etat_present, etat_futur : etats;

    signal joueur1_R, joueur1_L, joueur2_R, joueur2_L, joueur3_R, joueur3_L, joueur4_R, joueur4_L : unsigned(3 downto 0) := "0000";
    signal bus_evaluer : std_logic_vector(1 downto 0) :="11";
    signal validation : std_logic:= '0';


BEGIN
    -- corps de l’architecture
    etatSystem: process (clk, rst)
    BEGIN
        if rst = '1' then    -- si reset
            etat_present <= RESET;   -- passe la machine en état IDLE
        elsif (rising_edge(clk)) then
            etat_present <= etat_futur;  -- a chaque coup d'horloge passe au prochain état
          --  sequence_color_present <= sequence_color_futur;
        end if;
    end process etatSystem;

    evaluer_Value : process(evaluer, validation)
    begin

      if (validation = '1' ) then
        bus_evaluer <= "11";
      elsif (evaluer = "01") then
        bus_evaluer <= "01";
      else
        bus_evaluer <= "11";
      end if;

    end process;

    -- corps de l’architecture
    etat_combi : process (etat_system, bus_evaluer, etat_present)
    BEGIN
        case( etat_present ) is
            when RESET =>
                etat_futur <= IDLE;
                validation <= '0';
            when IDLE =>
                if (etat_system = "110") then
                    etat_futur <= get_Value;
                else
                    etat_futur <= IDLE;

                end if;
            when get_Value =>
                validation <= '0';
                if (bus_evaluer = "01") then -- séquence réussie
                    etat_futur <= add_Value;
                else
                    etat_futur <= get_Value;
                end if;
            when add_Value =>

                if (etat_system = "110") then
                    etat_futur <= add_Value;
                    validation <= '1';
                else
                    etat_futur <= IDLE;
                    validation <= '0';
                end if;
            when others => etat_futur <= IDLE;
        end case ;
    end process etat_combi;


    compteur : process( etat_present, id_joueur)
    begin
        case( etat_present ) is
            when RESET =>
                    joueur1_R <= (others => '0'); joueur1_L <= (others => '0');
                    joueur2_R <= (others => '0'); joueur2_L <= (others => '0');
                    joueur3_R <= (others => '0'); joueur3_L <= (others => '0');
                    joueur4_R <= (others => '0'); joueur4_L <= (others => '0');
            when IDLE =>
                    joueur1_R <= joueur1_R; joueur1_L <= joueur1_L;
                    joueur2_R <= joueur2_R; joueur2_L <= joueur2_L;
                    joueur3_R <= joueur3_R; joueur3_L <= joueur3_L;
                    joueur4_R <= joueur4_R; joueur4_L <= joueur4_L;
            when get_Value =>
                    joueur1_R <= joueur1_R; joueur1_L <= joueur1_L;
                    joueur2_R <= joueur2_R; joueur2_L <= joueur2_L;
                    joueur3_R <= joueur3_R; joueur3_L <= joueur3_L;
                    joueur4_R <= joueur4_R; joueur4_L <= joueur4_L;
            when add_Value =>
                    if (bus_evaluer = "01" ) then
                      if (id_joueur = "00") then
                          if (( joueur1_L = "1001" ) and (joueur1_R = "1001")) then                   -- si la valeur dizaine est à 9
                              joueur1_L <= (others => '0');                                           -- et si la valeur unitée est à 9
                              joueur1_R <= (others => '0');                                           -- ça remet à tous '0'
                          elsif (( joueur1_L = "1001" ) and (joueur1_R < "1001")) then                -- sinon, si la valeur unité < 9, on incrémente l'incérmente de 1pts
                              joueur1_R <= joueur1_R + "1" ;
                          elsif (( joueur1_L < "1001" ) and (joueur1_R = "1001"))  then               -- sinon, la valeur dizaine < 9
                              joueur1_L <= joueur1_L + "1" ;                                          -- si la valeur unité est à 9
                              joueur1_R <= (others => '0');                                           -- on incrémente la valeur dizaine de 1 pts, et on remets la valeur uniée à 0
                          elsif ((joueur1_R < "1001") and (joueur1_R < "1001")) then                   -- si la valeur unité < 9
                              joueur1_R <= joueur1_R + "1" ;                                       -- on l'incrémente d'un pts
                          end if;

                      elsif (id_joueur = "01") then
                          if (( joueur2_L = "1001" ) and (joueur2_R = "1001") )then
                              joueur2_L <= (others => '0');
                              joueur2_R <= (others => '0');
                          elsif (( joueur2_L = "1001" ) and (joueur2_R < "1001")) then
                              joueur2_R <= joueur2_R + "1" ;
                          elsif (( joueur2_L < "1001" ) and (joueur2_R = "1001")) then
                              joueur2_L <= joueur2_L + "1" ;
                              joueur2_R <= (others => '0');
                          elsif (( joueur2_L < "1001" ) and  (joueur2_R < "1001")) then
                              joueur2_R <= joueur2_R + "1" ;
                          end if;
                      elsif (id_joueur = "10") then
                          if (( joueur3_L = "1001" ) and (joueur3_R = "1001")) then
                              joueur3_L <= (others => '0');
                              joueur3_R <= (others => '0');
                          elsif (( joueur3_L = "1001" ) and (joueur3_R < "1001")) then
                              joueur3_R <= joueur3_R + "1" ;
                          elsif (( joueur3_L < "1001" ) and (joueur3_R = "1001")) then
                              joueur3_L <= joueur3_L + "1" ;
                              joueur3_R <= (others => '0');
                          elsif (( joueur3_L < "1001" ) and(joueur3_R < "1001")) then
                                  joueur3_R <= joueur3_R + "1" ;
                          end if;
                      else
                          if (( joueur4_L = "1001" )  and (joueur4_R = "1001")) then
                              joueur4_L <= (others => '0');
                              joueur4_R <= (others => '0');
                          elsif (( joueur4_L = "1001" )  and(joueur4_R < "1001")) then
                              joueur4_R <= joueur4_R + "1" ;
                          elsif ((joueur4_L < "1001" ) and (joueur4_R = "1001")) then
                              joueur4_L <= joueur4_L + "1" ;
                              joueur4_R <= (others => '0');
                          elsif ((joueur4_L < "1001" ) and (joueur4_R < "1001")) then
                              joueur4_R <= joueur4_R + "1" ;
                          end if;
                      end if;
                    end if;

            when others =>
                    joueur1_R <= joueur1_R; joueur1_L <= joueur1_L;
                    joueur2_R <= joueur2_R; joueur2_L <= joueur2_L;
                    joueur3_R <= joueur3_R; joueur3_L <= joueur3_L;
                    joueur4_R <= joueur4_R; joueur4_L <= joueur4_L;

        end case ;
    end process ; -- compteur
    -- ensemble d’instructions concurrentes

    score_j1  <=  std_logic_vector(joueur1_L) & std_logic_vector(joueur1_R) ;  --con. à la var. de sortie avec conversion de type
    score_j2  <=  std_logic_vector(joueur2_L) & std_logic_vector(joueur2_R) ;
    score_j3  <=  std_logic_vector(joueur3_L) & std_logic_vector(joueur3_R) ;
    score_j4  <=  std_logic_vector(joueur4_L) & std_logic_vector(joueur4_R) ;

END ARCHITECTURE score_arch;

-- réalisation par Duc Huy Nguyen 
-- ISC-1B en 2020/2021