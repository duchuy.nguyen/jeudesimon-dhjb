--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: M.81d
--  \   \         Application: netgen
--  /   /         Filename: storage_synthesis.vhd
-- /___/   /\     Timestamp: Sun Jun 13 15:19:58 2021
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm storage -w -dir netgen/synthesis -ofmt vhdl -sim storage.ngc storage_synthesis.vhd 
-- Device	: xc3s50a-5-tq144
-- Input file	: storage.ngc
-- Output file	: C:\Users\eia\Documents\duchuy-vhdl\storage\netgen\synthesis\storage_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: storage
-- Xilinx	: C:\Xilinx\12.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity storage is
  port (
    clk : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    compteur_couleur : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    sequence_complete : out STD_LOGIC_VECTOR ( 59 downto 0 ); 
    couleur_aleatoire : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    etat_system : in STD_LOGIC_VECTOR ( 2 downto 0 ); 
    couleur_jouer : in STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end storage;

architecture Structure of storage is
  signal N0 : STD_LOGIC; 
  signal N11 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N13 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N15 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal N17 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal N20 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N26 : STD_LOGIC; 
  signal N30 : STD_LOGIC; 
  signal N31 : STD_LOGIC; 
  signal N32 : STD_LOGIC; 
  signal N4 : STD_LOGIC; 
  signal N48 : STD_LOGIC; 
  signal N5 : STD_LOGIC; 
  signal N55 : STD_LOGIC; 
  signal N56 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N62 : STD_LOGIC; 
  signal N66 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal N73 : STD_LOGIC; 
  signal N74 : STD_LOGIC; 
  signal N78 : STD_LOGIC; 
  signal N82 : STD_LOGIC; 
  signal N83 : STD_LOGIC; 
  signal N84 : STD_LOGIC; 
  signal N85 : STD_LOGIC; 
  signal N86 : STD_LOGIC; 
  signal N87 : STD_LOGIC; 
  signal N88 : STD_LOGIC; 
  signal N89 : STD_LOGIC; 
  signal bus_color_futur_0_1_38 : STD_LOGIC; 
  signal bus_color_futur_cmp_eq0004 : STD_LOGIC; 
  signal bus_color_futur_cmp_eq0005 : STD_LOGIC; 
  signal bus_color_futur_or000011_42 : STD_LOGIC; 
  signal bus_color_futur_or000023_43 : STD_LOGIC; 
  signal bus_color_futur_or0001 : STD_LOGIC; 
  signal bus_color_futur_or000111_45 : STD_LOGIC; 
  signal bus_color_futur_or000123_46 : STD_LOGIC; 
  signal clk_BUFGP_50 : STD_LOGIC; 
  signal compteur_1_1_52 : STD_LOGIC; 
  signal compteur_2_1_54 : STD_LOGIC; 
  signal compteur_mux0003_1_112_69 : STD_LOGIC; 
  signal compteur_mux0003_1_127_70 : STD_LOGIC; 
  signal compteur_not0001 : STD_LOGIC; 
  signal couleur_aleatoire_0_IBUF_82 : STD_LOGIC; 
  signal couleur_aleatoire_1_IBUF_83 : STD_LOGIC; 
  signal couleur_aleatoire_2_IBUF_84 : STD_LOGIC; 
  signal couleur_aleatoire_3_IBUF_85 : STD_LOGIC; 
  signal couleur_jouer_0_IBUF_90 : STD_LOGIC; 
  signal couleur_jouer_1_IBUF_91 : STD_LOGIC; 
  signal couleur_jouer_2_IBUF_92 : STD_LOGIC; 
  signal couleur_jouer_3_IBUF_93 : STD_LOGIC; 
  signal etat_present_FSM_FFd1_94 : STD_LOGIC; 
  signal etat_present_FSM_FFd1_In : STD_LOGIC; 
  signal etat_present_FSM_FFd2_96 : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In22_98 : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In38_99 : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In55 : STD_LOGIC; 
  signal etat_present_FSM_FFd2_In64_101 : STD_LOGIC; 
  signal etat_present_cmp_eq0000 : STD_LOGIC; 
  signal etat_present_cmp_eq0003 : STD_LOGIC; 
  signal etat_system_0_IBUF_107 : STD_LOGIC; 
  signal etat_system_1_IBUF_108 : STD_LOGIC; 
  signal etat_system_2_IBUF_109 : STD_LOGIC; 
  signal reset_bus_color_110 : STD_LOGIC; 
  signal reset_bus_color_not0001 : STD_LOGIC; 
  signal rst_IBUF_113 : STD_LOGIC; 
  signal sequence_complete_0_174 : STD_LOGIC; 
  signal sequence_complete_0_cmp_eq0000 : STD_LOGIC; 
  signal sequence_complete_0_mux0003 : STD_LOGIC; 
  signal sequence_complete_0_not0001 : STD_LOGIC; 
  signal sequence_complete_1_178 : STD_LOGIC; 
  signal sequence_complete_10_179 : STD_LOGIC; 
  signal sequence_complete_10_mux0003 : STD_LOGIC; 
  signal sequence_complete_10_not0001 : STD_LOGIC; 
  signal sequence_complete_11_182 : STD_LOGIC; 
  signal sequence_complete_11_mux0003 : STD_LOGIC; 
  signal sequence_complete_12_184 : STD_LOGIC; 
  signal sequence_complete_12_mux0003 : STD_LOGIC; 
  signal sequence_complete_12_not0001 : STD_LOGIC; 
  signal sequence_complete_13_187 : STD_LOGIC; 
  signal sequence_complete_13_mux0003 : STD_LOGIC; 
  signal sequence_complete_13_not0001 : STD_LOGIC; 
  signal sequence_complete_14_190 : STD_LOGIC; 
  signal sequence_complete_14_mux0003 : STD_LOGIC; 
  signal sequence_complete_14_not0001 : STD_LOGIC; 
  signal sequence_complete_15_193 : STD_LOGIC; 
  signal sequence_complete_15_mux0003 : STD_LOGIC; 
  signal sequence_complete_16_195 : STD_LOGIC; 
  signal sequence_complete_16_mux0003 : STD_LOGIC; 
  signal sequence_complete_16_not0001 : STD_LOGIC; 
  signal sequence_complete_16_or0000 : STD_LOGIC; 
  signal sequence_complete_17_199 : STD_LOGIC; 
  signal sequence_complete_17_mux0003 : STD_LOGIC; 
  signal sequence_complete_17_not0001 : STD_LOGIC; 
  signal sequence_complete_18_202 : STD_LOGIC; 
  signal sequence_complete_18_mux0003 : STD_LOGIC; 
  signal sequence_complete_18_not0001 : STD_LOGIC; 
  signal sequence_complete_19_205 : STD_LOGIC; 
  signal sequence_complete_19_mux0003 : STD_LOGIC; 
  signal sequence_complete_1_mux0003 : STD_LOGIC; 
  signal sequence_complete_1_not0001 : STD_LOGIC; 
  signal sequence_complete_2_209 : STD_LOGIC; 
  signal sequence_complete_20_210 : STD_LOGIC; 
  signal sequence_complete_20_mux0003 : STD_LOGIC; 
  signal sequence_complete_20_not0001 : STD_LOGIC; 
  signal sequence_complete_21_213 : STD_LOGIC; 
  signal sequence_complete_21_mux0003 : STD_LOGIC; 
  signal sequence_complete_21_not0001 : STD_LOGIC; 
  signal sequence_complete_22_216 : STD_LOGIC; 
  signal sequence_complete_22_mux0003 : STD_LOGIC; 
  signal sequence_complete_22_not0001 : STD_LOGIC; 
  signal sequence_complete_23_219 : STD_LOGIC; 
  signal sequence_complete_23_mux0003 : STD_LOGIC; 
  signal sequence_complete_24_221 : STD_LOGIC; 
  signal sequence_complete_24_mux0003 : STD_LOGIC; 
  signal sequence_complete_24_not0001 : STD_LOGIC; 
  signal sequence_complete_25_224 : STD_LOGIC; 
  signal sequence_complete_25_mux0003 : STD_LOGIC; 
  signal sequence_complete_25_not0001 : STD_LOGIC; 
  signal sequence_complete_26_227 : STD_LOGIC; 
  signal sequence_complete_26_mux0003 : STD_LOGIC; 
  signal sequence_complete_26_not0001 : STD_LOGIC; 
  signal sequence_complete_27_230 : STD_LOGIC; 
  signal sequence_complete_27_mux0003 : STD_LOGIC; 
  signal sequence_complete_28_232 : STD_LOGIC; 
  signal sequence_complete_28_mux0003 : STD_LOGIC; 
  signal sequence_complete_28_not0001 : STD_LOGIC; 
  signal sequence_complete_29_235 : STD_LOGIC; 
  signal sequence_complete_29_mux0003 : STD_LOGIC; 
  signal sequence_complete_29_not0001 : STD_LOGIC; 
  signal sequence_complete_2_mux0003 : STD_LOGIC; 
  signal sequence_complete_2_not0001 : STD_LOGIC; 
  signal sequence_complete_3_240 : STD_LOGIC; 
  signal sequence_complete_30_241 : STD_LOGIC; 
  signal sequence_complete_30_mux0003 : STD_LOGIC; 
  signal sequence_complete_30_not0001 : STD_LOGIC; 
  signal sequence_complete_31_244 : STD_LOGIC; 
  signal sequence_complete_31_mux0003 : STD_LOGIC; 
  signal sequence_complete_32_246 : STD_LOGIC; 
  signal sequence_complete_32_mux0003 : STD_LOGIC; 
  signal sequence_complete_32_not0001 : STD_LOGIC; 
  signal sequence_complete_33_249 : STD_LOGIC; 
  signal sequence_complete_33_mux0003 : STD_LOGIC; 
  signal sequence_complete_34_251 : STD_LOGIC; 
  signal sequence_complete_34_mux0003 : STD_LOGIC; 
  signal sequence_complete_34_not0001 : STD_LOGIC; 
  signal sequence_complete_35_254 : STD_LOGIC; 
  signal sequence_complete_35_mux0003 : STD_LOGIC; 
  signal sequence_complete_36_256 : STD_LOGIC; 
  signal sequence_complete_36_mux0003 : STD_LOGIC; 
  signal sequence_complete_36_not0001 : STD_LOGIC; 
  signal sequence_complete_36_or0000 : STD_LOGIC; 
  signal sequence_complete_37_260 : STD_LOGIC; 
  signal sequence_complete_37_mux0003 : STD_LOGIC; 
  signal sequence_complete_37_not0001 : STD_LOGIC; 
  signal sequence_complete_38_263 : STD_LOGIC; 
  signal sequence_complete_38_mux0003 : STD_LOGIC; 
  signal sequence_complete_38_not0001 : STD_LOGIC; 
  signal sequence_complete_39_266 : STD_LOGIC; 
  signal sequence_complete_39_mux0003 : STD_LOGIC; 
  signal sequence_complete_3_mux0003 : STD_LOGIC; 
  signal sequence_complete_4_269 : STD_LOGIC; 
  signal sequence_complete_40_270 : STD_LOGIC; 
  signal sequence_complete_40_cmp_eq0001 : STD_LOGIC; 
  signal sequence_complete_40_mux0003 : STD_LOGIC; 
  signal sequence_complete_40_not0001 : STD_LOGIC; 
  signal sequence_complete_40_or0000 : STD_LOGIC; 
  signal sequence_complete_41_275 : STD_LOGIC; 
  signal sequence_complete_41_mux0003 : STD_LOGIC; 
  signal sequence_complete_41_not0001 : STD_LOGIC; 
  signal sequence_complete_42_278 : STD_LOGIC; 
  signal sequence_complete_42_mux0003 : STD_LOGIC; 
  signal sequence_complete_42_not0001 : STD_LOGIC; 
  signal sequence_complete_43_281 : STD_LOGIC; 
  signal sequence_complete_43_mux0003 : STD_LOGIC; 
  signal sequence_complete_44_283 : STD_LOGIC; 
  signal sequence_complete_44_mux0003 : STD_LOGIC; 
  signal sequence_complete_44_not0001 : STD_LOGIC; 
  signal sequence_complete_45_286 : STD_LOGIC; 
  signal sequence_complete_45_mux0003 : STD_LOGIC; 
  signal sequence_complete_45_not0001 : STD_LOGIC; 
  signal sequence_complete_46_289 : STD_LOGIC; 
  signal sequence_complete_46_mux0003 : STD_LOGIC; 
  signal sequence_complete_46_not0001 : STD_LOGIC; 
  signal sequence_complete_47_292 : STD_LOGIC; 
  signal sequence_complete_47_mux0003 : STD_LOGIC; 
  signal sequence_complete_48_294 : STD_LOGIC; 
  signal sequence_complete_48_mux0003 : STD_LOGIC; 
  signal sequence_complete_48_not0001 : STD_LOGIC; 
  signal sequence_complete_49_297 : STD_LOGIC; 
  signal sequence_complete_49_mux0003 : STD_LOGIC; 
  signal sequence_complete_49_not0001 : STD_LOGIC; 
  signal sequence_complete_4_mux0003 : STD_LOGIC; 
  signal sequence_complete_4_not0001 : STD_LOGIC; 
  signal sequence_complete_5_302 : STD_LOGIC; 
  signal sequence_complete_50_303 : STD_LOGIC; 
  signal sequence_complete_50_mux0003 : STD_LOGIC; 
  signal sequence_complete_50_not0001 : STD_LOGIC; 
  signal sequence_complete_51_306 : STD_LOGIC; 
  signal sequence_complete_51_mux0003 : STD_LOGIC; 
  signal sequence_complete_52_308 : STD_LOGIC; 
  signal sequence_complete_52_mux0003 : STD_LOGIC; 
  signal sequence_complete_52_not0001 : STD_LOGIC; 
  signal sequence_complete_53_311 : STD_LOGIC; 
  signal sequence_complete_53_mux0003 : STD_LOGIC; 
  signal sequence_complete_53_not0001 : STD_LOGIC; 
  signal sequence_complete_54_314 : STD_LOGIC; 
  signal sequence_complete_54_mux0003 : STD_LOGIC; 
  signal sequence_complete_54_not0001 : STD_LOGIC; 
  signal sequence_complete_55_317 : STD_LOGIC; 
  signal sequence_complete_55_mux0003 : STD_LOGIC; 
  signal sequence_complete_56_319 : STD_LOGIC; 
  signal sequence_complete_56_mux0003 : STD_LOGIC; 
  signal sequence_complete_56_not0001_321 : STD_LOGIC; 
  signal sequence_complete_57_322 : STD_LOGIC; 
  signal sequence_complete_57_mux0003 : STD_LOGIC; 
  signal sequence_complete_58_324 : STD_LOGIC; 
  signal sequence_complete_58_mux0003 : STD_LOGIC; 
  signal sequence_complete_58_not0001 : STD_LOGIC; 
  signal sequence_complete_59_327 : STD_LOGIC; 
  signal sequence_complete_59_mux0003 : STD_LOGIC; 
  signal sequence_complete_59_not0001 : STD_LOGIC; 
  signal sequence_complete_5_mux0003 : STD_LOGIC; 
  signal sequence_complete_5_not0001 : STD_LOGIC; 
  signal sequence_complete_6_332 : STD_LOGIC; 
  signal sequence_complete_6_mux0003 : STD_LOGIC; 
  signal sequence_complete_6_not0001 : STD_LOGIC; 
  signal sequence_complete_7_335 : STD_LOGIC; 
  signal sequence_complete_7_mux0003 : STD_LOGIC; 
  signal sequence_complete_8_337 : STD_LOGIC; 
  signal sequence_complete_8_mux0003 : STD_LOGIC; 
  signal sequence_complete_8_not0001 : STD_LOGIC; 
  signal sequence_complete_9_340 : STD_LOGIC; 
  signal sequence_complete_9_mux0003 : STD_LOGIC; 
  signal sequence_complete_9_not0001 : STD_LOGIC; 
  signal Madd_compteur_addsub0000_cy : STD_LOGIC_VECTOR ( 4 downto 4 ); 
  signal bus_color_futur : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal bus_color_present : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal compteur : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal compteur_mux0003 : STD_LOGIC_VECTOR ( 7 downto 1 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  bus_color_present_0 : FDP
    port map (
      C => clk_BUFGP_50,
      D => bus_color_futur(0),
      PRE => rst_IBUF_113,
      Q => bus_color_present(0)
    );
  bus_color_present_1 : FDP
    port map (
      C => clk_BUFGP_50,
      D => bus_color_futur(1),
      PRE => rst_IBUF_113,
      Q => bus_color_present(1)
    );
  reset_bus_color : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => etat_present_cmp_eq0003,
      G => reset_bus_color_not0001,
      Q => reset_bus_color_110
    );
  sequence_complete_43 : LD
    port map (
      D => sequence_complete_43_mux0003,
      G => sequence_complete_42_not0001,
      Q => sequence_complete_43_281
    );
  sequence_complete_38 : LD
    port map (
      D => sequence_complete_38_mux0003,
      G => sequence_complete_38_not0001,
      Q => sequence_complete_38_263
    );
  sequence_complete_44 : LD
    port map (
      D => sequence_complete_44_mux0003,
      G => sequence_complete_44_not0001,
      Q => sequence_complete_44_283
    );
  sequence_complete_39 : LD
    port map (
      D => sequence_complete_39_mux0003,
      G => sequence_complete_38_not0001,
      Q => sequence_complete_39_266
    );
  sequence_complete_50 : LD
    port map (
      D => sequence_complete_50_mux0003,
      G => sequence_complete_50_not0001,
      Q => sequence_complete_50_303
    );
  sequence_complete_45 : LD
    port map (
      D => sequence_complete_45_mux0003,
      G => sequence_complete_45_not0001,
      Q => sequence_complete_45_286
    );
  sequence_complete_51 : LD
    port map (
      D => sequence_complete_51_mux0003,
      G => sequence_complete_50_not0001,
      Q => sequence_complete_51_306
    );
  sequence_complete_46 : LD
    port map (
      D => sequence_complete_46_mux0003,
      G => sequence_complete_46_not0001,
      Q => sequence_complete_46_289
    );
  sequence_complete_52 : LD
    port map (
      D => sequence_complete_52_mux0003,
      G => sequence_complete_52_not0001,
      Q => sequence_complete_52_308
    );
  sequence_complete_47 : LD
    port map (
      D => sequence_complete_47_mux0003,
      G => sequence_complete_46_not0001,
      Q => sequence_complete_47_292
    );
  sequence_complete_53 : LD
    port map (
      D => sequence_complete_53_mux0003,
      G => sequence_complete_53_not0001,
      Q => sequence_complete_53_311
    );
  sequence_complete_48 : LD
    port map (
      D => sequence_complete_48_mux0003,
      G => sequence_complete_48_not0001,
      Q => sequence_complete_48_294
    );
  sequence_complete_49 : LD
    port map (
      D => sequence_complete_49_mux0003,
      G => sequence_complete_49_not0001,
      Q => sequence_complete_49_297
    );
  sequence_complete_54 : LD
    port map (
      D => sequence_complete_54_mux0003,
      G => sequence_complete_54_not0001,
      Q => sequence_complete_54_314
    );
  sequence_complete_55 : LD
    port map (
      D => sequence_complete_55_mux0003,
      G => sequence_complete_54_not0001,
      Q => sequence_complete_55_317
    );
  sequence_complete_56 : LD
    port map (
      D => sequence_complete_56_mux0003,
      G => sequence_complete_56_not0001_321,
      Q => sequence_complete_56_319
    );
  sequence_complete_57 : LD
    port map (
      D => sequence_complete_57_mux0003,
      G => sequence_complete_56_not0001_321,
      Q => sequence_complete_57_322
    );
  sequence_complete_58 : LD
    port map (
      D => sequence_complete_58_mux0003,
      G => sequence_complete_58_not0001,
      Q => sequence_complete_58_324
    );
  sequence_complete_59 : LD
    port map (
      D => sequence_complete_59_mux0003,
      G => sequence_complete_59_not0001,
      Q => sequence_complete_59_327
    );
  sequence_complete_0 : LD
    port map (
      D => sequence_complete_0_mux0003,
      G => sequence_complete_0_not0001,
      Q => sequence_complete_0_174
    );
  sequence_complete_1 : LD
    port map (
      D => sequence_complete_1_mux0003,
      G => sequence_complete_1_not0001,
      Q => sequence_complete_1_178
    );
  sequence_complete_2 : LD
    port map (
      D => sequence_complete_2_mux0003,
      G => sequence_complete_2_not0001,
      Q => sequence_complete_2_209
    );
  sequence_complete_3 : LD
    port map (
      D => sequence_complete_3_mux0003,
      G => sequence_complete_2_not0001,
      Q => sequence_complete_3_240
    );
  sequence_complete_4 : LD
    port map (
      D => sequence_complete_4_mux0003,
      G => sequence_complete_4_not0001,
      Q => sequence_complete_4_269
    );
  sequence_complete_5 : LD
    port map (
      D => sequence_complete_5_mux0003,
      G => sequence_complete_5_not0001,
      Q => sequence_complete_5_302
    );
  sequence_complete_10 : LD
    port map (
      D => sequence_complete_10_mux0003,
      G => sequence_complete_10_not0001,
      Q => sequence_complete_10_179
    );
  sequence_complete_6 : LD
    port map (
      D => sequence_complete_6_mux0003,
      G => sequence_complete_6_not0001,
      Q => sequence_complete_6_332
    );
  sequence_complete_11 : LD
    port map (
      D => sequence_complete_11_mux0003,
      G => sequence_complete_10_not0001,
      Q => sequence_complete_11_182
    );
  sequence_complete_7 : LD
    port map (
      D => sequence_complete_7_mux0003,
      G => sequence_complete_6_not0001,
      Q => sequence_complete_7_335
    );
  sequence_complete_12 : LD
    port map (
      D => sequence_complete_12_mux0003,
      G => sequence_complete_12_not0001,
      Q => sequence_complete_12_184
    );
  sequence_complete_8 : LD
    port map (
      D => sequence_complete_8_mux0003,
      G => sequence_complete_8_not0001,
      Q => sequence_complete_8_337
    );
  sequence_complete_13 : LD
    port map (
      D => sequence_complete_13_mux0003,
      G => sequence_complete_13_not0001,
      Q => sequence_complete_13_187
    );
  sequence_complete_9 : LD
    port map (
      D => sequence_complete_9_mux0003,
      G => sequence_complete_9_not0001,
      Q => sequence_complete_9_340
    );
  sequence_complete_14 : LD
    port map (
      D => sequence_complete_14_mux0003,
      G => sequence_complete_14_not0001,
      Q => sequence_complete_14_190
    );
  sequence_complete_20 : LD
    port map (
      D => sequence_complete_20_mux0003,
      G => sequence_complete_20_not0001,
      Q => sequence_complete_20_210
    );
  sequence_complete_15 : LD
    port map (
      D => sequence_complete_15_mux0003,
      G => sequence_complete_14_not0001,
      Q => sequence_complete_15_193
    );
  sequence_complete_21 : LD
    port map (
      D => sequence_complete_21_mux0003,
      G => sequence_complete_21_not0001,
      Q => sequence_complete_21_213
    );
  sequence_complete_16 : LD
    port map (
      D => sequence_complete_16_mux0003,
      G => sequence_complete_16_not0001,
      Q => sequence_complete_16_195
    );
  sequence_complete_22 : LD
    port map (
      D => sequence_complete_22_mux0003,
      G => sequence_complete_22_not0001,
      Q => sequence_complete_22_216
    );
  sequence_complete_17 : LD
    port map (
      D => sequence_complete_17_mux0003,
      G => sequence_complete_17_not0001,
      Q => sequence_complete_17_199
    );
  sequence_complete_23 : LD
    port map (
      D => sequence_complete_23_mux0003,
      G => sequence_complete_22_not0001,
      Q => sequence_complete_23_219
    );
  sequence_complete_18 : LD
    port map (
      D => sequence_complete_18_mux0003,
      G => sequence_complete_18_not0001,
      Q => sequence_complete_18_202
    );
  sequence_complete_19 : LD
    port map (
      D => sequence_complete_19_mux0003,
      G => sequence_complete_18_not0001,
      Q => sequence_complete_19_205
    );
  sequence_complete_24 : LD
    port map (
      D => sequence_complete_24_mux0003,
      G => sequence_complete_24_not0001,
      Q => sequence_complete_24_221
    );
  sequence_complete_30 : LD
    port map (
      D => sequence_complete_30_mux0003,
      G => sequence_complete_30_not0001,
      Q => sequence_complete_30_241
    );
  sequence_complete_25 : LD
    port map (
      D => sequence_complete_25_mux0003,
      G => sequence_complete_25_not0001,
      Q => sequence_complete_25_224
    );
  sequence_complete_31 : LD
    port map (
      D => sequence_complete_31_mux0003,
      G => sequence_complete_30_not0001,
      Q => sequence_complete_31_244
    );
  sequence_complete_26 : LD
    port map (
      D => sequence_complete_26_mux0003,
      G => sequence_complete_26_not0001,
      Q => sequence_complete_26_227
    );
  sequence_complete_32 : LD
    port map (
      D => sequence_complete_32_mux0003,
      G => sequence_complete_32_not0001,
      Q => sequence_complete_32_246
    );
  sequence_complete_27 : LD
    port map (
      D => sequence_complete_27_mux0003,
      G => sequence_complete_26_not0001,
      Q => sequence_complete_27_230
    );
  sequence_complete_33 : LD
    port map (
      D => sequence_complete_33_mux0003,
      G => sequence_complete_32_not0001,
      Q => sequence_complete_33_249
    );
  sequence_complete_28 : LD
    port map (
      D => sequence_complete_28_mux0003,
      G => sequence_complete_28_not0001,
      Q => sequence_complete_28_232
    );
  sequence_complete_34 : LD
    port map (
      D => sequence_complete_34_mux0003,
      G => sequence_complete_34_not0001,
      Q => sequence_complete_34_251
    );
  sequence_complete_29 : LD
    port map (
      D => sequence_complete_29_mux0003,
      G => sequence_complete_29_not0001,
      Q => sequence_complete_29_235
    );
  sequence_complete_40 : LD
    port map (
      D => sequence_complete_40_mux0003,
      G => sequence_complete_40_not0001,
      Q => sequence_complete_40_270
    );
  sequence_complete_35 : LD
    port map (
      D => sequence_complete_35_mux0003,
      G => sequence_complete_34_not0001,
      Q => sequence_complete_35_254
    );
  sequence_complete_41 : LD
    port map (
      D => sequence_complete_41_mux0003,
      G => sequence_complete_41_not0001,
      Q => sequence_complete_41_275
    );
  sequence_complete_36 : LD
    port map (
      D => sequence_complete_36_mux0003,
      G => sequence_complete_36_not0001,
      Q => sequence_complete_36_256
    );
  sequence_complete_42 : LD
    port map (
      D => sequence_complete_42_mux0003,
      G => sequence_complete_42_not0001,
      Q => sequence_complete_42_278
    );
  sequence_complete_37 : LD
    port map (
      D => sequence_complete_37_mux0003,
      G => sequence_complete_37_not0001,
      Q => sequence_complete_37_260
    );
  etat_present_FSM_FFd1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_50,
      CLR => rst_IBUF_113,
      D => etat_present_FSM_FFd1_In,
      Q => etat_present_FSM_FFd1_94
    );
  etat_present_FSM_FFd2 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_50,
      D => etat_present_FSM_FFd2_In,
      PRE => rst_IBUF_113,
      Q => etat_present_FSM_FFd2_96
    );
  compteur_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(1),
      G => compteur_not0001,
      Q => compteur(1)
    );
  compteur_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(2),
      G => compteur_not0001,
      Q => compteur(2)
    );
  compteur_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(3),
      G => compteur_not0001,
      Q => compteur(3)
    );
  compteur_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(4),
      G => compteur_not0001,
      Q => compteur(4)
    );
  compteur_5 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(5),
      G => compteur_not0001,
      Q => compteur(5)
    );
  compteur_6 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(6),
      G => compteur_not0001,
      Q => compteur(6)
    );
  compteur_7 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(7),
      G => compteur_not0001,
      Q => compteur(7)
    );
  sequence_complete_36_or00001 : LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => compteur(1),
      I1 => compteur(2),
      O => sequence_complete_36_or0000
    );
  sequence_complete_32_not000111 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      O => N11
    );
  sequence_complete_24_mux000311 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => N4,
      O => N18
    );
  sequence_complete_21_not000111 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(3),
      O => N17
    );
  sequence_complete_13_not000111 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(3),
      O => N16
    );
  sequence_complete_10_not000131 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => compteur(3),
      I1 => compteur(2),
      O => N15
    );
  sequence_complete_0_not000111 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => etat_present_FSM_FFd1_94,
      I1 => etat_present_FSM_FFd2_96,
      O => etat_present_cmp_eq0003
    );
  sequence_complete_0_cmp_eq000031 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => compteur(6),
      I1 => compteur(7),
      O => N55
    );
  sequence_complete_0_cmp_eq000011 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(3),
      O => N14
    );
  compteur_not00011 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      O => compteur_not0001
    );
  sequence_complete_55_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N4,
      I1 => bus_color_present(1),
      I2 => N30,
      O => sequence_complete_55_mux0003
    );
  sequence_complete_54_not000111 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N7,
      I1 => compteur(2),
      I2 => compteur(1),
      O => N30
    );
  sequence_complete_53_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N19,
      I1 => N7,
      I2 => compteur(2),
      O => sequence_complete_53_mux0003
    );
  sequence_complete_51_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N4,
      I1 => bus_color_present(1),
      I2 => N31,
      O => sequence_complete_51_mux0003
    );
  sequence_complete_50_not000111 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N7,
      I1 => compteur(2),
      I2 => compteur(1),
      O => N31
    );
  sequence_complete_4_mux00031 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => N12,
      I1 => compteur(4),
      I2 => compteur(3),
      O => sequence_complete_4_mux0003
    );
  sequence_complete_49_not000111 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N7,
      I1 => compteur(1),
      I2 => N4,
      O => N32
    );
  sequence_complete_49_mux000321 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => compteur(1),
      I2 => N4,
      O => N19
    );
  sequence_complete_49_mux000311 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(3),
      I2 => compteur(5),
      O => N7
    );
  sequence_complete_49_mux00031 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N7,
      I1 => compteur(2),
      I2 => N19,
      O => sequence_complete_49_mux0003
    );
  sequence_complete_41_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => N5,
      I2 => sequence_complete_40_cmp_eq0001,
      O => sequence_complete_41_mux0003
    );
  sequence_complete_40_or00001 : LUT3
    generic map(
      INIT => X"FB"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(3),
      I2 => compteur(1),
      O => sequence_complete_40_or0000
    );
  sequence_complete_40_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N5,
      I1 => bus_color_present(0),
      I2 => sequence_complete_40_cmp_eq0001,
      O => sequence_complete_40_mux0003
    );
  sequence_complete_33_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => N5,
      I2 => N26,
      O => sequence_complete_33_mux0003
    );
  sequence_complete_32_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N5,
      I1 => bus_color_present(0),
      I2 => N26,
      O => sequence_complete_32_mux0003
    );
  sequence_complete_28_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => N12,
      I1 => compteur(4),
      I2 => compteur(3),
      O => sequence_complete_28_mux0003
    );
  sequence_complete_20_mux00031 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(3),
      I2 => N12,
      O => sequence_complete_20_mux0003
    );
  sequence_complete_17_not000111 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N4,
      I1 => compteur(5),
      I2 => compteur(4),
      O => N21
    );
  sequence_complete_12_mux00031 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N12,
      I1 => compteur(4),
      I2 => compteur(3),
      O => sequence_complete_12_mux0003
    );
  sequence_complete_10_not000141 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => N4,
      I1 => compteur(5),
      I2 => compteur(4),
      O => N20
    );
  sequence_complete_10_mux000321 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => compteur(1),
      I1 => compteur(5),
      I2 => N4,
      O => N6
    );
  N51 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => N4,
      I1 => compteur(4),
      I2 => compteur(5),
      O => N5
    );
  sequence_complete_0_not00011 : LUT3
    generic map(
      INIT => X"64"
    )
    port map (
      I0 => etat_present_FSM_FFd1_94,
      I1 => etat_present_FSM_FFd2_96,
      I2 => sequence_complete_0_cmp_eq0000,
      O => sequence_complete_0_not0001
    );
  sequence_complete_9_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => N15,
      I2 => N19,
      I3 => compteur(5),
      O => sequence_complete_9_mux0003
    );
  sequence_complete_8_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => sequence_complete_40_cmp_eq0001,
      I2 => N18,
      I3 => compteur(5),
      O => sequence_complete_8_mux0003
    );
  sequence_complete_7_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => N17,
      I2 => N6,
      I3 => compteur(4),
      O => sequence_complete_7_mux0003
    );
  sequence_complete_6_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N17,
      I2 => compteur(1),
      I3 => N20,
      O => sequence_complete_6_not0001
    );
  sequence_complete_6_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => N17,
      I2 => N6,
      I3 => compteur(4),
      O => sequence_complete_6_mux0003
    );
  sequence_complete_5_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N17,
      I2 => compteur(1),
      I3 => N20,
      O => sequence_complete_5_not0001
    );
  sequence_complete_5_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => N17,
      I2 => N19,
      I3 => compteur(5),
      O => sequence_complete_5_mux0003
    );
  sequence_complete_59_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => etat_present_cmp_eq0003,
      I2 => N55,
      I3 => N48,
      O => sequence_complete_59_mux0003
    );
  sequence_complete_58_not000111 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N15,
      I1 => compteur(1),
      I2 => compteur(5),
      I3 => compteur(4),
      O => N48
    );
  sequence_complete_57_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N15,
      I1 => N19,
      I2 => compteur(5),
      I3 => compteur(4),
      O => sequence_complete_57_mux0003
    );
  sequence_complete_56_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(5),
      I2 => sequence_complete_40_cmp_eq0001,
      I3 => N18,
      O => sequence_complete_56_mux0003
    );
  sequence_complete_52_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N7,
      I2 => sequence_complete_36_or0000,
      I3 => N4,
      O => sequence_complete_52_not0001
    );
  sequence_complete_4_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => N11,
      I1 => sequence_complete_36_or0000,
      I2 => compteur(3),
      I3 => N20,
      O => sequence_complete_4_not0001
    );
  sequence_complete_48_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N4,
      I2 => sequence_complete_16_or0000,
      I3 => compteur(5),
      O => sequence_complete_48_not0001
    );
  sequence_complete_48_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N18,
      I1 => compteur(5),
      I2 => N26,
      I3 => compteur(4),
      O => sequence_complete_48_mux0003
    );
  sequence_complete_47_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N16,
      I1 => compteur(1),
      I2 => bus_color_present(1),
      I3 => N5,
      O => sequence_complete_47_mux0003
    );
  sequence_complete_46_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N16,
      I2 => compteur(1),
      I3 => N5,
      O => sequence_complete_46_not0001
    );
  sequence_complete_46_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N16,
      I1 => compteur(1),
      I2 => N5,
      I3 => bus_color_present(0),
      O => sequence_complete_46_mux0003
    );
  sequence_complete_45_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N5,
      I2 => compteur(1),
      I3 => N16,
      O => sequence_complete_45_not0001
    );
  sequence_complete_45_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => N16,
      I2 => N5,
      I3 => compteur(1),
      O => sequence_complete_45_mux0003
    );
  sequence_complete_44_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(3),
      I2 => sequence_complete_36_or0000,
      I3 => N5,
      O => sequence_complete_44_not0001
    );
  sequence_complete_44_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N5,
      I1 => compteur(3),
      I2 => bus_color_present(0),
      I3 => sequence_complete_36_or0000,
      O => sequence_complete_44_mux0003
    );
  sequence_complete_43_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N15,
      I2 => N5,
      I3 => bus_color_present(1),
      O => sequence_complete_43_mux0003
    );
  sequence_complete_42_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N5,
      I2 => compteur(1),
      I3 => N15,
      O => sequence_complete_42_not0001
    );
  sequence_complete_42_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N15,
      I2 => bus_color_present(0),
      I3 => N5,
      O => sequence_complete_42_mux0003
    );
  sequence_complete_3_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(1),
      I2 => N14,
      I3 => compteur(4),
      O => sequence_complete_3_mux0003
    );
  sequence_complete_39_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N17,
      I2 => N5,
      I3 => bus_color_present(1),
      O => sequence_complete_39_mux0003
    );
  sequence_complete_38_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N5,
      I2 => compteur(1),
      I3 => N17,
      O => sequence_complete_38_not0001
    );
  sequence_complete_38_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N17,
      I2 => bus_color_present(0),
      I3 => N5,
      O => sequence_complete_38_mux0003
    );
  sequence_complete_37_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N5,
      I2 => compteur(1),
      I3 => N17,
      O => sequence_complete_37_not0001
    );
  sequence_complete_37_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => bus_color_present(1),
      I1 => N17,
      I2 => N5,
      I3 => compteur(1),
      O => sequence_complete_37_mux0003
    );
  sequence_complete_36_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(3),
      I2 => sequence_complete_36_or0000,
      I3 => N5,
      O => sequence_complete_36_not0001
    );
  sequence_complete_36_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(3),
      I1 => N5,
      I2 => bus_color_present(0),
      I3 => sequence_complete_36_or0000,
      O => sequence_complete_36_mux0003
    );
  sequence_complete_35_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N14,
      I2 => N5,
      I3 => bus_color_present(1),
      O => sequence_complete_35_mux0003
    );
  sequence_complete_34_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N5,
      I2 => compteur(1),
      I3 => N14,
      O => sequence_complete_34_not0001
    );
  sequence_complete_34_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(1),
      I1 => N14,
      I2 => bus_color_present(0),
      I3 => N5,
      O => sequence_complete_34_mux0003
    );
  sequence_complete_31_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N16,
      I1 => bus_color_present(1),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_31_mux0003
    );
  sequence_complete_30_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N16,
      O => sequence_complete_30_not0001
    );
  sequence_complete_30_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N16,
      I1 => bus_color_present(0),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_30_mux0003
    );
  sequence_complete_2_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N20,
      I2 => compteur(1),
      I3 => N14,
      O => sequence_complete_2_not0001
    );
  sequence_complete_2_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(0),
      I2 => N14,
      I3 => compteur(4),
      O => sequence_complete_2_mux0003
    );
  sequence_complete_29_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N16,
      O => sequence_complete_29_not0001
    );
  sequence_complete_29_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N19,
      I1 => compteur(4),
      I2 => N16,
      I3 => compteur(5),
      O => sequence_complete_29_mux0003
    );
  sequence_complete_28_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(3),
      I2 => sequence_complete_36_or0000,
      I3 => N21,
      O => sequence_complete_28_not0001
    );
  sequence_complete_27_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N15,
      I1 => bus_color_present(1),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_27_mux0003
    );
  sequence_complete_26_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N15,
      O => sequence_complete_26_not0001
    );
  sequence_complete_26_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N15,
      I1 => bus_color_present(0),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_26_mux0003
    );
  sequence_complete_25_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N19,
      I1 => compteur(4),
      I2 => N15,
      I3 => compteur(5),
      O => sequence_complete_25_mux0003
    );
  sequence_complete_24_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => sequence_complete_40_cmp_eq0001,
      I1 => compteur(4),
      I2 => N18,
      I3 => compteur(5),
      O => sequence_complete_24_mux0003
    );
  sequence_complete_23_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N17,
      I1 => bus_color_present(1),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_23_mux0003
    );
  sequence_complete_22_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N17,
      O => sequence_complete_22_not0001
    );
  sequence_complete_22_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N17,
      I1 => bus_color_present(0),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_22_mux0003
    );
  sequence_complete_21_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N17,
      O => sequence_complete_21_not0001
    );
  sequence_complete_21_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N19,
      I1 => compteur(4),
      I2 => N17,
      I3 => compteur(5),
      O => sequence_complete_21_mux0003
    );
  sequence_complete_20_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(3),
      I2 => sequence_complete_36_or0000,
      I3 => N21,
      O => sequence_complete_20_not0001
    );
  sequence_complete_1_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => N19,
      I2 => N14,
      I3 => compteur(5),
      O => sequence_complete_1_mux0003
    );
  sequence_complete_19_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N14,
      I1 => bus_color_present(1),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_19_mux0003
    );
  sequence_complete_18_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N21,
      I2 => compteur(1),
      I3 => N14,
      O => sequence_complete_18_not0001
    );
  sequence_complete_18_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N14,
      I1 => bus_color_present(0),
      I2 => compteur(4),
      I3 => N6,
      O => sequence_complete_18_mux0003
    );
  sequence_complete_17_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N19,
      I1 => compteur(4),
      I2 => N14,
      I3 => compteur(5),
      O => sequence_complete_17_mux0003
    );
  sequence_complete_16_or00001 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(1),
      I2 => compteur(3),
      I3 => compteur(2),
      O => sequence_complete_16_or0000
    );
  sequence_complete_16_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => N11,
      I1 => sequence_complete_16_or0000,
      I2 => compteur(5),
      I3 => N4,
      O => sequence_complete_16_not0001
    );
  sequence_complete_16_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N18,
      I1 => N26,
      I2 => compteur(4),
      I3 => compteur(5),
      O => sequence_complete_16_mux0003
    );
  sequence_complete_15_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(1),
      I2 => N16,
      I3 => compteur(4),
      O => sequence_complete_15_mux0003
    );
  sequence_complete_14_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N20,
      I2 => compteur(1),
      I3 => N16,
      O => sequence_complete_14_not0001
    );
  sequence_complete_14_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(0),
      I2 => N16,
      I3 => compteur(4),
      O => sequence_complete_14_mux0003
    );
  sequence_complete_13_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => N20,
      I2 => compteur(1),
      I3 => N16,
      O => sequence_complete_13_not0001
    );
  sequence_complete_13_mux00031 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => N19,
      I2 => N16,
      I3 => compteur(5),
      O => sequence_complete_13_mux0003
    );
  sequence_complete_12_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(3),
      I2 => sequence_complete_36_or0000,
      I3 => N20,
      O => sequence_complete_12_not0001
    );
  sequence_complete_11_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(1),
      I2 => N15,
      I3 => compteur(4),
      O => sequence_complete_11_mux0003
    );
  sequence_complete_10_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => N20,
      I2 => compteur(1),
      I3 => N15,
      O => sequence_complete_10_not0001
    );
  sequence_complete_10_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N6,
      I1 => bus_color_present(0),
      I2 => N15,
      I3 => compteur(4),
      O => sequence_complete_10_mux0003
    );
  sequence_complete_0_cmp_eq00001 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(4),
      I1 => N55,
      I2 => N26,
      I3 => compteur(5),
      O => sequence_complete_0_cmp_eq0000
    );
  sequence_complete_59_not00011 : LUT4
    generic map(
      INIT => X"6222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N48,
      I3 => N55,
      O => sequence_complete_59_not0001
    );
  sequence_complete_56_not0001 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N11,
      I1 => compteur(5),
      I2 => compteur(4),
      I3 => N56,
      O => sequence_complete_56_not0001_321
    );
  etat_present_cmp_eq00001 : LUT3
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => etat_system_1_IBUF_108,
      I1 => etat_system_0_IBUF_107,
      I2 => etat_system_2_IBUF_109,
      O => etat_present_cmp_eq0000
    );
  etat_present_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_cmp_eq0000,
      I2 => etat_present_FSM_FFd1_94,
      O => etat_present_FSM_FFd1_In
    );
  bus_color_futur_or000011 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_aleatoire_1_IBUF_83,
      I1 => couleur_aleatoire_2_IBUF_84,
      I2 => couleur_aleatoire_0_IBUF_82,
      I3 => couleur_aleatoire_3_IBUF_85,
      O => bus_color_futur_or000011_42
    );
  bus_color_futur_or000023 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_jouer_1_IBUF_91,
      I1 => couleur_jouer_2_IBUF_92,
      I2 => couleur_jouer_0_IBUF_90,
      I3 => couleur_jouer_3_IBUF_93,
      O => bus_color_futur_or000023_43
    );
  bus_color_futur_0_SW0 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => bus_color_futur_cmp_eq0005,
      I1 => bus_color_futur_cmp_eq0004,
      O => etat_present_FSM_FFd2_In55
    );
  compteur_mux0003_4_Q : LUT4
    generic map(
      INIT => X"6C00"
    )
    port map (
      I0 => compteur(3),
      I1 => compteur(4),
      I2 => N62,
      I3 => N13,
      O => compteur_mux0003(4)
    );
  compteur_mux0003_5_1 : LUT3
    generic map(
      INIT => X"60"
    )
    port map (
      I0 => compteur(5),
      I1 => Madd_compteur_addsub0000_cy(4),
      I2 => N87,
      O => compteur_mux0003(5)
    );
  compteur_mux0003_6_1 : LUT4
    generic map(
      INIT => X"6A00"
    )
    port map (
      I0 => compteur(6),
      I1 => compteur(5),
      I2 => N85,
      I3 => N13,
      O => compteur_mux0003(6)
    );
  bus_color_futur_cmp_eq00051 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_jouer_0_IBUF_90,
      I1 => couleur_jouer_1_IBUF_91,
      I2 => couleur_jouer_2_IBUF_92,
      I3 => couleur_jouer_3_IBUF_93,
      O => bus_color_futur_cmp_eq0005
    );
  bus_color_futur_cmp_eq00041 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_aleatoire_1_IBUF_83,
      I1 => couleur_aleatoire_3_IBUF_85,
      I2 => couleur_aleatoire_2_IBUF_84,
      I3 => couleur_aleatoire_0_IBUF_82,
      O => bus_color_futur_cmp_eq0004
    );
  bus_color_futur_or000111 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_jouer_0_IBUF_90,
      I1 => couleur_jouer_2_IBUF_92,
      I2 => couleur_jouer_1_IBUF_91,
      I3 => couleur_jouer_3_IBUF_93,
      O => bus_color_futur_or000111_45
    );
  bus_color_futur_or000123 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => couleur_aleatoire_2_IBUF_84,
      I1 => couleur_aleatoire_3_IBUF_85,
      I2 => couleur_aleatoire_1_IBUF_83,
      I3 => couleur_aleatoire_0_IBUF_82,
      O => bus_color_futur_or000123_46
    );
  bus_color_futur_or000124 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => bus_color_futur_or000111_45,
      I1 => bus_color_futur_or000123_46,
      O => bus_color_futur_or0001
    );
  etat_present_FSM_FFd2_In22 : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => couleur_jouer_1_IBUF_91,
      I1 => couleur_jouer_0_IBUF_90,
      I2 => couleur_jouer_3_IBUF_93,
      I3 => couleur_jouer_2_IBUF_92,
      O => etat_present_FSM_FFd2_In22_98
    );
  etat_present_FSM_FFd2_In38 : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => couleur_aleatoire_1_IBUF_83,
      I1 => couleur_aleatoire_0_IBUF_82,
      I2 => couleur_aleatoire_3_IBUF_85,
      I3 => couleur_aleatoire_2_IBUF_84,
      O => etat_present_FSM_FFd2_In38_99
    );
  etat_present_FSM_FFd2_In64 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => etat_present_FSM_FFd2_In22_98,
      I1 => etat_present_FSM_FFd2_In38_99,
      I2 => etat_present_FSM_FFd2_In55,
      I3 => bus_color_futur_or0001,
      O => etat_present_FSM_FFd2_In64_101
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_113
    );
  couleur_aleatoire_3_IBUF : IBUF
    port map (
      I => couleur_aleatoire(3),
      O => couleur_aleatoire_3_IBUF_85
    );
  couleur_aleatoire_2_IBUF : IBUF
    port map (
      I => couleur_aleatoire(2),
      O => couleur_aleatoire_2_IBUF_84
    );
  couleur_aleatoire_1_IBUF : IBUF
    port map (
      I => couleur_aleatoire(1),
      O => couleur_aleatoire_1_IBUF_83
    );
  couleur_aleatoire_0_IBUF : IBUF
    port map (
      I => couleur_aleatoire(0),
      O => couleur_aleatoire_0_IBUF_82
    );
  etat_system_2_IBUF : IBUF
    port map (
      I => etat_system(2),
      O => etat_system_2_IBUF_109
    );
  etat_system_1_IBUF : IBUF
    port map (
      I => etat_system(1),
      O => etat_system_1_IBUF_108
    );
  etat_system_0_IBUF : IBUF
    port map (
      I => etat_system(0),
      O => etat_system_0_IBUF_107
    );
  couleur_jouer_3_IBUF : IBUF
    port map (
      I => couleur_jouer(3),
      O => couleur_jouer_3_IBUF_93
    );
  couleur_jouer_2_IBUF : IBUF
    port map (
      I => couleur_jouer(2),
      O => couleur_jouer_2_IBUF_92
    );
  couleur_jouer_1_IBUF : IBUF
    port map (
      I => couleur_jouer(1),
      O => couleur_jouer_1_IBUF_91
    );
  couleur_jouer_0_IBUF : IBUF
    port map (
      I => couleur_jouer(0),
      O => couleur_jouer_0_IBUF_90
    );
  compteur_couleur_7_OBUF : OBUF
    port map (
      I => compteur(7),
      O => compteur_couleur(7)
    );
  compteur_couleur_6_OBUF : OBUF
    port map (
      I => compteur(6),
      O => compteur_couleur(6)
    );
  compteur_couleur_5_OBUF : OBUF
    port map (
      I => compteur(5),
      O => compteur_couleur(5)
    );
  compteur_couleur_4_OBUF : OBUF
    port map (
      I => compteur(4),
      O => compteur_couleur(4)
    );
  compteur_couleur_3_OBUF : OBUF
    port map (
      I => compteur(3),
      O => compteur_couleur(3)
    );
  compteur_couleur_2_OBUF : OBUF
    port map (
      I => compteur(2),
      O => compteur_couleur(2)
    );
  compteur_couleur_1_OBUF : OBUF
    port map (
      I => compteur(1),
      O => compteur_couleur(1)
    );
  compteur_couleur_0_OBUF : OBUF
    port map (
      I => N0,
      O => compteur_couleur(0)
    );
  sequence_complete_59_OBUF : OBUF
    port map (
      I => sequence_complete_59_327,
      O => sequence_complete(59)
    );
  sequence_complete_58_OBUF : OBUF
    port map (
      I => sequence_complete_58_324,
      O => sequence_complete(58)
    );
  sequence_complete_57_OBUF : OBUF
    port map (
      I => sequence_complete_57_322,
      O => sequence_complete(57)
    );
  sequence_complete_56_OBUF : OBUF
    port map (
      I => sequence_complete_56_319,
      O => sequence_complete(56)
    );
  sequence_complete_55_OBUF : OBUF
    port map (
      I => sequence_complete_55_317,
      O => sequence_complete(55)
    );
  sequence_complete_54_OBUF : OBUF
    port map (
      I => sequence_complete_54_314,
      O => sequence_complete(54)
    );
  sequence_complete_53_OBUF : OBUF
    port map (
      I => sequence_complete_53_311,
      O => sequence_complete(53)
    );
  sequence_complete_52_OBUF : OBUF
    port map (
      I => sequence_complete_52_308,
      O => sequence_complete(52)
    );
  sequence_complete_51_OBUF : OBUF
    port map (
      I => sequence_complete_51_306,
      O => sequence_complete(51)
    );
  sequence_complete_50_OBUF : OBUF
    port map (
      I => sequence_complete_50_303,
      O => sequence_complete(50)
    );
  sequence_complete_49_OBUF : OBUF
    port map (
      I => sequence_complete_49_297,
      O => sequence_complete(49)
    );
  sequence_complete_48_OBUF : OBUF
    port map (
      I => sequence_complete_48_294,
      O => sequence_complete(48)
    );
  sequence_complete_47_OBUF : OBUF
    port map (
      I => sequence_complete_47_292,
      O => sequence_complete(47)
    );
  sequence_complete_46_OBUF : OBUF
    port map (
      I => sequence_complete_46_289,
      O => sequence_complete(46)
    );
  sequence_complete_45_OBUF : OBUF
    port map (
      I => sequence_complete_45_286,
      O => sequence_complete(45)
    );
  sequence_complete_44_OBUF : OBUF
    port map (
      I => sequence_complete_44_283,
      O => sequence_complete(44)
    );
  sequence_complete_43_OBUF : OBUF
    port map (
      I => sequence_complete_43_281,
      O => sequence_complete(43)
    );
  sequence_complete_42_OBUF : OBUF
    port map (
      I => sequence_complete_42_278,
      O => sequence_complete(42)
    );
  sequence_complete_41_OBUF : OBUF
    port map (
      I => sequence_complete_41_275,
      O => sequence_complete(41)
    );
  sequence_complete_40_OBUF : OBUF
    port map (
      I => sequence_complete_40_270,
      O => sequence_complete(40)
    );
  sequence_complete_39_OBUF : OBUF
    port map (
      I => sequence_complete_39_266,
      O => sequence_complete(39)
    );
  sequence_complete_38_OBUF : OBUF
    port map (
      I => sequence_complete_38_263,
      O => sequence_complete(38)
    );
  sequence_complete_37_OBUF : OBUF
    port map (
      I => sequence_complete_37_260,
      O => sequence_complete(37)
    );
  sequence_complete_36_OBUF : OBUF
    port map (
      I => sequence_complete_36_256,
      O => sequence_complete(36)
    );
  sequence_complete_35_OBUF : OBUF
    port map (
      I => sequence_complete_35_254,
      O => sequence_complete(35)
    );
  sequence_complete_34_OBUF : OBUF
    port map (
      I => sequence_complete_34_251,
      O => sequence_complete(34)
    );
  sequence_complete_33_OBUF : OBUF
    port map (
      I => sequence_complete_33_249,
      O => sequence_complete(33)
    );
  sequence_complete_32_OBUF : OBUF
    port map (
      I => sequence_complete_32_246,
      O => sequence_complete(32)
    );
  sequence_complete_31_OBUF : OBUF
    port map (
      I => sequence_complete_31_244,
      O => sequence_complete(31)
    );
  sequence_complete_30_OBUF : OBUF
    port map (
      I => sequence_complete_30_241,
      O => sequence_complete(30)
    );
  sequence_complete_29_OBUF : OBUF
    port map (
      I => sequence_complete_29_235,
      O => sequence_complete(29)
    );
  sequence_complete_28_OBUF : OBUF
    port map (
      I => sequence_complete_28_232,
      O => sequence_complete(28)
    );
  sequence_complete_27_OBUF : OBUF
    port map (
      I => sequence_complete_27_230,
      O => sequence_complete(27)
    );
  sequence_complete_26_OBUF : OBUF
    port map (
      I => sequence_complete_26_227,
      O => sequence_complete(26)
    );
  sequence_complete_25_OBUF : OBUF
    port map (
      I => sequence_complete_25_224,
      O => sequence_complete(25)
    );
  sequence_complete_24_OBUF : OBUF
    port map (
      I => sequence_complete_24_221,
      O => sequence_complete(24)
    );
  sequence_complete_23_OBUF : OBUF
    port map (
      I => sequence_complete_23_219,
      O => sequence_complete(23)
    );
  sequence_complete_22_OBUF : OBUF
    port map (
      I => sequence_complete_22_216,
      O => sequence_complete(22)
    );
  sequence_complete_21_OBUF : OBUF
    port map (
      I => sequence_complete_21_213,
      O => sequence_complete(21)
    );
  sequence_complete_20_OBUF : OBUF
    port map (
      I => sequence_complete_20_210,
      O => sequence_complete(20)
    );
  sequence_complete_19_OBUF : OBUF
    port map (
      I => sequence_complete_19_205,
      O => sequence_complete(19)
    );
  sequence_complete_18_OBUF : OBUF
    port map (
      I => sequence_complete_18_202,
      O => sequence_complete(18)
    );
  sequence_complete_17_OBUF : OBUF
    port map (
      I => sequence_complete_17_199,
      O => sequence_complete(17)
    );
  sequence_complete_16_OBUF : OBUF
    port map (
      I => sequence_complete_16_195,
      O => sequence_complete(16)
    );
  sequence_complete_15_OBUF : OBUF
    port map (
      I => sequence_complete_15_193,
      O => sequence_complete(15)
    );
  sequence_complete_14_OBUF : OBUF
    port map (
      I => sequence_complete_14_190,
      O => sequence_complete(14)
    );
  sequence_complete_13_OBUF : OBUF
    port map (
      I => sequence_complete_13_187,
      O => sequence_complete(13)
    );
  sequence_complete_12_OBUF : OBUF
    port map (
      I => sequence_complete_12_184,
      O => sequence_complete(12)
    );
  sequence_complete_11_OBUF : OBUF
    port map (
      I => sequence_complete_11_182,
      O => sequence_complete(11)
    );
  sequence_complete_10_OBUF : OBUF
    port map (
      I => sequence_complete_10_179,
      O => sequence_complete(10)
    );
  sequence_complete_9_OBUF : OBUF
    port map (
      I => sequence_complete_9_340,
      O => sequence_complete(9)
    );
  sequence_complete_8_OBUF : OBUF
    port map (
      I => sequence_complete_8_337,
      O => sequence_complete(8)
    );
  sequence_complete_7_OBUF : OBUF
    port map (
      I => sequence_complete_7_335,
      O => sequence_complete(7)
    );
  sequence_complete_6_OBUF : OBUF
    port map (
      I => sequence_complete_6_332,
      O => sequence_complete(6)
    );
  sequence_complete_5_OBUF : OBUF
    port map (
      I => sequence_complete_5_302,
      O => sequence_complete(5)
    );
  sequence_complete_4_OBUF : OBUF
    port map (
      I => sequence_complete_4_269,
      O => sequence_complete(4)
    );
  sequence_complete_3_OBUF : OBUF
    port map (
      I => sequence_complete_3_240,
      O => sequence_complete(3)
    );
  sequence_complete_2_OBUF : OBUF
    port map (
      I => sequence_complete_2_209,
      O => sequence_complete(2)
    );
  sequence_complete_1_OBUF : OBUF
    port map (
      I => sequence_complete_1_178,
      O => sequence_complete(1)
    );
  sequence_complete_0_OBUF : OBUF
    port map (
      I => sequence_complete_0_174,
      O => sequence_complete(0)
    );
  compteur_mux0003_7_Q : LUT4
    generic map(
      INIT => X"9A00"
    )
    port map (
      I0 => compteur(7),
      I1 => N66,
      I2 => Madd_compteur_addsub0000_cy(4),
      I3 => N13,
      O => compteur_mux0003(7)
    );
  compteur_mux0003_1_129_SW3 : LUT4
    generic map(
      INIT => X"E5CF"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(6),
      I2 => compteur(3),
      I3 => compteur(1),
      O => N73
    );
  compteur_mux0003_1_129_SW4 : LUT3
    generic map(
      INIT => X"87"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(1),
      I2 => compteur(3),
      O => N74
    );
  compteur_mux0003_3_1 : LUT4
    generic map(
      INIT => X"082A"
    )
    port map (
      I0 => compteur_mux0003_1_127_70,
      I1 => N86,
      I2 => N74,
      I3 => N73,
      O => compteur_mux0003(3)
    );
  compteur_mux0003_2_1 : LUT4
    generic map(
      INIT => X"0060"
    )
    port map (
      I0 => compteur(1),
      I1 => compteur(2),
      I2 => compteur_mux0003_1_127_70,
      I3 => N88,
      O => compteur_mux0003(2)
    );
  compteur_mux0003_1_2 : LUT4
    generic map(
      INIT => X"0444"
    )
    port map (
      I0 => compteur(1),
      I1 => N89,
      I2 => compteur(2),
      I3 => N78,
      O => compteur_mux0003(1)
    );
  sequence_complete_58_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => N4,
      I2 => N48,
      O => sequence_complete_58_mux0003
    );
  sequence_complete_54_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => N4,
      I2 => N30,
      O => sequence_complete_54_mux0003
    );
  sequence_complete_50_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => N4,
      I2 => N31,
      O => sequence_complete_50_mux0003
    );
  sequence_complete_25_not000111 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => compteur(3),
      I1 => compteur(2),
      I2 => compteur(1),
      O => sequence_complete_40_cmp_eq0001
    );
  sequence_complete_0_cmp_eq000021 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(3),
      I2 => compteur(1),
      O => N26
    );
  N41 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => compteur(6),
      I1 => compteur(7),
      I2 => etat_present_FSM_FFd1_94,
      I3 => etat_present_FSM_FFd2_96,
      O => N4
    );
  sequence_complete_9_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_cmp_eq0001,
      I3 => N20,
      O => sequence_complete_9_not0001
    );
  sequence_complete_8_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_or0000,
      I3 => N20,
      O => sequence_complete_8_not0001
    );
  sequence_complete_58_not00012 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N48,
      I3 => N4,
      O => sequence_complete_58_not0001
    );
  sequence_complete_54_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N30,
      I3 => N4,
      O => sequence_complete_54_not0001
    );
  sequence_complete_53_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N32,
      I3 => compteur(2),
      O => sequence_complete_53_not0001
    );
  sequence_complete_52_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N7,
      I1 => N4,
      I2 => bus_color_present(0),
      I3 => sequence_complete_36_or0000,
      O => sequence_complete_52_mux0003
    );
  sequence_complete_50_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N31,
      I3 => N4,
      O => sequence_complete_50_not0001
    );
  sequence_complete_49_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => compteur(2),
      I3 => N32,
      O => sequence_complete_49_not0001
    );
  sequence_complete_41_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_cmp_eq0001,
      I3 => N5,
      O => sequence_complete_41_not0001
    );
  sequence_complete_40_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_or0000,
      I3 => N5,
      O => sequence_complete_40_not0001
    );
  sequence_complete_32_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N26,
      I3 => N5,
      O => sequence_complete_32_not0001
    );
  sequence_complete_25_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_cmp_eq0001,
      I3 => N21,
      O => sequence_complete_25_not0001
    );
  sequence_complete_24_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => sequence_complete_40_or0000,
      I3 => N21,
      O => sequence_complete_24_not0001
    );
  sequence_complete_1_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N26,
      I3 => N20,
      O => sequence_complete_1_not0001
    );
  sequence_complete_17_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd1_94,
      I2 => N26,
      I3 => N21,
      O => sequence_complete_17_not0001
    );
  sequence_complete_12_mux000311 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(5),
      I1 => N4,
      I2 => bus_color_present(0),
      I3 => sequence_complete_36_or0000,
      O => N12
    );
  sequence_complete_0_mux00031 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => bus_color_present(0),
      I1 => sequence_complete_0_cmp_eq0000,
      I2 => etat_present_FSM_FFd1_94,
      I3 => etat_present_FSM_FFd2_96,
      O => sequence_complete_0_mux0003
    );
  sequence_complete_56_not0001_SW0 : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => compteur(2),
      I1 => N4,
      I2 => compteur(3),
      I3 => compteur(1),
      O => N56
    );
  bus_color_futur_1_1 : LUT4
    generic map(
      INIT => X"AAAB"
    )
    port map (
      I0 => reset_bus_color_110,
      I1 => bus_color_futur_or000011_42,
      I2 => bus_color_futur_or000023_43,
      I3 => bus_color_futur_or0001,
      O => bus_color_futur(1)
    );
  compteur_1_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(1),
      G => compteur_not0001,
      Q => compteur_1_1_52
    );
  compteur_2_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => compteur_mux0003(2),
      G => compteur_not0001,
      Q => compteur_2_1_54
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_50
    );
  reset_bus_color_not00011_INV_0 : INV
    port map (
      I => etat_present_FSM_FFd2_96,
      O => reset_bus_color_not0001
    );
  etat_present_FSM_FFd2_In104 : MUXF5
    port map (
      I0 => N82,
      I1 => N83,
      S => etat_present_FSM_FFd1_94,
      O => etat_present_FSM_FFd2_In
    );
  etat_present_FSM_FFd2_In104_F : LUT4
    generic map(
      INIT => X"0040"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_system_2_IBUF_109,
      I2 => etat_system_1_IBUF_108,
      I3 => etat_system_0_IBUF_107,
      O => N82
    );
  etat_present_FSM_FFd2_In104_G : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => etat_present_FSM_FFd2_96,
      I1 => etat_present_FSM_FFd2_In64_101,
      I2 => reset_bus_color_110,
      O => N83
    );
  XST_VCC : VCC
    port map (
      P => N84
    );
  bus_color_futur_0_1 : LUT4
    generic map(
      INIT => X"0302"
    )
    port map (
      I0 => bus_color_futur_or0001,
      I1 => bus_color_futur_or000011_42,
      I2 => bus_color_futur_or000023_43,
      I3 => etat_present_FSM_FFd2_In55,
      O => bus_color_futur_0_1_38
    );
  bus_color_futur_0_f5 : MUXF5
    port map (
      I0 => bus_color_futur_0_1_38,
      I1 => N84,
      S => reset_bus_color_110,
      O => bus_color_futur(0)
    );
  compteur_mux0003_4_SW0 : LUT2_L
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => compteur(2),
      I1 => compteur(1),
      LO => N62
    );
  Madd_compteur_addsub0000_cy_4_11 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(3),
      I2 => compteur(2),
      I3 => compteur(1),
      LO => N85,
      O => Madd_compteur_addsub0000_cy(4)
    );
  compteur_mux0003_1_112 : LUT4_D
    generic map(
      INIT => X"1FFF"
    )
    port map (
      I0 => compteur_2_1_54,
      I1 => compteur_1_1_52,
      I2 => compteur(5),
      I3 => compteur(4),
      LO => N86,
      O => compteur_mux0003_1_112_69
    );
  compteur_mux0003_1_129 : LUT4_D
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => compteur_mux0003_1_127_70,
      I1 => compteur(6),
      I2 => compteur(3),
      I3 => compteur_mux0003_1_112_69,
      LO => N87,
      O => N13
    );
  compteur_mux0003_7_SW0_SW0 : LUT2_L
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => compteur(6),
      I1 => compteur(5),
      LO => N66
    );
  compteur_mux0003_2_1_SW0 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => compteur(4),
      I1 => compteur(5),
      I2 => compteur(3),
      I3 => compteur(6),
      LO => N88,
      O => N78
    );
  compteur_mux0003_1_127 : LUT3_D
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => etat_present_FSM_FFd1_94,
      I1 => etat_present_FSM_FFd2_96,
      I2 => compteur(7),
      LO => N89,
      O => compteur_mux0003_1_127_70
    );

end Structure;

