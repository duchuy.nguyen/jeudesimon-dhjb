force clk 0 0, 1 1 -repeat 2
# une séquence de 2
force rst 1 0, 0 5
# valeur 1  au temps zér, valeur 0 au temps 5

# première couleure
force couleur_aleatoire  1111 7

force etat_system 110 0

# première couleure
force couleur_aleatoire  0001 10

force etat_system 000 22

#force couleur_aleatoire  1111 20

force etat_system 110 23

# deuxième couleure
force couleur_aleatoire  0010 25

force etat_system 000 27

force etat_system 110 30

# troisième couleure
force couleur_aleatoire  0100 50

force etat_system 000 55

force etat_system 110 57

force couleur_aleatoire  1111 60

# quatrième couleure
force couleur_aleatoire  1000 100

force rst 1 110, 0 120

run 200
