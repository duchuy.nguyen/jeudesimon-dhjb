-- commentaire
LIBRARY ieee;                -- bibliotheque a charger
USE ieee.std_logic_1164.all; -- utilisation des definitions std_logic
use ieee.numeric_std.all;



ENTITY storage IS  -- calculer le score et le convertir en BCD
   PORT(
        -- declaration des entrées
        etat_system          : in std_logic_vector(2 downto 0);
        couleur_jouer        : in std_logic_vector(3 downto 0);
        couleur_aleatoire    : in std_logic_vector(3 downto 0);
        clk                  : in std_logic;
        rst                  : in std_logic;



        -- declaration des sorties
        sequence_complete : out std_logic_vector(59 downto 0);
        compteur_couleur  : out std_logic_vector(7 downto 0)
    );

END ENTITY storage;

-- Entity est juste :)

ARCHITECTURE storage_arch OF storage IS

Type etats is (IDLE, get_value, stock_value, reset);
signal etat_present, etat_futur : etats;
signal bus_color_present, bus_color_futur : std_logic_vector(2 downto 0);
signal compteur: integer range 0 to 150 := 0;
signal reset_bus_color: std_logic := '0';

BEGIN

    registre : process(clk,rst)
    begin
        if (rst = '1') then
            etat_present <= reset;
            bus_color_present <= "111";
        elsif rising_edge(clk) then
            etat_present <= etat_futur;
            bus_color_present <= bus_color_futur;

        end if;
    end process;

    -- le clk est utilise uniquement sur le process de registre (C'est plus compréhensible dans le code)
    couleure : process (couleur_aleatoire, couleur_jouer, reset_bus_color) is
    begin
    -- il faut préparer l'état suivant
    -- celui-ci sera attribuer au coup de clock
        if (reset_bus_color = '1') then
            bus_color_futur <= "111";
        elsif (couleur_aleatoire ="0001" or couleur_jouer ="0001" ) then                  --blue
            bus_color_futur <= "000";
        elsif (couleur_aleatoire ="0010" or couleur_jouer ="0010") then              -- vert
            bus_color_futur <= "001";
        elsif (couleur_aleatoire ="0100" or couleur_jouer ="0100") then              -- jaune
            bus_color_futur <= "010";
        elsif (couleur_aleatoire ="1000" or couleur_jouer ="1000") then              -- rouge
            bus_color_futur <= "011";
        else
            bus_color_futur <= "111";    --couleur neutre
        end if;

    end process couleure;

    etatCombi: process(etat_system, bus_color_futur,etat_present)
    begin
        case( etat_present ) is
            when IDLE =>

                if (etat_system = "110") then   -- phase d'écoute
                    etat_futur <= get_value;
                else
                    etat_futur <= IDLE;

                end if;

            when get_value =>
                if (bus_color_futur /= "111") then    -- il y a un changement
                    etat_futur <= stock_value;
                else
                    etat_futur <= get_value;               -- on attend
                end if ;

            when stock_value =>
                if (etat_system = "110") then
                    etat_futur <= stock_value;
                else
                    etat_futur <= IDLE;
                end if;

            when reset =>
                etat_futur <= IDLE;

            when others => etat_futur <= IDLE;

        end case;

    end process;

    output_sequence: process(etat_present)
    begin
      if (etat_present = reset) then
          sequence_complete <= (others => '0');
          compteur <= 0;
      elsif (etat_present = IDLE) then
          reset_bus_color <= '0';

      elsif (etat_present = stock_value ) then
          sequence_complete((compteur + 1) downto compteur ) <= bus_color_present(1 downto 0);
          if (compteur <= 120) then
              compteur <= compteur + 2;
          else
              compteur <= 0;
          end if;
          reset_bus_color <= '1';

      end if;
    end process;

    compteur_couleur <= std_logic_vector(to_unsigned(compteur, compteur_couleur'length));

END ARCHITECTURE storage_arch;
