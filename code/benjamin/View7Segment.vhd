-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Vonlanthen Benjamin
-- Date        : 05.05.2021
-- Version     : 1.0
-- Description : Ce composant permet d'afficher le mode de jeu, le nombre de joueurs ou les scores
--               sur les 7 ségments en fonction de l'état du système.



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity View7Segment is
    Port (  Etat_systeme : in  STD_LOGIC_VECTOR(2 downto 0);
            Score_J1 : in  std_logic_vector(7 downto 0);
            Score_J2 : in  std_logic_vector(7 downto 0);
            Score_J3 : in  std_logic_vector(7 downto 0);
            Score_J4 : in  std_logic_vector(7 downto 0);
            Mode_jeu : in  std_logic_vector(1 downto 0);
            Mode_nbr_joueur : in  std_logic_vector(2 downto 0);

            Segment_J1 : out  std_logic_vector(7 downto 0);
            Segment_J2 : out  std_logic_vector(7 downto 0);
            Segment_J3 : out  std_logic_vector(7 downto 0);
            Segment_J4 : out  std_logic_vector(7 downto 0));
end View7Segment;
architecture View7Segment_arch of View7Segment is
begin
    PROCESS (Etat_systeme, Score_J1, Score_J2, Score_J3, Score_J4, Mode_jeu, Mode_nbr_joueur)
    BEGIN
        IF Etat_systeme = "000" THEN --état choix du mode de jeu
            Segment_J1 <= "000000" & Mode_jeu;
            Segment_J2 <= "00000000";
            Segment_J3 <= "00000000";
            Segment_J4 <= "00000000";
        ELSIF Etat_systeme = "011" THEN --état choix du nombre de joueur
        Segment_J1 <= "00000" & Mode_nbr_joueur;
        Segment_J2 <= "00000000";
        Segment_J3 <= "00000000";
        Segment_J4 <= "00000000";
        else --sinon, on affiche les scores
            Segment_J1 <= Score_J1;
            Segment_J2 <= Score_J2;
            Segment_J3 <= Score_J3;
            Segment_J4 <= Score_J4;
        END IF;
    END PROCESS;
end View7Segment_arch;