#Etat de base
force etat_systeme(2) 0 0
force etat_systeme(1) 0 0
force etat_systeme(0) 0 0

force mode_nbr_joueur(2) 0 0
force mode_nbr_joueur(1) 0 0
force mode_nbr_joueur(0) 0 0

force evaluer(1) 0 0
force evaluer(0) 0 0

#Etat apr�s le choix du nombre de joueur
force mode_nbr_joueur(2) 1 2

force etat_systeme(2) 1 4
force etat_systeme(0) 1 4

force evaluer(0) 1 6

force evaluer(0) 0 8

#Etat lors de la partie
force etat_systeme(1) 1 10

force mode_nbr_joueur(2) 0 12

force evaluer(0) 1 14

force evaluer(0) 0 16

force evaluer(1) 1 18

force evaluer(1) 0 20

force evaluer(0) 1 22

force evaluer(0) 0 24

force evaluer(1) 1 26

force evaluer(1) 0 28

force evaluer(0) 1 30

force evaluer(0) 0 32

run 40