-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Defferrard Julien
-- Date        : 06.05.2021
-- Version     : 1.0
-- Description : Ce composant permet de gérer le commutateur rotatif

-- https://www.conrad.fr/p/codeur-iduino-1485328-1-pcs-1485328?searchSuggest=product&searchTerm=1485328&searchType=suggest
-- https://asset.conrad.com/media10/add/160267/c1/-/en/001485328DS01/fiche-technique-1485328-codeur-iduino-1485328-1-pcs.pdf

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Rotary is
    Port (
      cl, dt, rst, clk: in STD_LOGIC;
      rotary: out STD_LOGIC_VECTOR(1 downto 0)
    );
end Rotary;

architecture Architecture_Rotary of Rotary is
    type etat is (neutre, clockwise, counterclockwise, success);
    signal etat_present, etat_futur : etat;
    signal counter : INTEGER range 0 to 2 := 0;
    signal direction : INTEGER range -1 to 1 := 0;
begin
  process (clk, rst)
  begin
    if rst = '1' then
      etat_present <= neutre;
    elsif rising_edge(clk) then
      etat_present <= etat_futur;
    end if;
  end process;

  process (etat_present, cl, dt)
  begin
    case etat_present is
      when neutre =>
        if cl = '0' then
          etat_futur <= clockwise;
        elsif dt = '0' then
          etat_futur <= counterclockwise;
        else
          etat_futur <= neutre;
        end if;
        direction <= 0;
      when clockwise =>
        direction <= 1;
        if dt = '0' then
          etat_futur <= success;
        elsif cl = '1' then
          etat_futur <= neutre;
          direction <= 0;
        else
          etat_futur <= clockwise;
        end if;
      when counterclockwise =>
        direction <= -1;
        if cl = '0' then
          etat_futur <= success;
        elsif dt = '1' then
          etat_futur <= neutre;
          direction <= 0;
        else
          etat_futur <= counterclockwise;
        end if;
      when success =>
        etat_futur <= neutre;
        direction <= 0;
      when others => etat_futur <= neutre;
    end case;
  end process;

  process (etat_present)
  begin
    if etat_present = success then
      if (counter /= 0 and direction = -1) or (counter /= 2 and direction = 1) then
       counter <= counter + direction;
      end if;
    else
      counter <= counter;
    end if;
  end process;

  rotary <= conv_std_logic_vector(counter, 2);
end Architecture_Rotary;
