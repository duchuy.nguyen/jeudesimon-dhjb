# Input Clock
force clk 0 0, 1 1 -repeat 2
force rst 1 0, 0 1
force btn_rouge 0 0
force btn_jaune 0 0
force btn_bleu 0 0
force btn_vert 0 0
force btn_mode 0 0
force btn_start 0 0
force com_options 00 0
force id_joueur 00 0
force joueur_vivant 000 0

# Test jeu standard
force btn_start 1 10, 0 12
force btn_bleu 1 26, 0 28

force btn_bleu 1 42, 0 44
force btn_bleu 1 46, 0 48

force btn_bleu 1 62, 0 64
force btn_bleu 1 68, 0 70
force btn_vert 1 72, 0 74

run 100