-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            David Rossy
-- 
-- Create Date:         18:34:00 06/03/2021 
-- Design Name: 
-- Module Name:         ValidationBoutonEtat - ValidationBoutonEtat_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions: 
-- Version:             1.4
-- Description:         Ce composant permet stabiliser la sortie d'un bouton anti-rebond
--                      En appuyant 1 fois dessus on obtient un '1'
--                      En appuyant 2 fois dessus on obtient un '0'
--                      En appuyant 3 fois dessus on obtient un '1'
--                      etc.
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- --------------------------------------------------------------------------------
entity ValidationBoutonEtat is
    Port (
            clk             : in  STD_LOGIC;
            rst             : in  STD_LOGIC;
            btn             : in  STD_LOGIC;
            etat_actif      : in  STD_LOGIC_VECTOR (2 downto 0);
            etat_systeme    : in  STD_LOGIC_VECTOR (2 downto 0);
            validation      : out STD_LOGIC
         );
end ValidationBoutonEtat;
-- --------------------------------------------------------------------------------
architecture ValidationBoutonEtat_arch of ValidationBoutonEtat is
    --Représente les etats possibles de la sortie validation
    --Abreviation : ZST => Zero Stable, ZNST => Zero Non Stable, UST => Un Stable, ...
    type etat is (ZST, ZNST, UST, UNST);
    signal etat_actuel : etat;
    
begin
    registre : process (clk, rst, etat_systeme)
    begin
        if rst = '1' then
            etat_actuel <= ZST;
        elsif rising_edge (clk) then
            -- Ne se declenche que lorsque l'etat du systeme correspond a l'etat actif
            -- Sinon etat_actuel ne change pas (ZST par defaut)
            if etat_actif = etat_systeme then
                if btn = '1' then
                    case etat_actuel is
                        when ZST    => etat_actuel <= UNST;
                        when ZNST   => etat_actuel <= ZNST;
                        when UST    => etat_actuel <= ZNST;
                        when UNST   => etat_actuel <= UNST;
                        when others => etat_actuel <= ZST;
                    end case;
                else
                    case etat_actuel is
                        when ZST    => etat_actuel <= ZST;
                        when ZNST   => etat_actuel <= ZST;
                        when UST    => etat_actuel <= UST;
                        when UNST   => etat_actuel <= UST;
                        when others => etat_actuel <= ZST;
                    end case;
                end if;
            end if;
        end if;
    end process registre;
    

    sortie : process (etat_actuel)
    begin
        case etat_actuel is
            when ZST    => validation <= '0';
            when ZNST   => validation <= '0';
            when UST    => validation <= '1';
            when UNST   => validation <= '1';
            when others => validation <= '0';
        end case;
    end process sortie;


end architecture ValidationBoutonEtat_arch;
-- --------------------------------------------------------------------------------