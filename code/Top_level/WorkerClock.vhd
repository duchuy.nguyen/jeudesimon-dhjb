-- --------------------------------------------------------------------------------
-- Company:           HEIA-FR
-- Engineer:          David Rossy
-- 
-- Create Date:       16:05:35 05/13/2,021 
-- Design Name:     
-- Module Name:       WorkerClock - WorkerClock_arch
-- Project Name:      Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions:
-- Version:           1.4
-- Description:       Ce composant permet de transformer le signal d'une horloge de base (intégrée à 65 KHz)
--                    en une horloge à 2Hz (une impulsion envoyée toutes les 500ms).
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
-- --------------------------------------------------------------------------------
entity WorkerClock is
    Port (clk      : in   STD_LOGIC;
          rst      : in   STD_LOGIC;
          clk_2hz  : out  STD_LOGIC);
end WorkerClock;
-- --------------------------------------------------------------------------------
architecture WorkerClock_arch of WorkerClock is
  signal compteur   : unsigned(15 downto 0);
  signal impulsion  : std_logic;

begin
    -- --------------------------------------------------
    compte : process (clk, rst)
      begin
        if rst = '1' then
          compteur <= (others => '0');
          impulsion <= '0';
        elsif rising_edge(clk) then
          if compteur = "0111111011110100" then -- 0111'1110'1111'0100 correspong à 32500 en binaire
            compteur <= (others => '0');
            impulsion <= '1';
          else
            compteur <= compteur + "0000000000000001";
            impulsion <= '0';
          end if;
        end if;
      end process;
      -- --------------------------------------------------


    clk_2hz <= impulsion;

    
end architecture WorkerClock_arch;
-- --------------------------------------------------------------------------------