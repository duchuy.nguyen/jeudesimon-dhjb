--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: M.81d
--  \   \         Application: netgen
--  /   /         Filename: top_level_synthesis.vhd
-- /___/   /\     Timestamp: Sun Jun 20 09:54:49 2021
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm top_level -w -dir netgen/synthesis -ofmt vhdl -sim top_level.ngc top_level_synthesis.vhd 
-- Device	: xc3s50a-5-tq144
-- Input file	: top_level.ngc
-- Output file	: C:\Users\eia\Documents\Technum\Projets\Top_level\netgen\synthesis\top_level_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: top_level
-- Xilinx	: C:\Xilinx\12.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity top_level is
  port (
    clk : in STD_LOGIC := 'X'; 
    buzzer : out STD_LOGIC; 
    btn_rouge : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    btn_bleu : in STD_LOGIC := 'X'; 
    led_rouge : out STD_LOGIC; 
    led_vert : out STD_LOGIC; 
    btn_jaune : in STD_LOGIC := 'X'; 
    btn_start : in STD_LOGIC := 'X'; 
    led_jaune : out STD_LOGIC; 
    btn_vert : in STD_LOGIC := 'X'; 
    btn_mode : in STD_LOGIC := 'X'; 
    led_bleu : out STD_LOGIC; 
    segment_j1 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    segment_j2 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    segment_j3 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    segment_j4 : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    com_options : in STD_LOGIC_VECTOR ( 1 downto 0 ) 
  );
end top_level;

architecture Structure of top_level is
  signal ColorHandler_couleur_futur_mux0001_0_1 : STD_LOGIC; 
  signal ColorHandler_couleur_futur_mux0001_1_1 : STD_LOGIC; 
  signal ColorHandler_couleur_futur_mux0001_2_1 : STD_LOGIC; 
  signal ColorHandler_couleur_futur_mux0001_3_1 : STD_LOGIC; 
  signal GameHandler_Mrom_mode_nbr_joueur : STD_LOGIC; 
  signal GameHandler_Mrom_mode_nbr_joueur2 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_N0 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_N7 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_0_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_3_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_4_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_5_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_6_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_12 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_121_23 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_122_24 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_24_25 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_21_27 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_36_28 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_42_29 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_1_31 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_16_32 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_21_33 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_9_34 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_3_Q_35 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29_37 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_present_0_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_present_3_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_present_4_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_present_5_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_etat_present_6_Q : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_reinit_43 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_reinit_and0000 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_In : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd2_47 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_49 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_In : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd2_51 : STD_LOGIC; 
  signal GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000 : STD_LOGIC; 
  signal GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53 : STD_LOGIC; 
  signal GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_In : STD_LOGIC; 
  signal GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd2_55 : STD_LOGIC; 
  signal GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56 : STD_LOGIC; 
  signal GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_In : STD_LOGIC; 
  signal GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58 : STD_LOGIC; 
  signal GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_In : STD_LOGIC; 
  signal GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd3_60 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_10 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_101 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_3_f7_78 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_3_f71 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_4_f6_80 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_4_f61 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_4_f7_82 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_4_f71 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f5_84 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f51 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f5_0_rt_86 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f5_0_rt1_87 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f5_rt_88 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f5_rt1_89 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f6_90 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f61 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f62 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_5_f63 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f5_94 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f51 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f52 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f53 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f54 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f55 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f6_100 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_6_f61 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_71 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_72 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_73 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_75 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_76 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_77 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f5_108 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f51 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f52 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f53 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f54 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_7_f55 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_8 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_81 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_810 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_811 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_82 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_83 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_84 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_85 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_86 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_87 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_88 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_89 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_8_f5_126 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_8_f51 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_9 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_91 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_92 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_93 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_94 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_95 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_96 : STD_LOGIC; 
  signal Listener_Mmux_color_comparante_mux0001_97 : STD_LOGIC; 
  signal Listener_N0 : STD_LOGIC; 
  signal Listener_N11 : STD_LOGIC; 
  signal Listener_N18 : STD_LOGIC; 
  signal Listener_color_comparante_cmp_lt0000 : STD_LOGIC; 
  signal Listener_compteur_local_int_cmp_eq0001 : STD_LOGIC; 
  signal Listener_compteur_local_int_not0001 : STD_LOGIC; 
  signal Listener_counter_reset_170 : STD_LOGIC; 
  signal Listener_counter_reset_mux0003 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne0001 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne000113_177 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne000126_178 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne000133_179 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne000154_180 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne00017_181 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne000173 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne0001731_183 : STD_LOGIC; 
  signal Listener_etat_futur_cmp_ne0001732_184 : STD_LOGIC; 
  signal Listener_etat_futur_mux0005_2_2_188 : STD_LOGIC; 
  signal Listener_etat_futur_mux0005_2_34_SW0 : STD_LOGIC; 
  signal Listener_etat_futur_mux0005_2_34_SW01_190 : STD_LOGIC; 
  signal Listener_etat_futur_not0001 : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal N108 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal N116 : STD_LOGIC; 
  signal N122 : STD_LOGIC; 
  signal N124 : STD_LOGIC; 
  signal N126 : STD_LOGIC; 
  signal N132 : STD_LOGIC; 
  signal N148 : STD_LOGIC; 
  signal N152 : STD_LOGIC; 
  signal N154 : STD_LOGIC; 
  signal N156 : STD_LOGIC; 
  signal N158 : STD_LOGIC; 
  signal N162 : STD_LOGIC; 
  signal N164 : STD_LOGIC; 
  signal N166 : STD_LOGIC; 
  signal N180 : STD_LOGIC; 
  signal N184 : STD_LOGIC; 
  signal N188 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal N190 : STD_LOGIC; 
  signal N192 : STD_LOGIC; 
  signal N196 : STD_LOGIC; 
  signal N198 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal N200 : STD_LOGIC; 
  signal N204 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N214 : STD_LOGIC; 
  signal N218 : STD_LOGIC; 
  signal N219 : STD_LOGIC; 
  signal N220 : STD_LOGIC; 
  signal N221 : STD_LOGIC; 
  signal N222 : STD_LOGIC; 
  signal N223 : STD_LOGIC; 
  signal N224 : STD_LOGIC; 
  signal N225 : STD_LOGIC; 
  signal N226 : STD_LOGIC; 
  signal N227 : STD_LOGIC; 
  signal N228 : STD_LOGIC; 
  signal N229 : STD_LOGIC; 
  signal N23 : STD_LOGIC; 
  signal N230 : STD_LOGIC; 
  signal N231 : STD_LOGIC; 
  signal N232 : STD_LOGIC; 
  signal N233 : STD_LOGIC; 
  signal N234 : STD_LOGIC; 
  signal N235 : STD_LOGIC; 
  signal N236 : STD_LOGIC; 
  signal N237 : STD_LOGIC; 
  signal N238 : STD_LOGIC; 
  signal N239 : STD_LOGIC; 
  signal N240 : STD_LOGIC; 
  signal N241 : STD_LOGIC; 
  signal N242 : STD_LOGIC; 
  signal N243 : STD_LOGIC; 
  signal N244 : STD_LOGIC; 
  signal N245 : STD_LOGIC; 
  signal N246 : STD_LOGIC; 
  signal N247 : STD_LOGIC; 
  signal N25 : STD_LOGIC; 
  signal N27 : STD_LOGIC; 
  signal N29 : STD_LOGIC; 
  signal N31 : STD_LOGIC; 
  signal N33 : STD_LOGIC; 
  signal N62 : STD_LOGIC; 
  signal N82 : STD_LOGIC; 
  signal N84 : STD_LOGIC; 
  signal N85 : STD_LOGIC; 
  signal N96 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal PlayerHandler_Joueur1_0_mux0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur1_0_not0001 : STD_LOGIC; 
  signal PlayerHandler_Joueur2_0_and0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur2_0_and0001 : STD_LOGIC; 
  signal PlayerHandler_Joueur2_0_cmp_eq0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur2_0_mux0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_and0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_and0001 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_cmp_eq0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_mux0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_mux000044 : STD_LOGIC; 
  signal PlayerHandler_Joueur3_0_mux0000441_290 : STD_LOGIC; 
  signal PlayerHandler_Joueur4_0_and0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur4_0_and0001 : STD_LOGIC; 
  signal PlayerHandler_Joueur4_0_cmp_eq0000 : STD_LOGIC; 
  signal PlayerHandler_Joueur4_0_mux0000_295 : STD_LOGIC; 
  signal PlayerHandler_JoueurEnCours_cmp_eq0000 : STD_LOGIC; 
  signal PlayerHandler_JoueurEnCours_not0000 : STD_LOGIC; 
  signal PlayerHandler_Mmux_JoueurEnCours_mux0007_3_302 : STD_LOGIC; 
  signal PlayerHandler_Mmux_JoueurEnCours_mux0007_31_303 : STD_LOGIC; 
  signal PlayerHandler_Mmux_JoueurEnCours_mux0007_4_304 : STD_LOGIC; 
  signal PlayerHandler_Mmux_JoueurEnCours_mux0007_41_305 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_10 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_101 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_3_f7_364 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_3_f71 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_4_f6_366 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_4_f61 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_4_f7_368 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_4_f71 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_370 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f51 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt_372 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt1_373 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt_374 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt1_375 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_376 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f61 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f62 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_5_f63 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_380 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f51 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f52 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f53 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f54 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f55 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f6_386 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_6_f61 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_71 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_72 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_73 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_75 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_76 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_77 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_394 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f51 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f52 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f53 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f54 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_7_f55 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_8 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_81 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_810 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_811 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_82 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_83 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_84 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_85 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_86 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_87 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_88 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_89 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_8_f5_412 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_8_f51 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_9 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_91 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_92 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_93 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_94 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_95 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_96 : STD_LOGIC; 
  signal SequenceHandler_Mmux_couleur_temp_mux0001_97 : STD_LOGIC; 
  signal SequenceHandler_N111 : STD_LOGIC; 
  signal SequenceHandler_N14 : STD_LOGIC; 
  signal SequenceHandler_N20 : STD_LOGIC; 
  signal SequenceHandler_N21 : STD_LOGIC; 
  signal SequenceHandler_N24 : STD_LOGIC; 
  signal SequenceHandler_N25 : STD_LOGIC; 
  signal SequenceHandler_N3 : STD_LOGIC; 
  signal SequenceHandler_N30 : STD_LOGIC; 
  signal SequenceHandler_N41 : STD_LOGIC; 
  signal SequenceHandler_Result_0_1 : STD_LOGIC; 
  signal SequenceHandler_Result_1_1 : STD_LOGIC; 
  signal SequenceHandler_Result_2_1 : STD_LOGIC; 
  signal SequenceHandler_Result_3_1 : STD_LOGIC; 
  signal SequenceHandler_compteur_demonstration_and0000 : STD_LOGIC; 
  signal SequenceHandler_compteur_demonstration_and0001 : STD_LOGIC; 
  signal SequenceHandler_compteur_ecouter_mux0003_5_33_460 : STD_LOGIC; 
  signal SequenceHandler_compteur_ecouter_mux0003_7_11_463 : STD_LOGIC; 
  signal SequenceHandler_compteur_ecouter_mux0003_7_17_464 : STD_LOGIC; 
  signal SequenceHandler_compteur_ecouter_not0001 : STD_LOGIC; 
  signal SequenceHandler_compteur_sequence_and0000 : STD_LOGIC; 
  signal SequenceHandler_couleur_temp_cmp_lt0000 : STD_LOGIC; 
  signal SequenceHandler_d_couleur_futur_mux0002_0_1_492 : STD_LOGIC; 
  signal SequenceHandler_d_couleur_futur_mux0002_1_1_494 : STD_LOGIC; 
  signal SequenceHandler_d_couleur_futur_mux0002_2_1_496 : STD_LOGIC; 
  signal SequenceHandler_d_couleur_futur_mux0002_3_1_498 : STD_LOGIC; 
  signal SequenceHandler_d_couleur_futur_or0000 : STD_LOGIC; 
  signal SequenceHandler_delay_cmp_ge0000 : STD_LOGIC; 
  signal SequenceHandler_delay_mux0001_2_1_512 : STD_LOGIC; 
  signal SequenceHandler_delay_not0001_514 : STD_LOGIC; 
  signal SequenceHandler_e_couleur_futur_mux0003_1_1 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_mux0004_0_13_534 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_mux0004_0_16_535 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_mux0004_4_52_540 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_mux0004_4_62_541 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_not0001 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_not000112_543 : STD_LOGIC; 
  signal SequenceHandler_etat_futur_not000136_544 : STD_LOGIC; 
  signal SequenceHandler_etat_present_Acst_inv : STD_LOGIC; 
  signal SequenceHandler_etat_systeme_temp_0_0_not0000 : STD_LOGIC; 
  signal SequenceHandler_s_couleur_futur_cmp_le0000 : STD_LOGIC; 
  signal SequenceHandler_s_couleur_futur_not0001 : STD_LOGIC; 
  signal Storage_N0 : STD_LOGIC; 
  signal Storage_N11 : STD_LOGIC; 
  signal Storage_N12 : STD_LOGIC; 
  signal Storage_N13 : STD_LOGIC; 
  signal Storage_N14 : STD_LOGIC; 
  signal Storage_N15 : STD_LOGIC; 
  signal Storage_N16 : STD_LOGIC; 
  signal Storage_N17 : STD_LOGIC; 
  signal Storage_N18 : STD_LOGIC; 
  signal Storage_N19 : STD_LOGIC; 
  signal Storage_N20 : STD_LOGIC; 
  signal Storage_N21 : STD_LOGIC; 
  signal Storage_N26 : STD_LOGIC; 
  signal Storage_N35 : STD_LOGIC; 
  signal Storage_N36 : STD_LOGIC; 
  signal Storage_N38 : STD_LOGIC; 
  signal Storage_N4 : STD_LOGIC; 
  signal Storage_N5 : STD_LOGIC; 
  signal Storage_N56 : STD_LOGIC; 
  signal Storage_N6 : STD_LOGIC; 
  signal Storage_N62 : STD_LOGIC; 
  signal Storage_N7 : STD_LOGIC; 
  signal Storage_bus_color_futur_0_15_593 : STD_LOGIC; 
  signal Storage_bus_color_futur_0_51 : STD_LOGIC; 
  signal Storage_bus_color_futur_0_70 : STD_LOGIC; 
  signal Storage_bus_color_futur_1_15_597 : STD_LOGIC; 
  signal Storage_bus_color_futur_1_39_598 : STD_LOGIC; 
  signal Storage_bus_color_futur_cmp_eq0005 : STD_LOGIC; 
  signal Storage_compteur_mux0003_1_112_610 : STD_LOGIC; 
  signal Storage_compteur_mux0003_1_116_611 : STD_LOGIC; 
  signal Storage_compteur_not0001 : STD_LOGIC; 
  signal Storage_etat_present_FSM_FFd1_619 : STD_LOGIC; 
  signal Storage_etat_present_FSM_FFd1_In : STD_LOGIC; 
  signal Storage_etat_present_FSM_FFd2_621 : STD_LOGIC; 
  signal Storage_etat_present_FSM_FFd2_In : STD_LOGIC; 
  signal Storage_etat_present_FSM_FFd2_In56_623 : STD_LOGIC; 
  signal Storage_etat_present_cmp_eq0003 : STD_LOGIC; 
  signal Storage_reset_bus_color_625 : STD_LOGIC; 
  signal Storage_reset_bus_color_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_0_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_0_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_10_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_10_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_11_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_12_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_12_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_13_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_13_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_14_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_14_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_15_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_16_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_16_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_16_or0000 : STD_LOGIC; 
  signal Storage_sequence_complete_17_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_17_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_18_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_18_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_19_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_1_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_1_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_20_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_20_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_21_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_21_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_22_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_22_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_23_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_24_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_24_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_25_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_25_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_26_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_26_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_27_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_28_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_28_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_29_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_29_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_2_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_2_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_30_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_30_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_31_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_32_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_32_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_33_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_34_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_34_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_35_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_36_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_36_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_36_or0000 : STD_LOGIC; 
  signal Storage_sequence_complete_37_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_37_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_38_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_38_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_39_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_3_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_40_cmp_eq0001 : STD_LOGIC; 
  signal Storage_sequence_complete_40_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_40_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_40_or0000 : STD_LOGIC; 
  signal Storage_sequence_complete_41_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_41_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_42_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_42_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_43_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_44_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_44_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_45_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_45_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_46_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_46_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_47_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_48_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_48_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_49_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_49_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_4_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_4_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_50_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_50_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_51_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_52_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_52_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_53_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_53_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_54_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_54_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_55_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_56_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_56_not0001_773 : STD_LOGIC; 
  signal Storage_sequence_complete_57_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_58_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_58_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_59_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_59_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_5_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_5_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_6_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_6_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_7_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_8_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_8_not0001 : STD_LOGIC; 
  signal Storage_sequence_complete_9_mux0003 : STD_LOGIC; 
  signal Storage_sequence_complete_9_not0001 : STD_LOGIC; 
  signal View7Segments_Segment_J1_1_1 : STD_LOGIC; 
  signal View7Segments_Segment_J1_1_11_796 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_1_rt_809 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_2_rt_811 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_3_rt_813 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_4_rt_815 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_5_rt_817 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_6_rt_819 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_7_rt_821 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy_8_rt_823 : STD_LOGIC; 
  signal ViewSound_Madd_compteur_futur_addsub0000_xor_9_rt_825 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_4_rt_831 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_0_Q_834 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_1_Q_835 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_2_Q_836 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_3_Q_837 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_5_Q_838 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_6_Q_839 : STD_LOGIC; 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_7_Q_840 : STD_LOGIC; 
  signal ViewSound_buzzer12_841 : STD_LOGIC; 
  signal ViewSound_buzzer_time_1_Q : STD_LOGIC; 
  signal ViewSound_buzzer_time_2_Q : STD_LOGIC; 
  signal ViewSound_buzzer_time_3_Q : STD_LOGIC; 
  signal ViewSound_buzzer_time_4_Q : STD_LOGIC; 
  signal ViewSound_buzzer_time_6_Q : STD_LOGIC; 
  signal ViewSound_compteur_futur_0_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_1_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_2_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_3_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_4_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_5_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_6_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_7_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_8_norst : STD_LOGIC; 
  signal ViewSound_compteur_futur_9_norst : STD_LOGIC; 
  signal ViewSound_etat_futur : STD_LOGIC; 
  signal ViewSound_fin_comptage : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_10_rt_872 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_11_rt_874 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_12_rt_876 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_13_rt_878 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_14_rt_880 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_1_rt_882 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_2_rt_884 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_3_rt_886 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_4_rt_888 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_5_rt_890 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_6_rt_892 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_7_rt_894 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_8_rt_896 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_cy_9_rt_898 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_0 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_1 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_10 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_11 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_12 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_13 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_14 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_15 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_2 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_3 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_4 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_5 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_6 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_7 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_8 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_eqn_9 : STD_LOGIC; 
  signal WorkerClock_Mcount_compteur_xor_15_rt_916 : STD_LOGIC; 
  signal WorkerClock_compteur_cmp_eq0000 : STD_LOGIC; 
  signal WorkerClock_compteur_cmp_eq000016_934 : STD_LOGIC; 
  signal WorkerClock_compteur_cmp_eq000033_935 : STD_LOGIC; 
  signal WorkerClock_compteur_cmp_eq00004_936 : STD_LOGIC; 
  signal WorkerClock_compteur_cmp_eq000041_937 : STD_LOGIC; 
  signal WorkerClock_impulsion_938 : STD_LOGIC; 
  signal WorkerScore_N01 : STD_LOGIC; 
  signal WorkerScore_N11 : STD_LOGIC; 
  signal WorkerScore_N2 : STD_LOGIC; 
  signal WorkerScore_N21 : STD_LOGIC; 
  signal WorkerScore_N23 : STD_LOGIC; 
  signal WorkerScore_N33 : STD_LOGIC; 
  signal WorkerScore_N34 : STD_LOGIC; 
  signal WorkerScore_N35 : STD_LOGIC; 
  signal WorkerScore_N36 : STD_LOGIC; 
  signal WorkerScore_N37 : STD_LOGIC; 
  signal WorkerScore_N38 : STD_LOGIC; 
  signal WorkerScore_N39 : STD_LOGIC; 
  signal WorkerScore_N40 : STD_LOGIC; 
  signal WorkerScore_N41 : STD_LOGIC; 
  signal WorkerScore_N42 : STD_LOGIC; 
  signal WorkerScore_N57 : STD_LOGIC; 
  signal WorkerScore_N58 : STD_LOGIC; 
  signal WorkerScore_N59 : STD_LOGIC; 
  signal WorkerScore_N60 : STD_LOGIC; 
  signal WorkerScore_etat_present_FSM_FFd1_958 : STD_LOGIC; 
  signal WorkerScore_etat_present_FSM_FFd1_In : STD_LOGIC; 
  signal WorkerScore_etat_present_FSM_FFd2_960 : STD_LOGIC; 
  signal WorkerScore_etat_present_FSM_FFd2_In_961 : STD_LOGIC; 
  signal WorkerScore_joueur1_L_mux0004_0_2 : STD_LOGIC; 
  signal WorkerScore_joueur1_L_mux0004_2_1_970 : STD_LOGIC; 
  signal WorkerScore_joueur1_L_mux0004_3_1_972 : STD_LOGIC; 
  signal WorkerScore_joueur1_L_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur1_R_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur1_R_not000113_983 : STD_LOGIC; 
  signal WorkerScore_joueur2_L_mux0005_2_1_991 : STD_LOGIC; 
  signal WorkerScore_joueur2_L_mux0005_3_1_993 : STD_LOGIC; 
  signal WorkerScore_joueur2_L_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur2_R_mux0006_2_1_1002 : STD_LOGIC; 
  signal WorkerScore_joueur2_R_mux0006_3_1_1004 : STD_LOGIC; 
  signal WorkerScore_joueur2_R_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur3_L_mux0006_2_1_1013 : STD_LOGIC; 
  signal WorkerScore_joueur3_L_mux0006_3_1_1015 : STD_LOGIC; 
  signal WorkerScore_joueur3_L_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur3_R_mux0007_2_1_1024 : STD_LOGIC; 
  signal WorkerScore_joueur3_R_mux0007_3_1_1026 : STD_LOGIC; 
  signal WorkerScore_joueur3_R_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur4_L_mux0006_2_1_1035 : STD_LOGIC; 
  signal WorkerScore_joueur4_L_mux0006_3_1_1037 : STD_LOGIC; 
  signal WorkerScore_joueur4_L_not0001 : STD_LOGIC; 
  signal WorkerScore_joueur4_R_mux0007_2_1_1046 : STD_LOGIC; 
  signal WorkerScore_joueur4_R_mux0007_3_1_1048 : STD_LOGIC; 
  signal WorkerScore_joueur4_R_not0001 : STD_LOGIC; 
  signal WorkerScore_validation_1050 : STD_LOGIC; 
  signal WorkerScore_validation_mux0000 : STD_LOGIC; 
  signal WorkerScore_validation_or0000 : STD_LOGIC; 
  signal btn_bleu_IBUF_1054 : STD_LOGIC; 
  signal btn_jaune_IBUF_1056 : STD_LOGIC; 
  signal btn_mode_IBUF_1058 : STD_LOGIC; 
  signal btn_rouge_IBUF_1060 : STD_LOGIC; 
  signal btn_start_IBUF_1062 : STD_LOGIC; 
  signal btn_vert_IBUF_1064 : STD_LOGIC; 
  signal buzzer_OBUF_1066 : STD_LOGIC; 
  signal clk_BUFGP_1068 : STD_LOGIC; 
  signal com_options_0_IBUF_1071 : STD_LOGIC; 
  signal com_options_1_IBUF_1072 : STD_LOGIC; 
  signal led_bleu_OBUF_1078 : STD_LOGIC; 
  signal led_jaune_OBUF_1080 : STD_LOGIC; 
  signal led_rouge_OBUF_1082 : STD_LOGIC; 
  signal led_vert_OBUF_1084 : STD_LOGIC; 
  signal rst_IBUF_1086 : STD_LOGIC; 
  signal segment_j1_0_OBUF_1095 : STD_LOGIC; 
  signal segment_j1_1_OBUF_1096 : STD_LOGIC; 
  signal segment_j1_2_OBUF_1097 : STD_LOGIC; 
  signal segment_j1_3_OBUF_1098 : STD_LOGIC; 
  signal segment_j1_4_OBUF_1099 : STD_LOGIC; 
  signal segment_j1_5_OBUF_1100 : STD_LOGIC; 
  signal segment_j1_6_OBUF_1101 : STD_LOGIC; 
  signal segment_j1_7_OBUF_1102 : STD_LOGIC; 
  signal segment_j2_0_OBUF_1111 : STD_LOGIC; 
  signal segment_j2_1_OBUF_1112 : STD_LOGIC; 
  signal segment_j2_2_OBUF_1113 : STD_LOGIC; 
  signal segment_j2_3_OBUF_1114 : STD_LOGIC; 
  signal segment_j2_4_OBUF_1115 : STD_LOGIC; 
  signal segment_j2_5_OBUF_1116 : STD_LOGIC; 
  signal segment_j2_6_OBUF_1117 : STD_LOGIC; 
  signal segment_j2_7_OBUF_1118 : STD_LOGIC; 
  signal segment_j3_0_OBUF_1127 : STD_LOGIC; 
  signal segment_j3_1_OBUF_1128 : STD_LOGIC; 
  signal segment_j3_2_OBUF_1129 : STD_LOGIC; 
  signal segment_j3_3_OBUF_1130 : STD_LOGIC; 
  signal segment_j3_4_OBUF_1131 : STD_LOGIC; 
  signal segment_j3_5_OBUF_1132 : STD_LOGIC; 
  signal segment_j3_6_OBUF_1133 : STD_LOGIC; 
  signal segment_j3_7_OBUF_1134 : STD_LOGIC; 
  signal segment_j4_0_OBUF_1143 : STD_LOGIC; 
  signal segment_j4_1_OBUF_1144 : STD_LOGIC; 
  signal segment_j4_2_OBUF_1145 : STD_LOGIC; 
  signal segment_j4_3_OBUF_1146 : STD_LOGIC; 
  signal segment_j4_4_OBUF_1147 : STD_LOGIC; 
  signal segment_j4_5_OBUF_1148 : STD_LOGIC; 
  signal segment_j4_6_OBUF_1149 : STD_LOGIC; 
  signal segment_j4_7_OBUF_1150 : STD_LOGIC; 
  signal ColorHandler_couleur_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal ColorHandler_couleur_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Listener_Mcompar_color_comparante_cmp_lt0000_cy : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal Listener_Mcompar_color_comparante_cmp_lt0000_lut : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal Listener_bus_color_futur : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal Listener_bus_color_present : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal Listener_color_a_tester : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Listener_color_a_tester_mux0001 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Listener_color_comparante : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Listener_color_comparante_mux0001 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Listener_compteur_local_int : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal Listener_compteur_local_int_mux0004 : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal Listener_etat_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Listener_etat_futur_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Listener_etat_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Listener_timer_checker : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Listener_timer_checker_mux0002 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal PlayerHandler_Joueur1 : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal PlayerHandler_Joueur2 : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal PlayerHandler_Joueur3 : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal PlayerHandler_Joueur4 : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal PlayerHandler_JoueurEnCours : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal PlayerHandler_JoueurEnCours_mux0007 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Result : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_Mcompar_delay_cmp_ge0000_cy : STD_LOGIC_VECTOR ( 6 downto 1 ); 
  signal SequenceHandler_Mcompar_delay_cmp_ge0000_lut : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy : STD_LOGIC_VECTOR ( 6 downto 1 ); 
  signal SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_Result : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_compteur_demonstration : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_compteur_ecouter : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_compteur_ecouter_mux0003 : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_compteur_sequence : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal SequenceHandler_couleur_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_couleur_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_couleur_temp : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal SequenceHandler_couleur_temp_mux0001 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal SequenceHandler_d_couleur_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_d_couleur_futur_mux0002 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_d_couleur_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_delay : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_delay_mux0001 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_e_couleur_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_e_couleur_futur_mux0003 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_e_couleur_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_etat_futur : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal SequenceHandler_etat_futur_mux0004 : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal SequenceHandler_etat_present : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal SequenceHandler_etat_systeme_temp : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal SequenceHandler_s_couleur_futur : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_s_couleur_futur_mux0003 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal SequenceHandler_s_couleur_present : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Storage_Madd_compteur_addsub0000_cy : STD_LOGIC_VECTOR ( 4 downto 4 ); 
  signal Storage_bus_color_futur : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Storage_bus_color_present : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Storage_compteur : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal Storage_compteur_mux0003 : STD_LOGIC_VECTOR ( 7 downto 1 ); 
  signal Storage_sequence_complete : STD_LOGIC_VECTOR ( 59 downto 0 ); 
  signal ViewGame_couleur_futur : STD_LOGIC_VECTOR ( 5 downto 1 ); 
  signal ViewGame_couleur_present : STD_LOGIC_VECTOR ( 5 downto 1 ); 
  signal ViewSound_Madd_compteur_futur_addsub0000_cy : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal ViewSound_Madd_compteur_futur_addsub0000_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy : STD_LOGIC_VECTOR ( 6 downto 0 ); 
  signal ViewSound_compteur_present : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal ViewSound_etat_present : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal WorkerClock_Mcount_compteur_cy : STD_LOGIC_VECTOR ( 14 downto 0 ); 
  signal WorkerClock_Mcount_compteur_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal WorkerClock_compteur : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal WorkerScore_joueur1_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur1_L_mux0004 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur1_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur1_R_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur2_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur2_L_mux0005 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur2_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur2_R_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur3_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur3_L_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur3_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur3_R_mux0007 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur4_L : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur4_L_mux0006 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur4_R : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal WorkerScore_joueur4_R_mux0007 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal couleur_jouee : STD_LOGIC_VECTOR ( 2 downto 2 ); 
  signal etat_systeme : STD_LOGIC_VECTOR ( 2 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  XST_VCC : VCC
    port map (
      P => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0)
    );
  WorkerClock_impulsion : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_compteur_cmp_eq0000,
      Q => WorkerClock_impulsion_938
    );
  GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd3 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => btn_mode_IBUF_1058,
      Q => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd3_60
    );
  GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_In,
      Q => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56
    );
  GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_In,
      Q => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58
    );
  GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_In,
      Q => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53
    );
  GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => btn_mode_IBUF_1058,
      Q => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd2_55
    );
  WorkerClock_compteur_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_0,
      Q => WorkerClock_compteur(0)
    );
  WorkerClock_compteur_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_1,
      Q => WorkerClock_compteur(1)
    );
  WorkerClock_compteur_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_2,
      Q => WorkerClock_compteur(2)
    );
  WorkerClock_compteur_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_3,
      Q => WorkerClock_compteur(3)
    );
  WorkerClock_compteur_4 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_4,
      Q => WorkerClock_compteur(4)
    );
  WorkerClock_compteur_5 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_5,
      Q => WorkerClock_compteur(5)
    );
  WorkerClock_compteur_6 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_6,
      Q => WorkerClock_compteur(6)
    );
  WorkerClock_compteur_7 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_7,
      Q => WorkerClock_compteur(7)
    );
  WorkerClock_compteur_8 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_8,
      Q => WorkerClock_compteur(8)
    );
  WorkerClock_compteur_9 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_9,
      Q => WorkerClock_compteur(9)
    );
  WorkerClock_compteur_10 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_10,
      Q => WorkerClock_compteur(10)
    );
  WorkerClock_compteur_11 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_11,
      Q => WorkerClock_compteur(11)
    );
  WorkerClock_compteur_12 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_12,
      Q => WorkerClock_compteur(12)
    );
  WorkerClock_compteur_13 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_13,
      Q => WorkerClock_compteur(13)
    );
  WorkerClock_compteur_14 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_14,
      Q => WorkerClock_compteur(14)
    );
  WorkerClock_compteur_15 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerClock_Mcount_compteur_eqn_15,
      Q => WorkerClock_compteur(15)
    );
  WorkerClock_Mcount_compteur_cy_0_Q : MUXCY
    port map (
      CI => N0,
      DI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      S => WorkerClock_Mcount_compteur_lut(0),
      O => WorkerClock_Mcount_compteur_cy(0)
    );
  WorkerClock_Mcount_compteur_xor_0_Q : XORCY
    port map (
      CI => N0,
      LI => WorkerClock_Mcount_compteur_lut(0),
      O => Result(0)
    );
  WorkerClock_Mcount_compteur_cy_1_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(0),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_1_rt_882,
      O => WorkerClock_Mcount_compteur_cy(1)
    );
  WorkerClock_Mcount_compteur_xor_1_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(0),
      LI => WorkerClock_Mcount_compteur_cy_1_rt_882,
      O => Result(1)
    );
  WorkerClock_Mcount_compteur_cy_2_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(1),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_2_rt_884,
      O => WorkerClock_Mcount_compteur_cy(2)
    );
  WorkerClock_Mcount_compteur_xor_2_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(1),
      LI => WorkerClock_Mcount_compteur_cy_2_rt_884,
      O => Result(2)
    );
  WorkerClock_Mcount_compteur_cy_3_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(2),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_3_rt_886,
      O => WorkerClock_Mcount_compteur_cy(3)
    );
  WorkerClock_Mcount_compteur_xor_3_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(2),
      LI => WorkerClock_Mcount_compteur_cy_3_rt_886,
      O => Result(3)
    );
  WorkerClock_Mcount_compteur_cy_4_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(3),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_4_rt_888,
      O => WorkerClock_Mcount_compteur_cy(4)
    );
  WorkerClock_Mcount_compteur_xor_4_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(3),
      LI => WorkerClock_Mcount_compteur_cy_4_rt_888,
      O => Result(4)
    );
  WorkerClock_Mcount_compteur_cy_5_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(4),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_5_rt_890,
      O => WorkerClock_Mcount_compteur_cy(5)
    );
  WorkerClock_Mcount_compteur_xor_5_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(4),
      LI => WorkerClock_Mcount_compteur_cy_5_rt_890,
      O => Result(5)
    );
  WorkerClock_Mcount_compteur_cy_6_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(5),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_6_rt_892,
      O => WorkerClock_Mcount_compteur_cy(6)
    );
  WorkerClock_Mcount_compteur_xor_6_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(5),
      LI => WorkerClock_Mcount_compteur_cy_6_rt_892,
      O => Result(6)
    );
  WorkerClock_Mcount_compteur_cy_7_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(6),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_7_rt_894,
      O => WorkerClock_Mcount_compteur_cy(7)
    );
  WorkerClock_Mcount_compteur_xor_7_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(6),
      LI => WorkerClock_Mcount_compteur_cy_7_rt_894,
      O => Result(7)
    );
  WorkerClock_Mcount_compteur_cy_8_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(7),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_8_rt_896,
      O => WorkerClock_Mcount_compteur_cy(8)
    );
  WorkerClock_Mcount_compteur_xor_8_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(7),
      LI => WorkerClock_Mcount_compteur_cy_8_rt_896,
      O => Result(8)
    );
  WorkerClock_Mcount_compteur_cy_9_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(8),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_9_rt_898,
      O => WorkerClock_Mcount_compteur_cy(9)
    );
  WorkerClock_Mcount_compteur_xor_9_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(8),
      LI => WorkerClock_Mcount_compteur_cy_9_rt_898,
      O => Result(9)
    );
  WorkerClock_Mcount_compteur_cy_10_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(9),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_10_rt_872,
      O => WorkerClock_Mcount_compteur_cy(10)
    );
  WorkerClock_Mcount_compteur_xor_10_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(9),
      LI => WorkerClock_Mcount_compteur_cy_10_rt_872,
      O => Result(10)
    );
  WorkerClock_Mcount_compteur_cy_11_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(10),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_11_rt_874,
      O => WorkerClock_Mcount_compteur_cy(11)
    );
  WorkerClock_Mcount_compteur_xor_11_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(10),
      LI => WorkerClock_Mcount_compteur_cy_11_rt_874,
      O => Result(11)
    );
  WorkerClock_Mcount_compteur_cy_12_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(11),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_12_rt_876,
      O => WorkerClock_Mcount_compteur_cy(12)
    );
  WorkerClock_Mcount_compteur_xor_12_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(11),
      LI => WorkerClock_Mcount_compteur_cy_12_rt_876,
      O => Result(12)
    );
  WorkerClock_Mcount_compteur_cy_13_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(12),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_13_rt_878,
      O => WorkerClock_Mcount_compteur_cy(13)
    );
  WorkerClock_Mcount_compteur_xor_13_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(12),
      LI => WorkerClock_Mcount_compteur_cy_13_rt_878,
      O => Result(13)
    );
  WorkerClock_Mcount_compteur_cy_14_Q : MUXCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(13),
      DI => N0,
      S => WorkerClock_Mcount_compteur_cy_14_rt_880,
      O => WorkerClock_Mcount_compteur_cy(14)
    );
  WorkerClock_Mcount_compteur_xor_14_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(13),
      LI => WorkerClock_Mcount_compteur_cy_14_rt_880,
      O => Result(14)
    );
  WorkerClock_Mcount_compteur_xor_15_Q : XORCY
    port map (
      CI => WorkerClock_Mcount_compteur_cy(14),
      LI => WorkerClock_Mcount_compteur_xor_15_rt_916,
      O => Result(15)
    );
  ColorHandler_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ColorHandler_couleur_futur(3),
      Q => ColorHandler_couleur_present(3)
    );
  ColorHandler_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ColorHandler_couleur_futur(2),
      Q => ColorHandler_couleur_present(2)
    );
  ColorHandler_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ColorHandler_couleur_futur(1),
      Q => ColorHandler_couleur_present(1)
    );
  ColorHandler_couleur_present_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ColorHandler_couleur_futur(0),
      Q => ColorHandler_couleur_present(0)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_7_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(6),
      DI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_7_Q_840,
      O => ViewSound_fin_comptage
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_7_Q : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => ViewSound_compteur_present(8),
      I1 => ViewSound_compteur_present(9),
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_7_Q_840
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_6_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(5),
      DI => ViewSound_compteur_present(7),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_6_Q_839,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(6)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(7),
      I1 => ViewSound_buzzer_time_6_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_6_Q_839
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_5_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(4),
      DI => ViewSound_compteur_present(6),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_5_Q_838,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(5)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(6),
      I1 => ViewSound_buzzer_time_6_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_5_Q_838
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_4_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(3),
      DI => N0,
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_4_rt_831,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(4)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_3_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(2),
      DI => ViewSound_compteur_present(4),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_3_Q_837,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(3)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(4),
      I1 => ViewSound_buzzer_time_4_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_3_Q_837
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_2_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(1),
      DI => ViewSound_compteur_present(3),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_2_Q_836,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(2)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(3),
      I1 => ViewSound_buzzer_time_3_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_2_Q_836
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_1_Q : MUXCY
    port map (
      CI => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(0),
      DI => ViewSound_compteur_present(2),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_1_Q_835,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(1)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(2),
      I1 => ViewSound_buzzer_time_2_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_1_Q_835
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_0_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => ViewSound_compteur_present(1),
      S => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_0_Q_834,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy(0)
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_0_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => ViewSound_compteur_present(1),
      I1 => ViewSound_buzzer_time_1_Q,
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_lut_0_Q_834
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_9_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(8),
      LI => ViewSound_Madd_compteur_futur_addsub0000_xor_9_rt_825,
      O => ViewSound_compteur_futur_9_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_8_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(7),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_8_rt_823,
      O => ViewSound_compteur_futur_8_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_8_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(7),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_8_rt_823,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(8)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_7_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(6),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_7_rt_821,
      O => ViewSound_compteur_futur_7_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_7_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(6),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_7_rt_821,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(7)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_6_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(5),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_6_rt_819,
      O => ViewSound_compteur_futur_6_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_6_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(5),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_6_rt_819,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(6)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_5_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(4),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_5_rt_817,
      O => ViewSound_compteur_futur_5_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_5_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(4),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_5_rt_817,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(5)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_4_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(3),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_4_rt_815,
      O => ViewSound_compteur_futur_4_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_4_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(3),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_4_rt_815,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(4)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_3_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(2),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_3_rt_813,
      O => ViewSound_compteur_futur_3_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_3_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(2),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_3_rt_813,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(3)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_2_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(1),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_2_rt_811,
      O => ViewSound_compteur_futur_2_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_2_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(1),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_2_rt_811,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(2)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_1_Q : XORCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(0),
      LI => ViewSound_Madd_compteur_futur_addsub0000_cy_1_rt_809,
      O => ViewSound_compteur_futur_1_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_1_Q : MUXCY
    port map (
      CI => ViewSound_Madd_compteur_futur_addsub0000_cy(0),
      DI => N0,
      S => ViewSound_Madd_compteur_futur_addsub0000_cy_1_rt_809,
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(1)
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_0_Q : XORCY
    port map (
      CI => N0,
      LI => ViewSound_Madd_compteur_futur_addsub0000_lut(0),
      O => ViewSound_compteur_futur_0_norst
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_0_Q : MUXCY
    port map (
      CI => N0,
      DI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      S => ViewSound_Madd_compteur_futur_addsub0000_lut(0),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy(0)
    );
  ViewSound_compteur_present_9 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_9_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(9)
    );
  ViewSound_compteur_present_8 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_8_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(8)
    );
  ViewSound_compteur_present_7 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_7_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(7)
    );
  ViewSound_compteur_present_6 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_6_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(6)
    );
  ViewSound_compteur_present_5 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_5_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(5)
    );
  ViewSound_compteur_present_4 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_4_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(4)
    );
  ViewSound_compteur_present_3 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_3_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(3)
    );
  ViewSound_compteur_present_2 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_2_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(2)
    );
  ViewSound_compteur_present_1 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_1_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(1)
    );
  ViewSound_compteur_present_0 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_compteur_futur_0_norst,
      R => ViewSound_fin_comptage,
      Q => ViewSound_compteur_present(0)
    );
  ViewSound_etat_present_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      D => ViewSound_etat_futur,
      Q => ViewSound_etat_present(0)
    );
  WorkerScore_joueur4_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_L_mux0006(3),
      G => WorkerScore_joueur4_L_not0001,
      Q => WorkerScore_joueur4_L(3)
    );
  WorkerScore_joueur4_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_L_mux0006(2),
      G => WorkerScore_joueur4_L_not0001,
      Q => WorkerScore_joueur4_L(2)
    );
  WorkerScore_joueur4_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_L_mux0006(1),
      G => WorkerScore_joueur4_L_not0001,
      Q => WorkerScore_joueur4_L(1)
    );
  WorkerScore_joueur4_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_L_mux0006(0),
      G => WorkerScore_joueur4_L_not0001,
      Q => WorkerScore_joueur4_L(0)
    );
  WorkerScore_joueur3_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_R_mux0007(3),
      G => WorkerScore_joueur3_R_not0001,
      Q => WorkerScore_joueur3_R(3)
    );
  WorkerScore_joueur3_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_R_mux0007(2),
      G => WorkerScore_joueur3_R_not0001,
      Q => WorkerScore_joueur3_R(2)
    );
  WorkerScore_joueur3_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_R_mux0007(1),
      G => WorkerScore_joueur3_R_not0001,
      Q => WorkerScore_joueur3_R(1)
    );
  WorkerScore_joueur3_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_R_mux0007(0),
      G => WorkerScore_joueur3_R_not0001,
      Q => WorkerScore_joueur3_R(0)
    );
  WorkerScore_joueur3_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_L_mux0006(3),
      G => WorkerScore_joueur3_L_not0001,
      Q => WorkerScore_joueur3_L(3)
    );
  WorkerScore_joueur3_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_L_mux0006(2),
      G => WorkerScore_joueur3_L_not0001,
      Q => WorkerScore_joueur3_L(2)
    );
  WorkerScore_joueur3_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_L_mux0006(1),
      G => WorkerScore_joueur3_L_not0001,
      Q => WorkerScore_joueur3_L(1)
    );
  WorkerScore_joueur3_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur3_L_mux0006(0),
      G => WorkerScore_joueur3_L_not0001,
      Q => WorkerScore_joueur3_L(0)
    );
  WorkerScore_joueur2_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_R_mux0006(3),
      G => WorkerScore_joueur2_R_not0001,
      Q => WorkerScore_joueur2_R(3)
    );
  WorkerScore_joueur2_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_R_mux0006(2),
      G => WorkerScore_joueur2_R_not0001,
      Q => WorkerScore_joueur2_R(2)
    );
  WorkerScore_joueur2_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_R_mux0006(1),
      G => WorkerScore_joueur2_R_not0001,
      Q => WorkerScore_joueur2_R(1)
    );
  WorkerScore_joueur2_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_R_mux0006(0),
      G => WorkerScore_joueur2_R_not0001,
      Q => WorkerScore_joueur2_R(0)
    );
  WorkerScore_joueur2_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_L_mux0005(3),
      G => WorkerScore_joueur2_L_not0001,
      Q => WorkerScore_joueur2_L(3)
    );
  WorkerScore_joueur2_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_L_mux0005(2),
      G => WorkerScore_joueur2_L_not0001,
      Q => WorkerScore_joueur2_L(2)
    );
  WorkerScore_joueur2_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_L_mux0005(1),
      G => WorkerScore_joueur2_L_not0001,
      Q => WorkerScore_joueur2_L(1)
    );
  WorkerScore_joueur2_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur2_L_mux0005(0),
      G => WorkerScore_joueur2_L_not0001,
      Q => WorkerScore_joueur2_L(0)
    );
  WorkerScore_joueur1_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_R_mux0005(3),
      G => WorkerScore_joueur1_R_not0001,
      Q => WorkerScore_joueur1_R(3)
    );
  WorkerScore_joueur1_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_R_mux0005(2),
      G => WorkerScore_joueur1_R_not0001,
      Q => WorkerScore_joueur1_R(2)
    );
  WorkerScore_joueur1_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_R_mux0005(1),
      G => WorkerScore_joueur1_R_not0001,
      Q => WorkerScore_joueur1_R(1)
    );
  WorkerScore_joueur1_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_R_mux0005(0),
      G => WorkerScore_joueur1_R_not0001,
      Q => WorkerScore_joueur1_R(0)
    );
  WorkerScore_joueur1_L_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_L_mux0004(3),
      G => WorkerScore_joueur1_L_not0001,
      Q => WorkerScore_joueur1_L(3)
    );
  WorkerScore_joueur1_L_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_L_mux0004(2),
      G => WorkerScore_joueur1_L_not0001,
      Q => WorkerScore_joueur1_L(2)
    );
  WorkerScore_joueur1_L_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_L_mux0004(1),
      G => WorkerScore_joueur1_L_not0001,
      Q => WorkerScore_joueur1_L(1)
    );
  WorkerScore_joueur1_L_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur1_L_mux0004(0),
      G => WorkerScore_joueur1_L_not0001,
      Q => WorkerScore_joueur1_L(0)
    );
  WorkerScore_joueur4_R_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_R_mux0007(3),
      G => WorkerScore_joueur4_R_not0001,
      Q => WorkerScore_joueur4_R(3)
    );
  WorkerScore_joueur4_R_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_R_mux0007(2),
      G => WorkerScore_joueur4_R_not0001,
      Q => WorkerScore_joueur4_R(2)
    );
  WorkerScore_joueur4_R_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_R_mux0007(1),
      G => WorkerScore_joueur4_R_not0001,
      Q => WorkerScore_joueur4_R(1)
    );
  WorkerScore_joueur4_R_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_joueur4_R_mux0007(0),
      G => WorkerScore_joueur4_R_not0001,
      Q => WorkerScore_joueur4_R(0)
    );
  WorkerScore_etat_present_FSM_FFd2 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      D => WorkerScore_etat_present_FSM_FFd2_In_961,
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => WorkerScore_etat_present_FSM_FFd2_960
    );
  WorkerScore_etat_present_FSM_FFd1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => WorkerScore_etat_present_FSM_FFd1_In,
      Q => WorkerScore_etat_present_FSM_FFd1_958
    );
  WorkerScore_validation : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => WorkerScore_validation_mux0000,
      G => WorkerScore_validation_or0000,
      Q => WorkerScore_validation_1050
    );
  Storage_compteur_7 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(7),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(7)
    );
  Storage_compteur_6 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(6),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(6)
    );
  Storage_compteur_5 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(5),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(5)
    );
  Storage_compteur_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(4),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(4)
    );
  Storage_compteur_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(3),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(3)
    );
  Storage_compteur_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(2),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(2)
    );
  Storage_compteur_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_compteur_mux0003(1),
      G => Storage_compteur_not0001,
      Q => Storage_compteur(1)
    );
  Storage_etat_present_FSM_FFd2 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      D => Storage_etat_present_FSM_FFd2_In,
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Storage_etat_present_FSM_FFd2_621
    );
  Storage_etat_present_FSM_FFd1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => Storage_etat_present_FSM_FFd1_In,
      Q => Storage_etat_present_FSM_FFd1_619
    );
  Storage_sequence_complete_37 : LD
    port map (
      D => Storage_sequence_complete_37_mux0003,
      G => Storage_sequence_complete_37_not0001,
      Q => Storage_sequence_complete(37)
    );
  Storage_sequence_complete_42 : LD
    port map (
      D => Storage_sequence_complete_42_mux0003,
      G => Storage_sequence_complete_42_not0001,
      Q => Storage_sequence_complete(42)
    );
  Storage_sequence_complete_36 : LD
    port map (
      D => Storage_sequence_complete_36_mux0003,
      G => Storage_sequence_complete_36_not0001,
      Q => Storage_sequence_complete(36)
    );
  Storage_sequence_complete_41 : LD
    port map (
      D => Storage_sequence_complete_41_mux0003,
      G => Storage_sequence_complete_41_not0001,
      Q => Storage_sequence_complete(41)
    );
  Storage_sequence_complete_35 : LD
    port map (
      D => Storage_sequence_complete_35_mux0003,
      G => Storage_sequence_complete_34_not0001,
      Q => Storage_sequence_complete(35)
    );
  Storage_sequence_complete_40 : LD
    port map (
      D => Storage_sequence_complete_40_mux0003,
      G => Storage_sequence_complete_40_not0001,
      Q => Storage_sequence_complete(40)
    );
  Storage_sequence_complete_29 : LD
    port map (
      D => Storage_sequence_complete_29_mux0003,
      G => Storage_sequence_complete_29_not0001,
      Q => Storage_sequence_complete(29)
    );
  Storage_sequence_complete_34 : LD
    port map (
      D => Storage_sequence_complete_34_mux0003,
      G => Storage_sequence_complete_34_not0001,
      Q => Storage_sequence_complete(34)
    );
  Storage_sequence_complete_28 : LD
    port map (
      D => Storage_sequence_complete_28_mux0003,
      G => Storage_sequence_complete_28_not0001,
      Q => Storage_sequence_complete(28)
    );
  Storage_sequence_complete_33 : LD
    port map (
      D => Storage_sequence_complete_33_mux0003,
      G => Storage_sequence_complete_32_not0001,
      Q => Storage_sequence_complete(33)
    );
  Storage_sequence_complete_27 : LD
    port map (
      D => Storage_sequence_complete_27_mux0003,
      G => Storage_sequence_complete_26_not0001,
      Q => Storage_sequence_complete(27)
    );
  Storage_sequence_complete_32 : LD
    port map (
      D => Storage_sequence_complete_32_mux0003,
      G => Storage_sequence_complete_32_not0001,
      Q => Storage_sequence_complete(32)
    );
  Storage_sequence_complete_26 : LD
    port map (
      D => Storage_sequence_complete_26_mux0003,
      G => Storage_sequence_complete_26_not0001,
      Q => Storage_sequence_complete(26)
    );
  Storage_sequence_complete_31 : LD
    port map (
      D => Storage_sequence_complete_31_mux0003,
      G => Storage_sequence_complete_30_not0001,
      Q => Storage_sequence_complete(31)
    );
  Storage_sequence_complete_25 : LD
    port map (
      D => Storage_sequence_complete_25_mux0003,
      G => Storage_sequence_complete_25_not0001,
      Q => Storage_sequence_complete(25)
    );
  Storage_sequence_complete_30 : LD
    port map (
      D => Storage_sequence_complete_30_mux0003,
      G => Storage_sequence_complete_30_not0001,
      Q => Storage_sequence_complete(30)
    );
  Storage_sequence_complete_24 : LD
    port map (
      D => Storage_sequence_complete_24_mux0003,
      G => Storage_sequence_complete_24_not0001,
      Q => Storage_sequence_complete(24)
    );
  Storage_sequence_complete_19 : LD
    port map (
      D => Storage_sequence_complete_19_mux0003,
      G => Storage_sequence_complete_18_not0001,
      Q => Storage_sequence_complete(19)
    );
  Storage_sequence_complete_18 : LD
    port map (
      D => Storage_sequence_complete_18_mux0003,
      G => Storage_sequence_complete_18_not0001,
      Q => Storage_sequence_complete(18)
    );
  Storage_sequence_complete_23 : LD
    port map (
      D => Storage_sequence_complete_23_mux0003,
      G => Storage_sequence_complete_22_not0001,
      Q => Storage_sequence_complete(23)
    );
  Storage_sequence_complete_17 : LD
    port map (
      D => Storage_sequence_complete_17_mux0003,
      G => Storage_sequence_complete_17_not0001,
      Q => Storage_sequence_complete(17)
    );
  Storage_sequence_complete_22 : LD
    port map (
      D => Storage_sequence_complete_22_mux0003,
      G => Storage_sequence_complete_22_not0001,
      Q => Storage_sequence_complete(22)
    );
  Storage_sequence_complete_16 : LD
    port map (
      D => Storage_sequence_complete_16_mux0003,
      G => Storage_sequence_complete_16_not0001,
      Q => Storage_sequence_complete(16)
    );
  Storage_sequence_complete_21 : LD
    port map (
      D => Storage_sequence_complete_21_mux0003,
      G => Storage_sequence_complete_21_not0001,
      Q => Storage_sequence_complete(21)
    );
  Storage_sequence_complete_15 : LD
    port map (
      D => Storage_sequence_complete_15_mux0003,
      G => Storage_sequence_complete_14_not0001,
      Q => Storage_sequence_complete(15)
    );
  Storage_sequence_complete_20 : LD
    port map (
      D => Storage_sequence_complete_20_mux0003,
      G => Storage_sequence_complete_20_not0001,
      Q => Storage_sequence_complete(20)
    );
  Storage_sequence_complete_14 : LD
    port map (
      D => Storage_sequence_complete_14_mux0003,
      G => Storage_sequence_complete_14_not0001,
      Q => Storage_sequence_complete(14)
    );
  Storage_sequence_complete_9 : LD
    port map (
      D => Storage_sequence_complete_9_mux0003,
      G => Storage_sequence_complete_9_not0001,
      Q => Storage_sequence_complete(9)
    );
  Storage_sequence_complete_13 : LD
    port map (
      D => Storage_sequence_complete_13_mux0003,
      G => Storage_sequence_complete_13_not0001,
      Q => Storage_sequence_complete(13)
    );
  Storage_sequence_complete_8 : LD
    port map (
      D => Storage_sequence_complete_8_mux0003,
      G => Storage_sequence_complete_8_not0001,
      Q => Storage_sequence_complete(8)
    );
  Storage_sequence_complete_12 : LD
    port map (
      D => Storage_sequence_complete_12_mux0003,
      G => Storage_sequence_complete_12_not0001,
      Q => Storage_sequence_complete(12)
    );
  Storage_sequence_complete_7 : LD
    port map (
      D => Storage_sequence_complete_7_mux0003,
      G => Storage_sequence_complete_6_not0001,
      Q => Storage_sequence_complete(7)
    );
  Storage_sequence_complete_11 : LD
    port map (
      D => Storage_sequence_complete_11_mux0003,
      G => Storage_sequence_complete_10_not0001,
      Q => Storage_sequence_complete(11)
    );
  Storage_sequence_complete_6 : LD
    port map (
      D => Storage_sequence_complete_6_mux0003,
      G => Storage_sequence_complete_6_not0001,
      Q => Storage_sequence_complete(6)
    );
  Storage_sequence_complete_10 : LD
    port map (
      D => Storage_sequence_complete_10_mux0003,
      G => Storage_sequence_complete_10_not0001,
      Q => Storage_sequence_complete(10)
    );
  Storage_sequence_complete_5 : LD
    port map (
      D => Storage_sequence_complete_5_mux0003,
      G => Storage_sequence_complete_5_not0001,
      Q => Storage_sequence_complete(5)
    );
  Storage_sequence_complete_4 : LD
    port map (
      D => Storage_sequence_complete_4_mux0003,
      G => Storage_sequence_complete_4_not0001,
      Q => Storage_sequence_complete(4)
    );
  Storage_sequence_complete_3 : LD
    port map (
      D => Storage_sequence_complete_3_mux0003,
      G => Storage_sequence_complete_2_not0001,
      Q => Storage_sequence_complete(3)
    );
  Storage_sequence_complete_2 : LD
    port map (
      D => Storage_sequence_complete_2_mux0003,
      G => Storage_sequence_complete_2_not0001,
      Q => Storage_sequence_complete(2)
    );
  Storage_sequence_complete_1 : LD
    port map (
      D => Storage_sequence_complete_1_mux0003,
      G => Storage_sequence_complete_1_not0001,
      Q => Storage_sequence_complete(1)
    );
  Storage_sequence_complete_0 : LD
    port map (
      D => Storage_sequence_complete_0_mux0003,
      G => Storage_sequence_complete_0_not0001,
      Q => Storage_sequence_complete(0)
    );
  Storage_sequence_complete_59 : LD
    port map (
      D => Storage_sequence_complete_59_mux0003,
      G => Storage_sequence_complete_59_not0001,
      Q => Storage_sequence_complete(59)
    );
  Storage_sequence_complete_58 : LD
    port map (
      D => Storage_sequence_complete_58_mux0003,
      G => Storage_sequence_complete_58_not0001,
      Q => Storage_sequence_complete(58)
    );
  Storage_sequence_complete_57 : LD
    port map (
      D => Storage_sequence_complete_57_mux0003,
      G => Storage_sequence_complete_56_not0001_773,
      Q => Storage_sequence_complete(57)
    );
  Storage_sequence_complete_56 : LD
    port map (
      D => Storage_sequence_complete_56_mux0003,
      G => Storage_sequence_complete_56_not0001_773,
      Q => Storage_sequence_complete(56)
    );
  Storage_sequence_complete_55 : LD
    port map (
      D => Storage_sequence_complete_55_mux0003,
      G => Storage_sequence_complete_54_not0001,
      Q => Storage_sequence_complete(55)
    );
  Storage_sequence_complete_54 : LD
    port map (
      D => Storage_sequence_complete_54_mux0003,
      G => Storage_sequence_complete_54_not0001,
      Q => Storage_sequence_complete(54)
    );
  Storage_sequence_complete_49 : LD
    port map (
      D => Storage_sequence_complete_49_mux0003,
      G => Storage_sequence_complete_49_not0001,
      Q => Storage_sequence_complete(49)
    );
  Storage_sequence_complete_48 : LD
    port map (
      D => Storage_sequence_complete_48_mux0003,
      G => Storage_sequence_complete_48_not0001,
      Q => Storage_sequence_complete(48)
    );
  Storage_sequence_complete_53 : LD
    port map (
      D => Storage_sequence_complete_53_mux0003,
      G => Storage_sequence_complete_53_not0001,
      Q => Storage_sequence_complete(53)
    );
  Storage_sequence_complete_47 : LD
    port map (
      D => Storage_sequence_complete_47_mux0003,
      G => Storage_sequence_complete_46_not0001,
      Q => Storage_sequence_complete(47)
    );
  Storage_sequence_complete_52 : LD
    port map (
      D => Storage_sequence_complete_52_mux0003,
      G => Storage_sequence_complete_52_not0001,
      Q => Storage_sequence_complete(52)
    );
  Storage_sequence_complete_46 : LD
    port map (
      D => Storage_sequence_complete_46_mux0003,
      G => Storage_sequence_complete_46_not0001,
      Q => Storage_sequence_complete(46)
    );
  Storage_sequence_complete_51 : LD
    port map (
      D => Storage_sequence_complete_51_mux0003,
      G => Storage_sequence_complete_50_not0001,
      Q => Storage_sequence_complete(51)
    );
  Storage_sequence_complete_45 : LD
    port map (
      D => Storage_sequence_complete_45_mux0003,
      G => Storage_sequence_complete_45_not0001,
      Q => Storage_sequence_complete(45)
    );
  Storage_sequence_complete_50 : LD
    port map (
      D => Storage_sequence_complete_50_mux0003,
      G => Storage_sequence_complete_50_not0001,
      Q => Storage_sequence_complete(50)
    );
  Storage_sequence_complete_39 : LD
    port map (
      D => Storage_sequence_complete_39_mux0003,
      G => Storage_sequence_complete_38_not0001,
      Q => Storage_sequence_complete(39)
    );
  Storage_sequence_complete_44 : LD
    port map (
      D => Storage_sequence_complete_44_mux0003,
      G => Storage_sequence_complete_44_not0001,
      Q => Storage_sequence_complete(44)
    );
  Storage_sequence_complete_38 : LD
    port map (
      D => Storage_sequence_complete_38_mux0003,
      G => Storage_sequence_complete_38_not0001,
      Q => Storage_sequence_complete(38)
    );
  Storage_sequence_complete_43 : LD
    port map (
      D => Storage_sequence_complete_43_mux0003,
      G => Storage_sequence_complete_42_not0001,
      Q => Storage_sequence_complete(43)
    );
  Storage_reset_bus_color : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Storage_etat_present_cmp_eq0003,
      G => Storage_reset_bus_color_not0001,
      Q => Storage_reset_bus_color_625
    );
  Storage_bus_color_present_1 : FDP
    port map (
      C => clk_BUFGP_1068,
      D => Storage_bus_color_futur(1),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Storage_bus_color_present(1)
    );
  Storage_bus_color_present_0 : FDP
    port map (
      C => clk_BUFGP_1068,
      D => Storage_bus_color_futur(0),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Storage_bus_color_present(0)
    );
  Listener_compteur_local_int_7 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(7),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(7)
    );
  Listener_compteur_local_int_6 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(6),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(6)
    );
  Listener_compteur_local_int_5 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(5),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(5)
    );
  Listener_compteur_local_int_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(4),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(4)
    );
  Listener_compteur_local_int_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(3),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(3)
    );
  Listener_compteur_local_int_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(2),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(2)
    );
  Listener_compteur_local_int_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_compteur_local_int_mux0004(1),
      G => Listener_compteur_local_int_not0001,
      Q => Listener_compteur_local_int(1)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_7_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(6),
      DI => Listener_compteur_local_int(7),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(7),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Listener_compteur_local_int(7),
      I1 => Storage_compteur(7),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(7)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_6_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(5),
      DI => Listener_compteur_local_int(6),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(6),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(6)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Listener_compteur_local_int(6),
      I1 => Storage_compteur(6),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(6)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_5_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(4),
      DI => Listener_compteur_local_int(5),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(5),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(5)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Listener_compteur_local_int(5),
      I1 => Storage_compteur(5),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(5)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_4_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(3),
      DI => Listener_compteur_local_int(4),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(4),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(4)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Listener_compteur_local_int(4),
      I1 => Storage_compteur(4),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(4)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_3_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(2),
      DI => Listener_compteur_local_int(3),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(3),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(3)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Listener_compteur_local_int(3),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(3)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_2_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(1),
      DI => Listener_compteur_local_int(2),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(2),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(2)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Listener_compteur_local_int(2),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(2)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_1_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => Listener_compteur_local_int(1),
      S => Listener_Mcompar_color_comparante_cmp_lt0000_lut(1),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_cy(1)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Listener_compteur_local_int(1),
      O => Listener_Mcompar_color_comparante_cmp_lt0000_lut(1)
    );
  Listener_Mmux_color_comparante_mux0001_5_f5 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_5_f5_rt_88,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f5_rt1_89,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_5_f5_84
    );
  Listener_Mmux_color_comparante_mux0001_6_f5 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_8,
      I1 => Listener_Mmux_color_comparante_mux0001_71,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f5_94
    );
  Listener_Mmux_color_comparante_mux0001_4_f6 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_6_f5_94,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f5_84,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_4_f6_80
    );
  Listener_Mmux_color_comparante_mux0001_6_f5_0 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_81,
      I1 => Listener_Mmux_color_comparante_mux0001_72,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f51
    );
  Listener_Mmux_color_comparante_mux0001_7_f5 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_9,
      I1 => Listener_Mmux_color_comparante_mux0001_82,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f5_108
    );
  Listener_Mmux_color_comparante_mux0001_5_f6 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_7_f5_108,
      I1 => Listener_Mmux_color_comparante_mux0001_6_f51,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_5_f6_90
    );
  Listener_Mmux_color_comparante_mux0001_3_f7 : MUXF7
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_5_f6_90,
      I1 => Listener_Mmux_color_comparante_mux0001_4_f6_80,
      S => Listener_compteur_local_int(4),
      O => Listener_Mmux_color_comparante_mux0001_3_f7_78
    );
  Listener_Mmux_color_comparante_mux0001_6_f5_1 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_83,
      I1 => Listener_Mmux_color_comparante_mux0001_73,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f52
    );
  Listener_Mmux_color_comparante_mux0001_7_f5_0 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_91,
      I1 => Listener_Mmux_color_comparante_mux0001_84,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f51
    );
  Listener_Mmux_color_comparante_mux0001_5_f6_0 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_7_f51,
      I1 => Listener_Mmux_color_comparante_mux0001_6_f52,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_5_f61
    );
  Listener_Mmux_color_comparante_mux0001_7_f5_1 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_92,
      I1 => Listener_Mmux_color_comparante_mux0001_85,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f52
    );
  Listener_Mmux_color_comparante_mux0001_8_f5 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_10,
      I1 => Listener_Mmux_color_comparante_mux0001_93,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_8_f5_126
    );
  Listener_Mmux_color_comparante_mux0001_6_f6 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_8_f5_126,
      I1 => Listener_Mmux_color_comparante_mux0001_7_f52,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_6_f6_100
    );
  Listener_Mmux_color_comparante_mux0001_4_f7 : MUXF7
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_6_f6_100,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f61,
      S => Listener_compteur_local_int(4),
      O => Listener_Mmux_color_comparante_mux0001_4_f7_82
    );
  Listener_Mmux_color_comparante_mux0001_2_f8 : MUXF8
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_4_f7_82,
      I1 => Listener_Mmux_color_comparante_mux0001_3_f7_78,
      S => Listener_compteur_local_int(5),
      O => Listener_color_comparante_mux0001(0)
    );
  Listener_Mmux_color_comparante_mux0001_5_f5_0 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_5_f5_0_rt_86,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f5_0_rt1_87,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_5_f51
    );
  Listener_Mmux_color_comparante_mux0001_6_f5_2 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_86,
      I1 => Listener_Mmux_color_comparante_mux0001_75,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f53
    );
  Listener_Mmux_color_comparante_mux0001_4_f6_0 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_6_f53,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f51,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_4_f61
    );
  Listener_Mmux_color_comparante_mux0001_6_f5_3 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_87,
      I1 => Listener_Mmux_color_comparante_mux0001_76,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f54
    );
  Listener_Mmux_color_comparante_mux0001_7_f5_2 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_94,
      I1 => Listener_Mmux_color_comparante_mux0001_88,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f53
    );
  Listener_Mmux_color_comparante_mux0001_5_f6_1 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_7_f53,
      I1 => Listener_Mmux_color_comparante_mux0001_6_f54,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_5_f62
    );
  Listener_Mmux_color_comparante_mux0001_3_f7_0 : MUXF7
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_5_f62,
      I1 => Listener_Mmux_color_comparante_mux0001_4_f61,
      S => Listener_compteur_local_int(4),
      O => Listener_Mmux_color_comparante_mux0001_3_f71
    );
  Listener_Mmux_color_comparante_mux0001_6_f5_4 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_89,
      I1 => Listener_Mmux_color_comparante_mux0001_77,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_6_f55
    );
  Listener_Mmux_color_comparante_mux0001_7_f5_3 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_95,
      I1 => Listener_Mmux_color_comparante_mux0001_810,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f54
    );
  Listener_Mmux_color_comparante_mux0001_5_f6_2 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_7_f54,
      I1 => Listener_Mmux_color_comparante_mux0001_6_f55,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_5_f63
    );
  Listener_Mmux_color_comparante_mux0001_7_f5_4 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_96,
      I1 => Listener_Mmux_color_comparante_mux0001_811,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_7_f55
    );
  Listener_Mmux_color_comparante_mux0001_8_f5_0 : MUXF5
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_101,
      I1 => Listener_Mmux_color_comparante_mux0001_97,
      S => Listener_compteur_local_int(1),
      O => Listener_Mmux_color_comparante_mux0001_8_f51
    );
  Listener_Mmux_color_comparante_mux0001_6_f6_0 : MUXF6
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_8_f51,
      I1 => Listener_Mmux_color_comparante_mux0001_7_f55,
      S => Listener_compteur_local_int(3),
      O => Listener_Mmux_color_comparante_mux0001_6_f61
    );
  Listener_Mmux_color_comparante_mux0001_4_f7_0 : MUXF7
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_6_f61,
      I1 => Listener_Mmux_color_comparante_mux0001_5_f63,
      S => Listener_compteur_local_int(4),
      O => Listener_Mmux_color_comparante_mux0001_4_f71
    );
  Listener_Mmux_color_comparante_mux0001_2_f8_0 : MUXF8
    port map (
      I0 => Listener_Mmux_color_comparante_mux0001_4_f71,
      I1 => Listener_Mmux_color_comparante_mux0001_3_f71,
      S => Listener_compteur_local_int(5),
      O => Listener_color_comparante_mux0001(1)
    );
  Listener_timer_checker_3 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => Listener_counter_reset_170,
      D => Listener_timer_checker_mux0002(0),
      G => WorkerClock_impulsion_938,
      GE => Listener_etat_present(0),
      Q => Listener_timer_checker(3)
    );
  Listener_timer_checker_2 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => Listener_counter_reset_170,
      D => Listener_timer_checker_mux0002(1),
      G => WorkerClock_impulsion_938,
      GE => Listener_etat_present(0),
      Q => Listener_timer_checker(2)
    );
  Listener_timer_checker_1 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => Listener_counter_reset_170,
      D => Listener_timer_checker_mux0002(2),
      G => WorkerClock_impulsion_938,
      GE => Listener_etat_present(0),
      Q => Listener_timer_checker(1)
    );
  Listener_timer_checker_0 : LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => Listener_counter_reset_170,
      D => Listener_timer_checker_mux0002(3),
      G => WorkerClock_impulsion_938,
      GE => Listener_etat_present(0),
      Q => Listener_timer_checker(0)
    );
  Listener_etat_present_0 : FDC
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => Listener_etat_futur(0),
      Q => Listener_etat_present(0)
    );
  Listener_etat_present_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => Listener_etat_futur(1),
      Q => Listener_etat_present(1)
    );
  Listener_etat_present_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => Listener_etat_futur(2),
      Q => Listener_etat_present(2)
    );
  Listener_etat_present_3 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      D => Listener_etat_futur(3),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Listener_etat_present(3)
    );
  Listener_color_comparante_1 : LDE
    port map (
      D => Listener_color_comparante_mux0001(1),
      G => Listener_etat_present(0),
      GE => Listener_color_comparante_cmp_lt0000,
      Q => Listener_color_comparante(1)
    );
  Listener_color_comparante_0 : LDE
    port map (
      D => Listener_color_comparante_mux0001(0),
      G => Listener_etat_present(0),
      GE => Listener_color_comparante_cmp_lt0000,
      Q => Listener_color_comparante(0)
    );
  Listener_color_a_tester_1 : LDE
    port map (
      D => Listener_color_a_tester_mux0001(1),
      G => Listener_etat_present(0),
      GE => Listener_color_comparante_cmp_lt0000,
      Q => Listener_color_a_tester(1)
    );
  Listener_color_a_tester_0 : LDE
    port map (
      D => Listener_color_a_tester_mux0001(0),
      G => Listener_etat_present(0),
      GE => Listener_color_comparante_cmp_lt0000,
      Q => Listener_color_a_tester(0)
    );
  Listener_etat_futur_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_etat_futur_mux0005(3),
      G => Listener_etat_futur_not0001,
      Q => Listener_etat_futur(3)
    );
  Listener_etat_futur_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_etat_futur_mux0005(2),
      G => Listener_etat_futur_not0001,
      Q => Listener_etat_futur(2)
    );
  Listener_etat_futur_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => Listener_etat_futur_mux0005(1),
      G => Listener_etat_futur_not0001,
      Q => Listener_etat_futur(1)
    );
  Listener_etat_futur_0 : LD
    generic map(
      INIT => '1'
    )
    port map (
      D => Listener_etat_futur_mux0005(0),
      G => Listener_etat_futur_not0001,
      Q => Listener_etat_futur(0)
    );
  Listener_counter_reset : LD
    port map (
      D => Listener_counter_reset_mux0003,
      G => Listener_etat_present(0),
      Q => Listener_counter_reset_170
    );
  Listener_bus_color_present_2 : FDP
    port map (
      C => clk_BUFGP_1068,
      D => Listener_bus_color_futur(2),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Listener_bus_color_present(2)
    );
  Listener_bus_color_present_1 : FDP
    port map (
      C => clk_BUFGP_1068,
      D => Listener_bus_color_futur(1),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Listener_bus_color_present(1)
    );
  Listener_bus_color_present_0 : FDP
    port map (
      C => clk_BUFGP_1068,
      D => Listener_bus_color_futur(0),
      PRE => GameHandler_gestion_etat_syteme_reinit_43,
      Q => Listener_bus_color_present(0)
    );
  SequenceHandler_compteur_ecouter_7 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(7),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(7)
    );
  SequenceHandler_compteur_ecouter_6 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(6),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(6)
    );
  SequenceHandler_compteur_ecouter_5 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(5),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(5)
    );
  SequenceHandler_compteur_ecouter_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(4),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(4)
    );
  SequenceHandler_compteur_ecouter_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(3),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(3)
    );
  SequenceHandler_compteur_ecouter_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(2),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(2)
    );
  SequenceHandler_compteur_ecouter_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_compteur_ecouter_mux0003(1),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_compteur_ecouter(1)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_7_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(6),
      DI => SequenceHandler_compteur_ecouter(7),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(7),
      O => SequenceHandler_delay_cmp_ge0000
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(7),
      I1 => Storage_compteur(7),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(7)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_6_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(5),
      DI => SequenceHandler_compteur_ecouter(6),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(6),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(6)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(6),
      I1 => Storage_compteur(6),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(6)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_5_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(4),
      DI => SequenceHandler_compteur_ecouter(5),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(5),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(5)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(5),
      I1 => Storage_compteur(5),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(5)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_4_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(3),
      DI => SequenceHandler_compteur_ecouter(4),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(4),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(4)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(4),
      I1 => Storage_compteur(4),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(4)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_3_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(2),
      DI => SequenceHandler_compteur_ecouter(3),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(3),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(3)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(3),
      I1 => Storage_compteur(3),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(3)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_2_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(1),
      DI => SequenceHandler_compteur_ecouter(2),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(2),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(2)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(2),
      I1 => Storage_compteur(2),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(2)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_cy_1_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => SequenceHandler_compteur_ecouter(1),
      S => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(1),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_cy(1)
    );
  SequenceHandler_Mcompar_delay_cmp_ge0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(1),
      I1 => Storage_compteur(1),
      O => SequenceHandler_Mcompar_delay_cmp_ge0000_lut(1)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_7_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(6),
      DI => Storage_compteur(7),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(7),
      O => SequenceHandler_s_couleur_futur_cmp_le0000
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(7),
      I1 => SequenceHandler_compteur_sequence(7),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(7)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_6_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(5),
      DI => Storage_compteur(6),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(6),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(6)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => SequenceHandler_compteur_sequence(6),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(6)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_5_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(4),
      DI => Storage_compteur(5),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(5),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(5)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(5),
      I1 => SequenceHandler_compteur_sequence(5),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(5)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_4_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(3),
      DI => Storage_compteur(4),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(4),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(4)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(4)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_3_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(2),
      DI => Storage_compteur(3),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(3),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(3)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(3)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_2_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(1),
      DI => Storage_compteur(2),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(2),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(2)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => SequenceHandler_compteur_sequence(2),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(2)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy_1_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => Storage_compteur(1),
      S => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(1),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_cy(1)
    );
  SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mcompar_s_couleur_futur_cmp_le0000_lut(1)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_7_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(6),
      DI => SequenceHandler_compteur_sequence(7),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(7),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(7)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(7),
      I1 => Storage_compteur(7),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(7)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_6_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(5),
      DI => SequenceHandler_compteur_sequence(6),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(6),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(6)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(6),
      I1 => Storage_compteur(6),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(6)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_5_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(4),
      DI => SequenceHandler_compteur_sequence(5),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(5),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(5)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(5),
      I1 => Storage_compteur(5),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(5)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_4_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(3),
      DI => SequenceHandler_compteur_sequence(4),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(4),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(4)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(4),
      I1 => Storage_compteur(4),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(4)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_3_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(2),
      DI => SequenceHandler_compteur_sequence(3),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(3),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(3)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(3),
      I1 => Storage_compteur(3),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(3)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_2_Q : MUXCY
    port map (
      CI => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(1),
      DI => SequenceHandler_compteur_sequence(2),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(2),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(2)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_compteur(2),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(2)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_1_Q : MUXCY
    port map (
      CI => Listener_Mcompar_color_comparante_cmp_lt0000_cy(0),
      DI => SequenceHandler_compteur_sequence(1),
      S => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(1),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(1)
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(1),
      I1 => Storage_compteur(1),
      O => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_lut(1)
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt_374,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt1_375,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_370
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_8,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_71,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_380
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_4_f6 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_380,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_370,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_4_f6_366
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_0 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_81,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_72,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f51
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_9,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_82,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_394
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f6 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_394,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f51,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_376
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_3_f7 : MUXF7
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_376,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_4_f6_366,
      S => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_3_f7_364
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_1 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_83,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_73,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f52
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_0 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_91,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_84,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f51
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_0 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f51,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f52,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f61
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_1 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_92,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_85,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f52
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_8_f5 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_10,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_93,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_8_f5_412
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f6 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_8_f5_412,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f52,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f6_386
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_4_f7 : MUXF7
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f6_386,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f61,
      S => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_4_f7_368
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_2_f8 : MUXF8
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_4_f7_368,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_3_f7_364,
      S => SequenceHandler_compteur_sequence(5),
      O => SequenceHandler_couleur_temp_mux0001(0)
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt_372,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt1_373,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f51
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_2 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_86,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_75,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f53
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_4_f6_0 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f53,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f51,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_4_f61
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_3 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_87,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_76,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f54
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_2 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_94,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_88,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f53
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_1 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f53,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f54,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f62
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_3_f7_0 : MUXF7
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f62,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_4_f61,
      S => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_3_f71
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f5_4 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_89,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_77,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f55
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_3 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_95,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_810,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f54
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f6_2 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f54,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f55,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f63
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_7_f5_4 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_96,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_811,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_7_f55
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_8_f5_0 : MUXF5
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_101,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_97,
      S => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_8_f51
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_6_f6_0 : MUXF6
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_8_f51,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_7_f55,
      S => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_6_f61
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_4_f7_0 : MUXF7
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_6_f61,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_5_f63,
      S => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_4_f71
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_2_f8_0 : MUXF8
    port map (
      I0 => SequenceHandler_Mmux_couleur_temp_mux0001_4_f71,
      I1 => SequenceHandler_Mmux_couleur_temp_mux0001_3_f71,
      S => SequenceHandler_compteur_sequence(5),
      O => SequenceHandler_couleur_temp_mux0001(1)
    );
  SequenceHandler_compteur_demonstration_3 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_demonstration_and0001,
      CLR => SequenceHandler_compteur_demonstration_and0000,
      D => SequenceHandler_Result_3_1,
      Q => SequenceHandler_compteur_demonstration(3)
    );
  SequenceHandler_compteur_demonstration_2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_demonstration_and0001,
      CLR => SequenceHandler_compteur_demonstration_and0000,
      D => SequenceHandler_Result_2_1,
      Q => SequenceHandler_compteur_demonstration(2)
    );
  SequenceHandler_compteur_demonstration_1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_demonstration_and0001,
      CLR => SequenceHandler_compteur_demonstration_and0000,
      D => SequenceHandler_Result_1_1,
      Q => SequenceHandler_compteur_demonstration(1)
    );
  SequenceHandler_compteur_demonstration_0 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_demonstration_and0001,
      CLR => SequenceHandler_compteur_demonstration_and0000,
      D => SequenceHandler_Result_0_1,
      Q => SequenceHandler_compteur_demonstration(0)
    );
  SequenceHandler_compteur_sequence_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(7),
      Q => SequenceHandler_compteur_sequence(7)
    );
  SequenceHandler_compteur_sequence_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(6),
      Q => SequenceHandler_compteur_sequence(6)
    );
  SequenceHandler_compteur_sequence_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(5),
      Q => SequenceHandler_compteur_sequence(5)
    );
  SequenceHandler_compteur_sequence_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(4),
      Q => SequenceHandler_compteur_sequence(4)
    );
  SequenceHandler_compteur_sequence_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(3),
      Q => SequenceHandler_compteur_sequence(3)
    );
  SequenceHandler_compteur_sequence_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(2),
      Q => SequenceHandler_compteur_sequence(2)
    );
  SequenceHandler_compteur_sequence_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => SequenceHandler_compteur_sequence_and0000,
      D => SequenceHandler_Result(1),
      Q => SequenceHandler_compteur_sequence(1)
    );
  SequenceHandler_couleur_temp_1 : LDE
    port map (
      D => SequenceHandler_couleur_temp_mux0001(1),
      G => SequenceHandler_etat_present(0),
      GE => SequenceHandler_couleur_temp_cmp_lt0000,
      Q => SequenceHandler_couleur_temp(1)
    );
  SequenceHandler_couleur_temp_0 : LDE
    port map (
      D => SequenceHandler_couleur_temp_mux0001(0),
      G => SequenceHandler_etat_present(0),
      GE => SequenceHandler_couleur_temp_cmp_lt0000,
      Q => SequenceHandler_couleur_temp(0)
    );
  SequenceHandler_etat_present_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_etat_futur(2),
      Q => SequenceHandler_etat_present(2)
    );
  SequenceHandler_etat_present_0 : FDC
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_etat_futur(0),
      Q => SequenceHandler_etat_present(0)
    );
  SequenceHandler_etat_present_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_etat_futur(1),
      Q => SequenceHandler_etat_present(1)
    );
  SequenceHandler_etat_present_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_etat_futur(3),
      Q => SequenceHandler_etat_present(3)
    );
  SequenceHandler_etat_present_4 : FDP
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      D => SequenceHandler_etat_futur(4),
      PRE => SequenceHandler_etat_present_Acst_inv,
      Q => SequenceHandler_etat_present(4)
    );
  SequenceHandler_etat_systeme_temp_2 : LDE
    port map (
      D => etat_systeme(2),
      G => SequenceHandler_etat_present(4),
      GE => SequenceHandler_etat_systeme_temp_0_0_not0000,
      Q => SequenceHandler_etat_systeme_temp(2)
    );
  SequenceHandler_etat_systeme_temp_1 : LDE
    port map (
      D => etat_systeme(1),
      G => SequenceHandler_etat_present(4),
      GE => SequenceHandler_etat_systeme_temp_0_0_not0000,
      Q => SequenceHandler_etat_systeme_temp(1)
    );
  SequenceHandler_etat_systeme_temp_0 : LDE
    port map (
      D => etat_systeme(0),
      G => SequenceHandler_etat_present(4),
      GE => SequenceHandler_etat_systeme_temp_0_0_not0000,
      Q => SequenceHandler_etat_systeme_temp(0)
    );
  SequenceHandler_e_couleur_futur_3 : LD
    port map (
      D => SequenceHandler_e_couleur_futur_mux0003(3),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_e_couleur_futur(3)
    );
  SequenceHandler_e_couleur_futur_2 : LD
    port map (
      D => SequenceHandler_e_couleur_futur_mux0003(2),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_e_couleur_futur(2)
    );
  SequenceHandler_e_couleur_futur_1 : LD
    port map (
      D => SequenceHandler_e_couleur_futur_mux0003(1),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_e_couleur_futur(1)
    );
  SequenceHandler_e_couleur_futur_0 : LD
    port map (
      D => SequenceHandler_e_couleur_futur_mux0003(0),
      G => SequenceHandler_compteur_ecouter_not0001,
      Q => SequenceHandler_e_couleur_futur(0)
    );
  SequenceHandler_d_couleur_futur_3 : LD
    port map (
      D => SequenceHandler_d_couleur_futur_mux0002(3),
      G => SequenceHandler_d_couleur_futur_or0000,
      Q => SequenceHandler_d_couleur_futur(3)
    );
  SequenceHandler_d_couleur_futur_2 : LD
    port map (
      D => SequenceHandler_d_couleur_futur_mux0002(2),
      G => SequenceHandler_d_couleur_futur_or0000,
      Q => SequenceHandler_d_couleur_futur(2)
    );
  SequenceHandler_d_couleur_futur_1 : LD
    port map (
      D => SequenceHandler_d_couleur_futur_mux0002(1),
      G => SequenceHandler_d_couleur_futur_or0000,
      Q => SequenceHandler_d_couleur_futur(1)
    );
  SequenceHandler_d_couleur_futur_0 : LD
    port map (
      D => SequenceHandler_d_couleur_futur_mux0002(0),
      G => SequenceHandler_d_couleur_futur_or0000,
      Q => SequenceHandler_d_couleur_futur(0)
    );
  SequenceHandler_s_couleur_futur_3 : LD
    port map (
      D => SequenceHandler_s_couleur_futur_mux0003(3),
      G => SequenceHandler_s_couleur_futur_not0001,
      Q => SequenceHandler_s_couleur_futur(3)
    );
  SequenceHandler_s_couleur_futur_2 : LD
    port map (
      D => SequenceHandler_s_couleur_futur_mux0003(2),
      G => SequenceHandler_s_couleur_futur_not0001,
      Q => SequenceHandler_s_couleur_futur(2)
    );
  SequenceHandler_s_couleur_futur_1 : LD
    port map (
      D => SequenceHandler_s_couleur_futur_mux0003(1),
      G => SequenceHandler_s_couleur_futur_not0001,
      Q => SequenceHandler_s_couleur_futur(1)
    );
  SequenceHandler_s_couleur_futur_0 : LD
    port map (
      D => SequenceHandler_s_couleur_futur_mux0003(0),
      G => SequenceHandler_s_couleur_futur_not0001,
      Q => SequenceHandler_s_couleur_futur(0)
    );
  SequenceHandler_delay_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_delay_mux0001(3),
      G => SequenceHandler_delay_not0001_514,
      Q => SequenceHandler_delay(3)
    );
  SequenceHandler_delay_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_delay_mux0001(2),
      G => SequenceHandler_delay_not0001_514,
      Q => SequenceHandler_delay(2)
    );
  SequenceHandler_delay_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_delay_mux0001(1),
      G => SequenceHandler_delay_not0001_514,
      Q => SequenceHandler_delay(1)
    );
  SequenceHandler_delay_0 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_delay_mux0001(0),
      G => SequenceHandler_delay_not0001_514,
      Q => SequenceHandler_delay(0)
    );
  SequenceHandler_etat_futur_4 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_etat_futur_mux0004(4),
      G => SequenceHandler_etat_futur_not0001,
      Q => SequenceHandler_etat_futur(4)
    );
  SequenceHandler_etat_futur_3 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_etat_futur_mux0004(3),
      G => SequenceHandler_etat_futur_not0001,
      Q => SequenceHandler_etat_futur(3)
    );
  SequenceHandler_etat_futur_2 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_etat_futur_mux0004(2),
      G => SequenceHandler_etat_futur_not0001,
      Q => SequenceHandler_etat_futur(2)
    );
  SequenceHandler_etat_futur_1 : LD
    generic map(
      INIT => '0'
    )
    port map (
      D => SequenceHandler_etat_futur_mux0004(1),
      G => SequenceHandler_etat_futur_not0001,
      Q => SequenceHandler_etat_futur(1)
    );
  SequenceHandler_etat_futur_0 : LD
    generic map(
      INIT => '1'
    )
    port map (
      D => SequenceHandler_etat_futur_mux0004(0),
      G => SequenceHandler_etat_futur_not0001,
      Q => SequenceHandler_etat_futur(0)
    );
  SequenceHandler_e_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_e_couleur_futur(3),
      Q => SequenceHandler_e_couleur_present(3)
    );
  SequenceHandler_e_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_e_couleur_futur(2),
      Q => SequenceHandler_e_couleur_present(2)
    );
  SequenceHandler_e_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_e_couleur_futur(1),
      Q => SequenceHandler_e_couleur_present(1)
    );
  SequenceHandler_e_couleur_present_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_e_couleur_futur(0),
      Q => SequenceHandler_e_couleur_present(0)
    );
  SequenceHandler_s_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_s_couleur_futur(3),
      Q => SequenceHandler_s_couleur_present(3)
    );
  SequenceHandler_s_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_s_couleur_futur(2),
      Q => SequenceHandler_s_couleur_present(2)
    );
  SequenceHandler_s_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_s_couleur_futur(1),
      Q => SequenceHandler_s_couleur_present(1)
    );
  SequenceHandler_s_couleur_present_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_s_couleur_futur(0),
      Q => SequenceHandler_s_couleur_present(0)
    );
  SequenceHandler_d_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_d_couleur_futur(3),
      Q => SequenceHandler_d_couleur_present(3)
    );
  SequenceHandler_d_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_d_couleur_futur(2),
      Q => SequenceHandler_d_couleur_present(2)
    );
  SequenceHandler_d_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_d_couleur_futur(1),
      Q => SequenceHandler_d_couleur_present(1)
    );
  SequenceHandler_d_couleur_present_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_d_couleur_futur(0),
      Q => SequenceHandler_d_couleur_present(0)
    );
  SequenceHandler_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_couleur_futur(3),
      Q => SequenceHandler_couleur_present(3)
    );
  SequenceHandler_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_couleur_futur(2),
      Q => SequenceHandler_couleur_present(2)
    );
  SequenceHandler_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_couleur_futur(1),
      Q => SequenceHandler_couleur_present(1)
    );
  SequenceHandler_couleur_present_0 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => SequenceHandler_etat_present_Acst_inv,
      D => SequenceHandler_couleur_futur(0),
      Q => SequenceHandler_couleur_present(0)
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_2_f5 : MUXF5
    port map (
      I0 => PlayerHandler_Mmux_JoueurEnCours_mux0007_4_304,
      I1 => PlayerHandler_Mmux_JoueurEnCours_mux0007_3_302,
      S => PlayerHandler_JoueurEnCours(1),
      O => PlayerHandler_JoueurEnCours_mux0007(0)
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_2_f5_0 : MUXF5
    port map (
      I0 => PlayerHandler_Mmux_JoueurEnCours_mux0007_41_305,
      I1 => PlayerHandler_Mmux_JoueurEnCours_mux0007_31_303,
      S => PlayerHandler_JoueurEnCours(1),
      O => PlayerHandler_JoueurEnCours_mux0007(1)
    );
  PlayerHandler_JoueurEnCours_1 : LDC
    port map (
      CLR => PlayerHandler_JoueurEnCours_cmp_eq0000,
      D => PlayerHandler_JoueurEnCours_mux0007(0),
      G => PlayerHandler_JoueurEnCours_not0000,
      Q => PlayerHandler_JoueurEnCours(1)
    );
  PlayerHandler_JoueurEnCours_0 : LDC
    port map (
      CLR => PlayerHandler_JoueurEnCours_cmp_eq0000,
      D => PlayerHandler_JoueurEnCours_mux0007(1),
      G => PlayerHandler_JoueurEnCours_not0000,
      Q => PlayerHandler_JoueurEnCours(0)
    );
  PlayerHandler_Joueur1_0 : LDPE_1
    port map (
      D => PlayerHandler_Joueur1_0_mux0000,
      G => PlayerHandler_Joueur1_0_not0001,
      GE => PlayerHandler_JoueurEnCours_not0000,
      PRE => PlayerHandler_JoueurEnCours_cmp_eq0000,
      Q => PlayerHandler_Joueur1(0)
    );
  PlayerHandler_Joueur2_0 : LDCPE
    port map (
      CLR => PlayerHandler_Joueur2_0_and0000,
      D => PlayerHandler_Joueur2_0_mux0000,
      G => PlayerHandler_Joueur2_0_cmp_eq0000,
      GE => PlayerHandler_JoueurEnCours_not0000,
      PRE => PlayerHandler_Joueur2_0_and0001,
      Q => PlayerHandler_Joueur2(0)
    );
  PlayerHandler_Joueur3_0 : LDCPE
    port map (
      CLR => PlayerHandler_Joueur3_0_and0000,
      D => PlayerHandler_Joueur3_0_mux0000,
      G => PlayerHandler_Joueur3_0_cmp_eq0000,
      GE => PlayerHandler_JoueurEnCours_not0000,
      PRE => PlayerHandler_Joueur3_0_and0001,
      Q => PlayerHandler_Joueur3(0)
    );
  PlayerHandler_Joueur4_0 : LDCPE
    port map (
      CLR => PlayerHandler_Joueur4_0_and0000,
      D => PlayerHandler_Joueur4_0_mux0000_295,
      G => PlayerHandler_Joueur4_0_cmp_eq0000,
      GE => PlayerHandler_JoueurEnCours_not0000,
      PRE => PlayerHandler_Joueur4_0_and0001,
      Q => PlayerHandler_Joueur4(0)
    );
  ViewGame_couleur_present_2 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ViewGame_couleur_futur(2),
      Q => ViewGame_couleur_present(2)
    );
  ViewGame_couleur_present_1 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ViewGame_couleur_futur(1),
      Q => ViewGame_couleur_present(1)
    );
  ViewGame_couleur_present_5 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ViewGame_couleur_futur(5),
      Q => ViewGame_couleur_present(5)
    );
  ViewGame_couleur_present_3 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ViewGame_couleur_futur(3),
      Q => ViewGame_couleur_present(3)
    );
  ViewGame_couleur_present_4 : FDC
    port map (
      C => clk_BUFGP_1068,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => ViewGame_couleur_futur(4),
      Q => ViewGame_couleur_present(4)
    );
  GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_In,
      Q => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45
    );
  GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => btn_start_IBUF_1062,
      Q => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd2_47
    );
  GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_In,
      Q => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_49
    );
  GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CE => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      CLR => GameHandler_gestion_etat_syteme_reinit_43,
      D => btn_start_IBUF_1062,
      Q => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd2_51
    );
  GameHandler_gestion_etat_syteme_etat_present_0 : FDP
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_1068,
      D => GameHandler_gestion_etat_syteme_etat_futur_0_Q,
      PRE => rst_IBUF_1086,
      Q => GameHandler_gestion_etat_syteme_etat_present_0_Q
    );
  GameHandler_gestion_etat_syteme_etat_present_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_3_Q,
      Q => GameHandler_gestion_etat_syteme_etat_present_3_Q
    );
  GameHandler_gestion_etat_syteme_etat_present_6 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_6_Q,
      Q => GameHandler_gestion_etat_syteme_etat_present_6_Q
    );
  GameHandler_gestion_etat_syteme_etat_present_4 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_4_Q,
      Q => GameHandler_gestion_etat_syteme_etat_present_4_Q
    );
  GameHandler_gestion_etat_syteme_etat_present_5 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_5_Q,
      Q => GameHandler_gestion_etat_syteme_etat_present_5_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_0 : FDP
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_1068,
      D => GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_Q,
      PRE => rst_IBUF_1086,
      Q => GameHandler_gestion_etat_syteme_etat_futur_0_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_mux0003_3_Q_35,
      Q => GameHandler_gestion_etat_syteme_etat_futur_3_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_6 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_Q,
      Q => GameHandler_gestion_etat_syteme_etat_futur_6_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_4 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_Q,
      Q => GameHandler_gestion_etat_syteme_etat_futur_4_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_5 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1068,
      CLR => rst_IBUF_1086,
      D => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_Q,
      Q => GameHandler_gestion_etat_syteme_etat_futur_5_Q
    );
  GameHandler_gestion_etat_syteme_reinit : FDP
    port map (
      C => clk_BUFGP_1068,
      D => GameHandler_gestion_etat_syteme_reinit_and0000,
      PRE => rst_IBUF_1086,
      Q => GameHandler_gestion_etat_syteme_reinit_43
    );
  WorkerScore_validation_or00001 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd1_958,
      I1 => WorkerScore_etat_present_FSM_FFd2_960,
      O => WorkerScore_validation_or0000
    );
  WorkerScore_validation_mux00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd1_958,
      I1 => WorkerScore_N35,
      O => WorkerScore_validation_mux0000
    );
  WorkerScore_joueur1_L_not000111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd1_958,
      I1 => WorkerScore_etat_present_FSM_FFd2_960,
      O => WorkerScore_N33
    );
  Storage_sequence_complete_36_or00001 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(1),
      O => Storage_sequence_complete_36_or0000
    );
  Storage_sequence_complete_32_not000111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd1_619,
      I1 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_N11
    );
  Storage_sequence_complete_24_mux000311 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N4,
      O => Storage_N18
    );
  Storage_sequence_complete_21_not000111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_compteur(2),
      O => Storage_N17
    );
  Storage_sequence_complete_13_not000111 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(3),
      O => Storage_N16
    );
  Storage_sequence_complete_10_not000131 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(3),
      O => Storage_N15
    );
  Storage_sequence_complete_0_not000111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      O => Storage_etat_present_cmp_eq0003
    );
  Storage_bus_color_futur_cmp_eq000531 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => Storage_compteur(7),
      O => Storage_N62
    );
  Storage_bus_color_futur_cmp_eq000511 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(3),
      O => Storage_N14
    );
  SequenceHandler_etat_futur_mux0004_4_41 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(1),
      I1 => SequenceHandler_compteur_demonstration(2),
      O => SequenceHandler_N25
    );
  SequenceHandler_compteur_demonstration_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => SequenceHandler_etat_present(2),
      I1 => SequenceHandler_etat_present(4),
      O => SequenceHandler_compteur_demonstration_and0000
    );
  PlayerHandler_JoueurEnCours_not00001 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Listener_etat_present(2),
      O => PlayerHandler_JoueurEnCours_not0000
    );
  PlayerHandler_Joueur4_0_cmp_eq00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_JoueurEnCours(1),
      O => PlayerHandler_Joueur4_0_cmp_eq0000
    );
  PlayerHandler_Joueur3_0_cmp_eq00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_JoueurEnCours(1),
      O => PlayerHandler_Joueur3_0_cmp_eq0000
    );
  PlayerHandler_Joueur2_0_cmp_eq00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(1),
      I1 => PlayerHandler_JoueurEnCours(0),
      O => PlayerHandler_Joueur2_0_cmp_eq0000
    );
  PlayerHandler_Joueur1_0_not00011 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_JoueurEnCours(1),
      O => PlayerHandler_Joueur1_0_not0001
    );
  Storage_compteur_not00011 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      O => Storage_compteur_not0001
    );
  WorkerScore_joueur1_L_mux0004_0_311 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(1),
      I1 => PlayerHandler_JoueurEnCours(0),
      I2 => WorkerScore_N34,
      O => WorkerScore_N57
    );
  Storage_sequence_complete_55_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N4,
      I1 => Storage_bus_color_present(1),
      I2 => Storage_N35,
      O => Storage_sequence_complete_55_mux0003
    );
  Storage_sequence_complete_54_not000111 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N7,
      I1 => Storage_compteur(2),
      I2 => Storage_compteur(1),
      O => Storage_N35
    );
  Storage_sequence_complete_53_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N19,
      I1 => Storage_N7,
      I2 => Storage_compteur(2),
      O => Storage_sequence_complete_53_mux0003
    );
  Storage_sequence_complete_51_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N4,
      I1 => Storage_bus_color_present(1),
      I2 => Storage_N36,
      O => Storage_sequence_complete_51_mux0003
    );
  Storage_sequence_complete_50_not000111 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_N7,
      I2 => Storage_compteur(1),
      O => Storage_N36
    );
  Storage_sequence_complete_4_mux00031 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(3),
      I2 => Storage_N12,
      O => Storage_sequence_complete_4_mux0003
    );
  Storage_sequence_complete_49_not000111 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Storage_N7,
      I2 => Storage_N4,
      O => Storage_N38
    );
  Storage_sequence_complete_49_mux000321 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Storage_bus_color_present(1),
      I2 => Storage_N4,
      O => Storage_N19
    );
  Storage_sequence_complete_49_mux000311 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_compteur(4),
      I2 => Storage_compteur(5),
      O => Storage_N7
    );
  Storage_sequence_complete_49_mux00031 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_N7,
      I2 => Storage_N19,
      O => Storage_sequence_complete_49_mux0003
    );
  Storage_sequence_complete_41_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N5,
      I2 => Storage_sequence_complete_40_cmp_eq0001,
      O => Storage_sequence_complete_41_mux0003
    );
  Storage_sequence_complete_40_or00001 : LUT3
    generic map(
      INIT => X"FD"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_compteur(2),
      I2 => Storage_compteur(1),
      O => Storage_sequence_complete_40_or0000
    );
  Storage_sequence_complete_40_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N5,
      I1 => Storage_bus_color_present(0),
      I2 => Storage_sequence_complete_40_cmp_eq0001,
      O => Storage_sequence_complete_40_mux0003
    );
  Storage_sequence_complete_33_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N5,
      I2 => Storage_N26,
      O => Storage_sequence_complete_33_mux0003
    );
  Storage_sequence_complete_32_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N5,
      I1 => Storage_bus_color_present(0),
      I2 => Storage_N26,
      O => Storage_sequence_complete_32_mux0003
    );
  Storage_sequence_complete_28_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_N12,
      I1 => Storage_compteur(4),
      I2 => Storage_compteur(3),
      O => Storage_sequence_complete_28_mux0003
    );
  Storage_sequence_complete_20_mux00031 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_compteur(4),
      I2 => Storage_N12,
      O => Storage_sequence_complete_20_mux0003
    );
  Storage_sequence_complete_17_not000111 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(5),
      I1 => Storage_N4,
      I2 => Storage_compteur(4),
      O => Storage_N21
    );
  Storage_sequence_complete_12_mux00031 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_N12,
      I2 => Storage_compteur(3),
      O => Storage_sequence_complete_12_mux0003
    );
  Storage_sequence_complete_10_not000141 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => Storage_compteur(5),
      I1 => Storage_compteur(4),
      I2 => Storage_N4,
      O => Storage_N20
    );
  Storage_sequence_complete_10_mux000321 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(5),
      I1 => Storage_compteur(1),
      I2 => Storage_N4,
      O => Storage_N6
    );
  Storage_N51 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_N4,
      I2 => Storage_compteur(5),
      O => Storage_N5
    );
  SequenceHandler_compteur_ecouter_mux0003_7_31 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Listener_etat_present(2),
      I2 => SequenceHandler_etat_present(3),
      O => SequenceHandler_N24
    );
  SequenceHandler_compteur_ecouter_mux0003_7_21 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Listener_etat_present(2),
      I1 => Listener_etat_present(1),
      I2 => SequenceHandler_etat_present(3),
      O => SequenceHandler_N21
    );
  Listener_compteur_local_int_cmp_eq00011 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Listener_bus_color_present(2),
      I1 => Listener_bus_color_present(1),
      I2 => Listener_bus_color_present(0),
      O => Listener_compteur_local_int_cmp_eq0001
    );
  SequenceHandler_compteur_sequence_2_91 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(41),
      I2 => Storage_sequence_complete(45),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_87
    );
  SequenceHandler_compteur_sequence_2_81 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(2),
      I2 => Storage_sequence_complete(6),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_93
    );
  SequenceHandler_compteur_sequence_2_71 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(35),
      I2 => Storage_sequence_complete(39),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_88
    );
  SequenceHandler_compteur_sequence_2_61 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(34),
      I2 => Storage_sequence_complete(38),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_82
    );
  SequenceHandler_compteur_sequence_2_51 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(33),
      I2 => Storage_sequence_complete(37),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_94
    );
  SequenceHandler_compteur_sequence_2_41 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(27),
      I2 => Storage_sequence_complete(31),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_77
    );
  SequenceHandler_compteur_sequence_2_31 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(26),
      I2 => Storage_sequence_complete(30),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_73
    );
  SequenceHandler_compteur_sequence_2_271 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(0),
      I2 => Storage_sequence_complete(4),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_10
    );
  SequenceHandler_compteur_sequence_2_261 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(8),
      I2 => Storage_sequence_complete(12),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_92
    );
  SequenceHandler_compteur_sequence_2_251 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(16),
      I2 => Storage_sequence_complete(20),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_91
    );
  SequenceHandler_compteur_sequence_2_241 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(24),
      I2 => Storage_sequence_complete(28),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_83
    );
  SequenceHandler_compteur_sequence_2_231 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(32),
      I2 => Storage_sequence_complete(36),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_9
    );
  SequenceHandler_compteur_sequence_2_221 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(40),
      I2 => Storage_sequence_complete(44),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_81
    );
  SequenceHandler_compteur_sequence_2_211 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(48),
      I2 => Storage_sequence_complete(52),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_8
    );
  SequenceHandler_compteur_sequence_2_201 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(1),
      I2 => Storage_sequence_complete(5),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_101
    );
  SequenceHandler_compteur_sequence_2_21 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(25),
      I2 => Storage_sequence_complete(29),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_89
    );
  SequenceHandler_compteur_sequence_2_191 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(19),
      I2 => Storage_sequence_complete(23),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_810
    );
  SequenceHandler_compteur_sequence_2_181 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(18),
      I2 => Storage_sequence_complete(22),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_84
    );
  SequenceHandler_compteur_sequence_2_171 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(17),
      I2 => Storage_sequence_complete(21),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_95
    );
  SequenceHandler_compteur_sequence_2_161 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(11),
      I2 => Storage_sequence_complete(15),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_811
    );
  SequenceHandler_compteur_sequence_2_151 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(10),
      I2 => Storage_sequence_complete(14),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_85
    );
  SequenceHandler_compteur_sequence_2_141 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(9),
      I2 => Storage_sequence_complete(13),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_96
    );
  SequenceHandler_compteur_sequence_2_131 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(49),
      I2 => Storage_sequence_complete(53),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_86
    );
  SequenceHandler_compteur_sequence_2_121 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(3),
      I2 => Storage_sequence_complete(7),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_97
    );
  SequenceHandler_compteur_sequence_2_111 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(43),
      I2 => Storage_sequence_complete(47),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_76
    );
  SequenceHandler_compteur_sequence_2_101 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(42),
      I2 => Storage_sequence_complete(46),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_72
    );
  SequenceHandler_compteur_sequence_2_11 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(51),
      I2 => Storage_sequence_complete(55),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_75
    );
  SequenceHandler_compteur_sequence_2_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => Storage_sequence_complete(50),
      I2 => Storage_sequence_complete(54),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_71
    );
  Listener_compteur_local_int_2_61 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(0),
      I2 => Storage_sequence_complete(4),
      O => Listener_Mmux_color_comparante_mux0001_10
    );
  Listener_compteur_local_int_2_51 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(8),
      I2 => Storage_sequence_complete(12),
      O => Listener_Mmux_color_comparante_mux0001_92
    );
  Listener_compteur_local_int_2_41 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(16),
      I2 => Storage_sequence_complete(20),
      O => Listener_Mmux_color_comparante_mux0001_91
    );
  Listener_compteur_local_int_2_31 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(24),
      I2 => Storage_sequence_complete(28),
      O => Listener_Mmux_color_comparante_mux0001_83
    );
  Listener_compteur_local_int_2_21 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(32),
      I2 => Storage_sequence_complete(36),
      O => Listener_Mmux_color_comparante_mux0001_9
    );
  Listener_compteur_local_int_2_1491 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(41),
      I2 => Storage_sequence_complete(45),
      O => Listener_Mmux_color_comparante_mux0001_87
    );
  Listener_compteur_local_int_2_1481 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(2),
      I2 => Storage_sequence_complete(6),
      O => Listener_Mmux_color_comparante_mux0001_93
    );
  Listener_compteur_local_int_2_1471 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(35),
      I2 => Storage_sequence_complete(39),
      O => Listener_Mmux_color_comparante_mux0001_88
    );
  Listener_compteur_local_int_2_1461 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(34),
      I2 => Storage_sequence_complete(38),
      O => Listener_Mmux_color_comparante_mux0001_82
    );
  Listener_compteur_local_int_2_1451 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(33),
      I2 => Storage_sequence_complete(37),
      O => Listener_Mmux_color_comparante_mux0001_94
    );
  Listener_compteur_local_int_2_1441 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(27),
      I2 => Storage_sequence_complete(31),
      O => Listener_Mmux_color_comparante_mux0001_77
    );
  Listener_compteur_local_int_2_1431 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(26),
      I2 => Storage_sequence_complete(30),
      O => Listener_Mmux_color_comparante_mux0001_73
    );
  Listener_compteur_local_int_2_14201 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(1),
      I2 => Storage_sequence_complete(5),
      O => Listener_Mmux_color_comparante_mux0001_101
    );
  Listener_compteur_local_int_2_1421 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(25),
      I2 => Storage_sequence_complete(29),
      O => Listener_Mmux_color_comparante_mux0001_89
    );
  Listener_compteur_local_int_2_14191 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(19),
      I2 => Storage_sequence_complete(23),
      O => Listener_Mmux_color_comparante_mux0001_810
    );
  Listener_compteur_local_int_2_14181 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(18),
      I2 => Storage_sequence_complete(22),
      O => Listener_Mmux_color_comparante_mux0001_84
    );
  Listener_compteur_local_int_2_14171 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(17),
      I2 => Storage_sequence_complete(21),
      O => Listener_Mmux_color_comparante_mux0001_95
    );
  Listener_compteur_local_int_2_14161 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(11),
      I2 => Storage_sequence_complete(15),
      O => Listener_Mmux_color_comparante_mux0001_811
    );
  Listener_compteur_local_int_2_14151 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(10),
      I2 => Storage_sequence_complete(14),
      O => Listener_Mmux_color_comparante_mux0001_85
    );
  Listener_compteur_local_int_2_14141 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(9),
      I2 => Storage_sequence_complete(13),
      O => Listener_Mmux_color_comparante_mux0001_96
    );
  Listener_compteur_local_int_2_14131 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(49),
      I2 => Storage_sequence_complete(53),
      O => Listener_Mmux_color_comparante_mux0001_86
    );
  Listener_compteur_local_int_2_14121 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(3),
      I2 => Storage_sequence_complete(7),
      O => Listener_Mmux_color_comparante_mux0001_97
    );
  Listener_compteur_local_int_2_14111 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(43),
      I2 => Storage_sequence_complete(47),
      O => Listener_Mmux_color_comparante_mux0001_76
    );
  Listener_compteur_local_int_2_14101 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(42),
      I2 => Storage_sequence_complete(46),
      O => Listener_Mmux_color_comparante_mux0001_72
    );
  Listener_compteur_local_int_2_1411 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(51),
      I2 => Storage_sequence_complete(55),
      O => Listener_Mmux_color_comparante_mux0001_75
    );
  Listener_compteur_local_int_2_141 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(50),
      I2 => Storage_sequence_complete(54),
      O => Listener_Mmux_color_comparante_mux0001_71
    );
  Listener_compteur_local_int_2_11 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(40),
      I2 => Storage_sequence_complete(44),
      O => Listener_Mmux_color_comparante_mux0001_81
    );
  Listener_compteur_local_int_2_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Listener_compteur_local_int(2),
      I1 => Storage_sequence_complete(48),
      I2 => Storage_sequence_complete(52),
      O => Listener_Mmux_color_comparante_mux0001_8
    );
  Storage_sequence_complete_0_not00011 : LUT3
    generic map(
      INIT => X"64"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd1_619,
      I1 => Storage_etat_present_FSM_FFd2_621,
      I2 => Storage_bus_color_futur_cmp_eq0005,
      O => Storage_sequence_complete_0_not0001
    );
  WorkerScore_joueur4_R_not00011 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => WorkerScore_joueur4_R(3),
      I1 => WorkerScore_N40,
      I2 => WorkerScore_N59,
      I3 => WorkerScore_N33,
      O => WorkerScore_joueur4_R_not0001
    );
  WorkerScore_joueur4_R_mux0007_0_21 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => WorkerScore_N01,
      I2 => WorkerScore_N34,
      I3 => PlayerHandler_JoueurEnCours(1),
      O => WorkerScore_N40
    );
  WorkerScore_joueur3_R_mux0007_0_21 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => WorkerScore_N11,
      I1 => PlayerHandler_JoueurEnCours(0),
      I2 => WorkerScore_N34,
      I3 => PlayerHandler_JoueurEnCours(1),
      O => WorkerScore_N41
    );
  WorkerScore_joueur2_R_mux0006_0_21 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => WorkerScore_N2,
      I1 => PlayerHandler_JoueurEnCours(1),
      I2 => WorkerScore_N34,
      I3 => PlayerHandler_JoueurEnCours(0),
      O => WorkerScore_N42
    );
  WorkerScore_joueur1_L_not00011 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => WorkerScore_joueur1_L(3),
      I1 => WorkerScore_N39,
      I2 => WorkerScore_N60,
      I3 => WorkerScore_N33,
      O => WorkerScore_joueur1_L_not0001
    );
  WorkerScore_joueur1_L_mux0004_0_31 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => WorkerScore_joueur1_R(3),
      I1 => WorkerScore_joueur1_R(0),
      I2 => WorkerScore_N58,
      I3 => WorkerScore_N57,
      O => WorkerScore_N39
    );
  Storage_sequence_complete_9_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(4),
      I2 => Storage_N19,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_9_mux0003
    );
  Storage_sequence_complete_8_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_sequence_complete_40_cmp_eq0001,
      I1 => Storage_compteur(4),
      I2 => Storage_N18,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_8_mux0003
    );
  Storage_sequence_complete_7_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(1),
      O => Storage_sequence_complete_7_mux0003
    );
  Storage_sequence_complete_6_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_6_not0001
    );
  Storage_sequence_complete_6_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_6_mux0003
    );
  Storage_sequence_complete_5_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_5_not0001
    );
  Storage_sequence_complete_5_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(4),
      I2 => Storage_N19,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_5_mux0003
    );
  Storage_sequence_complete_59_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_etat_present_cmp_eq0003,
      I2 => Storage_N62,
      I3 => Storage_N56,
      O => Storage_sequence_complete_59_mux0003
    );
  Storage_sequence_complete_58_not000111 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(1),
      I2 => Storage_compteur(5),
      I3 => Storage_compteur(4),
      O => Storage_N56
    );
  Storage_sequence_complete_57_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_N19,
      I2 => Storage_compteur(5),
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_57_mux0003
    );
  Storage_sequence_complete_56_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(5),
      I2 => Storage_sequence_complete_40_cmp_eq0001,
      I3 => Storage_N18,
      O => Storage_sequence_complete_56_mux0003
    );
  Storage_sequence_complete_52_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N7,
      I2 => Storage_sequence_complete_36_or0000,
      I3 => Storage_N4,
      O => Storage_sequence_complete_52_not0001
    );
  Storage_sequence_complete_4_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_sequence_complete_36_or0000,
      I2 => Storage_compteur(3),
      I3 => Storage_N20,
      O => Storage_sequence_complete_4_not0001
    );
  Storage_sequence_complete_48_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N4,
      I2 => Storage_sequence_complete_16_or0000,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_48_not0001
    );
  Storage_sequence_complete_48_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N18,
      I1 => Storage_compteur(5),
      I2 => Storage_N26,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_48_mux0003
    );
  Storage_sequence_complete_47_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_47_mux0003
    );
  Storage_sequence_complete_46_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_46_not0001
    );
  Storage_sequence_complete_46_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(1),
      I2 => Storage_N5,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_46_mux0003
    );
  Storage_sequence_complete_45_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_45_not0001
    );
  Storage_sequence_complete_45_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_45_mux0003
    );
  Storage_sequence_complete_44_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_compteur(3),
      I2 => Storage_sequence_complete_36_or0000,
      I3 => Storage_N5,
      O => Storage_sequence_complete_44_not0001
    );
  Storage_sequence_complete_44_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_sequence_complete_36_or0000,
      I2 => Storage_N5,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_44_mux0003
    );
  Storage_sequence_complete_43_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_43_mux0003
    );
  Storage_sequence_complete_42_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N15,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_42_not0001
    );
  Storage_sequence_complete_42_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(1),
      I2 => Storage_N5,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_42_mux0003
    );
  Storage_sequence_complete_3_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(1),
      O => Storage_sequence_complete_3_mux0003
    );
  Storage_sequence_complete_39_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_39_mux0003
    );
  Storage_sequence_complete_38_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_38_not0001
    );
  Storage_sequence_complete_38_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(1),
      I2 => Storage_N5,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_38_mux0003
    );
  Storage_sequence_complete_37_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_37_not0001
    );
  Storage_sequence_complete_37_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_37_mux0003
    );
  Storage_sequence_complete_36_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_sequence_complete_36_or0000,
      I2 => Storage_compteur(3),
      I3 => Storage_N5,
      O => Storage_sequence_complete_36_not0001
    );
  Storage_sequence_complete_36_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_sequence_complete_36_or0000,
      I2 => Storage_N5,
      I3 => Storage_compteur(3),
      O => Storage_sequence_complete_36_mux0003
    );
  Storage_sequence_complete_35_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(1),
      I2 => Storage_bus_color_present(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_35_mux0003
    );
  Storage_sequence_complete_34_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N14,
      I2 => Storage_compteur(1),
      I3 => Storage_N5,
      O => Storage_sequence_complete_34_not0001
    );
  Storage_sequence_complete_34_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(1),
      I2 => Storage_N5,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_34_mux0003
    );
  Storage_sequence_complete_31_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N16,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_31_mux0003
    );
  Storage_sequence_complete_30_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_30_not0001
    );
  Storage_sequence_complete_30_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N16,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_30_mux0003
    );
  Storage_sequence_complete_2_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N14,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_2_not0001
    );
  Storage_sequence_complete_2_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_2_mux0003
    );
  Storage_sequence_complete_29_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_29_not0001
    );
  Storage_sequence_complete_29_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(5),
      I2 => Storage_N19,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_29_mux0003
    );
  Storage_sequence_complete_28_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_compteur(3),
      I2 => Storage_sequence_complete_36_or0000,
      I3 => Storage_N21,
      O => Storage_sequence_complete_28_not0001
    );
  Storage_sequence_complete_27_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N15,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_27_mux0003
    );
  Storage_sequence_complete_26_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N15,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_26_not0001
    );
  Storage_sequence_complete_26_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N15,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_26_mux0003
    );
  Storage_sequence_complete_25_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(5),
      I2 => Storage_N19,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_25_mux0003
    );
  Storage_sequence_complete_24_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(5),
      I2 => Storage_sequence_complete_40_cmp_eq0001,
      I3 => Storage_N18,
      O => Storage_sequence_complete_24_mux0003
    );
  Storage_sequence_complete_23_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N17,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_23_mux0003
    );
  Storage_sequence_complete_22_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_22_not0001
    );
  Storage_sequence_complete_22_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N17,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_22_mux0003
    );
  Storage_sequence_complete_21_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N17,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_21_not0001
    );
  Storage_sequence_complete_21_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N17,
      I1 => Storage_compteur(5),
      I2 => Storage_N19,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_21_mux0003
    );
  Storage_sequence_complete_20_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_sequence_complete_36_or0000,
      I2 => Storage_compteur(3),
      I3 => Storage_N21,
      O => Storage_sequence_complete_20_not0001
    );
  Storage_sequence_complete_1_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(4),
      I2 => Storage_N19,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_1_mux0003
    );
  Storage_sequence_complete_19_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(1),
      I1 => Storage_N14,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_19_mux0003
    );
  Storage_sequence_complete_18_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N14,
      I2 => Storage_compteur(1),
      I3 => Storage_N21,
      O => Storage_sequence_complete_18_not0001
    );
  Storage_sequence_complete_18_mux00031 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N14,
      I2 => Storage_N6,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_18_mux0003
    );
  Storage_sequence_complete_17_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N14,
      I1 => Storage_compteur(5),
      I2 => Storage_N19,
      I3 => Storage_compteur(4),
      O => Storage_sequence_complete_17_mux0003
    );
  Storage_sequence_complete_16_or00001 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(1),
      I2 => Storage_compteur(2),
      I3 => Storage_compteur(3),
      O => Storage_sequence_complete_16_or0000
    );
  Storage_sequence_complete_16_not00011 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_compteur(5),
      I2 => Storage_sequence_complete_16_or0000,
      I3 => Storage_N4,
      O => Storage_sequence_complete_16_not0001
    );
  Storage_sequence_complete_16_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(5),
      I2 => Storage_N18,
      I3 => Storage_N26,
      O => Storage_sequence_complete_16_mux0003
    );
  Storage_sequence_complete_15_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(1),
      O => Storage_sequence_complete_15_mux0003
    );
  Storage_sequence_complete_14_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_14_not0001
    );
  Storage_sequence_complete_14_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_14_mux0003
    );
  Storage_sequence_complete_13_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N16,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_13_not0001
    );
  Storage_sequence_complete_13_mux00031 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N16,
      I1 => Storage_compteur(4),
      I2 => Storage_N19,
      I3 => Storage_compteur(5),
      O => Storage_sequence_complete_13_mux0003
    );
  Storage_sequence_complete_12_not00011 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_compteur(3),
      I2 => Storage_sequence_complete_36_or0000,
      I3 => Storage_N20,
      O => Storage_sequence_complete_12_not0001
    );
  Storage_sequence_complete_11_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(1),
      O => Storage_sequence_complete_11_mux0003
    );
  Storage_sequence_complete_10_not00011 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_N15,
      I2 => Storage_compteur(1),
      I3 => Storage_N20,
      O => Storage_sequence_complete_10_not0001
    );
  Storage_sequence_complete_10_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_N15,
      I1 => Storage_compteur(4),
      I2 => Storage_N6,
      I3 => Storage_bus_color_present(0),
      O => Storage_sequence_complete_10_mux0003
    );
  Storage_bus_color_futur_cmp_eq00051 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N26,
      I1 => Storage_compteur(5),
      I2 => Storage_N62,
      I3 => Storage_compteur(4),
      O => Storage_bus_color_futur_cmp_eq0005
    );
  SequenceHandler_s_couleur_futur_not00011 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => SequenceHandler_etat_present(0),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_s_couleur_futur_cmp_le0000,
      I3 => SequenceHandler_etat_present(4),
      O => SequenceHandler_s_couleur_futur_not0001
    );
  SequenceHandler_s_couleur_futur_mux0003_3_1 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => SequenceHandler_couleur_temp(1),
      I1 => SequenceHandler_couleur_temp(0),
      I2 => SequenceHandler_s_couleur_futur_cmp_le0000,
      I3 => SequenceHandler_etat_present(1),
      O => SequenceHandler_s_couleur_futur_mux0003(3)
    );
  SequenceHandler_s_couleur_futur_mux0003_2_1 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => SequenceHandler_couleur_temp(1),
      I1 => SequenceHandler_couleur_temp(0),
      I2 => SequenceHandler_s_couleur_futur_cmp_le0000,
      I3 => SequenceHandler_etat_present(1),
      O => SequenceHandler_s_couleur_futur_mux0003(2)
    );
  SequenceHandler_s_couleur_futur_mux0003_1_1 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => SequenceHandler_couleur_temp(0),
      I1 => SequenceHandler_couleur_temp(1),
      I2 => SequenceHandler_s_couleur_futur_cmp_le0000,
      I3 => SequenceHandler_etat_present(1),
      O => SequenceHandler_s_couleur_futur_mux0003(1)
    );
  SequenceHandler_s_couleur_futur_mux0003_0_1 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => SequenceHandler_etat_present(1),
      I1 => SequenceHandler_couleur_temp(1),
      I2 => SequenceHandler_s_couleur_futur_cmp_le0000,
      I3 => SequenceHandler_couleur_temp(0),
      O => SequenceHandler_s_couleur_futur_mux0003(0)
    );
  Listener_etat_futur_mux0005_3_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => Listener_etat_present(3),
      I1 => Listener_N11,
      I2 => Listener_etat_present(1),
      I3 => Listener_etat_present(2),
      O => Listener_etat_futur_mux0005(3)
    );
  Storage_sequence_complete_59_not00011 : LUT4
    generic map(
      INIT => X"6222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N56,
      I3 => Storage_N62,
      O => Storage_sequence_complete_59_not0001
    );
  SequenceHandler_compteur_ecouter_not00011 : LUT4
    generic map(
      INIT => X"7D28"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => Listener_etat_present(1),
      I2 => Listener_etat_present(2),
      I3 => SequenceHandler_etat_present(4),
      O => SequenceHandler_compteur_ecouter_not0001
    );
  Storage_sequence_complete_56_not0001 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => Storage_N11,
      I1 => Storage_compteur(5),
      I2 => Storage_compteur(4),
      I3 => N2,
      O => Storage_sequence_complete_56_not0001_773
    );
  WorkerScore_joueur3_L_mux0006_0_1_SW0 : LUT4
    generic map(
      INIT => X"F7FF"
    )
    port map (
      I0 => WorkerScore_joueur3_R(0),
      I1 => PlayerHandler_JoueurEnCours(1),
      I2 => PlayerHandler_JoueurEnCours(0),
      I3 => WorkerScore_N34,
      O => N19
    );
  WorkerScore_joueur3_L_mux0006_0_1 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => WorkerScore_joueur3_R(3),
      I1 => WorkerScore_joueur3_R(2),
      I2 => WorkerScore_joueur3_R(1),
      I3 => N19,
      O => WorkerScore_N37
    );
  WorkerScore_joueur2_L_mux0005_0_1_SW0 : LUT4
    generic map(
      INIT => X"F7FF"
    )
    port map (
      I0 => WorkerScore_joueur2_R(0),
      I1 => PlayerHandler_JoueurEnCours(0),
      I2 => PlayerHandler_JoueurEnCours(1),
      I3 => WorkerScore_N34,
      O => N21
    );
  WorkerScore_joueur2_L_mux0005_0_1 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => WorkerScore_joueur2_R(3),
      I1 => WorkerScore_joueur2_R(2),
      I2 => WorkerScore_joueur2_R(1),
      I3 => N21,
      O => WorkerScore_N38
    );
  WorkerScore_joueur1_R_not000113 : LUT4
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => WorkerScore_N58,
      I1 => WorkerScore_joueur1_L(3),
      I2 => WorkerScore_joueur1_R(0),
      I3 => WorkerScore_N60,
      O => WorkerScore_joueur1_R_not000113_983
    );
  WorkerScore_joueur1_R_not000132 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => WorkerScore_joueur1_R(3),
      I1 => WorkerScore_N57,
      I2 => WorkerScore_joueur1_R_not000113_983,
      I3 => WorkerScore_N33,
      O => WorkerScore_joueur1_R_not0001
    );
  SequenceHandler_etat_futur_mux0004_3_SW0 : LUT4
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => SequenceHandler_etat_present(4),
      I1 => SequenceHandler_etat_systeme_temp(1),
      I2 => SequenceHandler_etat_systeme_temp(2),
      I3 => SequenceHandler_etat_systeme_temp(0),
      O => N23
    );
  SequenceHandler_etat_futur_mux0004_2_SW0 : LUT4
    generic map(
      INIT => X"AA8A"
    )
    port map (
      I0 => SequenceHandler_etat_present(4),
      I1 => SequenceHandler_etat_systeme_temp(0),
      I2 => SequenceHandler_etat_systeme_temp(2),
      I3 => SequenceHandler_etat_systeme_temp(1),
      O => N25
    );
  SequenceHandler_etat_futur_mux0004_2_Q : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => N25,
      I1 => etat_systeme(1),
      I2 => etat_systeme(2),
      I3 => etat_systeme(0),
      O => SequenceHandler_etat_futur_mux0004(2)
    );
  SequenceHandler_etat_futur_mux0004_0_13 : LUT4
    generic map(
      INIT => X"3133"
    )
    port map (
      I0 => SequenceHandler_etat_systeme_temp(0),
      I1 => etat_systeme(1),
      I2 => SequenceHandler_etat_systeme_temp(1),
      I3 => SequenceHandler_etat_systeme_temp(2),
      O => SequenceHandler_etat_futur_mux0004_0_13_534
    );
  SequenceHandler_etat_futur_mux0004_0_27 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => WorkerClock_impulsion_938,
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_futur_mux0004_0_13_534,
      I3 => SequenceHandler_etat_futur_mux0004_0_16_535,
      O => SequenceHandler_etat_futur_mux0004(0)
    );
  SequenceHandler_etat_futur_not000112 : LUT4
    generic map(
      INIT => X"10FF"
    )
    port map (
      I0 => SequenceHandler_N111,
      I1 => SequenceHandler_delay(0),
      I2 => SequenceHandler_delay_cmp_ge0000,
      I3 => SequenceHandler_etat_present(3),
      O => SequenceHandler_etat_futur_not000112_543
    );
  SequenceHandler_etat_futur_mux0004_4_62 : LUT4
    generic map(
      INIT => X"FF32"
    )
    port map (
      I0 => SequenceHandler_etat_systeme_temp(2),
      I1 => etat_systeme(2),
      I2 => SequenceHandler_N20,
      I3 => SequenceHandler_etat_futur_mux0004_4_52_540,
      O => SequenceHandler_etat_futur_mux0004_4_62_541
    );
  Listener_bus_color_futur_0_1 : LUT3
    generic map(
      INIT => X"51"
    )
    port map (
      I0 => btn_bleu_IBUF_1054,
      I1 => btn_jaune_IBUF_1056,
      I2 => btn_vert_IBUF_1064,
      O => Listener_bus_color_futur(0)
    );
  Listener_bus_color_futur_2_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => btn_jaune_IBUF_1056,
      I1 => btn_rouge_IBUF_1060,
      I2 => btn_vert_IBUF_1064,
      I3 => btn_bleu_IBUF_1054,
      O => Listener_bus_color_futur(2)
    );
  SequenceHandler_e_couleur_futur_mux0003_0_1 : LUT4
    generic map(
      INIT => X"2820"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => Listener_etat_present(1),
      I2 => Listener_etat_present(2),
      I3 => btn_bleu_IBUF_1054,
      O => SequenceHandler_e_couleur_futur_mux0003(0)
    );
  ViewGame_led_vert_and00001 : LUT4
    generic map(
      INIT => X"1110"
    )
    port map (
      I0 => com_options_0_IBUF_1071,
      I1 => GameHandler_gestion_etat_syteme_reinit_43,
      I2 => ViewGame_couleur_present(4),
      I3 => ViewGame_couleur_present(5),
      O => led_vert_OBUF_1084
    );
  ViewGame_led_rouge_and00001 : LUT4
    generic map(
      INIT => X"1110"
    )
    port map (
      I0 => com_options_0_IBUF_1071,
      I1 => GameHandler_gestion_etat_syteme_reinit_43,
      I2 => ViewGame_couleur_present(3),
      I3 => ViewGame_couleur_present(5),
      O => led_rouge_OBUF_1082
    );
  ViewGame_led_jaune_and00001 : LUT4
    generic map(
      INIT => X"1110"
    )
    port map (
      I0 => com_options_0_IBUF_1071,
      I1 => GameHandler_gestion_etat_syteme_reinit_43,
      I2 => ViewGame_couleur_present(2),
      I3 => ViewGame_couleur_present(5),
      O => led_jaune_OBUF_1080
    );
  ViewGame_led_bleu_and00001 : LUT4
    generic map(
      INIT => X"1110"
    )
    port map (
      I0 => com_options_0_IBUF_1071,
      I1 => GameHandler_gestion_etat_syteme_reinit_43,
      I2 => ViewGame_couleur_present(1),
      I3 => ViewGame_couleur_present(5),
      O => led_bleu_OBUF_1078
    );
  SequenceHandler_compteur_sequence_and00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => SequenceHandler_etat_present(1),
      I1 => WorkerClock_impulsion_938,
      O => SequenceHandler_compteur_sequence_and0000
    );
  GameHandler_gestion_etat_syteme_reinit_and00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_0_Q,
      O => GameHandler_gestion_etat_syteme_reinit_and0000
    );
  SequenceHandler_Mcount_compteur_demonstration_xor_1_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(1),
      I1 => SequenceHandler_compteur_demonstration(0),
      O => SequenceHandler_Result_1_1
    );
  SequenceHandler_Maccum_compteur_sequence_xor_2_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(2),
      I1 => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Result(2)
    );
  SequenceHandler_Mcount_compteur_demonstration_xor_2_11 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(2),
      I1 => SequenceHandler_compteur_demonstration(0),
      I2 => SequenceHandler_compteur_demonstration(1),
      O => SequenceHandler_Result_2_1
    );
  SequenceHandler_Maccum_compteur_sequence_xor_3_11 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(3),
      I1 => SequenceHandler_compteur_sequence(1),
      I2 => SequenceHandler_compteur_sequence(2),
      O => SequenceHandler_Result(3)
    );
  GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"9C"
    )
    port map (
      I0 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd2_55,
      I1 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I2 => btn_mode_IBUF_1058,
      O => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_In
    );
  GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"9C"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd2_51,
      I1 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_49,
      I2 => btn_start_IBUF_1062,
      O => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_In
    );
  GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"9C"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd2_47,
      I1 => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45,
      I2 => btn_start_IBUF_1062,
      O => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_In
    );
  ViewGame_couleur_futur_cmp_eq00041 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => SequenceHandler_couleur_present(0),
      I1 => SequenceHandler_couleur_present(1),
      I2 => SequenceHandler_couleur_present(2),
      I3 => SequenceHandler_couleur_present(3),
      O => ViewGame_couleur_futur(5)
    );
  ViewGame_couleur_futur_cmp_eq00031 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => SequenceHandler_couleur_present(3),
      I1 => SequenceHandler_couleur_present(0),
      I2 => SequenceHandler_couleur_present(1),
      I3 => SequenceHandler_couleur_present(2),
      O => ViewGame_couleur_futur(4)
    );
  ViewGame_couleur_futur_cmp_eq00021 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => SequenceHandler_couleur_present(2),
      I1 => SequenceHandler_couleur_present(0),
      I2 => SequenceHandler_couleur_present(1),
      I3 => SequenceHandler_couleur_present(3),
      O => ViewGame_couleur_futur(3)
    );
  ViewGame_couleur_futur_cmp_eq00011 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => SequenceHandler_couleur_present(1),
      I1 => SequenceHandler_couleur_present(0),
      I2 => SequenceHandler_couleur_present(2),
      I3 => SequenceHandler_couleur_present(3),
      O => ViewGame_couleur_futur(2)
    );
  ViewGame_couleur_futur_cmp_eq00001 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => SequenceHandler_couleur_present(0),
      I1 => SequenceHandler_couleur_present(1),
      I2 => SequenceHandler_couleur_present(2),
      I3 => SequenceHandler_couleur_present(3),
      O => ViewGame_couleur_futur(1)
    );
  GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_In1 : LUT4
    generic map(
      INIT => X"DC8C"
    )
    port map (
      I0 => btn_mode_IBUF_1058,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd3_60,
      I3 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      O => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_In
    );
  GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_In1 : LUT4
    generic map(
      INIT => X"8C9C"
    )
    port map (
      I0 => btn_mode_IBUF_1058,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd3_60,
      I3 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      O => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_In
    );
  SequenceHandler_Mcount_compteur_demonstration_xor_3_11 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(3),
      I1 => SequenceHandler_compteur_demonstration(0),
      I2 => SequenceHandler_compteur_demonstration(1),
      I3 => SequenceHandler_compteur_demonstration(2),
      O => SequenceHandler_Result_3_1
    );
  PlayerHandler_Joueur1_0_mux00001 : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => PlayerHandler_Joueur1(0),
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      O => PlayerHandler_Joueur1_0_mux0000
    );
  SequenceHandler_compteur_ecouter_mux0003_1_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => SequenceHandler_N21,
      I1 => SequenceHandler_compteur_ecouter(1),
      I2 => Storage_compteur(1),
      I3 => SequenceHandler_N24,
      O => SequenceHandler_compteur_ecouter_mux0003(1)
    );
  Listener_timer_checker_mux0002_3_1 : LUT4
    generic map(
      INIT => X"0155"
    )
    port map (
      I0 => Listener_timer_checker(0),
      I1 => Listener_timer_checker(2),
      I2 => Listener_timer_checker(1),
      I3 => Listener_timer_checker(3),
      O => Listener_timer_checker_mux0002(3)
    );
  Listener_color_a_tester_mux0001_1_1 : LUT4
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => Listener_bus_color_present(1),
      I1 => Listener_bus_color_present(0),
      I2 => Listener_bus_color_present(2),
      I3 => Listener_color_a_tester(1),
      O => Listener_color_a_tester_mux0001(1)
    );
  Listener_color_a_tester_mux0001_0_1 : LUT4
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => Listener_bus_color_present(0),
      I1 => Listener_bus_color_present(1),
      I2 => Listener_bus_color_present(2),
      I3 => Listener_color_a_tester(0),
      O => Listener_color_a_tester_mux0001(0)
    );
  WorkerScore_joueur4_R_mux0007_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N40,
      I1 => WorkerScore_joueur4_R(1),
      I2 => WorkerScore_joueur4_R(0),
      I3 => WorkerScore_joueur4_R(3),
      O => WorkerScore_joueur4_R_mux0007(1)
    );
  WorkerScore_joueur4_L_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N36,
      I1 => WorkerScore_joueur4_L(1),
      I2 => WorkerScore_joueur4_L(0),
      I3 => WorkerScore_joueur4_L(3),
      O => WorkerScore_joueur4_L_mux0006(1)
    );
  WorkerScore_joueur3_R_mux0007_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N41,
      I1 => WorkerScore_joueur3_R(1),
      I2 => WorkerScore_joueur3_R(0),
      I3 => WorkerScore_joueur3_R(3),
      O => WorkerScore_joueur3_R_mux0007(1)
    );
  WorkerScore_joueur3_L_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N37,
      I1 => WorkerScore_joueur3_L(1),
      I2 => WorkerScore_joueur3_L(0),
      I3 => WorkerScore_joueur3_L(3),
      O => WorkerScore_joueur3_L_mux0006(1)
    );
  WorkerScore_joueur2_R_mux0006_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N42,
      I1 => WorkerScore_joueur2_R(1),
      I2 => WorkerScore_joueur2_R(0),
      I3 => WorkerScore_joueur2_R(3),
      O => WorkerScore_joueur2_R_mux0006(1)
    );
  WorkerScore_joueur2_L_mux0005_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N38,
      I1 => WorkerScore_joueur2_L(1),
      I2 => WorkerScore_joueur2_L(0),
      I3 => WorkerScore_joueur2_L(3),
      O => WorkerScore_joueur2_L_mux0005(1)
    );
  WorkerScore_joueur1_R_mux0005_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N57,
      I1 => WorkerScore_joueur1_R(1),
      I2 => WorkerScore_joueur1_R(0),
      I3 => WorkerScore_joueur1_R(3),
      O => WorkerScore_joueur1_R_mux0005(1)
    );
  WorkerScore_joueur1_L_mux0004_1_1 : LUT4
    generic map(
      INIT => X"0028"
    )
    port map (
      I0 => WorkerScore_N39,
      I1 => WorkerScore_joueur1_L(1),
      I2 => WorkerScore_joueur1_L(0),
      I3 => WorkerScore_joueur1_L(3),
      O => WorkerScore_joueur1_L_mux0004(1)
    );
  Listener_timer_checker_mux0002_2_1 : LUT4
    generic map(
      INIT => X"0466"
    )
    port map (
      I0 => Listener_timer_checker(1),
      I1 => Listener_timer_checker(0),
      I2 => Listener_timer_checker(2),
      I3 => Listener_timer_checker(3),
      O => Listener_timer_checker_mux0002(2)
    );
  Listener_timer_checker_mux0002_1_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => Listener_timer_checker(3),
      I1 => Listener_timer_checker(2),
      I2 => Listener_timer_checker(0),
      I3 => Listener_timer_checker(1),
      O => Listener_timer_checker_mux0002(1)
    );
  Listener_timer_checker_mux0002_0_1 : LUT4
    generic map(
      INIT => X"1810"
    )
    port map (
      I0 => Listener_timer_checker(2),
      I1 => Listener_timer_checker(1),
      I2 => Listener_timer_checker(3),
      I3 => Listener_timer_checker(0),
      O => Listener_timer_checker_mux0002(0)
    );
  SequenceHandler_couleur_futur_3_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => SequenceHandler_e_couleur_present(3),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_d_couleur_present(3),
      I3 => SequenceHandler_etat_present(2),
      O => N27
    );
  SequenceHandler_couleur_futur_3_Q : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => SequenceHandler_s_couleur_present(3),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_present(0),
      I3 => N27,
      O => SequenceHandler_couleur_futur(3)
    );
  SequenceHandler_couleur_futur_2_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => SequenceHandler_e_couleur_present(2),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_d_couleur_present(2),
      I3 => SequenceHandler_etat_present(2),
      O => N29
    );
  SequenceHandler_couleur_futur_2_Q : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => SequenceHandler_s_couleur_present(2),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_present(0),
      I3 => N29,
      O => SequenceHandler_couleur_futur(2)
    );
  SequenceHandler_couleur_futur_1_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => SequenceHandler_e_couleur_present(1),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_d_couleur_present(1),
      I3 => SequenceHandler_etat_present(2),
      O => N31
    );
  SequenceHandler_couleur_futur_1_Q : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => SequenceHandler_s_couleur_present(1),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_present(0),
      I3 => N31,
      O => SequenceHandler_couleur_futur(1)
    );
  SequenceHandler_couleur_futur_0_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => SequenceHandler_e_couleur_present(0),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_d_couleur_present(0),
      I3 => SequenceHandler_etat_present(2),
      O => N33
    );
  SequenceHandler_couleur_futur_0_Q : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => SequenceHandler_s_couleur_present(0),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_present(0),
      I3 => N33,
      O => SequenceHandler_couleur_futur(0)
    );
  GameHandler_Mrom_mode_nbr_joueur21 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      O => GameHandler_Mrom_mode_nbr_joueur2
    );
  SequenceHandler_d_couleur_futur_or00001 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => SequenceHandler_etat_present(2),
      I1 => SequenceHandler_etat_present(4),
      O => SequenceHandler_d_couleur_futur_or0000
    );
  SequenceHandler_Maccum_compteur_sequence_xor_5_11 : LUT3
    generic map(
      INIT => X"9C"
    )
    port map (
      I0 => N237,
      I1 => SequenceHandler_compteur_sequence(5),
      I2 => SequenceHandler_compteur_sequence(4),
      O => SequenceHandler_Result(5)
    );
  SequenceHandler_Maccum_compteur_sequence_xor_6_11 : LUT4
    generic map(
      INIT => X"CC6C"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(4),
      I1 => SequenceHandler_compteur_sequence(6),
      I2 => SequenceHandler_compteur_sequence(5),
      I3 => SequenceHandler_N41,
      O => SequenceHandler_Result(6)
    );
  SequenceHandler_etat_systeme_temp_0_0_not00002 : LUT3
    generic map(
      INIT => X"F6"
    )
    port map (
      I0 => etat_systeme(2),
      I1 => SequenceHandler_etat_systeme_temp(2),
      I2 => N236,
      O => SequenceHandler_etat_systeme_temp_0_0_not0000
    );
  WorkerScore_joueur4_R_mux0007_3_111 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => WorkerScore_joueur4_R(1),
      I1 => WorkerScore_joueur4_R(2),
      O => WorkerScore_N59
    );
  WorkerScore_joueur1_R_mux0005_3_111 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => WorkerScore_joueur1_R(1),
      I1 => WorkerScore_joueur1_R(2),
      O => WorkerScore_N58
    );
  WorkerScore_joueur1_L_mux0004_3_111 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => WorkerScore_joueur1_L(1),
      I1 => WorkerScore_joueur1_L(2),
      O => WorkerScore_N60
    );
  WorkerScore_joueur4_R_mux0007_2_11 : LUT3
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => WorkerScore_joueur4_L(2),
      I1 => WorkerScore_joueur4_L(1),
      I2 => WorkerScore_joueur4_L(3),
      O => WorkerScore_N01
    );
  WorkerScore_joueur4_L_mux0006_0_2 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => WorkerScore_joueur4_L(0),
      I1 => WorkerScore_N01,
      I2 => WorkerScore_N36,
      O => WorkerScore_joueur4_L_mux0006(0)
    );
  WorkerScore_joueur3_R_mux0007_2_11 : LUT3
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => WorkerScore_joueur3_L(2),
      I1 => WorkerScore_joueur3_L(1),
      I2 => WorkerScore_joueur3_L(3),
      O => WorkerScore_N11
    );
  WorkerScore_joueur3_R_mux0007_0_11 : LUT3
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => WorkerScore_joueur3_R(2),
      I1 => WorkerScore_joueur3_R(1),
      I2 => WorkerScore_joueur3_R(3),
      O => WorkerScore_N23
    );
  WorkerScore_joueur3_R_mux0007_0_1 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => WorkerScore_joueur3_R(0),
      I1 => WorkerScore_N23,
      I2 => WorkerScore_N41,
      O => WorkerScore_joueur3_R_mux0007(0)
    );
  WorkerScore_joueur3_L_mux0006_0_2 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => WorkerScore_joueur3_L(0),
      I1 => WorkerScore_N11,
      I2 => WorkerScore_N37,
      O => WorkerScore_joueur3_L_mux0006(0)
    );
  WorkerScore_joueur2_R_mux0006_2_11 : LUT3
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => WorkerScore_joueur2_L(2),
      I1 => WorkerScore_joueur2_L(1),
      I2 => WorkerScore_joueur2_L(3),
      O => WorkerScore_N2
    );
  WorkerScore_joueur2_R_mux0006_0_11 : LUT3
    generic map(
      INIT => X"1F"
    )
    port map (
      I0 => WorkerScore_joueur2_R(2),
      I1 => WorkerScore_joueur2_R(1),
      I2 => WorkerScore_joueur2_R(3),
      O => WorkerScore_N21
    );
  WorkerScore_joueur2_R_mux0006_0_1 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => WorkerScore_joueur2_R(0),
      I1 => WorkerScore_N21,
      I2 => WorkerScore_N42,
      O => WorkerScore_joueur2_R_mux0006(0)
    );
  WorkerScore_joueur2_L_mux0005_0_2 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => WorkerScore_joueur2_L(0),
      I1 => WorkerScore_N2,
      I2 => WorkerScore_N38,
      O => WorkerScore_joueur2_L_mux0005(0)
    );
  SequenceHandler_delay_mux0001_0_11 : LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      I0 => SequenceHandler_delay(3),
      I1 => SequenceHandler_delay(2),
      I2 => SequenceHandler_delay(1),
      O => SequenceHandler_N111
    );
  WorkerScore_joueur4_R_mux0007_0_1 : LUT4
    generic map(
      INIT => X"4050"
    )
    port map (
      I0 => WorkerScore_joueur4_R(0),
      I1 => WorkerScore_N59,
      I2 => WorkerScore_N40,
      I3 => WorkerScore_joueur4_R(3),
      O => WorkerScore_joueur4_R_mux0007(0)
    );
  WorkerScore_joueur1_R_mux0005_0_1 : LUT4
    generic map(
      INIT => X"4050"
    )
    port map (
      I0 => WorkerScore_joueur1_R(0),
      I1 => WorkerScore_N58,
      I2 => WorkerScore_N57,
      I3 => WorkerScore_joueur1_R(3),
      O => WorkerScore_joueur1_R_mux0005(0)
    );
  WorkerScore_joueur1_L_mux0004_0_1 : LUT4
    generic map(
      INIT => X"4050"
    )
    port map (
      I0 => WorkerScore_joueur1_L(0),
      I1 => WorkerScore_N60,
      I2 => WorkerScore_N39,
      I3 => WorkerScore_joueur1_L(3),
      O => WorkerScore_joueur1_L_mux0004(0)
    );
  SequenceHandler_delay_mux0001_0_1 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => SequenceHandler_N111,
      I1 => SequenceHandler_delay(0),
      I2 => SequenceHandler_etat_present(3),
      I3 => SequenceHandler_delay_cmp_ge0000,
      O => SequenceHandler_delay_mux0001(0)
    );
  Listener_compteur_local_int_mux0004_4_11 : LUT4
    generic map(
      INIT => X"6FF6"
    )
    port map (
      I0 => Listener_color_a_tester(0),
      I1 => Listener_color_comparante(0),
      I2 => Listener_color_a_tester(1),
      I3 => Listener_color_comparante(1),
      O => Listener_N0
    );
  SequenceHandler_e_couleur_futur_mux0003_3_1 : LUT4
    generic map(
      INIT => X"2820"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => Listener_etat_present(1),
      I2 => Listener_etat_present(2),
      I3 => Storage_bus_color_futur_0_51,
      O => SequenceHandler_e_couleur_futur_mux0003(3)
    );
  SequenceHandler_e_couleur_futur_mux0003_2_1 : LUT4
    generic map(
      INIT => X"2820"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => Listener_etat_present(1),
      I2 => Listener_etat_present(2),
      I3 => couleur_jouee(2),
      O => SequenceHandler_e_couleur_futur_mux0003(2)
    );
  SequenceHandler_Maccum_compteur_sequence_xor_7_1 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(7),
      I1 => SequenceHandler_compteur_sequence(6),
      I2 => SequenceHandler_compteur_sequence(5),
      I3 => N62,
      O => SequenceHandler_Result(7)
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_24 : LUT4
    generic map(
      INIT => X"FFAE"
    )
    port map (
      I0 => N240,
      I1 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      I2 => SequenceHandler_etat_present(4),
      I3 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_24_25
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_31 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_futur_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_24_25,
      I2 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_12,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_9 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => N238,
      I1 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I2 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_49,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_9_34
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_16 : LUT3
    generic map(
      INIT => X"F4"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45,
      I1 => GameHandler_gestion_etat_syteme_etat_present_0_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_16_32
    );
  Storage_compteur_mux0003_4_Q : LUT4
    generic map(
      INIT => X"6C00"
    )
    port map (
      I0 => Storage_compteur(3),
      I1 => Storage_compteur(4),
      I2 => N82,
      I3 => Storage_N13,
      O => Storage_compteur_mux0003(4)
    );
  SequenceHandler_delay_mux0001_1_SW0 : LUT4
    generic map(
      INIT => X"F77F"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => SequenceHandler_delay_cmp_ge0000,
      I2 => SequenceHandler_delay(0),
      I3 => SequenceHandler_delay(1),
      O => N84
    );
  SequenceHandler_delay_mux0001_1_SW1 : LUT4
    generic map(
      INIT => X"F7FF"
    )
    port map (
      I0 => SequenceHandler_delay(0),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_delay(1),
      I3 => SequenceHandler_delay_cmp_ge0000,
      O => N85
    );
  SequenceHandler_delay_mux0001_1_Q : LUT4
    generic map(
      INIT => X"2373"
    )
    port map (
      I0 => SequenceHandler_delay(2),
      I1 => N84,
      I2 => SequenceHandler_delay(3),
      I3 => N85,
      O => SequenceHandler_delay_mux0001(1)
    );
  View7Segments_Segment_J1_2_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => etat_systeme(2),
      I1 => WorkerScore_joueur1_R(2),
      I2 => GameHandler_Mrom_mode_nbr_joueur2,
      I3 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      O => segment_j1_2_OBUF_1097
    );
  ViewSound_buzzer12 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => SequenceHandler_couleur_present(0),
      I1 => SequenceHandler_couleur_present(1),
      I2 => SequenceHandler_couleur_present(2),
      I3 => SequenceHandler_couleur_present(3),
      O => ViewSound_buzzer12_841
    );
  WorkerClock_Mcount_compteur_eqn_01 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(0),
      O => WorkerClock_Mcount_compteur_eqn_0
    );
  WorkerScore_etat_present_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd1_958,
      I1 => WorkerScore_etat_present_FSM_FFd2_960,
      I2 => N247,
      O => WorkerScore_etat_present_FSM_FFd1_In
    );
  GameHandler_gestion_etat_syteme_etat_present_vecteur_2_1 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => etat_systeme(2)
    );
  Storage_etat_present_FSM_FFd1_In1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => PlayerHandler_JoueurEnCours_cmp_eq0000,
      I2 => Storage_etat_present_FSM_FFd1_619,
      O => Storage_etat_present_FSM_FFd1_In
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_21 : LUT4
    generic map(
      INIT => X"2820"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      I3 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_21_27
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_36 : LUT3
    generic map(
      INIT => X"F4"
    )
    port map (
      I0 => SequenceHandler_etat_present(4),
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_36_28
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_42 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_futur_0_Q,
      I1 => GameHandler_gestion_etat_syteme_N0,
      I2 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29_37,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_Q
    );
  WorkerClock_compteur_cmp_eq00004 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => WorkerClock_compteur(4),
      I1 => WorkerClock_compteur(5),
      I2 => WorkerClock_compteur(6),
      I3 => WorkerClock_compteur(7),
      O => WorkerClock_compteur_cmp_eq00004_936
    );
  WorkerClock_compteur_cmp_eq000016 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => WorkerClock_compteur(2),
      I1 => WorkerClock_compteur(3),
      I2 => WorkerClock_compteur(0),
      I3 => WorkerClock_compteur(1),
      O => WorkerClock_compteur_cmp_eq000016_934
    );
  WorkerClock_compteur_cmp_eq000033 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => WorkerClock_compteur(11),
      I1 => WorkerClock_compteur(8),
      I2 => WorkerClock_compteur(9),
      I3 => WorkerClock_compteur(10),
      O => WorkerClock_compteur_cmp_eq000033_935
    );
  WorkerClock_compteur_cmp_eq000041 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => WorkerClock_compteur(13),
      I1 => WorkerClock_compteur(15),
      I2 => WorkerClock_compteur(14),
      I3 => WorkerClock_compteur(12),
      O => WorkerClock_compteur_cmp_eq000041_937
    );
  WorkerClock_compteur_cmp_eq000053 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq00004_936,
      I1 => WorkerClock_compteur_cmp_eq000016_934,
      I2 => WorkerClock_compteur_cmp_eq000033_935,
      I3 => WorkerClock_compteur_cmp_eq000041_937,
      O => WorkerClock_compteur_cmp_eq0000
    );
  Storage_compteur_mux0003_1_2 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => N246,
      O => Storage_compteur_mux0003(1)
    );
  Storage_compteur_mux0003_5_1 : LUT3
    generic map(
      INIT => X"60"
    )
    port map (
      I0 => Storage_compteur(5),
      I1 => N242,
      I2 => Storage_N13,
      O => Storage_compteur_mux0003(5)
    );
  Storage_compteur_mux0003_2_1 : LUT3
    generic map(
      INIT => X"60"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Storage_compteur(2),
      I2 => Storage_N13,
      O => Storage_compteur_mux0003(2)
    );
  Storage_compteur_mux0003_6_1 : LUT4
    generic map(
      INIT => X"6A00"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => Storage_compteur(5),
      I2 => Storage_Madd_compteur_addsub0000_cy(4),
      I3 => Storage_N13,
      O => Storage_compteur_mux0003(6)
    );
  Storage_compteur_mux0003_3_1 : LUT4
    generic map(
      INIT => X"6C00"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(3),
      I2 => Storage_compteur(1),
      I3 => Storage_N13,
      O => Storage_compteur_mux0003(3)
    );
  SequenceHandler_delay_mux0001_3_SW0 : LUT4
    generic map(
      INIT => X"8808"
    )
    port map (
      I0 => SequenceHandler_delay_cmp_ge0000,
      I1 => SequenceHandler_delay(3),
      I2 => SequenceHandler_delay(1),
      I3 => SequenceHandler_delay(0),
      O => N96
    );
  SequenceHandler_delay_mux0001_3_SW1 : LUT4
    generic map(
      INIT => X"D75F"
    )
    port map (
      I0 => SequenceHandler_delay_cmp_ge0000,
      I1 => SequenceHandler_delay(0),
      I2 => SequenceHandler_delay(3),
      I3 => SequenceHandler_delay(1),
      O => N97
    );
  SequenceHandler_delay_mux0001_3_Q : LUT4
    generic map(
      INIT => X"20A8"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => SequenceHandler_delay(2),
      I2 => N96,
      I3 => N97,
      O => SequenceHandler_delay_mux0001(3)
    );
  Storage_compteur_mux0003_1_112 : LUT4
    generic map(
      INIT => X"57FF"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(1),
      I2 => Storage_compteur(2),
      I3 => Storage_compteur(5),
      O => Storage_compteur_mux0003_1_112_610
    );
  Storage_compteur_mux0003_1_116 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => Storage_compteur(3),
      O => Storage_compteur_mux0003_1_116_611
    );
  GameHandler_gestion_etat_syteme_etat_present_vecteur_1_1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      O => etat_systeme(1)
    );
  GameHandler_gestion_etat_syteme_etat_present_vecteur_0_1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => etat_systeme(0)
    );
  GameHandler_Mrom_mode_nbr_joueur11 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      O => GameHandler_Mrom_mode_nbr_joueur
    );
  WorkerClock_Mcount_compteur_eqn_16 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(1),
      O => WorkerClock_Mcount_compteur_eqn_1
    );
  WorkerClock_Mcount_compteur_eqn_21 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(2),
      O => WorkerClock_Mcount_compteur_eqn_2
    );
  ViewSound_buzzer_time_6_1 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => SequenceHandler_couleur_present(3),
      I1 => SequenceHandler_couleur_present(1),
      I2 => SequenceHandler_couleur_present(0),
      I3 => SequenceHandler_couleur_present(2),
      O => ViewSound_buzzer_time_6_Q
    );
  WorkerClock_Mcount_compteur_eqn_31 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(3),
      O => WorkerClock_Mcount_compteur_eqn_3
    );
  WorkerClock_Mcount_compteur_eqn_41 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(4),
      O => WorkerClock_Mcount_compteur_eqn_4
    );
  ViewSound_buzzer_time_4_1 : LUT4
    generic map(
      INIT => X"0114"
    )
    port map (
      I0 => SequenceHandler_couleur_present(0),
      I1 => SequenceHandler_couleur_present(2),
      I2 => SequenceHandler_couleur_present(3),
      I3 => SequenceHandler_couleur_present(1),
      O => ViewSound_buzzer_time_4_Q
    );
  WorkerClock_Mcount_compteur_eqn_51 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(5),
      O => WorkerClock_Mcount_compteur_eqn_5
    );
  SequenceHandler_compteur_ecouter_mux0003_7_511 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(4),
      I1 => SequenceHandler_compteur_ecouter(5),
      I2 => SequenceHandler_compteur_ecouter(3),
      I3 => N244,
      O => SequenceHandler_N30
    );
  ViewSound_buzzer_time_3_1 : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => SequenceHandler_couleur_present(2),
      I1 => SequenceHandler_couleur_present(1),
      I2 => SequenceHandler_couleur_present(3),
      I3 => SequenceHandler_couleur_present(0),
      O => ViewSound_buzzer_time_3_Q
    );
  WorkerClock_Mcount_compteur_eqn_61 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(6),
      O => WorkerClock_Mcount_compteur_eqn_6
    );
  ViewSound_buzzer_time_2_1 : LUT4
    generic map(
      INIT => X"0110"
    )
    port map (
      I0 => SequenceHandler_couleur_present(2),
      I1 => SequenceHandler_couleur_present(3),
      I2 => SequenceHandler_couleur_present(0),
      I3 => SequenceHandler_couleur_present(1),
      O => ViewSound_buzzer_time_2_Q
    );
  WorkerClock_Mcount_compteur_eqn_71 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(7),
      O => WorkerClock_Mcount_compteur_eqn_7
    );
  ViewSound_etat_futur_0_mux00001 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => ViewSound_etat_present(0),
      I1 => ViewSound_fin_comptage,
      O => ViewSound_etat_futur
    );
  ViewSound_buzzer_time_1_1 : LUT4
    generic map(
      INIT => X"0114"
    )
    port map (
      I0 => SequenceHandler_couleur_present(1),
      I1 => SequenceHandler_couleur_present(2),
      I2 => SequenceHandler_couleur_present(3),
      I3 => SequenceHandler_couleur_present(0),
      O => ViewSound_buzzer_time_1_Q
    );
  WorkerClock_Mcount_compteur_eqn_81 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(8),
      O => WorkerClock_Mcount_compteur_eqn_8
    );
  WorkerClock_Mcount_compteur_eqn_91 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(9),
      O => WorkerClock_Mcount_compteur_eqn_9
    );
  WorkerClock_Mcount_compteur_eqn_101 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(10),
      O => WorkerClock_Mcount_compteur_eqn_10
    );
  SequenceHandler_compteur_ecouter_mux0003_6_Q : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => SequenceHandler_N24,
      I2 => SequenceHandler_N21,
      I3 => N108,
      O => SequenceHandler_compteur_ecouter_mux0003(6)
    );
  SequenceHandler_compteur_ecouter_mux0003_7_11 : LUT4
    generic map(
      INIT => X"AA2A"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(7),
      I1 => SequenceHandler_compteur_ecouter(5),
      I2 => SequenceHandler_compteur_ecouter(6),
      I3 => N245,
      O => SequenceHandler_compteur_ecouter_mux0003_7_11_463
    );
  SequenceHandler_compteur_ecouter_mux0003_7_17 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(7),
      I1 => SequenceHandler_compteur_ecouter(6),
      I2 => SequenceHandler_N30,
      O => SequenceHandler_compteur_ecouter_mux0003_7_17_464
    );
  WorkerClock_Mcount_compteur_eqn_111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(11),
      O => WorkerClock_Mcount_compteur_eqn_11
    );
  WorkerClock_Mcount_compteur_eqn_121 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(12),
      O => WorkerClock_Mcount_compteur_eqn_12
    );
  WorkerClock_Mcount_compteur_eqn_131 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(13),
      O => WorkerClock_Mcount_compteur_eqn_13
    );
  WorkerClock_Mcount_compteur_eqn_141 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(14),
      O => WorkerClock_Mcount_compteur_eqn_14
    );
  WorkerClock_Mcount_compteur_eqn_151 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => WorkerClock_compteur_cmp_eq0000,
      I1 => Result(15),
      O => WorkerClock_Mcount_compteur_eqn_15
    );
  Storage_bus_color_futur_1_15 : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => ColorHandler_couleur_present(2),
      I1 => ColorHandler_couleur_present(0),
      I2 => ColorHandler_couleur_present(1),
      I3 => ColorHandler_couleur_present(3),
      O => Storage_bus_color_futur_1_15_597
    );
  Storage_bus_color_futur_1_97 : LUT4
    generic map(
      INIT => X"FF32"
    )
    port map (
      I0 => Storage_bus_color_futur_0_70,
      I1 => Storage_N0,
      I2 => Storage_bus_color_futur_0_51,
      I3 => Storage_bus_color_futur_1_39_598,
      O => Storage_bus_color_futur(1)
    );
  Storage_bus_color_futur_0_15 : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => ColorHandler_couleur_present(1),
      I1 => ColorHandler_couleur_present(0),
      I2 => ColorHandler_couleur_present(2),
      I3 => ColorHandler_couleur_present(3),
      O => Storage_bus_color_futur_0_15_593
    );
  Listener_bus_color_out_3_11 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => btn_bleu_IBUF_1054,
      I1 => btn_vert_IBUF_1064,
      O => Listener_bus_color_futur(1)
    );
  Listener_etat_futur_cmp_ne00017 : LUT4
    generic map(
      INIT => X"F8A8"
    )
    port map (
      I0 => Listener_bus_color_present(1),
      I1 => btn_vert_IBUF_1064,
      I2 => btn_bleu_IBUF_1054,
      I3 => Listener_bus_color_present(0),
      O => Listener_etat_futur_cmp_ne00017_181
    );
  Listener_etat_futur_cmp_ne000113 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => btn_vert_IBUF_1064,
      I1 => Listener_bus_color_present(0),
      I2 => btn_jaune_IBUF_1056,
      O => Listener_etat_futur_cmp_ne000113_177
    );
  Listener_etat_futur_cmp_ne000126 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => btn_jaune_IBUF_1056,
      I1 => btn_rouge_IBUF_1060,
      I2 => btn_vert_IBUF_1064,
      I3 => btn_bleu_IBUF_1054,
      O => Listener_etat_futur_cmp_ne000126_178
    );
  Listener_etat_futur_cmp_ne000133 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => Listener_etat_futur_cmp_ne00017_181,
      I1 => Listener_bus_color_present(2),
      I2 => Listener_etat_futur_cmp_ne000126_178,
      I3 => Listener_etat_futur_cmp_ne000113_177,
      O => Listener_etat_futur_cmp_ne000133_179
    );
  Listener_etat_futur_cmp_ne000154 : LUT3
    generic map(
      INIT => X"1B"
    )
    port map (
      I0 => btn_vert_IBUF_1064,
      I1 => Listener_bus_color_present(1),
      I2 => Listener_bus_color_present(0),
      O => Listener_etat_futur_cmp_ne000154_180
    );
  Listener_etat_futur_cmp_ne0001114 : LUT4
    generic map(
      INIT => X"FF32"
    )
    port map (
      I0 => Listener_etat_futur_cmp_ne000173,
      I1 => btn_bleu_IBUF_1054,
      I2 => Listener_etat_futur_cmp_ne000154_180,
      I3 => Listener_etat_futur_cmp_ne000133_179,
      O => Listener_etat_futur_cmp_ne0001
    );
  Storage_etat_present_FSM_FFd2_In56 : LUT4
    generic map(
      INIT => X"FEE9"
    )
    port map (
      I0 => ColorHandler_couleur_present(2),
      I1 => ColorHandler_couleur_present(3),
      I2 => ColorHandler_couleur_present(0),
      I3 => ColorHandler_couleur_present(1),
      O => Storage_etat_present_FSM_FFd2_In56_623
    );
  btn_rouge_IBUF : IBUF
    port map (
      I => btn_rouge,
      O => btn_rouge_IBUF_1060
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_1086
    );
  btn_bleu_IBUF : IBUF
    port map (
      I => btn_bleu,
      O => btn_bleu_IBUF_1054
    );
  btn_jaune_IBUF : IBUF
    port map (
      I => btn_jaune,
      O => btn_jaune_IBUF_1056
    );
  btn_start_IBUF : IBUF
    port map (
      I => btn_start,
      O => btn_start_IBUF_1062
    );
  btn_vert_IBUF : IBUF
    port map (
      I => btn_vert,
      O => btn_vert_IBUF_1064
    );
  btn_mode_IBUF : IBUF
    port map (
      I => btn_mode,
      O => btn_mode_IBUF_1058
    );
  com_options_1_IBUF : IBUF
    port map (
      I => com_options(1),
      O => com_options_1_IBUF_1072
    );
  com_options_0_IBUF : IBUF
    port map (
      I => com_options(0),
      O => com_options_0_IBUF_1071
    );
  buzzer_OBUF : OBUF
    port map (
      I => buzzer_OBUF_1066,
      O => buzzer
    );
  led_rouge_OBUF : OBUF
    port map (
      I => led_rouge_OBUF_1082,
      O => led_rouge
    );
  led_vert_OBUF : OBUF
    port map (
      I => led_vert_OBUF_1084,
      O => led_vert
    );
  led_jaune_OBUF : OBUF
    port map (
      I => led_jaune_OBUF_1080,
      O => led_jaune
    );
  led_bleu_OBUF : OBUF
    port map (
      I => led_bleu_OBUF_1078,
      O => led_bleu
    );
  segment_j1_7_OBUF : OBUF
    port map (
      I => segment_j1_7_OBUF_1102,
      O => segment_j1(7)
    );
  segment_j1_6_OBUF : OBUF
    port map (
      I => segment_j1_6_OBUF_1101,
      O => segment_j1(6)
    );
  segment_j1_5_OBUF : OBUF
    port map (
      I => segment_j1_5_OBUF_1100,
      O => segment_j1(5)
    );
  segment_j1_4_OBUF : OBUF
    port map (
      I => segment_j1_4_OBUF_1099,
      O => segment_j1(4)
    );
  segment_j1_3_OBUF : OBUF
    port map (
      I => segment_j1_3_OBUF_1098,
      O => segment_j1(3)
    );
  segment_j1_2_OBUF : OBUF
    port map (
      I => segment_j1_2_OBUF_1097,
      O => segment_j1(2)
    );
  segment_j1_1_OBUF : OBUF
    port map (
      I => segment_j1_1_OBUF_1096,
      O => segment_j1(1)
    );
  segment_j1_0_OBUF : OBUF
    port map (
      I => segment_j1_0_OBUF_1095,
      O => segment_j1(0)
    );
  segment_j2_7_OBUF : OBUF
    port map (
      I => segment_j2_7_OBUF_1118,
      O => segment_j2(7)
    );
  segment_j2_6_OBUF : OBUF
    port map (
      I => segment_j2_6_OBUF_1117,
      O => segment_j2(6)
    );
  segment_j2_5_OBUF : OBUF
    port map (
      I => segment_j2_5_OBUF_1116,
      O => segment_j2(5)
    );
  segment_j2_4_OBUF : OBUF
    port map (
      I => segment_j2_4_OBUF_1115,
      O => segment_j2(4)
    );
  segment_j2_3_OBUF : OBUF
    port map (
      I => segment_j2_3_OBUF_1114,
      O => segment_j2(3)
    );
  segment_j2_2_OBUF : OBUF
    port map (
      I => segment_j2_2_OBUF_1113,
      O => segment_j2(2)
    );
  segment_j2_1_OBUF : OBUF
    port map (
      I => segment_j2_1_OBUF_1112,
      O => segment_j2(1)
    );
  segment_j2_0_OBUF : OBUF
    port map (
      I => segment_j2_0_OBUF_1111,
      O => segment_j2(0)
    );
  segment_j3_7_OBUF : OBUF
    port map (
      I => segment_j3_7_OBUF_1134,
      O => segment_j3(7)
    );
  segment_j3_6_OBUF : OBUF
    port map (
      I => segment_j3_6_OBUF_1133,
      O => segment_j3(6)
    );
  segment_j3_5_OBUF : OBUF
    port map (
      I => segment_j3_5_OBUF_1132,
      O => segment_j3(5)
    );
  segment_j3_4_OBUF : OBUF
    port map (
      I => segment_j3_4_OBUF_1131,
      O => segment_j3(4)
    );
  segment_j3_3_OBUF : OBUF
    port map (
      I => segment_j3_3_OBUF_1130,
      O => segment_j3(3)
    );
  segment_j3_2_OBUF : OBUF
    port map (
      I => segment_j3_2_OBUF_1129,
      O => segment_j3(2)
    );
  segment_j3_1_OBUF : OBUF
    port map (
      I => segment_j3_1_OBUF_1128,
      O => segment_j3(1)
    );
  segment_j3_0_OBUF : OBUF
    port map (
      I => segment_j3_0_OBUF_1127,
      O => segment_j3(0)
    );
  segment_j4_7_OBUF : OBUF
    port map (
      I => segment_j4_7_OBUF_1150,
      O => segment_j4(7)
    );
  segment_j4_6_OBUF : OBUF
    port map (
      I => segment_j4_6_OBUF_1149,
      O => segment_j4(6)
    );
  segment_j4_5_OBUF : OBUF
    port map (
      I => segment_j4_5_OBUF_1148,
      O => segment_j4(5)
    );
  segment_j4_4_OBUF : OBUF
    port map (
      I => segment_j4_4_OBUF_1147,
      O => segment_j4(4)
    );
  segment_j4_3_OBUF : OBUF
    port map (
      I => segment_j4_3_OBUF_1146,
      O => segment_j4(3)
    );
  segment_j4_2_OBUF : OBUF
    port map (
      I => segment_j4_2_OBUF_1145,
      O => segment_j4(2)
    );
  segment_j4_1_OBUF : OBUF
    port map (
      I => segment_j4_1_OBUF_1144,
      O => segment_j4(1)
    );
  segment_j4_0_OBUF : OBUF
    port map (
      I => segment_j4_0_OBUF_1143,
      O => segment_j4(0)
    );
  ColorHandler_couleur_futur_3 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ColorHandler_couleur_futur_mux0001_0_1,
      R => ColorHandler_couleur_present(3),
      Q => ColorHandler_couleur_futur(3)
    );
  ColorHandler_couleur_futur_mux0001_0_11 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => ColorHandler_couleur_present(0),
      I1 => ColorHandler_couleur_present(1),
      I2 => ColorHandler_couleur_present(2),
      O => ColorHandler_couleur_futur_mux0001_0_1
    );
  ColorHandler_couleur_futur_2 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ColorHandler_couleur_futur_mux0001_1_1,
      R => ColorHandler_couleur_present(3),
      Q => ColorHandler_couleur_futur(2)
    );
  ColorHandler_couleur_futur_mux0001_1_11 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => ColorHandler_couleur_present(0),
      I1 => ColorHandler_couleur_present(2),
      I2 => ColorHandler_couleur_present(1),
      O => ColorHandler_couleur_futur_mux0001_1_1
    );
  ColorHandler_couleur_futur_1 : FDR
    port map (
      C => clk_BUFGP_1068,
      D => ColorHandler_couleur_futur_mux0001_2_1,
      R => ColorHandler_couleur_present(3),
      Q => ColorHandler_couleur_futur(1)
    );
  ColorHandler_couleur_futur_mux0001_2_11 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => ColorHandler_couleur_present(1),
      I1 => ColorHandler_couleur_present(2),
      I2 => ColorHandler_couleur_present(0),
      O => ColorHandler_couleur_futur_mux0001_2_1
    );
  ColorHandler_couleur_futur_0 : FDS
    port map (
      C => clk_BUFGP_1068,
      D => ColorHandler_couleur_futur_mux0001_3_1,
      S => ColorHandler_couleur_present(3),
      Q => ColorHandler_couleur_futur(0)
    );
  ColorHandler_couleur_futur_mux0001_3_11 : LUT3
    generic map(
      INIT => X"E9"
    )
    port map (
      I0 => ColorHandler_couleur_present(1),
      I1 => ColorHandler_couleur_present(2),
      I2 => ColorHandler_couleur_present(0),
      O => ColorHandler_couleur_futur_mux0001_3_1
    );
  WorkerClock_Mcount_compteur_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(1),
      O => WorkerClock_Mcount_compteur_cy_1_rt_882
    );
  WorkerClock_Mcount_compteur_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(2),
      O => WorkerClock_Mcount_compteur_cy_2_rt_884
    );
  WorkerClock_Mcount_compteur_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(3),
      O => WorkerClock_Mcount_compteur_cy_3_rt_886
    );
  WorkerClock_Mcount_compteur_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(4),
      O => WorkerClock_Mcount_compteur_cy_4_rt_888
    );
  WorkerClock_Mcount_compteur_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(5),
      O => WorkerClock_Mcount_compteur_cy_5_rt_890
    );
  WorkerClock_Mcount_compteur_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(6),
      O => WorkerClock_Mcount_compteur_cy_6_rt_892
    );
  WorkerClock_Mcount_compteur_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(7),
      O => WorkerClock_Mcount_compteur_cy_7_rt_894
    );
  WorkerClock_Mcount_compteur_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(8),
      O => WorkerClock_Mcount_compteur_cy_8_rt_896
    );
  WorkerClock_Mcount_compteur_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(9),
      O => WorkerClock_Mcount_compteur_cy_9_rt_898
    );
  WorkerClock_Mcount_compteur_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(10),
      O => WorkerClock_Mcount_compteur_cy_10_rt_872
    );
  WorkerClock_Mcount_compteur_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(11),
      O => WorkerClock_Mcount_compteur_cy_11_rt_874
    );
  WorkerClock_Mcount_compteur_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(12),
      O => WorkerClock_Mcount_compteur_cy_12_rt_876
    );
  WorkerClock_Mcount_compteur_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(13),
      O => WorkerClock_Mcount_compteur_cy_13_rt_878
    );
  WorkerClock_Mcount_compteur_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(14),
      O => WorkerClock_Mcount_compteur_cy_14_rt_880
    );
  ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(5),
      O => ViewSound_Mcompar_fin_comptage_cmp_ge0000_cy_4_rt_831
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(8),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_8_rt_823
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(7),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_7_rt_821
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(6),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_6_rt_819
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(5),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_5_rt_817
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(4),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_4_rt_815
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(3),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_3_rt_813
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(2),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_2_rt_811
    );
  ViewSound_Madd_compteur_futur_addsub0000_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(1),
      O => ViewSound_Madd_compteur_futur_addsub0000_cy_1_rt_809
    );
  WorkerClock_Mcount_compteur_xor_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => WorkerClock_compteur(15),
      O => WorkerClock_Mcount_compteur_xor_15_rt_916
    );
  ViewSound_Madd_compteur_futur_addsub0000_xor_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => ViewSound_compteur_present(9),
      O => ViewSound_Madd_compteur_futur_addsub0000_xor_9_rt_825
    );
  Listener_Mmux_color_comparante_mux0001_5_f5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(56),
      O => Listener_Mmux_color_comparante_mux0001_5_f5_rt_88
    );
  Listener_Mmux_color_comparante_mux0001_5_f5_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(58),
      O => Listener_Mmux_color_comparante_mux0001_5_f5_rt1_89
    );
  Listener_Mmux_color_comparante_mux0001_5_f5_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(57),
      O => Listener_Mmux_color_comparante_mux0001_5_f5_0_rt_86
    );
  Listener_Mmux_color_comparante_mux0001_5_f5_0_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(59),
      O => Listener_Mmux_color_comparante_mux0001_5_f5_0_rt1_87
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(56),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt_374
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(58),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_rt1_375
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(57),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt_372
    );
  SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt1 : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Storage_sequence_complete(59),
      O => SequenceHandler_Mmux_couleur_temp_mux0001_5_f5_0_rt1_373
    );
  Listener_etat_futur_mux0005_0_23 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => Listener_etat_futur_cmp_ne0001,
      I1 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      I2 => Listener_compteur_local_int_cmp_eq0001,
      I3 => Listener_N0,
      O => Listener_counter_reset_mux0003
    );
  Listener_compteur_local_int_not000111_SW4 : LUT3
    generic map(
      INIT => X"59"
    )
    port map (
      I0 => Listener_compteur_local_int(5),
      I1 => Listener_compteur_local_int(4),
      I2 => N241,
      O => N122
    );
  Listener_etat_futur_mux0005_0_23_SW0 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => Listener_compteur_local_int_cmp_eq0001,
      I1 => Listener_etat_present(0),
      I2 => Listener_counter_reset_170,
      I3 => Listener_etat_futur_cmp_ne0001,
      O => N126
    );
  Listener_compteur_local_int_mux0004_1_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N126,
      I1 => Listener_N0,
      I2 => Listener_compteur_local_int(1),
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(1)
    );
  Listener_compteur_local_int_mux0004_6_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N126,
      I1 => Listener_N0,
      I2 => N114,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(6)
    );
  Listener_compteur_local_int_mux0004_7_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N126,
      I1 => Listener_N0,
      I2 => N116,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(7)
    );
  Listener_compteur_local_int_mux0004_4_1 : LUT4
    generic map(
      INIT => X"0041"
    )
    port map (
      I0 => N132,
      I1 => Listener_compteur_local_int(4),
      I2 => Listener_N18,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(4)
    );
  Listener_compteur_local_int_mux0004_2_1 : LUT4
    generic map(
      INIT => X"0014"
    )
    port map (
      I0 => N132,
      I1 => Listener_compteur_local_int(1),
      I2 => Listener_compteur_local_int(2),
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(2)
    );
  Listener_compteur_local_int_mux0004_5_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N126,
      I1 => Listener_N0,
      I2 => N122,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(5)
    );
  Listener_compteur_local_int_mux0004_3_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N126,
      I1 => Listener_N0,
      I2 => N124,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_compteur_local_int_mux0004(3)
    );
  PlayerHandler_JoueurEnCours_cmp_eq00001 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => PlayerHandler_JoueurEnCours_cmp_eq0000
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_3_Q : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => N239,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_3_Q,
      I2 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I3 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_1_31,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_3_Q_35
    );
  Storage_etat_present_FSM_FFd2_In180_SW0 : LUT4
    generic map(
      INIT => X"FFB8"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_In56_623,
      I1 => N243,
      I2 => Listener_bus_color_futur(2),
      I3 => Storage_reset_bus_color_625,
      O => N148
    );
  Storage_etat_present_FSM_FFd2_In180 : LUT4
    generic map(
      INIT => X"9810"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd1_619,
      I1 => Storage_etat_present_FSM_FFd2_621,
      I2 => PlayerHandler_JoueurEnCours_cmp_eq0000,
      I3 => N148,
      O => Storage_etat_present_FSM_FFd2_In
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_31 : LUT4
    generic map(
      INIT => X"4F45"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_Joueur4(0),
      I2 => PlayerHandler_Joueur1(0),
      I3 => PlayerHandler_Joueur2(0),
      O => PlayerHandler_Mmux_JoueurEnCours_mux0007_31_303
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_41 : LUT4
    generic map(
      INIT => X"4F45"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_Joueur2(0),
      I2 => PlayerHandler_Joueur3(0),
      I3 => PlayerHandler_Joueur4(0),
      O => PlayerHandler_Mmux_JoueurEnCours_mux0007_41_305
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_4 : LUT4
    generic map(
      INIT => X"AF8D"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_Joueur3(0),
      I2 => PlayerHandler_Joueur2(0),
      I3 => PlayerHandler_Joueur4(0),
      O => PlayerHandler_Mmux_JoueurEnCours_mux0007_4_304
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_37 : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_futur_4_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_16_32,
      I2 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_21_33,
      I3 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_9_34,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_Q
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_47 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_21_27,
      I1 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      I2 => SequenceHandler_etat_present(4),
      I3 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_42_29,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_Q
    );
  SequenceHandler_compteur_ecouter_mux0003_5_46 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => SequenceHandler_N21,
      I1 => SequenceHandler_compteur_ecouter_mux0003_5_33_460,
      I2 => Storage_compteur(5),
      I3 => SequenceHandler_N24,
      O => SequenceHandler_compteur_ecouter_mux0003(5)
    );
  PlayerHandler_Mmux_JoueurEnCours_mux0007_3 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(0),
      I1 => PlayerHandler_Joueur1(0),
      I2 => PlayerHandler_Joueur2(0),
      I3 => PlayerHandler_Joueur4(0),
      O => PlayerHandler_Mmux_JoueurEnCours_mux0007_3_302
    );
  SequenceHandler_delay_not0001_SW2 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => SequenceHandler_delay(0),
      I1 => SequenceHandler_delay(1),
      I2 => SequenceHandler_delay(2),
      I3 => SequenceHandler_delay(3),
      O => N152
    );
  SequenceHandler_delay_not0001 : LUT4
    generic map(
      INIT => X"E444"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => SequenceHandler_etat_present(4),
      I2 => SequenceHandler_delay_cmp_ge0000,
      I3 => N152,
      O => SequenceHandler_delay_not0001_514
    );
  Listener_compteur_local_int_not00012 : LUT4
    generic map(
      INIT => X"5D08"
    )
    port map (
      I0 => Listener_etat_present(0),
      I1 => Listener_counter_reset_mux0003,
      I2 => Listener_counter_reset_170,
      I3 => Listener_etat_present(3),
      O => Listener_compteur_local_int_not0001
    );
  SequenceHandler_etat_futur_not000144_SW0 : LUT4
    generic map(
      INIT => X"F010"
    )
    port map (
      I0 => SequenceHandler_etat_present(0),
      I1 => SequenceHandler_etat_present(1),
      I2 => SequenceHandler_etat_futur_not000112_543,
      I3 => WorkerClock_impulsion_938,
      O => N154
    );
  SequenceHandler_etat_futur_not000144 : LUT4
    generic map(
      INIT => X"8808"
    )
    port map (
      I0 => SequenceHandler_etat_futur_not000136_544,
      I1 => N154,
      I2 => SequenceHandler_etat_present(4),
      I3 => SequenceHandler_etat_systeme_temp_0_0_not0000,
      O => SequenceHandler_etat_futur_not0001
    );
  SequenceHandler_etat_futur_mux0004_4_84_SW0 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => SequenceHandler_delay(0),
      I1 => SequenceHandler_delay_cmp_ge0000,
      I2 => SequenceHandler_etat_present(3),
      O => N156
    );
  SequenceHandler_etat_futur_mux0004_4_100 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N158,
      I1 => SequenceHandler_N25,
      I2 => SequenceHandler_compteur_demonstration(3),
      I3 => SequenceHandler_etat_present(2),
      O => SequenceHandler_etat_futur_mux0004(4)
    );
  Listener_etat_futur_mux0005_0_40 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Listener_etat_present(3),
      I1 => Listener_N11,
      I2 => N164,
      I3 => Listener_etat_present(0),
      O => Listener_etat_futur_mux0005(0)
    );
  SequenceHandler_compteur_ecouter_mux0003_4_SW2 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(4),
      I1 => SequenceHandler_compteur_ecouter(1),
      I2 => SequenceHandler_compteur_ecouter(2),
      I3 => SequenceHandler_compteur_ecouter(3),
      O => N166
    );
  SequenceHandler_compteur_ecouter_mux0003_4_Q : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => SequenceHandler_N24,
      I2 => N166,
      I3 => SequenceHandler_N21,
      O => SequenceHandler_compteur_ecouter_mux0003(4)
    );
  WorkerScore_joueur1_R_mux0005_3_SW2 : LUT4
    generic map(
      INIT => X"BFFD"
    )
    port map (
      I0 => WorkerScore_joueur1_R(3),
      I1 => WorkerScore_joueur1_R(2),
      I2 => WorkerScore_joueur1_R(1),
      I3 => WorkerScore_joueur1_R(0),
      O => N180
    );
  View7Segments_Segment_J1_0_31 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_cmp_eq0000
    );
  View7Segments_Segment_J1_0_21 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000
    );
  SequenceHandler_Maccum_compteur_sequence_xor_4_12 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(4),
      I1 => SequenceHandler_compteur_sequence(1),
      I2 => SequenceHandler_compteur_sequence(2),
      I3 => SequenceHandler_compteur_sequence(3),
      O => SequenceHandler_Result(4)
    );
  SequenceHandler_compteur_demonstration_and00011 : LUT3
    generic map(
      INIT => X"C8"
    )
    port map (
      I0 => SequenceHandler_etat_present(2),
      I1 => WorkerClock_impulsion_938,
      I2 => SequenceHandler_etat_present(4),
      O => SequenceHandler_compteur_demonstration_and0001
    );
  Listener_compteur_local_int_not000111_SW1_SW0 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Listener_compteur_local_int(3),
      I1 => Listener_compteur_local_int(4),
      I2 => Listener_compteur_local_int(5),
      I3 => Listener_compteur_local_int(6),
      O => N184
    );
  Listener_etat_futur_mux0005_3_11 : LUT3
    generic map(
      INIT => X"FD"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => Listener_N11
    );
  PlayerHandler_Joueur4_0_and00011 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I2 => PlayerHandler_JoueurEnCours_cmp_eq0000,
      O => PlayerHandler_Joueur4_0_and0001
    );
  PlayerHandler_Joueur4_0_and00001 : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours_cmp_eq0000,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      O => PlayerHandler_Joueur4_0_and0000
    );
  SequenceHandler_etat_futur_mux0004_3_Q : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => N23,
      I1 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => SequenceHandler_etat_futur_mux0004(3)
    );
  SequenceHandler_etat_futur_mux0004_4_52_SW0 : LUT4
    generic map(
      INIT => X"8808"
    )
    port map (
      I0 => SequenceHandler_etat_systeme_temp(0),
      I1 => SequenceHandler_etat_systeme_temp(1),
      I2 => etat_systeme(2),
      I3 => SequenceHandler_etat_systeme_temp(2),
      O => N188
    );
  SequenceHandler_etat_futur_mux0004_4_52 : LUT4
    generic map(
      INIT => X"0F08"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => N188,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => SequenceHandler_etat_futur_mux0004_4_52_540
    );
  Storage_sequence_complete_58_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N4,
      I2 => Storage_N56,
      O => Storage_sequence_complete_58_mux0003
    );
  Storage_sequence_complete_54_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N4,
      I2 => Storage_N35,
      O => Storage_sequence_complete_54_mux0003
    );
  Storage_sequence_complete_50_mux00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => Storage_bus_color_present(0),
      I1 => Storage_N4,
      I2 => Storage_N36,
      O => Storage_sequence_complete_50_mux0003
    );
  Storage_sequence_complete_25_not000111 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Storage_compteur(2),
      I2 => Storage_compteur(3),
      O => Storage_sequence_complete_40_cmp_eq0001
    );
  Storage_bus_color_futur_cmp_eq000521 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => Storage_compteur(1),
      I1 => Storage_compteur(2),
      I2 => Storage_compteur(3),
      O => Storage_N26
    );
  Storage_N41 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd1_619,
      I1 => Storage_compteur(6),
      I2 => Storage_compteur(7),
      I3 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_N4
    );
  PlayerHandler_Joueur3_0_and00011 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => PlayerHandler_Joueur3_0_mux0000,
      I1 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => PlayerHandler_Joueur3_0_and0001
    );
  PlayerHandler_Joueur3_0_and00001 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => PlayerHandler_Joueur3_0_mux0000,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => PlayerHandler_Joueur3_0_and0000
    );
  PlayerHandler_Joueur2_0_and00011 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => PlayerHandler_Joueur2_0_mux0000,
      I1 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => PlayerHandler_Joueur2_0_and0001
    );
  PlayerHandler_Joueur2_0_and00001 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => PlayerHandler_Joueur2_0_mux0000,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => PlayerHandler_Joueur2_0_and0000
    );
  WorkerScore_joueur4_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => WorkerScore_etat_present_FSM_FFd1_958,
      I2 => WorkerScore_N01,
      I3 => WorkerScore_N36,
      O => WorkerScore_joueur4_L_not0001
    );
  WorkerScore_joueur3_R_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => WorkerScore_etat_present_FSM_FFd1_958,
      I2 => WorkerScore_N23,
      I3 => WorkerScore_N41,
      O => WorkerScore_joueur3_R_not0001
    );
  WorkerScore_joueur3_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => WorkerScore_etat_present_FSM_FFd1_958,
      I2 => WorkerScore_N11,
      I3 => WorkerScore_N37,
      O => WorkerScore_joueur3_L_not0001
    );
  WorkerScore_joueur2_R_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => WorkerScore_etat_present_FSM_FFd1_958,
      I2 => WorkerScore_N21,
      I3 => WorkerScore_N42,
      O => WorkerScore_joueur2_R_not0001
    );
  WorkerScore_joueur2_L_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => WorkerScore_etat_present_FSM_FFd1_958,
      I2 => WorkerScore_N2,
      I3 => WorkerScore_N38,
      O => WorkerScore_joueur2_L_not0001
    );
  Storage_sequence_complete_9_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N20,
      I3 => Storage_sequence_complete_40_cmp_eq0001,
      O => Storage_sequence_complete_9_not0001
    );
  Storage_sequence_complete_8_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => Storage_N20,
      I1 => Storage_sequence_complete_40_or0000,
      I2 => Storage_etat_present_FSM_FFd1_619,
      I3 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_sequence_complete_8_not0001
    );
  Storage_sequence_complete_58_not00012 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N4,
      I3 => Storage_N56,
      O => Storage_sequence_complete_58_not0001
    );
  Storage_sequence_complete_54_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N4,
      I3 => Storage_N35,
      O => Storage_sequence_complete_54_not0001
    );
  Storage_sequence_complete_53_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_compteur(2),
      I3 => Storage_N38,
      O => Storage_sequence_complete_53_not0001
    );
  Storage_sequence_complete_52_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(1),
      I2 => Storage_N18,
      I3 => Storage_N7,
      O => Storage_sequence_complete_52_mux0003
    );
  Storage_sequence_complete_50_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N4,
      I3 => Storage_N36,
      O => Storage_sequence_complete_50_not0001
    );
  Storage_sequence_complete_49_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => Storage_N38,
      I1 => Storage_compteur(2),
      I2 => Storage_etat_present_FSM_FFd1_619,
      I3 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_sequence_complete_49_not0001
    );
  Storage_sequence_complete_41_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N5,
      I3 => Storage_sequence_complete_40_cmp_eq0001,
      O => Storage_sequence_complete_41_not0001
    );
  Storage_sequence_complete_40_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => Storage_N5,
      I1 => Storage_sequence_complete_40_or0000,
      I2 => Storage_etat_present_FSM_FFd1_619,
      I3 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_sequence_complete_40_not0001
    );
  Storage_sequence_complete_32_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N5,
      I3 => Storage_N26,
      O => Storage_sequence_complete_32_not0001
    );
  Storage_sequence_complete_25_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N21,
      I3 => Storage_sequence_complete_40_cmp_eq0001,
      O => Storage_sequence_complete_25_not0001
    );
  Storage_sequence_complete_24_not00011 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => Storage_N21,
      I1 => Storage_sequence_complete_40_or0000,
      I2 => Storage_etat_present_FSM_FFd1_619,
      I3 => Storage_etat_present_FSM_FFd2_621,
      O => Storage_sequence_complete_24_not0001
    );
  Storage_sequence_complete_1_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N20,
      I3 => Storage_N26,
      O => Storage_sequence_complete_1_not0001
    );
  Storage_sequence_complete_17_not00011 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Storage_etat_present_FSM_FFd2_621,
      I1 => Storage_etat_present_FSM_FFd1_619,
      I2 => Storage_N21,
      I3 => Storage_N26,
      O => Storage_sequence_complete_17_not0001
    );
  Storage_sequence_complete_12_mux000311 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N18,
      I1 => Storage_compteur(5),
      I2 => Storage_compteur(2),
      I3 => Storage_compteur(1),
      O => Storage_N12
    );
  Storage_sequence_complete_0_mux00031 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_bus_color_futur_cmp_eq0005,
      I1 => Storage_etat_present_FSM_FFd2_621,
      I2 => Storage_bus_color_present(0),
      I3 => Storage_etat_present_FSM_FFd1_619,
      O => Storage_sequence_complete_0_mux0003
    );
  SequenceHandler_etat_futur_mux0004_1_1 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(7),
      I1 => WorkerClock_impulsion_938,
      I2 => SequenceHandler_etat_present(0),
      O => SequenceHandler_etat_futur_mux0004(1)
    );
  Listener_etat_futur_not00011 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => Listener_etat_present(0),
      I1 => Listener_etat_futur_cmp_ne0001,
      I2 => Listener_compteur_local_int_cmp_eq0001,
      I3 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_etat_futur_not0001
    );
  Listener_etat_futur_mux0005_1_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Listener_N11,
      I2 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      I3 => Listener_etat_present(0),
      O => Listener_etat_futur_mux0005(1)
    );
  Storage_sequence_complete_56_not0001_SW0 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Storage_N4,
      I1 => Storage_compteur(2),
      I2 => Storage_compteur(3),
      I3 => Storage_compteur(1),
      O => N2
    );
  WorkerScore_joueur4_L_mux0006_0_1_SW1 : LUT4
    generic map(
      INIT => X"FBFF"
    )
    port map (
      I0 => WorkerScore_joueur4_R(1),
      I1 => WorkerScore_joueur4_R(0),
      I2 => WorkerScore_joueur4_R(2),
      I3 => WorkerScore_joueur4_R(3),
      O => N190
    );
  WorkerScore_joueur4_L_mux0006_0_1 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => PlayerHandler_JoueurEnCours(1),
      I1 => N190,
      I2 => WorkerScore_N34,
      I3 => PlayerHandler_JoueurEnCours(0),
      O => WorkerScore_N36
    );
  SequenceHandler_etat_futur_mux0004_0_16 : LUT4
    generic map(
      INIT => X"8880"
    )
    port map (
      I0 => etat_systeme(2),
      I1 => SequenceHandler_etat_present(4),
      I2 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => SequenceHandler_etat_futur_mux0004_0_16_535
    );
  SequenceHandler_etat_futur_not000136 : LUT4
    generic map(
      INIT => X"C8FF"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(2),
      I1 => SequenceHandler_compteur_demonstration(3),
      I2 => SequenceHandler_compteur_demonstration(1),
      I3 => SequenceHandler_etat_present(2),
      O => SequenceHandler_etat_futur_not000136_544
    );
  Listener_etat_futur_mux0005_0_23_SW3_SW0 : LUT4
    generic map(
      INIT => X"80FF"
    )
    port map (
      I0 => Listener_bus_color_present(0),
      I1 => Listener_bus_color_present(1),
      I2 => Listener_bus_color_present(2),
      I3 => Listener_etat_present(0),
      O => N192
    );
  Listener_etat_futur_mux0005_0_23_SW3 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => Listener_etat_futur_cmp_ne0001,
      I1 => Listener_counter_reset_170,
      I2 => Listener_N0,
      I3 => N192,
      O => N132
    );
  WorkerScore_joueur1_R_mux0005_2_SW1 : LUT4
    generic map(
      INIT => X"EBAF"
    )
    port map (
      I0 => WorkerScore_joueur1_R(3),
      I1 => WorkerScore_joueur1_R(1),
      I2 => WorkerScore_joueur1_R(2),
      I3 => WorkerScore_joueur1_R(0),
      O => N196
    );
  WorkerScore_joueur1_R_mux0005_2_Q : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => WorkerScore_N34,
      I1 => PlayerHandler_JoueurEnCours(0),
      I2 => PlayerHandler_JoueurEnCours(1),
      I3 => N196,
      O => WorkerScore_joueur1_R_mux0005(2)
    );
  ViewSound_buzzer34_SW1 : LUT4
    generic map(
      INIT => X"10FF"
    )
    port map (
      I0 => etat_systeme(1),
      I1 => etat_systeme(0),
      I2 => etat_systeme(2),
      I3 => com_options_1_IBUF_1072,
      O => N198
    );
  ViewSound_buzzer34 : LUT4
    generic map(
      INIT => X"FFAB"
    )
    port map (
      I0 => ViewSound_buzzer12_841,
      I1 => com_options_0_IBUF_1071,
      I2 => N198,
      I3 => ViewSound_etat_present(0),
      O => buzzer_OBUF_1066
    );
  WorkerScore_joueur1_R_mux0005_3_Q : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => WorkerScore_N34,
      I1 => N180,
      I2 => PlayerHandler_JoueurEnCours(1),
      I3 => PlayerHandler_JoueurEnCours(0),
      O => WorkerScore_joueur1_R_mux0005(3)
    );
  Storage_compteur_mux0003_7_Q : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => Storage_etat_present_cmp_eq0003,
      I1 => Storage_compteur(7),
      I2 => Storage_Madd_compteur_addsub0000_cy(4),
      I3 => N200,
      O => Storage_compteur_mux0003(7)
    );
  Storage_bus_color_futur_1_51 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => btn_rouge_IBUF_1060,
      I1 => btn_bleu_IBUF_1054,
      I2 => btn_vert_IBUF_1064,
      I3 => btn_jaune_IBUF_1056,
      O => Storage_bus_color_futur_0_51
    );
  Storage_bus_color_futur_1_70 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => btn_bleu_IBUF_1054,
      I1 => btn_jaune_IBUF_1056,
      I2 => btn_vert_IBUF_1064,
      O => Storage_bus_color_futur_0_70
    );
  View7Segments_Segment_J4_7_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_L(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_7_OBUF_1150
    );
  View7Segments_Segment_J4_6_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_L(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_6_OBUF_1149
    );
  View7Segments_Segment_J4_5_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_L(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_5_OBUF_1148
    );
  View7Segments_Segment_J4_4_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_L(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_4_OBUF_1147
    );
  View7Segments_Segment_J4_3_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_R(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_3_OBUF_1146
    );
  View7Segments_Segment_J4_2_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_R(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_2_OBUF_1145
    );
  View7Segments_Segment_J4_1_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_R(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_1_OBUF_1144
    );
  View7Segments_Segment_J4_0_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur4_R(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j4_0_OBUF_1143
    );
  View7Segments_Segment_J3_7_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_L(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_7_OBUF_1134
    );
  View7Segments_Segment_J3_6_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_L(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_6_OBUF_1133
    );
  View7Segments_Segment_J3_5_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_L(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_5_OBUF_1132
    );
  View7Segments_Segment_J3_4_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_L(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_4_OBUF_1131
    );
  View7Segments_Segment_J3_3_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_R(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_3_OBUF_1130
    );
  View7Segments_Segment_J3_2_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_R(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_2_OBUF_1129
    );
  View7Segments_Segment_J3_1_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_R(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_1_OBUF_1128
    );
  View7Segments_Segment_J3_0_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur3_R(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j3_0_OBUF_1127
    );
  View7Segments_Segment_J2_7_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_L(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_7_OBUF_1118
    );
  View7Segments_Segment_J2_6_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_L(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_6_OBUF_1117
    );
  View7Segments_Segment_J2_5_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_L(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_5_OBUF_1116
    );
  View7Segments_Segment_J2_4_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_L(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_4_OBUF_1115
    );
  View7Segments_Segment_J2_3_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_R(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_3_OBUF_1114
    );
  View7Segments_Segment_J2_2_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_R(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_2_OBUF_1113
    );
  View7Segments_Segment_J2_1_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_R(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_1_OBUF_1112
    );
  View7Segments_Segment_J2_0_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur2_R(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j2_0_OBUF_1111
    );
  View7Segments_Segment_J1_7_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur1_L(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j1_7_OBUF_1102
    );
  View7Segments_Segment_J1_6_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur1_L(2),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j1_6_OBUF_1101
    );
  View7Segments_Segment_J1_5_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur1_L(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j1_5_OBUF_1100
    );
  View7Segments_Segment_J1_4_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur1_L(0),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j1_4_OBUF_1099
    );
  View7Segments_Segment_J1_3_1 : LUT4
    generic map(
      INIT => X"CCC8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => WorkerScore_joueur1_R(3),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      O => segment_j1_3_OBUF_1098
    );
  Listener_etat_futur_mux0005_2_2 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => Listener_etat_present(2),
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => Listener_etat_futur_mux0005_2_2_188
    );
  SequenceHandler_etat_futur_mux0004_4_100_SW0_SW0 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => N156,
      I1 => SequenceHandler_N111,
      I2 => SequenceHandler_etat_futur_mux0004_4_62_541,
      I3 => SequenceHandler_etat_present(4),
      O => N204
    );
  SequenceHandler_etat_futur_mux0004_4_100_SW0 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => N204,
      I1 => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(7),
      I2 => SequenceHandler_etat_present(0),
      I3 => WorkerClock_impulsion_938,
      O => N158
    );
  Listener_etat_futur_mux0005_2_34 : LUT4
    generic map(
      INIT => X"AEAA"
    )
    port map (
      I0 => Listener_etat_futur_mux0005_2_2_188,
      I1 => Listener_etat_present(0),
      I2 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      I3 => N162,
      O => Listener_etat_futur_mux0005(2)
    );
  PlayerHandler_Joueur2_0_mux000039 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => Listener_etat_present(2),
      I1 => PlayerHandler_Joueur2(0),
      I2 => Listener_etat_present(1),
      I3 => PlayerHandler_JoueurEnCours_cmp_eq0000,
      O => PlayerHandler_Joueur2_0_mux0000
    );
  Storage_bus_color_futur_1_39_SW0 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => btn_bleu_IBUF_1054,
      I1 => btn_vert_IBUF_1064,
      I2 => btn_jaune_IBUF_1056,
      O => couleur_jouee(2)
    );
  Storage_bus_color_futur_0_97 : LUT4
    generic map(
      INIT => X"FFB8"
    )
    port map (
      I0 => N214,
      I1 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I2 => Storage_bus_color_futur_0_15_593,
      I3 => Storage_reset_bus_color_625,
      O => Storage_bus_color_futur(0)
    );
  View7Segments_Segment_J1_0_Q : MUXF5
    port map (
      I0 => N218,
      I1 => N219,
      S => etat_systeme(1),
      O => segment_j1_0_OBUF_1095
    );
  View7Segments_Segment_J1_0_F : LUT4
    generic map(
      INIT => X"ABA8"
    )
    port map (
      I0 => WorkerScore_joueur1_R(0),
      I1 => etat_systeme(0),
      I2 => etat_systeme(2),
      I3 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      O => N218
    );
  View7Segments_Segment_J1_0_G : LUT4
    generic map(
      INIT => X"DC8C"
    )
    port map (
      I0 => etat_systeme(2),
      I1 => WorkerScore_joueur1_R(0),
      I2 => etat_systeme(0),
      I3 => GameHandler_Mrom_mode_nbr_joueur,
      O => N219
    );
  SequenceHandler_compteur_ecouter_mux0003_2_Q : MUXF5
    port map (
      I0 => N220,
      I1 => N221,
      S => Listener_etat_present(2),
      O => SequenceHandler_compteur_ecouter_mux0003(2)
    );
  SequenceHandler_compteur_ecouter_mux0003_2_F : LUT4
    generic map(
      INIT => X"2080"
    )
    port map (
      I0 => SequenceHandler_etat_present(3),
      I1 => SequenceHandler_compteur_ecouter(2),
      I2 => Listener_etat_present(1),
      I3 => SequenceHandler_compteur_ecouter(1),
      O => N220
    );
  SequenceHandler_compteur_ecouter_mux0003_2_G : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Storage_compteur(2),
      I2 => SequenceHandler_etat_present(3),
      O => N221
    );
  SequenceHandler_compteur_ecouter_mux0003_3_Q : MUXF5
    port map (
      I0 => N222,
      I1 => N223,
      S => Listener_etat_present(2),
      O => SequenceHandler_compteur_ecouter_mux0003(3)
    );
  SequenceHandler_compteur_ecouter_mux0003_3_F : LUT4
    generic map(
      INIT => X"9000"
    )
    port map (
      I0 => SequenceHandler_N14,
      I1 => SequenceHandler_compteur_ecouter(3),
      I2 => Listener_etat_present(1),
      I3 => SequenceHandler_etat_present(3),
      O => N222
    );
  SequenceHandler_compteur_ecouter_mux0003_3_G : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Storage_compteur(3),
      I2 => SequenceHandler_etat_present(3),
      O => N223
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29 : MUXF5
    port map (
      I0 => N224,
      I1 => N225,
      S => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29_37
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29_F : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_0_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      I3 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      O => N224
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_29_G : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => Listener_etat_present(2),
      I1 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I2 => Listener_etat_present(1),
      O => N225
    );
  WorkerScore_etat_present_FSM_FFd2_In : MUXF5
    port map (
      I0 => N226,
      I1 => N227,
      S => WorkerScore_etat_present_FSM_FFd1_958,
      O => WorkerScore_etat_present_FSM_FFd2_In_961
    );
  WorkerScore_etat_present_FSM_FFd2_In_F : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => WorkerScore_etat_present_FSM_FFd2_960,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      O => N226
    );
  WorkerScore_etat_present_FSM_FFd2_In_G : LUT4
    generic map(
      INIT => X"AA8A"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd2_960,
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      I3 => WorkerScore_validation_1050,
      O => N227
    );
  SequenceHandler_compteur_ecouter_mux0003_7_37 : MUXF5
    port map (
      I0 => N228,
      I1 => N229,
      S => Listener_etat_present(2),
      O => SequenceHandler_compteur_ecouter_mux0003(7)
    );
  SequenceHandler_compteur_ecouter_mux0003_7_37_F : LUT4
    generic map(
      INIT => X"8880"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => SequenceHandler_etat_present(3),
      I2 => SequenceHandler_compteur_ecouter_mux0003_7_11_463,
      I3 => SequenceHandler_compteur_ecouter_mux0003_7_17_464,
      O => N228
    );
  SequenceHandler_compteur_ecouter_mux0003_7_37_G : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Storage_compteur(7),
      I2 => SequenceHandler_etat_present(3),
      O => N229
    );
  PlayerHandler_Joueur4_0_mux0000 : MUXF5
    port map (
      I0 => N230,
      I1 => N231,
      S => PlayerHandler_JoueurEnCours_cmp_eq0000,
      O => PlayerHandler_Joueur4_0_mux0000_295
    );
  PlayerHandler_Joueur4_0_mux0000_F : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => PlayerHandler_Joueur4(0),
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      O => N230
    );
  PlayerHandler_Joueur4_0_mux0000_G : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      I1 => GameHandler_Mrom_mode_nbr_joueur2,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      O => N231
    );
  Listener_etat_futur_mux0005_0_40_SW0 : MUXF5
    port map (
      I0 => N232,
      I1 => N233,
      S => Listener_compteur_local_int_cmp_eq0001,
      O => N164
    );
  Listener_etat_futur_mux0005_0_40_SW0_F : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      I1 => Listener_N0,
      I2 => Listener_etat_futur_cmp_ne0001,
      O => N232
    );
  Listener_etat_futur_mux0005_0_40_SW0_G : LUT4
    generic map(
      INIT => X"0155"
    )
    port map (
      I0 => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      I1 => Listener_timer_checker(2),
      I2 => Listener_timer_checker(1),
      I3 => Listener_timer_checker(3),
      O => N233
    );
  Storage_bus_color_futur_0_97_SW0 : MUXF5
    port map (
      I0 => N234,
      I1 => N235,
      S => Storage_bus_color_futur_cmp_eq0005,
      O => N214
    );
  Storage_bus_color_futur_0_97_SW0_F : LUT3
    generic map(
      INIT => X"51"
    )
    port map (
      I0 => btn_bleu_IBUF_1054,
      I1 => btn_jaune_IBUF_1056,
      I2 => btn_vert_IBUF_1064,
      O => N234
    );
  Storage_bus_color_futur_0_97_SW0_G : LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => ColorHandler_couleur_present(1),
      I1 => ColorHandler_couleur_present(0),
      I2 => ColorHandler_couleur_present(2),
      I3 => ColorHandler_couleur_present(3),
      O => N235
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_1068
    );
  WorkerClock_Mcount_compteur_lut_0_INV_0 : INV
    port map (
      I => WorkerClock_compteur(0),
      O => WorkerClock_Mcount_compteur_lut(0)
    );
  ViewSound_Madd_compteur_futur_addsub0000_lut_0_INV_0 : INV
    port map (
      I => ViewSound_compteur_present(0),
      O => ViewSound_Madd_compteur_futur_addsub0000_lut(0)
    );
  Listener_Mcompar_color_comparante_cmp_lt0000_cy_7_inv_INV_0 : INV
    port map (
      I => Listener_Mcompar_color_comparante_cmp_lt0000_cy(7),
      O => Listener_color_comparante_cmp_lt0000
    );
  SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy_7_inv_INV_0 : INV
    port map (
      I => SequenceHandler_Mcompar_couleur_temp_cmp_lt0000_cy(7),
      O => SequenceHandler_couleur_temp_cmp_lt0000
    );
  Storage_reset_bus_color_not00011_INV_0 : INV
    port map (
      I => Storage_etat_present_FSM_FFd2_621,
      O => Storage_reset_bus_color_not0001
    );
  SequenceHandler_etat_present_Acst_inv1_INV_0 : INV
    port map (
      I => GameHandler_gestion_etat_syteme_reinit_43,
      O => SequenceHandler_etat_present_Acst_inv
    );
  SequenceHandler_Mcount_compteur_demonstration_xor_0_11_INV_0 : INV
    port map (
      I => SequenceHandler_compteur_demonstration(0),
      O => SequenceHandler_Result_0_1
    );
  SequenceHandler_Maccum_compteur_sequence_xor_1_11_INV_0 : INV
    port map (
      I => SequenceHandler_compteur_sequence(1),
      O => SequenceHandler_Result(1)
    );
  WorkerScore_joueur1_L_mux0004_0_21 : LUT4
    generic map(
      INIT => X"0020"
    )
    port map (
      I0 => WorkerScore_etat_present_FSM_FFd1_958,
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      I3 => WorkerScore_etat_present_FSM_FFd2_960,
      O => WorkerScore_joueur1_L_mux0004_0_2
    );
  WorkerScore_joueur1_L_mux0004_0_2_f5 : MUXF5
    port map (
      I0 => WorkerScore_joueur1_L_mux0004_0_2,
      I1 => N0,
      S => WorkerScore_validation_1050,
      O => WorkerScore_N34
    );
  SequenceHandler_d_couleur_futur_mux0002_3_1 : LUT4
    generic map(
      INIT => X"1104"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(0),
      I1 => SequenceHandler_compteur_demonstration(3),
      I2 => SequenceHandler_compteur_demonstration(1),
      I3 => SequenceHandler_compteur_demonstration(2),
      O => SequenceHandler_d_couleur_futur_mux0002_3_1_498
    );
  SequenceHandler_d_couleur_futur_mux0002_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_d_couleur_futur_mux0002_3_1_498,
      S => SequenceHandler_etat_present(2),
      O => SequenceHandler_d_couleur_futur_mux0002(3)
    );
  SequenceHandler_d_couleur_futur_mux0002_1_1 : LUT4
    generic map(
      INIT => X"1104"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(0),
      I1 => SequenceHandler_compteur_demonstration(3),
      I2 => SequenceHandler_compteur_demonstration(2),
      I3 => SequenceHandler_compteur_demonstration(1),
      O => SequenceHandler_d_couleur_futur_mux0002_1_1_494
    );
  SequenceHandler_d_couleur_futur_mux0002_1_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_d_couleur_futur_mux0002_1_1_494,
      S => SequenceHandler_etat_present(2),
      O => SequenceHandler_d_couleur_futur_mux0002(1)
    );
  SequenceHandler_d_couleur_futur_mux0002_2_1 : LUT4
    generic map(
      INIT => X"0442"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(3),
      I1 => SequenceHandler_compteur_demonstration(1),
      I2 => SequenceHandler_compteur_demonstration(0),
      I3 => SequenceHandler_compteur_demonstration(2),
      O => SequenceHandler_d_couleur_futur_mux0002_2_1_496
    );
  SequenceHandler_d_couleur_futur_mux0002_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_d_couleur_futur_mux0002_2_1_496,
      S => SequenceHandler_etat_present(2),
      O => SequenceHandler_d_couleur_futur_mux0002(2)
    );
  SequenceHandler_d_couleur_futur_mux0002_0_1 : LUT4
    generic map(
      INIT => X"0412"
    )
    port map (
      I0 => SequenceHandler_compteur_demonstration(3),
      I1 => SequenceHandler_compteur_demonstration(2),
      I2 => SequenceHandler_compteur_demonstration(0),
      I3 => SequenceHandler_compteur_demonstration(1),
      O => SequenceHandler_d_couleur_futur_mux0002_0_1_492
    );
  SequenceHandler_d_couleur_futur_mux0002_0_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_d_couleur_futur_mux0002_0_1_492,
      S => SequenceHandler_etat_present(2),
      O => SequenceHandler_d_couleur_futur_mux0002(0)
    );
  SequenceHandler_e_couleur_futur_mux0003_1_11 : LUT4
    generic map(
      INIT => X"4644"
    )
    port map (
      I0 => Listener_etat_present(1),
      I1 => Listener_etat_present(2),
      I2 => btn_bleu_IBUF_1054,
      I3 => btn_vert_IBUF_1064,
      O => SequenceHandler_e_couleur_futur_mux0003_1_1
    );
  SequenceHandler_e_couleur_futur_mux0003_1_1_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_e_couleur_futur_mux0003_1_1,
      S => SequenceHandler_etat_present(3),
      O => SequenceHandler_e_couleur_futur_mux0003(1)
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_121 : LUT4
    generic map(
      INIT => X"ABAA"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      I3 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_121_23
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_122 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => Listener_etat_present(2),
      I1 => Listener_etat_present(1),
      I2 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_122_24
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_12_f5 : MUXF5
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_122_24,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_121_23,
      S => SequenceHandler_etat_present(4),
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_12
    );
  WorkerScore_joueur4_R_mux0007_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur4_R(3),
      I1 => WorkerScore_joueur4_R(2),
      I2 => WorkerScore_joueur4_R(0),
      I3 => WorkerScore_joueur4_R(1),
      O => WorkerScore_joueur4_R_mux0007_2_1_1046
    );
  WorkerScore_joueur4_R_mux0007_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur4_R_mux0007_2_1_1046,
      S => WorkerScore_N40,
      O => WorkerScore_joueur4_R_mux0007(2)
    );
  WorkerScore_joueur4_L_mux0006_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur4_L(3),
      I1 => WorkerScore_joueur4_L(2),
      I2 => WorkerScore_joueur4_L(0),
      I3 => WorkerScore_joueur4_L(1),
      O => WorkerScore_joueur4_L_mux0006_2_1_1035
    );
  WorkerScore_joueur4_L_mux0006_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur4_L_mux0006_2_1_1035,
      S => WorkerScore_N36,
      O => WorkerScore_joueur4_L_mux0006(2)
    );
  WorkerScore_joueur3_R_mux0007_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur3_R(3),
      I1 => WorkerScore_joueur3_R(2),
      I2 => WorkerScore_joueur3_R(0),
      I3 => WorkerScore_joueur3_R(1),
      O => WorkerScore_joueur3_R_mux0007_2_1_1024
    );
  WorkerScore_joueur3_R_mux0007_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur3_R_mux0007_2_1_1024,
      S => WorkerScore_N41,
      O => WorkerScore_joueur3_R_mux0007(2)
    );
  WorkerScore_joueur3_L_mux0006_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur3_L(3),
      I1 => WorkerScore_joueur3_L(2),
      I2 => WorkerScore_joueur3_L(0),
      I3 => WorkerScore_joueur3_L(1),
      O => WorkerScore_joueur3_L_mux0006_2_1_1013
    );
  WorkerScore_joueur3_L_mux0006_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur3_L_mux0006_2_1_1013,
      S => WorkerScore_N37,
      O => WorkerScore_joueur3_L_mux0006(2)
    );
  WorkerScore_joueur2_R_mux0006_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur2_R(3),
      I1 => WorkerScore_joueur2_R(2),
      I2 => WorkerScore_joueur2_R(0),
      I3 => WorkerScore_joueur2_R(1),
      O => WorkerScore_joueur2_R_mux0006_2_1_1002
    );
  WorkerScore_joueur2_R_mux0006_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur2_R_mux0006_2_1_1002,
      S => WorkerScore_N42,
      O => WorkerScore_joueur2_R_mux0006(2)
    );
  WorkerScore_joueur2_L_mux0005_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur2_L(3),
      I1 => WorkerScore_joueur2_L(2),
      I2 => WorkerScore_joueur2_L(0),
      I3 => WorkerScore_joueur2_L(1),
      O => WorkerScore_joueur2_L_mux0005_2_1_991
    );
  WorkerScore_joueur2_L_mux0005_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur2_L_mux0005_2_1_991,
      S => WorkerScore_N38,
      O => WorkerScore_joueur2_L_mux0005(2)
    );
  WorkerScore_joueur1_L_mux0004_2_1 : LUT4
    generic map(
      INIT => X"1444"
    )
    port map (
      I0 => WorkerScore_joueur1_L(3),
      I1 => WorkerScore_joueur1_L(2),
      I2 => WorkerScore_joueur1_L(0),
      I3 => WorkerScore_joueur1_L(1),
      O => WorkerScore_joueur1_L_mux0004_2_1_970
    );
  WorkerScore_joueur1_L_mux0004_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur1_L_mux0004_2_1_970,
      S => WorkerScore_N39,
      O => WorkerScore_joueur1_L_mux0004(2)
    );
  SequenceHandler_delay_mux0001_2_1 : LUT4
    generic map(
      INIT => X"2888"
    )
    port map (
      I0 => SequenceHandler_delay_cmp_ge0000,
      I1 => SequenceHandler_delay(2),
      I2 => SequenceHandler_delay(0),
      I3 => SequenceHandler_delay(1),
      O => SequenceHandler_delay_mux0001_2_1_512
    );
  SequenceHandler_delay_mux0001_2_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => SequenceHandler_delay_mux0001_2_1_512,
      S => SequenceHandler_etat_present(3),
      O => SequenceHandler_delay_mux0001(2)
    );
  View7Segments_Segment_J1_1_11 : LUT4
    generic map(
      INIT => X"FFC4"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I1 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      I3 => etat_systeme(2),
      O => View7Segments_Segment_J1_1_1
    );
  View7Segments_Segment_J1_1_12 : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_cmp_eq0000,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I2 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      O => View7Segments_Segment_J1_1_11_796
    );
  View7Segments_Segment_J1_1_1_f5 : MUXF5
    port map (
      I0 => View7Segments_Segment_J1_1_11_796,
      I1 => View7Segments_Segment_J1_1_1,
      S => WorkerScore_joueur1_R(1),
      O => segment_j1_1_OBUF_1096
    );
  Listener_etat_futur_cmp_ne0001731 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => btn_jaune_IBUF_1056,
      I1 => Listener_bus_color_present(0),
      O => Listener_etat_futur_cmp_ne0001731_183
    );
  Listener_etat_futur_cmp_ne0001732 : LUT4
    generic map(
      INIT => X"0155"
    )
    port map (
      I0 => btn_jaune_IBUF_1056,
      I1 => btn_vert_IBUF_1064,
      I2 => btn_rouge_IBUF_1060,
      I3 => Listener_bus_color_present(0),
      O => Listener_etat_futur_cmp_ne0001732_184
    );
  Listener_etat_futur_cmp_ne000173_f5 : MUXF5
    port map (
      I0 => Listener_etat_futur_cmp_ne0001732_184,
      I1 => Listener_etat_futur_cmp_ne0001731_183,
      S => Listener_bus_color_present(2),
      O => Listener_etat_futur_cmp_ne000173
    );
  Listener_etat_futur_mux0005_2_34_SW01 : LUT3
    generic map(
      INIT => X"C8"
    )
    port map (
      I0 => Listener_timer_checker(1),
      I1 => Listener_timer_checker(3),
      I2 => Listener_timer_checker(2),
      O => Listener_etat_futur_mux0005_2_34_SW0
    );
  Listener_etat_futur_mux0005_2_34_SW02 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Listener_N0,
      I1 => Listener_etat_futur_cmp_ne0001,
      O => Listener_etat_futur_mux0005_2_34_SW01_190
    );
  Listener_etat_futur_mux0005_2_34_SW0_f5 : MUXF5
    port map (
      I0 => Listener_etat_futur_mux0005_2_34_SW01_190,
      I1 => Listener_etat_futur_mux0005_2_34_SW0,
      S => Listener_compteur_local_int_cmp_eq0001,
      O => N162
    );
  WorkerScore_joueur4_R_mux0007_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur4_R(3),
      I1 => WorkerScore_joueur4_R(2),
      I2 => WorkerScore_joueur4_R(1),
      I3 => WorkerScore_joueur4_R(0),
      O => WorkerScore_joueur4_R_mux0007_3_1_1048
    );
  WorkerScore_joueur4_R_mux0007_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur4_R_mux0007_3_1_1048,
      S => WorkerScore_N40,
      O => WorkerScore_joueur4_R_mux0007(3)
    );
  WorkerScore_joueur4_L_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur4_L(3),
      I1 => WorkerScore_joueur4_L(2),
      I2 => WorkerScore_joueur4_L(1),
      I3 => WorkerScore_joueur4_L(0),
      O => WorkerScore_joueur4_L_mux0006_3_1_1037
    );
  WorkerScore_joueur4_L_mux0006_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur4_L_mux0006_3_1_1037,
      S => WorkerScore_N36,
      O => WorkerScore_joueur4_L_mux0006(3)
    );
  WorkerScore_joueur3_R_mux0007_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur3_R(3),
      I1 => WorkerScore_joueur3_R(2),
      I2 => WorkerScore_joueur3_R(1),
      I3 => WorkerScore_joueur3_R(0),
      O => WorkerScore_joueur3_R_mux0007_3_1_1026
    );
  WorkerScore_joueur3_R_mux0007_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur3_R_mux0007_3_1_1026,
      S => WorkerScore_N41,
      O => WorkerScore_joueur3_R_mux0007(3)
    );
  WorkerScore_joueur3_L_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur3_L(3),
      I1 => WorkerScore_joueur3_L(2),
      I2 => WorkerScore_joueur3_L(1),
      I3 => WorkerScore_joueur3_L(0),
      O => WorkerScore_joueur3_L_mux0006_3_1_1015
    );
  WorkerScore_joueur3_L_mux0006_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur3_L_mux0006_3_1_1015,
      S => WorkerScore_N37,
      O => WorkerScore_joueur3_L_mux0006(3)
    );
  WorkerScore_joueur2_R_mux0006_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur2_R(3),
      I1 => WorkerScore_joueur2_R(2),
      I2 => WorkerScore_joueur2_R(1),
      I3 => WorkerScore_joueur2_R(0),
      O => WorkerScore_joueur2_R_mux0006_3_1_1004
    );
  WorkerScore_joueur2_R_mux0006_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur2_R_mux0006_3_1_1004,
      S => WorkerScore_N42,
      O => WorkerScore_joueur2_R_mux0006(3)
    );
  WorkerScore_joueur2_L_mux0005_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur2_L(3),
      I1 => WorkerScore_joueur2_L(2),
      I2 => WorkerScore_joueur2_L(1),
      I3 => WorkerScore_joueur2_L(0),
      O => WorkerScore_joueur2_L_mux0005_3_1_993
    );
  WorkerScore_joueur2_L_mux0005_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur2_L_mux0005_3_1_993,
      S => WorkerScore_N38,
      O => WorkerScore_joueur2_L_mux0005(3)
    );
  WorkerScore_joueur1_L_mux0004_3_1 : LUT4
    generic map(
      INIT => X"4002"
    )
    port map (
      I0 => WorkerScore_joueur1_L(3),
      I1 => WorkerScore_joueur1_L(2),
      I2 => WorkerScore_joueur1_L(1),
      I3 => WorkerScore_joueur1_L(0),
      O => WorkerScore_joueur1_L_mux0004_3_1_972
    );
  WorkerScore_joueur1_L_mux0004_3_f5 : MUXF5
    port map (
      I0 => N0,
      I1 => WorkerScore_joueur1_L_mux0004_3_1_972,
      S => WorkerScore_N39,
      O => WorkerScore_joueur1_L_mux0004(3)
    );
  PlayerHandler_Joueur3_0_mux0000441 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd1_56,
      I1 => GameHandler_gestion_selection_nombre_joueur_etat_actuel_FSM_FFd2_58,
      O => PlayerHandler_Joueur3_0_mux000044
    );
  PlayerHandler_Joueur3_0_mux0000442 : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => PlayerHandler_Joueur3(0),
      I1 => Listener_etat_present(2),
      I2 => Listener_etat_present(1),
      O => PlayerHandler_Joueur3_0_mux0000441_290
    );
  PlayerHandler_Joueur3_0_mux000044_f5 : MUXF5
    port map (
      I0 => PlayerHandler_Joueur3_0_mux0000441_290,
      I1 => PlayerHandler_Joueur3_0_mux000044,
      S => PlayerHandler_JoueurEnCours_cmp_eq0000,
      O => PlayerHandler_Joueur3_0_mux0000
    );
  SequenceHandler_etat_systeme_temp_0_0_not000011 : LUT4_D
    generic map(
      INIT => X"6FF6"
    )
    port map (
      I0 => etat_systeme(0),
      I1 => SequenceHandler_etat_systeme_temp(0),
      I2 => etat_systeme(1),
      I3 => SequenceHandler_etat_systeme_temp(1),
      LO => N236,
      O => SequenceHandler_N20
    );
  SequenceHandler_Maccum_compteur_sequence_xor_4_111 : LUT3_D
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(1),
      I1 => SequenceHandler_compteur_sequence(2),
      I2 => SequenceHandler_compteur_sequence(3),
      LO => N237,
      O => SequenceHandler_N41
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_1 : LUT2_D
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_0_Q,
      I1 => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45,
      LO => N238,
      O => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_1_31
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_21 : LUT3_L
    generic map(
      INIT => X"32"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => SequenceHandler_etat_present(4),
      I2 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      LO => GameHandler_gestion_etat_syteme_etat_futur_mux0003_2_21_33
    );
  Storage_compteur_mux0003_4_SW0 : LUT2_L
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Storage_compteur(2),
      I1 => Storage_compteur(1),
      LO => N82
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_6_11 : LUT4_D
    generic map(
      INIT => X"FF32"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I1 => SequenceHandler_etat_present(4),
      I2 => GameHandler_gestion_etat_syteme_etat_present_4_Q,
      I3 => GameHandler_gestion_etat_syteme_N7,
      LO => N239,
      O => GameHandler_gestion_etat_syteme_N0
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_0_112 : LUT4_D
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      I1 => GameHandler_gestion_etat_syteme_valider_nombre_joueur_etat_actuel_FSM_FFd1_49,
      I2 => GameHandler_gestion_etat_syteme_valider_mode_jeu_etat_actuel_FSM_FFd1_45,
      I3 => GameHandler_gestion_etat_syteme_etat_present_0_Q,
      LO => N240,
      O => GameHandler_gestion_etat_syteme_N7
    );
  GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_42 : LUT3_L
    generic map(
      INIT => X"C8"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_N7,
      I1 => GameHandler_gestion_etat_syteme_etat_futur_5_Q,
      I2 => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_36_28,
      LO => GameHandler_gestion_etat_syteme_etat_futur_mux0003_1_42_29
    );
  Listener_compteur_local_int_mux0004_4_211 : LUT3_D
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => Listener_compteur_local_int(3),
      I1 => Listener_compteur_local_int(1),
      I2 => Listener_compteur_local_int(2),
      LO => N241,
      O => Listener_N18
    );
  Storage_Madd_compteur_addsub0000_cy_4_11 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => Storage_compteur(4),
      I1 => Storage_compteur(3),
      I2 => Storage_compteur(2),
      I3 => Storage_compteur(1),
      LO => N242,
      O => Storage_Madd_compteur_addsub0000_cy(4)
    );
  Storage_bus_color_futur_0_11 : LUT2_D
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => GameHandler_gestion_mode_selectionne_etat_actuel_FSM_FFd1_53,
      I1 => Storage_bus_color_futur_cmp_eq0005,
      LO => N243,
      O => Storage_N0
    );
  SequenceHandler_compteur_ecouter_mux0003_5_33 : LUT4_L
    generic map(
      INIT => X"CC6C"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(4),
      I1 => SequenceHandler_compteur_ecouter(5),
      I2 => SequenceHandler_compteur_ecouter(3),
      I3 => SequenceHandler_N14,
      LO => SequenceHandler_compteur_ecouter_mux0003_5_33_460
    );
  SequenceHandler_compteur_ecouter_mux0003_3_111 : LUT2_D
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(2),
      I1 => SequenceHandler_compteur_ecouter(1),
      LO => N244,
      O => SequenceHandler_N14
    );
  SequenceHandler_compteur_ecouter_mux0003_6_SW0 : LUT4_L
    generic map(
      INIT => X"F7A2"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(6),
      I1 => SequenceHandler_compteur_ecouter(5),
      I2 => SequenceHandler_N3,
      I3 => SequenceHandler_N30,
      LO => N108
    );
  Listener_compteur_local_int_not000111_SW5 : LUT3_L
    generic map(
      INIT => X"87"
    )
    port map (
      I0 => Listener_compteur_local_int(1),
      I1 => Listener_compteur_local_int(2),
      I2 => Listener_compteur_local_int(3),
      LO => N124
    );
  Listener_compteur_local_int_not000111_SW0 : LUT4_L
    generic map(
      INIT => X"5595"
    )
    port map (
      I0 => Listener_compteur_local_int(6),
      I1 => Listener_compteur_local_int(5),
      I2 => Listener_compteur_local_int(4),
      I3 => Listener_N18,
      LO => N114
    );
  SequenceHandler_compteur_ecouter_mux0003_5_110 : LUT4_D
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => SequenceHandler_compteur_ecouter(3),
      I1 => SequenceHandler_compteur_ecouter(1),
      I2 => SequenceHandler_compteur_ecouter(2),
      I3 => SequenceHandler_compteur_ecouter(4),
      LO => N245,
      O => SequenceHandler_N3
    );
  Storage_bus_color_futur_1_39 : LUT4_L
    generic map(
      INIT => X"FFB8"
    )
    port map (
      I0 => Storage_bus_color_futur_1_15_597,
      I1 => Storage_N0,
      I2 => couleur_jouee(2),
      I3 => Storage_reset_bus_color_625,
      LO => Storage_bus_color_futur_1_39_598
    );
  Storage_compteur_mux0003_1_129 : LUT4_D
    generic map(
      INIT => X"2220"
    )
    port map (
      I0 => Storage_etat_present_cmp_eq0003,
      I1 => Storage_compteur(7),
      I2 => Storage_compteur_mux0003_1_116_611,
      I3 => Storage_compteur_mux0003_1_112_610,
      LO => N246,
      O => Storage_N13
    );
  WorkerScore_etat_present_FSM_FFd2_In11 : LUT4_D
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => GameHandler_gestion_etat_syteme_etat_present_6_Q,
      I1 => GameHandler_gestion_etat_syteme_etat_present_5_Q,
      I2 => WorkerScore_etat_present_FSM_FFd2_960,
      I3 => GameHandler_gestion_etat_syteme_etat_present_3_Q,
      LO => N247,
      O => WorkerScore_N35
    );
  SequenceHandler_Maccum_compteur_sequence_xor_7_1_SW0 : LUT4_L
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => SequenceHandler_compteur_sequence(1),
      I1 => SequenceHandler_compteur_sequence(2),
      I2 => SequenceHandler_compteur_sequence(3),
      I3 => SequenceHandler_compteur_sequence(4),
      LO => N62
    );
  Listener_compteur_local_int_not000111_SW1 : LUT4_L
    generic map(
      INIT => X"9333"
    )
    port map (
      I0 => Listener_compteur_local_int(1),
      I1 => Listener_compteur_local_int(7),
      I2 => Listener_compteur_local_int(2),
      I3 => N184,
      LO => N116
    );
  Storage_compteur_mux0003_7_SW1 : LUT4_L
    generic map(
      INIT => X"8808"
    )
    port map (
      I0 => Storage_compteur(6),
      I1 => Storage_compteur(5),
      I2 => Storage_compteur(3),
      I3 => Storage_compteur_mux0003_1_112_610,
      LO => N200
    );

end Structure;

