-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Defferrard Julien
-- Date        : 06.05.2021
-- Version     : 1.0
-- Description : Ce composant permet de gérer les leds du jeu

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ViewGame is
    Port (
      clk, rst:	in STD_LOGIC;
      com_options: 	in  STD_LOGIC_VECTOR(1 downto 0);
      nouvelle_couleur: 	in  STD_LOGIC_VECTOR(3 downto 0);
      led_rouge, led_jaune, led_vert, led_bleu: out STD_LOGIC
    );
end ViewGame;

architecture Architecture_ViewGame of ViewGame is
  signal couleur_present, couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
begin
  process (clk, rst)
  begin
    if rst = '1' then
      couleur_present <= "0000";
    elsif rising_edge(clk) then
      couleur_present <= couleur_futur;
    end if;
  end process;

  process (nouvelle_couleur)
  begin
    case nouvelle_couleur is
      when "0001" => couleur_futur <= "0001";
      when "0010" => couleur_futur <= "0010";
      when "0100" => couleur_futur <= "0100";
      when "1000" => couleur_futur <= "1000";
      when "1111" => couleur_futur <= "1111";
      when others => couleur_futur <= "0000";
    end case;
  end process;

  led_bleu <= '1' when (couleur_present = "0001" or couleur_present = "1111") and (com_options = "00" or com_options = "10") and rst = '0' else '0';
  led_jaune <= '1' when (couleur_present = "0010" or couleur_present = "1111") and (com_options = "00" or com_options = "10") and rst = '0' else '0';
  led_rouge <= '1' when (couleur_present = "0100" or couleur_present = "1111") and (com_options = "00" or com_options = "10") and rst = '0' else '0';
  led_vert <= '1' when (couleur_present = "1000" or couleur_present = "1111") and (com_options = "00" or com_options = "10") and rst = '0' else '0';
end Architecture_ViewGame;
