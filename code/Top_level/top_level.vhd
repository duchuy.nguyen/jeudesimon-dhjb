-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            
-- 
-- Create Date:         13:16:20 05/14/2021 
-- Design Name: 
-- Module Name:         GameHandler - GameHandler_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions: 
-- Description:         
-- --------------------------------------------------------------------------------
LIBRARY ieee;                -- bibliotheque a charger
library work;
use ieee.STD_LOGIC_1164.all; -- utilisation des definitions STD_LOGIC
-- --------------------------------------------------------------------------------
entity top_level is
   port(
        -- declaration des entrées
        clk          : in STD_LOGIC;
        rst          : in STD_LOGIC;
        btn_rouge    : in STD_LOGIC;
        btn_jaune    : in STD_LOGIC;
        btn_vert     : in STD_LOGIC;
        btn_bleu     : in STD_LOGIC;
        btn_mode     : in STD_LOGIC;
        btn_start    : in STD_LOGIC;
        com_options  : in STD_LOGIC_VECTOR (1 downto 0);

        -- declaration des sorties
        led_rouge    : out STD_LOGIC;
        led_jaune    : out STD_LOGIC;
        led_vert     : out STD_LOGIC;
        led_bleu     : out STD_LOGIC;
        buzzer       : out STD_LOGIC;
        segment_j1   : out STD_LOGIC_VECTOR (7 downto 0);
        segment_j2   : out STD_LOGIC_VECTOR (7 downto 0);
        segment_j3   : out STD_LOGIC_VECTOR (7 downto 0);
        segment_j4   : out STD_LOGIC_VECTOR (7 downto 0)
       );
end entity top_level;
-- --------------------------------------------------------------------------------
architecture top_level_arch OF top_level IS
signal   score_j1          : STD_LOGIC_VECTOR (7 downto 0);
signal   score_j2          : STD_LOGIC_VECTOR (7 downto 0);
signal   score_j3          : STD_LOGIC_VECTOR (7 downto 0);
signal   score_j4          : STD_LOGIC_VECTOR (7 downto 0);
signal   clk_2hz           : STD_LOGIC;
signal   reset             : STD_LOGIC;
signal   sequence_complete : STD_LOGIC_VECTOR (59 downto 0);
signal   compteur_couleur  : STD_LOGIC_VECTOR (7 downto 0);
signal   evaluer           : STD_LOGIC_VECTOR (1 downto 0);
signal   couleur_jouee     : STD_LOGIC_VECTOR (3 downto 0);
signal   etat_systeme      : STD_LOGIC_VECTOR (2 downto 0);
signal   mode_jeu          : STD_LOGIC_VECTOR (1 downto 0);
signal   mode_nbr_joueur   : STD_LOGIC_VECTOR (2 downto 0);
signal   nouvelle_couleur  : STD_LOGIC_VECTOR (3 downto 0);
signal   fin_sequence      : STD_LOGIC;
signal   id_joueur         : STD_LOGIC_VECTOR (1 downto 0);
signal   couleur_aleatoire : STD_LOGIC_VECTOR (3 downto 0);
signal   joueur_vivant     : STD_LOGIC_Vector (2 downto 0);

begin
    -- --------------------------------------------------
    WorkerScore      :  entity work.workerScore(score_arch)
                        port map (
                                    -- Entrees
                                    clk =>clk,
                                    rst => reset,
                                    etat_system => etat_systeme,
                                    evaluer => evaluer,
                                    id_joueur => id_joueur,

                                    -- Sorties
                                    score_j1 => score_j1,
                                    score_j2 => score_j2,
                                    score_j3 => score_j3,
                                    score_j4 => score_j4
                                 );
    -- --------------------------------------------------


    -- --------------------------------------------------
    WorkerClock      :  entity work.WorkerClock(WorkerClock_arch)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,
                                    
                                    -- Sorties
                                    clk_2hz => clk_2hz
                                 );
    -- --------------------------------------------------
    

    -- --------------------------------------------------
    Storage          :  entity work.storage(storage_arch)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,
                                    etat_system => etat_systeme,
                                    couleur_jouer => couleur_jouee,
                                    couleur_aleatoire => couleur_aleatoire,
                                    mode_jeu => mode_jeu,

                                    --Sorties
                                    sequence_complete => sequence_complete,
                                    compteur_couleur => compteur_couleur
                                 );
    -- --------------------------------------------------

    -- --------------------------------------------------
    Listener         :  entity work.listener(listener_arch)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,
                                    clk_2hz => clk_2hz,
                                    compteur_couleur_vector => compteur_couleur,
                                    btn_rouge => btn_rouge,
                                    btn_jaune => btn_jaune,
                                    btn_bleu => btn_bleu,
                                    btn_vert => btn_vert,
                                    etat_system => etat_systeme,
                                    sequence_complete => sequence_complete,

                                    -- Sorties
                                    evaluer => evaluer,
                                    couleur_jouer => couleur_jouee
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    GameHandler      :  entity work.GameHandler(GameHandler_arch)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => rst,
                                    btn_mode => btn_mode,
                                    btn_start => btn_start,
                                    fin_sequence => fin_sequence,
                                    evaluer => evaluer,
                                    joueur_vivant => joueur_vivant,

                                    -- Sorties
                                    mode_jeu => mode_jeu,
                                    mode_nbr_joueur => mode_nbr_joueur,
                                    etat_systeme => etat_systeme,
                                    reset => reset
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    SequenceHandler  :  entity work.SequenceHandler(Architecture_SequenceHandler)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,
                                    clk_2hz => clk_2hz,
                                    etat_systeme => etat_systeme,
                                    sequence_complete => sequence_complete,
                                    couleur_jouee => couleur_jouee,
                                    evaluer => evaluer,
                                    compteur_couleur => compteur_couleur,

                                    -- Sorties
                                    nouvelle_couleur => nouvelle_couleur,
                                    fin_sequence => fin_sequence
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    PlayerHandler    :  entity work.PlayerHandler(PlayerHandler_arch)
                        port map (
                                    -- Entrees
                                    Etat_systeme => etat_systeme,
                                    Mode_nbr_joueur => mode_nbr_joueur,
                                    Evaluer => evaluer,

                                    -- Entrees/Sorties
                                    Id_joueur => id_joueur
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    ColorHandler     :  entity work.ColorHandler(Architecture_ColorHandler)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,

                                    -- Sorties
                                    couleur_aleatoire => couleur_aleatoire
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    View7Segments    :  entity work.View7Segment(View7Segment_arch)
                        port map (
                                    -- Entrees
                                    Etat_systeme => etat_systeme,
                                    Score_J1 => score_j1,
                                    Score_J2 => score_j2,
                                    Score_J3 => score_j3,
                                    Score_J4 => score_j4,
                                    Mode_jeu => mode_jeu,
                                    Mode_nbr_joueur => mode_nbr_joueur,

                                    -- Sorties
                                    Segment_J1 => segment_j1,
                                    Segment_J2 => segment_j2,
                                    Segment_J3 => segment_j3,
                                    Segment_J4 => segment_j4
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    ViewSound        :  entity work.ViewSound(ViewSound_arch)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    Etat_systeme => etat_systeme,
                                    Com_options => com_options,
                                    Nouvelle_couleur => nouvelle_couleur,

                                    -- Sorties
                                    buzzer => buzzer
                                 );
   -- --------------------------------------------------

    -- --------------------------------------------------
    ViewGame         :  entity work.ViewGame(Architecture_ViewGame)
                        port map (
                                    -- Entrees
                                    clk => clk,
                                    rst => reset,
                                    com_options => com_options,
                                    nouvelle_couleur => nouvelle_couleur,

                                    -- Sorties
                                    led_rouge => led_rouge,
                                    led_jaune => led_jaune,
                                    led_vert => led_vert,
                                    led_bleu => led_bleu
                                 );
   -- --------------------------------------------------

end architecture top_level_arch;
-- --------------------------------------------------------------------------------

