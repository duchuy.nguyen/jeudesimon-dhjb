-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Vonlanthen Benjamin
-- Date        : 09.06.2021
-- Version     : 2.1
-- Description : Ce composant permet l'utilisation du buzzer.



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all ;

entity ViewSound is
    Port (  Etat_systeme : in  STD_LOGIC_VECTOR(2 downto 0);
            Com_options : in  STD_LOGIC_VECTOR(1 downto 0);
            Nouvelle_couleur : in  STD_LOGIC_VECTOR(3 downto 0);
            clk : in std_logic;
            
            buzzer : out  std_logic);
end ViewSound;

architecture ViewSound_arch of ViewSound is
    type etats is (bas, haut);
	signal etat_present, etat_futur : etats;
    signal compteur_present, compteur_futur : unsigned(9 downto 0);
	signal buzzer_time : unsigned (9 downto 0) := to_unsigned(1000, 10);
    signal fin_comptage : STD_LOGIC;
    signal buzzer_ready : STD_LOGIC;

begin
    buzzer_ready <= '1' when etat_present = haut else '0';
    fin_comptage <= '1' when compteur_present >= buzzer_time else '0';

    registre_machine : PROCESS (clk)
	begin
		if rising_edge(clk) then
			etat_present <= etat_futur;
		end if;
	end PROCESS;

    combi_compteur: PROCESS (compteur_present, fin_comptage)
	begin
		if fin_comptage='1' then
			compteur_futur <= (others => '0');
		else
			compteur_futur <= compteur_present + 1;
		end if;
	end PROCESS;

    registre_compteur : PROCESS (clk)
	begin
		if rising_edge(clk) then
			compteur_present <= compteur_futur;
		end if;
	end PROCESS;

    combi_machine :	PROCESS (etat_present, fin_comptage)
	begin
		case etat_present is
			when bas => if fin_comptage = '1' then
										etat_futur <= haut;
									else
										etat_futur <= bas;
									end if;
			when haut => if fin_comptage ='1' then
										etat_futur <= bas;
									else
										etat_futur <= haut;
									end if;
			when others =>
									etat_futur <= bas;
			end case;
	end PROCESS;

    combi_buzzer : PROCESS (buzzer_ready, Etat_systeme, Com_options, Nouvelle_couleur)
    begin
        IF (Etat_systeme = "100" or Com_options /= "10") and Nouvelle_couleur /= "0000" THEN
            buzzer <= buzzer_ready;
        else
            buzzer <= '1';
        end if;
    end PROCESS;

    combi_timer : PROCESS (Nouvelle_couleur)
    begin
        case Nouvelle_couleur is
            when "1000" => buzzer_time <= to_unsigned(50, 10);
            when "0100" => buzzer_time <= to_unsigned(250, 10);
            when "0010" => buzzer_time <= to_unsigned(500, 10);
            when "0001" => buzzer_time <= to_unsigned(750, 10);
            when others => buzzer_time <= to_unsigned(1000, 10);
        end case;
    end PROCESS;
end ViewSound_arch;
