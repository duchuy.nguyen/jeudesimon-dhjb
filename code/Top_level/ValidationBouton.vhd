-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            David Rossy
-- 
-- Create Date:         18:34:00 06/03/2021 
-- Design Name: 
-- Module Name:         ValidationBouton - ValidationBouton_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions: 
-- Description:         Ce composant permet stabiliser la sortie d'un bouton anti-rebond
--                      En appuyant 1 fois dessus on obtient un '1'
--                      En appuyant 2 fois dessus on obtient un '0'
--                      En appuyant 3 fois dessus on obtient un '1'
--                      etc.
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- --------------------------------------------------------------------------------
entity ValidationBouton is
    Port (
            clk         : in  STD_LOGIC;
            rst         : in  STD_LOGIC;
            btn         : in  STD_LOGIC;
            validation  : out STD_LOGIC
         );
end ValidationBouton;
-- --------------------------------------------------------------------------------
architecture ValidationBouton_arch of ValidationBouton is
    --Représente les etats possibles de la sortie validation
    --Abreviation : ZST => Zero Stable, ZNST => Zero Non Stable, UST => Un Stable, ...
    type etat is (ZST, ZNST, UST, UNST);
    signal etat_actuel : etat;
    
begin
    registre : process (clk, rst)
    begin
        if rst = '1' then
            etat_actuel <= ZST;
        elsif rising_edge (clk) then
            if btn = '1' then
                case etat_actuel is
                    when ZST    => etat_actuel <= UNST;
                    when ZNST   => etat_actuel <= ZNST;
                    when UST    => etat_actuel <= ZNST;
                    when UNST   => etat_actuel <= UNST;
                    when others => etat_actuel <= ZST;
                end case;
            else
                case etat_actuel is
                    when ZST    => etat_actuel <= ZST;
                    when ZNST   => etat_actuel <= ZST;
                    when UST    => etat_actuel <= UST;
                    when UNST   => etat_actuel <= UST;
                    when others => etat_actuel <= ZST;
                end case;
            end if;
        end if;
    end process registre;

    sortie : process (etat_actuel)
    begin
        case etat_actuel is
            when ZST    => validation <= '0';
            when ZNST   => validation <= '0';
            when UST    => validation <= '1';
            when UNST   => validation <= '1';
            when others => validation <= '0';
        end case;
    end process sortie;

end architecture ValidationBouton_arch;
-- --------------------------------------------------------------------------------