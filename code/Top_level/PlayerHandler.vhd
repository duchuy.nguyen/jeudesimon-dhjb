-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Vonlanthen Benjamin
-- Date        : 16.06.2021
-- Version     : 2.4
-- Description : Ce composant permet de gerer les joueurs de la partie.



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all ;

entity PlayerHandler is
    Port ( Etat_systeme : in  STD_LOGIC_VECTOR (2 downto 0);
           Mode_nbr_joueur : in  STD_LOGIC_VECTOR (2 downto 0);
           Evaluer : in  STD_LOGIC_VECTOR (1 downto 0);
           Id_joueur : out  STD_LOGIC_VECTOR (1 downto 0);
           joueur_vivant : out std_logic_vector (2 downto 0));

end PlayerHandler;
architecture PlayerHandler_arch of PlayerHandler is
  --joueur en train de jouer
  signal JoueurEnCours : STD_LOGIC_VECTOR (1 downto 0);
  --variables correspondant à la vie des joueurs
    signal Joueur1 : STD_LOGIC_VECTOR (0 downto 0);
    signal Joueur2 : STD_LOGIC_VECTOR (0 downto 0);
    signal Joueur3 : STD_LOGIC_VECTOR (0 downto 0);
    signal Joueur4 : STD_LOGIC_VECTOR (0 downto 0);

begin
  PROCESS (Etat_systeme, Evaluer)
    BEGIN
        --Définition du nombre de joueurs
        IF Etat_systeme = "101" THEN --état après le choix du nombre de joueurs
        JoueurEnCours <= "00";
        Joueur1 <= "1";
            case Mode_nbr_joueur is
              when "010" =>
                        Joueur2 <= "1";
                        Joueur3 <= "0";
                        Joueur4 <= "0";
              when "011" =>
                        Joueur2 <= "1";
                        Joueur3 <= "1";
                        Joueur4 <= "0";
              when "100" =>
                        Joueur2 <= "1";
                        Joueur3 <= "1";
                        Joueur4 <= "1";
              when others =>
                        Joueur2 <= "0";
                        Joueur3 <= "0";
                        Joueur4 <= "0";
          end case;

          --Changement et élimination potentiel de joueur
        elsif Evaluer /= "00" THEN
          case JoueurEnCours is
              when "00" => --joueur 1
                IF Evaluer = "10" THEN
                    Joueur1 <= "0";
                end if;
                if Joueur2 = "1" then
                    JoueurEnCours <= "01";
                elsif Joueur3 = "1" then
                    JoueurEnCours <= "10";
                else
                    JoueurEnCours <= "11";
                end if ;

              when "01" => --joueur 2
                  IF Evaluer = "10" THEN
                      Joueur2 <= "0";
                  end if;
                  if Joueur3 = "1" then
                      JoueurEnCours <= "10";
                  elsif Joueur4 = "1" then
                      JoueurEnCours <= "11";
                  else
                      JoueurEnCours <= "00";
                  end if ;

              when "10" => --joueur 3
                  IF Evaluer = "10" THEN
                      Joueur3 <= "0";
                  end if;
                  if Joueur4 = "1" then
                      JoueurEnCours <= "11";
                  elsif Joueur1 = "1" then
                      JoueurEnCours <= "00";
                  else
                      JoueurEnCours <= "01";
                  end if ;

              when "11" => --joueur 4
                  IF Evaluer = "10" THEN
                      Joueur4 <= "0";
                  end if;
                  if Joueur1 = "1" then
                      JoueurEnCours <= "00";
                  elsif Joueur2 = "1" then
                      JoueurEnCours <= "01";
                  else
                      JoueurEnCours <= "10";
                  end if;
                when others => null;
          end case;
        END IF;
    END PROCESS;
    --mise à jour du nombre de joueurs en vie
    joueur_vivant <= STD_LOGIC_VECTOR(resize(unsigned(Joueur1),3) + resize(unsigned(Joueur2),3) + resize(unsigned(Joueur3),3) + resize(unsigned(Joueur4),3));
    --mise à jour du joueur en cours de jeu
    Id_joueur <= JoueurEnCours;
end PlayerHandler_arch;