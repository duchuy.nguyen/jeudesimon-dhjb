-- --------------------------------------------------------------------------------
-- Company:             HEIA-FR
-- Engineer:            David Rossy
-- 
-- Create Date:         18:34:00 06/03/2021 
-- Design Name: 
-- Module Name:         SelectionNombreJoueur - SelectionNombreJoueur_arch 
-- Project Name:        Jeu du Simon [ISC-1b] - DEFFERARD, NGUYEN, ROSSY, VONLANTHEN
-- Target Devices: 
-- Tool versions:
-- Version:             1.4
-- Description:         Ce composant permet d'alterner (et stabiliser) un etat entre
--                      010 / 011 / 100 avec un bouton anti-rebond lors de la selection
--                      du nombre de joueur pour le mode multijoueur
-- --------------------------------------------------------------------------------
library IEEE;
library work;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
-- --------------------------------------------------------------------------------
entity SelectionNombreJoueur is
    Port (
            clk              : in STD_LOGIC;
            rst              : in STD_LOGIC;
            btn_mode         : in STD_LOGIC;
            etat_actif      : in  STD_LOGIC_VECTOR (2 downto 0);
            etat_systeme    : in  STD_LOGIC_VECTOR (2 downto 0);
            mode_nbr_joueur  : out STD_LOGIC_VECTOR (1 downto 0)
         );
end SelectionNombreJoueur;
-- --------------------------------------------------------------------------------
architecture SelectionNombreJoueur_arch of SelectionNombreJoueur is
    --Représente les etats possibles pour le changement de selection du nombre de joueurs
    --Abreviation : DJS => Deux Joueurs Stable, DJNS => Deux Joueurs Non Stable, ...
    type etat is (DJS, DJNS, TJS, TJNS, QJS, QJNS);
    signal etat_actuel : etat;

    
begin
    registre : process (clk, rst, etat_systeme)
    begin
        if rst = '1' then
            etat_actuel <= DJS;
        elsif rising_edge (clk) then
            if etat_actif = etat_systeme then
                if btn_mode = '1' then
                    case etat_actuel is
                        when DJS    => etat_actuel <= DJNS;
                        when DJNS   => etat_actuel <= DJNS;
                        when TJS    => etat_actuel <= TJNS;
                        when TJNS   => etat_actuel <= TJNS;
                        when QJS    => etat_actuel <= QJNS;
                        when QJNS   => etat_actuel <= QJNS;
                        when others => etat_actuel <= DJS; --nombre de joueur par defaut (2)
                    end case;
                else
                    case etat_actuel is
                        when DJS    => etat_actuel <= DJS;
                        when DJNS   => etat_actuel <= TJS;
                        when TJS    => etat_actuel <= TJS;
                        when TJNS   => etat_actuel <= QJS;
                        when QJS    => etat_actuel <= QJS;
                        when QJNS   => etat_actuel <= DJS;
                        when others => etat_actuel <= DJS; --nombre de joueur par defaut (2)
                    end case;
                end if;
            end if;
        end if;
    end process registre;
    

    sortie : process (etat_actuel)
    begin
        case etat_actuel is
            when DJS    => mode_nbr_joueur <= "00";
            when DJNS   => mode_nbr_joueur <= "00";
            when TJS    => mode_nbr_joueur <= "01";
            when TJNS   => mode_nbr_joueur <= "01";
            when QJS    => mode_nbr_joueur <= "10";
            when QJNS   => mode_nbr_joueur <= "10";
            when others => mode_nbr_joueur <= "00"; --nombre de joueur par defaut (2)
        end case;
    end process sortie;


end architecture SelectionNombreJoueur_arch;
-- --------------------------------------------------------------------------------