-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Defferrard Julien
-- Date        : 05.05.2021
-- Version     : 1.0
-- Description : Ce composant permet de générer une couleur aléatoire

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ColorHandler is
    Port (
      clk, rst:	in STD_LOGIC;
      couleur_aleatoire: out  STD_LOGIC_VECTOR(3 downto 0)
    );
end ColorHandler;

architecture Architecture_ColorHandler of ColorHandler is
  signal couleur_present, couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
begin
  process (clk, rst)
  begin
    if rst = '1' then
      couleur_present <= "0000";
    elsif rising_edge(clk) then
      couleur_present <= couleur_futur;
    end if;
  end process;

  process (couleur_present, clk)
  begin
    if rising_edge(clk) then
      case couleur_present is
        when "0001" => couleur_futur <= "0010";
        when "0010" => couleur_futur <= "0100";
        when "0100" => couleur_futur <= "1000";
        when "1000" => couleur_futur <= "0001";
        when others => couleur_futur <= "0001";
      end case;
    end if;
  end process;

  couleur_aleatoire <= couleur_present;
end Architecture_ColorHandler;
