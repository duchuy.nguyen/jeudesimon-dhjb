force clk 0 0, 1 1 -repeat 2
force rst 1 0, 0 2

force cl 1 0
force dt 1 0

# clockwise
force cl 0 6, 1 10
force dt 0 9, 1 13

force cl 0 16, 1 20
force dt 0 19, 1 23

force cl 0 26, 1 30
force dt 0 28, 1 32

force cl 0 36, 1 40
force dt 0 38, 1 42

# counter-clockwise
force dt 0 46, 1 50
force cl 0 48, 1 52

force dt 0 56, 1 60
force cl 0 58, 1 62

force dt 0 66, 1 70
force cl 0 68, 1 72

force dt 0 76, 1 80
force cl 0 78, 1 82

# fake clockwise
force cl 0 86, 1 90

# clockwise
force cl 0 94, 1 98
force dt 0 96, 1 100

# fake counter-clockwise
force dt 0 104, 1 108

run 400
