force clk 0 0, 1 1 -repeat 2
force clk_2hz 0 0, 1 19 -repeat 20
force rst 0 0, 1 2
#force sequence_complete UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU1100110001011010 0
#force compteur_couleur 010000 0
force sequence_complete 110011000011010110011001011010110010001011010110011000101010 0
force compteur_couleur 111100 0
force evaluer 00 0
force couleur_jouee 0000 0

# 0ns | init -------------------------------------
force etat_systeme 000 0

# 0,03 | demonstration test ----------------------
force etat_systeme 100 30

# 0,225ns | checkpoint ---------------------------
force etat_systeme 000 225

# 0,25ns | sequence test -------------------------
force etat_systeme 101 250

# 1.5ns | checkpoint -----------------------------
force etat_systeme 000 1500

# 1.525ns | ecouter test --------------------------
force sequence_complete UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU1010 1525
force compteur_couleur 000100 1525
force etat_systeme 110 1525

# true answer
force evaluer 01 1550
force couleur_jouee 0100 1550

force evaluer 00 1551

force evaluer 01 1580
force couleur_jouee 1000 1580

force evaluer 00 1581

# 1.7ns | checkpoint -----------------------------
force etat_systeme 000 1700

# 1.725ns | ecouter test --------------------------
force etat_systeme 110 1725

# wrong answer
force evaluer 10 1735
force couleur_jouee 0100 1735

run 2000
