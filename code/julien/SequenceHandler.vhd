-- Projet      : Jeu du simon
-- Entity      : HEIA-FR
-- Author      : Defferrard Julien
-- Date        : 06.05.2021
-- Version     : 1.0
-- Description : Ce composant permettra d’afficher la séquence à reproduire au joueur. C’est également lui qui devra générer et gérer la séquence de démarrage-apprentissage.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SequenceHandler is
    Port (
      clk, rst:	in STD_LOGIC; -- NEW
      clk_2hz: in STD_LOGIC;
      etat_systeme: 	in  STD_LOGIC_VECTOR(2 downto 0);
      sequence_complete: in STD_LOGIC_VECTOR(59 downto 0);
      couleur_jouee: 	in  STD_LOGIC_VECTOR(3 downto 0);
      evaluer: in STD_LOGIC_VECTOR(1 downto 0); -- NEW
      compteur_couleur : in STD_LOGIC_VECTOR(7 downto 0); -- NEW
      nouvelle_couleur: out STD_LOGIC_VECTOR(3 downto 0);
      fin_sequence: out STD_LOGIC
    );
end SequenceHandler;

architecture Architecture_SequenceHandler of SequenceHandler is
  type etat is (pause, afficher, demonstration, ecouter, idle);
  signal etat_present, etat_futur : etat;
  signal couleur_present, couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
  signal s_couleur_present, s_couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
  signal d_couleur_present, d_couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
  signal e_couleur_present, e_couleur_futur : STD_LOGIC_VECTOR(3 downto 0);
  signal compteur_ecouter, compteur_sequence :  INTEGER range 0 to 150 := 0;
  signal compteur_demonstration : INTEGER range 0 to 10 := 0;
  signal etat_systeme_temp : STD_LOGIC_VECTOR(2 downto 0);
  signal couleur_temp : STD_LOGIC_VECTOR(1 downto 0);
  signal delay : INTEGER range 0 to 10 := 0;
begin
  p_init: process (clk, rst)
  begin
    if rst = '1' then
      etat_present <= idle;
      couleur_present <= "0000";
      s_couleur_present <= "0000";
      d_couleur_present <= "0000";
      e_couleur_present <= "0000";
    elsif rising_edge(clk) then
      etat_present <= etat_futur;
      couleur_present <= couleur_futur;
      s_couleur_present <= s_couleur_futur;
      d_couleur_present <= d_couleur_futur;
      e_couleur_present <= e_couleur_futur;
    end if;
  end process;

  p_machine_etat: process(etat_systeme, clk_2hz, etat_present, compteur_sequence, compteur_demonstration, compteur_ecouter)
  begin
      case etat_present is
        -- ------------ IDLE ------------
        when idle =>
          if etat_systeme /= etat_systeme_temp then
            case etat_systeme is
              when "100" => etat_futur <= demonstration;
              when "110" => etat_futur <= ecouter;
              when "101" => etat_futur <= pause;
              when others => etat_futur <= idle;
            end case;
            etat_systeme_temp <= etat_systeme;
          end if;
          fin_sequence <= '0';
          delay <= 0;
        -- ------------ PHASE DE SÉQUENCE À JOUER ------------
        when afficher =>
          if clk_2hz = '1' then
            if rising_edge(clk) then
              compteur_sequence <= compteur_sequence + 2;
            end if;
            etat_futur <= pause;
          end if;
        when pause =>
          if compteur_sequence < to_integer(unsigned(compteur_couleur)) then
            couleur_temp <= sequence_complete((compteur_sequence + 1) downto compteur_sequence);
          end if;
          if compteur_sequence >= to_integer(unsigned(compteur_couleur)) and clk_2hz = '1' then
            etat_futur <= idle;
            fin_sequence <= '1';
          elsif clk_2hz = '1' then
            etat_futur <= afficher;
          end if;
        -- ------------ PHASE DE DEMONSTRATION ------------
        when demonstration =>
          if compteur_demonstration >= 10 then
            etat_futur <= idle;
            fin_sequence <= '1';
          end if;
        -- ------------ PHASE D'ECOUTE ------------
        when ecouter =>
          if compteur_ecouter >= to_integer(unsigned(compteur_couleur)) then
            if delay = 10 then
              etat_futur <= idle;
              fin_sequence <= '1';
            else
              delay <= delay + 1;
            end if;
          end if;
        -- ------------ OTHERS ------------
        when others => etat_futur <= idle;
    end case;
  end process;

  -- ------------ PHASE DE SÉQUENCE À JOUER ------------
  p_sequence_a_jouer: process(etat_present, clk)
  begin
    --if rising_edge(clk_2hz) and etat_present = afficher then
    --    compteur_sequence <= compteur_sequence + 2;
    --end if;
    if etat_present = afficher and compteur_sequence <= to_integer(unsigned(compteur_couleur)) then
      case couleur_temp is
        when "00" => s_couleur_futur <= "0001";
        when "01" => s_couleur_futur <= "0010";
        when "10" => s_couleur_futur <= "0100";
        when others => s_couleur_futur <= "1000";
      end case;
    elsif etat_present = pause then
      s_couleur_futur <= "0000";
    elsif etat_present = idle then
      s_couleur_futur <= "0000";
      --compteur_sequence <= 0;
    end if;
  end process;

  -- ------------ PHASE DE DÉMONSTRATION ------------
  p_demonstration: process(etat_present, clk)
  begin
    if etat_present = demonstration then
      case compteur_demonstration is
        when 1 => d_couleur_futur <= "0001";
        when 2 => d_couleur_futur <= "0010";
        when 3 => d_couleur_futur <= "0100";
        when 4 => d_couleur_futur <= "1000";
        when 6|8 => d_couleur_futur <= "1111";
        when 9 => d_couleur_futur <= "0000";
        when others => d_couleur_futur <= "0000";
      end case;
      if rising_edge(clk) and clk_2hz = '1' then
        compteur_demonstration <= compteur_demonstration + 1;
      end if;
    elsif etat_present = idle then
      d_couleur_futur <= "0000";
      compteur_demonstration <= 0;
    end if;
  end process;

  -- ------------ PHASE D'ECOUTE ------------
  p_ecoute: process(etat_present, evaluer, clk)
  begin
    if etat_present = ecouter then
      if evaluer = "01" then -- TRUE ANSWER
        e_couleur_futur <= couleur_jouee;
        compteur_ecouter <= compteur_ecouter + 2;
      elsif evaluer = "10" then -- WRONG ANSWER
        e_couleur_futur <= "1111";
        compteur_ecouter <= to_integer(unsigned(compteur_couleur));
      else -- WAIT USER
      end if;
    elsif etat_present = idle then
      e_couleur_futur <= "0000";
      compteur_ecouter <= 0;
    end if;
  end process;

  -- ------------ OUT ------------
  p_color: process(etat_present, s_couleur_present, d_couleur_present, e_couleur_present)
  begin
    case etat_present is
      when idle => couleur_futur <= "0000";
      when afficher => couleur_futur <= s_couleur_present;
      when pause => couleur_futur <= s_couleur_present;
      when demonstration => couleur_futur <= d_couleur_present;
      when ecouter => couleur_futur <= e_couleur_present;
      when others => couleur_futur <= "0000";
    end case;
  end process;

  nouvelle_couleur <= couleur_present;
end Architecture_SequenceHandler;
